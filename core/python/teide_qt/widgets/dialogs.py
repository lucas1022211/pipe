"""
Information Dialog: Displays information to the user. Typically has an "OK" button.
Warning Dialog: Alerts the user about a potential issue.
Options to acknowledge or proceed with caution.
Confirmation Dialog: Asks the user to confirm an action.
Options such as "Yes," "No," "OK," and "Cancel."
File Dialog: Prompts the user to select a file or multiple files.
Color Dialog:Allows the user to pick a color.
Progress Dialog: Shows the progress of a lengthy operation. May include a progress bar and additional information.
Custom Dialog:A dialog tailored for a specific use case, potentially combining various elements like buttons, input fields, and checkboxes.
About Dialog: Provides information about the application, such as version number and credits.
Settings Dialog: Allows users to customize application settings.
Confirmation with Checkbox Dialog: Similar to a confirmation dialog but includes a checkbox for additional user choices.
Multi-Choice Dialog: Lets the user choose from multiple options.
Notification Dialog: Pops up briefly to notify the user about a non-critical event.
Critical Error Dialog: Similar to an error dialog but for critical issues that may require immediate attention.
Success Dialog: Indicates the successful completion of an operation.
Help Dialog: Provides help or additional information about a feature.
Login Dialog: Requests user credentials for authentication.
"""
# todo: Make common dialogs for error handling, warnings, etc...
