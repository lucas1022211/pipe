import os
import sys
from typing import TYPE_CHECKING, Optional

from qtpy import QtCore, QtGui, QtWidgets

import teide_core.helpers
import files
from teide_core import settings
from teide_core import db

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

SG_SERVER_URL = settings.get_attribute('shotgun_url')


folder = files.get_parent_dir(__file__, level=2)


class PasswordEdit(QtWidgets.QLineEdit):
    """
    Password LineEdit with icons to show/hide password entries.
    Based on this example https://kushaldas.in/posts/creating-password-input-widget-in-pyqt.html by Kushal Das.
    """

    def __init__(
        self,
        show_visibility=True,
        visible_icon=None,
        hidden_icon=None,
        icons_from_theme=True,
        *args,
        **kwargs
    ):
        super().__init__(*args, **kwargs)

        if icons_from_theme:
            self.visibleIcon = QtGui.QIcon.fromTheme("view-visible")
            self.hiddenIcon = QtGui.QIcon.fromTheme("view-hidden")
        else:
            if visible_icon:
                self.visibleIcon = visible_icon
            else:
                self.visibleIcon = QtGui.QIcon(os.path.join(folder, "resources/eye.svg"))
            if hidden_icon:
                self.hiddenIcon = hidden_icon
            else:
                self.hiddenIcon = QtGui.QIcon(
                    os.path.join(folder, "resources/hidden.svg")
                )

        self.setEchoMode(QtWidgets.QLineEdit.Password)

        if show_visibility:
            # Add the password hide/shown toggle at the end of the edit box.
            self.togglepasswordAction = self.addAction(
                self.visibleIcon, QtWidgets.QLineEdit.TrailingPosition
            )
            self.togglepasswordAction.triggered.connect(
                self.on_toggle_password_Action
            )

        self.password_shown = False

    def on_toggle_password_Action(self):
        if not self.password_shown:
            self.setEchoMode(QtWidgets.QLineEdit.Normal)
            self.password_shown = True
            self.togglepasswordAction.setIcon(self.hiddenIcon)
        else:
            self.setEchoMode(QtWidgets.QLineEdit.Password)
            self.password_shown = False
            self.togglepasswordAction.setIcon(self.visibleIcon)


class UserLogin(QtWidgets.QDialog):
    window_name = 'Shotgrid login'

    def __init__(self):
        super().__init__()
        self._conn: Optional[db.Connection] = None

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    def init_ui(self):
        self.setWindowTitle(self.window_name)
        self.url_label = QtWidgets.QLabel('URL:')
        self.url_line = QtWidgets.QLineEdit(SG_SERVER_URL)

        self.email_label = QtWidgets.QLabel('Email:')
        self.email_line = QtWidgets.QLineEdit()

        self.password_label = QtWidgets.QLabel('Password:')
        self.password_line = PasswordEdit(icons_from_theme=False)

        self.remember_label = QtWidgets.QLabel('Remember:')
        self.remember_cbox = QtWidgets.QCheckBox()
        self.remember_cbox.setChecked(True)

        self.login_button = QtWidgets.QPushButton('Login')
        self.login_button.setEnabled(False)

        layout = QtWidgets.QFormLayout()
        layout.addRow(self.url_label, self.url_line)
        layout.addRow(self.email_label, self.email_line)
        layout.addRow(self.password_label, self.password_line)
        layout.addRow(self.remember_label, self.remember_cbox)
        layout.addRow(self.login_button)

        self.setLayout(layout)

    def populate_ui(self):
        sg_email = os.getenv('BM_SG_EMAIL')
        sg_pass = os.getenv('BM_SG_PASS')
        if not sg_email or not sg_pass:
            _logger.info('Could not populate UI because the environment variables '
                          'BM_SG_EMAIL and BM_SG_PASS are not set.')
        else:
            self.email_line.setText(sg_email)
            self.password_line.setText(sg_pass)
            self._check_not_empty_fields()

    def connect_signals(self):
        self.url_line.textChanged.connect(self._check_not_empty_fields)
        self.email_line.textChanged.connect(self._check_not_empty_fields)
        self.password_line.textChanged.connect(self._check_not_empty_fields)
        self.login_button.clicked.connect(self.login_clicked)

    def _check_not_empty_fields(self):
        if self.url_line.text() and self.email_line.text() and self.password_line.text():
            self.login_button.setEnabled(True)
        else:
            self.login_button.setEnabled(False)

    def login_clicked(self):
        # Implement your login logic here
        from teide_core.db import Authentication, Connection
        conn = Connection(url=self.url_line.text(),
                          email=self.email_line.text(),
                          password=self.password_line.text())
        if conn.test_connection():
            if self.remember_cbox.isChecked():
                Authentication.save_authentication(url=self.url_line.text(),
                                                   email=self.email_line.text(),
                                                   password=self.password_line.text())
            self._conn = conn
            self.accept()
        else:
            self.password_line.setText('')


def main() -> Optional[db.Connection]:
    login_dialog = UserLogin()
    login_dialog.show()
    conn: Optional[db.Connection] = None
    if login_dialog.exec_() == QtWidgets.QDialog.Accepted:
        conn = login_dialog._conn
    else:
        login_dialog.close()
    return conn


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    ret = main()
