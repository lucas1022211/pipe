"""
Ingest dialog to prompt the user what files to add to pipeline.
Multiple files can be provided, the minimum required is a scene(workfile) and a thumbnail(preview media) but more can
be added such as:
    - scene: Its the workfile. ex: psd file or maya file.
    - thumbnail: Screen grab or low resolution image to preview the contents of the file.
    - playblast: LowQ movie file for tasks that require a video as preview.
    - output: generic output of a workfile that is located named such as workfile_<descriptor>.ext
    - texture: Asset texture files with an specific naming convention.
    - renderStill: Full quality render of unique frame and without render layers
    - renderLayer: Full quality render as imageSeq and render layers (masterLater, Background, Foreground)
    ,_____________________________________________,
    |                 INGEST ITEMS                |
    |               [version_media]               |      version_media is a thumbnail/preview media, it is used
    |    TST_101_010_0010_anm_refine_main_001     |      in SG for Version entity. gotten from <playblast-file>
    |  ,_______________________________________,  |
    |  | > <scene-file>                      X |  |
    |  |     Variant: main                     |  |
    |  |     Version: 001                      |  |
    |  | > <playblast-file>                  X |  |
    |  | v <output-A-file>                   X |  |
    |  |     Descriptor: camera                |  |
    |  | v <output-B-file>                   X |  |
    |  |     Descriptor: particles             |  |
    |  | v <render-A_%4d-imgSeq>             X |  |
    |  |     renderLayer: fx                   |  |
    |  |                                       |  |
    |  '---------------------------------------'  |
    |                                             |
    |         Cancel             Ingest           |
    '---------------------------------------------'
"""
import os
from pathlib import Path
from qtpy import QtCore, QtWidgets, QtGui

import files
from teide_core import filePath, publish
from teide_qt.widgets import collapsibleWidget

folder = files.get_parent_dir(__file__, level=2)


def create_styled_button(text):
    button = QtWidgets.QPushButton(text)
    button.setMinimumHeight(60)
    button.setStyleSheet("""
        QPushButton {
            border: 2px dashed #666;
            border-radius: 4px;
            padding: 10px;
            text-align: center;
            background: transparent;
        }
        QPushButton:hover {
            background: rgba(0, 0, 0, 0.1);
            border-color: #888;
        }
    """)
    return button


class IngestWindow(QtWidgets.QDialog):
    def __init__(self, tokens, parent=None):
        super().__init__(parent)
        self.tokens = tokens
        self.setWindowTitle("Ingest Version")
        self.resize(800, 600)

        self.init_ui()
        self.connect_signals()

    @classmethod
    def from_tokens(cls, tokens):
        return cls(tokens)

    def init_ui(self):
        main_layout = QtWidgets.QVBoxLayout(self)

        self.outputs_list = collapsibleWidget.OutputListWidget(tokens=self.tokens)
        main_layout.addWidget(self.outputs_list)

        # Ingest Buttons
        self.file_outputs_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(self.file_outputs_layout)

        self.ingest_button = QtWidgets.QPushButton('Ingest')
        self.file_outputs_layout.addWidget(self.ingest_button)

        self.ingest_publish_button = QtWidgets.QPushButton('Ingest and Publish')
        self.file_outputs_layout.addWidget(self.ingest_publish_button)

    def connect_signals(self):
        self.ingest_button.clicked.connect(self._dev_test)

    def _add_file_dialog(self, file_type: str):
        file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, f"Select {file_type} File")
        if file_path:
            self.outputs_list.add_collapsible(file_path)
            self.outputs_list.toggle_drop_list_wdg()

    def _dev_test(self):
        collapsible_widgets = self.outputs_list.get_collapsible_widgets()
        for wdg in collapsible_widgets:
            output_type = wdg.combo_box.currentData()
            print(f"Selected option: {type(output_type)}, {output_type}")


if __name__ == "__main__":
    import sys
    from teide_qt import utils
    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    teide = filePath.FilePath()
    input_scene = Path(r"P:\2024\defaultProject\06_Shots\episodes\101\sequences\010\shots\0010\anm\blender\work\TST_101_010_0010_anm_refine_main_001.blend")
    w_template = teide.getTemplateByName('t_shot_dcc_work_scene')
    input_tokens = w_template.parse(input_scene.as_posix())
    input_tokens['entity_type'] = 'shot'
    input_tokens['element_type'] = 'scene'
    input_tokens['mode'] = 'work'

    dialog = IngestWindow(tokens=input_tokens)
    dialog.show()

    app.exec()
