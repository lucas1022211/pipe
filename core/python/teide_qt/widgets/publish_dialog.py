"""
Publish dialog to show and prompt the user what publish items are found and which ones to publish.
    ,_____________________________________________,
    |                PUBLISH ITEMS                |
    |               [version_media]               |      version_media is a thumbnail/preview media, it is used
    |    TST_101_010_0010_anm_refine_main_001     |      in SG for Version entity. gotten from <playblast-file>
    |  ,_______________________________________,  |
    |  | [X] <scene-file>                      |  |
    |  | [X] <playblast-file>                  |  |
    |  | [X] <output-A-file>                   |  |
    |  | [ ] <output-B-file>                   |  |
    |  | [X] <render-A_%4d-imgSeq>             |  |
    |  |                                       |  |
    |  '---------------------------------------'  |
    |                                             |
    |         Cancel        Publish Selected      |
    '---------------------------------------------'
"""
import sys
from pathlib import Path
from typing import TYPE_CHECKING, List, Union

from qtpy import QtCore, QtGui, QtWidgets

from teide_core import filePath, publish
from teide_core.db import Connection
from teide_qt.widgets import thumbnail_dialog
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

ITEM_DATA_ROLE = QtCore.Qt.UserRole + 1

# todo: add section to add timelogging. How long did it take you to work on this version?
#  Version vs Task time logging?


class Publisher(QtWidgets.QDialog):
    window_name = 'Version Publisher'
    media_icon_height = 180

    def __init__(self, connection: Connection, area: publish.WorkArea):
        super().__init__()
        self._conn = connection
        self.area = area

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    @classmethod
    def from_tokens(cls, connection: Connection, tokens: filePath.TOKENS_TYPE) -> 'Publisher':
        area = publish.WorkArea.from_tokens(tokens=tokens)
        return cls(connection, area)

    def init_ui(self) -> None:
        """Initialize the UI components of the dialog."""
        self.setWindowTitle(self.window_name)
        self.resize(QtCore.QSize(440, 400))

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        font = QtGui.QFont("Helvetica", 10, QtGui.QFont.Bold)
        self.entity_label = QtWidgets.QLabel(parent=self)
        self.entity_label.setFont(font)
        self.entity_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        layout.addWidget(self.entity_label)

        self.thumbnail_label = thumbnail_dialog.MediaPreviewWidget(height=self.media_icon_height, parent=self)
        self.thumbnail_label.setHidden(True)
        self.thumbnail_label.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.CustomContextMenu)
        layout.addWidget(self.thumbnail_label, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)

        # Parameters widget
        self.dailies_cbox = QtWidgets.QCheckBox(parent=self)
        self.dailies_cbox.setText("Add to Dailies Playlist")
        self.dailies_cbox.setChecked(True)
        layout.addWidget(self.dailies_cbox, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)

        # List widget
        self.list_label = QtWidgets.QLabel(text="Output Items:", parent=self)
        self.list_wdg = QtWidgets.QListWidget(parent=self)
        self.list_wdg.setSelectionMode(QtWidgets.QAbstractItemView.SelectionMode.NoSelection)
        self.list_wdg.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        layout.addWidget(self.list_label)
        layout.addWidget(self.list_wdg)

        # Parameters widget
        self.description_label = QtWidgets.QLabel(text="Description:", parent=self)
        self.description_text = QtWidgets.QTextEdit(parent=self)
        self.description_text.setPlaceholderText("Describe key updates, creative changes,"
                                                 " or technical notes for this version.")
        self.description_text.setFixedHeight(80)
        layout.addWidget(self.description_label)
        layout.addWidget(self.description_text)

        # Buttons Row
        btn_layout = QtWidgets.QHBoxLayout()
        layout.addLayout(btn_layout)
        self.cancel_button = QtWidgets.QPushButton('Cancel', parent=self)
        btn_layout.addWidget(self.cancel_button)

        self.publish_button = QtWidgets.QPushButton('Publish Selected', parent=self)
        btn_layout.addWidget(self.publish_button)

    def populate_ui(self):
        # Set Label
        if self.area.scene_path:
            self.entity_label.setText(self.area.scene_path.stem.upper())

        # Add items to list
        output_items = self.area.get_output_items(existing_only=True, types=publish.OutputType.get_all())
        self.add_items_to_list(output_items)

        # Thumbnail
        if self.area.preview_media_item:
            self.resize(QtCore.QSize(self.width(), self.height() + self.media_icon_height))
            self.thumbnail_label.setHidden(False)
            self.thumbnail_label.add_media([item.src for item in self.area.imageable_items])

        self.cancel_button.setFocus()

    def connect_signals(self) -> None:
        """Connect signals to their respective slots."""
        self.publish_button.clicked.connect(self.run_publish)
        self.cancel_button.clicked.connect(self.close)

        self.thumbnail_label.pixmap_worker.all_jobs_done.connect(self._enable_context_menu)

    def _enable_context_menu(self):
        print("Trigger _enable context menu")
        self.thumbnail_label.customContextMenuRequested.connect(self.show_context_menu)

    def show_context_menu(self, position: QtCore.QPoint) -> None:
        """Show context menu for selecting preview media item."""
        context_menu = QtWidgets.QMenu(self)
        current_item = self.area.preview_media_item

        # Group items by their OutputType
        grouped_items = {}
        for item in self.area.imageable_items:
            item_type = item.output_type
            if item_type not in grouped_items:
                grouped_items[item_type] = []
            grouped_items[item_type].append(item)

        # Populate the menu with sections for each type
        for item_type, items in grouped_items.items():
            context_menu.addSection(item_type.name)
            for item in items:
                action = context_menu.addAction(item.name)
                if current_item and item.name == current_item.name:
                    action.setCheckable(True)
                    action.setChecked(True)
                action.triggered.connect(lambda checked, item=item: self.set_preview_media_item(item))
        context_menu.exec(self.thumbnail_label.mapToGlobal(position))

    def add_items_to_list(self, items: List[publish.OutputItem]) -> None:
        """Add a list of items to the QListWidget. Each item can be checked."""
        self.list_wdg.clear()
        for output_item in items:
            item = QtWidgets.QListWidgetItem(output_item.src.name)
            item.setFlags(item.flags() | QtCore.Qt.ItemFlag.ItemIsUserCheckable)
            item.setCheckState(QtCore.Qt.CheckState.Checked)
            item.setToolTip(output_item.src.as_posix())
            item.setData(ITEM_DATA_ROLE, output_item)
            self.list_wdg.addItem(item)
        self.list_wdg.sortItems(order=QtCore.Qt.SortOrder.AscendingOrder)

    def set_preview_media_item(self, item) -> None:
        """Set the selected item as the preview media item."""
        self.area.preview_media_item = item
        self.thumbnail_label.set_active_media(item.src)

    def list_checked_items(self) -> List[publish.OutputItem]:
        """Return a list of checked output items."""
        return [self.list_wdg.item(i).data(ITEM_DATA_ROLE) for i in range(self.list_wdg.count())
                if self.list_wdg.item(i).checkState() == QtCore.Qt.CheckState.Checked]

    def run_publish(self) -> None:
        """Execute the publish operation with the selected items."""
        checked_items = self.list_checked_items() or None
        publisher = publish.PublishAPI(conn=self._conn, area=self.area)
        publisher.do_publish(output_items=checked_items,
                             description=self.description_text.toPlainText(),
                             add_to_dailies=self.dailies_cbox.isChecked())
        
        QtWidgets.QMessageBox.information(self, "Publish Complete", "The publish operation has finished successfully.")
        self.close()


if __name__ == "__main__":
    from teide_core.db import SGAuthentication
    conn = SGAuthentication.login()
    from teide_qt import utils
    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    root = "P:/pipe_projects"  # Home-dev
    root = "X:/BM_Drive/projects"  # Bidaya-Triofox
    input_scene = Path(root + r"\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.blend")

    teide = filePath.FilePath()
    w_template = teide.getTemplateByName('t_shot_dcc_work_scene')
    input_tokens = w_template.parse(input_scene.as_posix())

    full_tokens = input_tokens.copy()
    full_tokens['entity_type'] = 'shot'
    full_tokens['dcc'] = 'blender'
    full_tokens['mode'] = 'work'
    full_tokens['element_type'] = 'scene'

    dialog = Publisher.from_tokens(conn, full_tokens)
    dialog.show()

    app.exec()
