from qtpy import QtWidgets, QtCore, QtGui
from dataclasses import dataclass
from typing import Any

from teide_core.db import SGAuthentication


@dataclass(frozen=True)
class SelectionItem:
    label: str
    data: Any

    def __hash__(self):
        return hash(self.label)

    def __eq__(self, other):
        return isinstance(other, SelectionItem) and self.label == other.label


class CustomSortFilterProxyModel(QtCore.QSortFilterProxyModel):
    def lessThan(self, left, right):
        left_item = self.sourceModel().itemFromIndex(left)
        right_item = self.sourceModel().itemFromIndex(right)

        # Check if items are checked
        left_checked = left_item.checkState() == QtCore.Qt.CheckState.Checked
        right_checked = right_item.checkState() == QtCore.Qt.CheckState.Checked

        # Sort by checked state first, then by text
        if left_checked != right_checked:
            return not right_checked  # Checked items come first
        return left_item.text() < right_item.text()  # Then sort alphabetically


class MultiSelectComboBox(QtWidgets.QComboBox):
    def __init__(self, placeholder="Any", parent=None):
        super().__init__(parent)
        self.placeholder = placeholder
        self._setup()

    def _setup(self):
        self.setEditable(True)
        self.lineEdit().setReadOnly(True)

        self.model = QtGui.QStandardItemModel()
        self.proxy_model = CustomSortFilterProxyModel()
        self.proxy_model.setSourceModel(self.model)
        self.setModel(self.proxy_model)

        self.view = QtWidgets.QListView()
        self.setView(self.view)
        self.lineEdit().installEventFilter(self)
        self.view.pressed.connect(self._handle_press)

        self._update_text()

    def _update_text(self):
        items = self.get_checked_items()

        if not items:
            self.setEditText(self.placeholder)
            self.setToolTip("")
            return

        text = items[0].label
        if len(items) > 1:
            text = f"{text} +{len(items) - 1}"

        self.setEditText(text)
        self.setToolTip(", ".join(i.label for i in items))

    def _handle_press(self, index):
        if not index.isValid():
            return

        source_index = self.proxy_model.mapToSource(index)
        item = self.model.itemFromIndex(source_index)

        if item.checkState() == QtCore.Qt.CheckState.Checked:
            item.setCheckState(QtCore.Qt.CheckState.Unchecked)
        else:
            item.setCheckState(QtCore.Qt.CheckState.Checked)

        self.proxy_model.sort(0, order=QtGui.Qt.SortOrder.AscendingOrder)
        self._update_text()

    def add_items(self, items):
        name_tag = 'name'
        if 'name' not in items[0].keys() and 'code' in items[0].keys():
            name_tag = 'code'
        self.model.clear()
        for item in sorted(items, key=lambda x: x[name_tag].lower()):
            selection = SelectionItem(label=item[name_tag], data=item)
            std_item = QtGui.QStandardItem(item[name_tag])
            std_item.setFlags(QtCore.Qt.ItemFlag.ItemIsEnabled)
            std_item.setCheckState(QtCore.Qt.CheckState.Unchecked)
            std_item.setData(selection, QtCore.Qt.ItemDataRole.UserRole)
            self.model.appendRow(std_item)
        self._update_text()

    def get_checked_items(self):
        checked_items = []
        for row in range(self.model.rowCount()):
            item = self.model.item(row)
            if item and item.checkState() == QtCore.Qt.CheckState.Checked:
                selection = item.data(QtCore.Qt.ItemDataRole.UserRole)
                checked_items.append(selection)
        return checked_items

    def set_checked_items(self, items):
        for row in range(self.model.rowCount()):
            item = self.model.item(row)
            if not item:
                continue
            selection = item.data(QtCore.Qt.ItemDataRole.UserRole)
            if selection.data in items:
                item.setCheckState(QtCore.Qt.CheckState.Checked)
            else:
                item.setCheckState(QtCore.Qt.CheckState.Unchecked)
        self._update_text()

    def paintEvent(self, event):
        opt = QtWidgets.QStyleOptionComboBox()
        self.initStyleOption(opt)
        painter = QtGui.QPainter(self)
        self.style().drawComplexControl(QtWidgets.QStyle.ComplexControl.CC_ComboBox, opt, painter, self)

    def eventFilter(self, obj, event):
        if obj == self.lineEdit() and event.type() == QtCore.QEvent.Type.MouseButtonPress and self.isEnabled():
            self.showPopup()
            return True
        return super().eventFilter(obj, event)

    def check_item_by_name(self, name: str):
        for row in range(self.model.rowCount()):
            item = self.model.item(row)
            if not item:
                continue
            selection = item.data(QtCore.Qt.ItemDataRole.UserRole)
            if selection.label.lower() == name.lower():
                if item.checkState() == QtCore.Qt.CheckState.Checked:
                    item.setCheckState(QtCore.Qt.CheckState.Unchecked)
                else:
                    item.setCheckState(QtCore.Qt.CheckState.Checked)

                self.proxy_model.sort(0, order=QtGui.Qt.SortOrder.AscendingOrder)
                self._update_text()
                break


class DemoWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()
        self.populate_ui()

    def init_ui(self):
        # Create central widget and layout
        central_widget = QtWidgets.QWidget()
        self.setCentralWidget(central_widget)

        layout = QtWidgets.QFormLayout(central_widget)

        # Assignee
        self.assignee_combo = MultiSelectComboBox(placeholder="Any")
        layout.addRow("Assignee", self.assignee_combo)

        # Reviewer
        self.reviewer_combo = MultiSelectComboBox(placeholder="Any")
        layout.addRow("Reviewer", self.reviewer_combo)

    def populate_ui(self):
        conn = SGAuthentication.login()

        users = conn.get_users()
        self.assignee_combo.add_items(users)
        self.reviewer_combo.add_items(users)


if __name__ == '__main__':
    import sys
    from teide_qt import utils

    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    # Create and show the window
    window = DemoWindow()
    window.show()

    app.exec()
