import sys
from functools import partial
from typing import TYPE_CHECKING, Optional

from qtpy.QtWidgets import *
from qtpy.QtCore import *
from qtpy.QtGui import *
from qtpy import QtCore, QtGui, QtWidgets

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class BaseTag(QWidget):
    def __init__(self, parent, text=None):
        super(BaseTag, self).__init__(parent)
        self._tagBar = parent
        self.icon = '+'
        self.action = self._tagBar.create_tags

        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        main_layout = QHBoxLayout(self)
        main_layout.setContentsMargins(0, 0, 0, 0)

        # Frame
        tag = QFrame()
        tag.setStyleSheet('QFrame {border:1px solid rgb(192, 192, 192); border-radius: 4px;}')
        tag_layout = QHBoxLayout(tag)
        #tag.setMaximumHeight(34)
        #tag.setStyleSheet('QFrame {border:1px solid rgb(192, 192, 192); height: 10px; margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px;}')

        # # Label
        if text:
            self.icon = 'X'
            self.action = partial(self._tagBar.delete_tag, text)
            self.label = QLabel(text)
            font = QFont()
            font.setPointSize(8)
            self.label.setFont(font)
            self.label.setStyleSheet(self.label.styleSheet() + 'border:0px;')
            tag_layout.addWidget(self.label)

        # Button
        action_btn = QPushButton(self.icon)
        action_btn.setFixedSize(20, 20)
        action_btn.setStyleSheet(action_btn.styleSheet() + 'QPushButton {height: 20px; background-color: transparent; border:0px; font-weight:bold;}')
        action_btn.clicked.connect(self.action)
        tag_layout.addWidget(action_btn)
        main_layout.addWidget(tag)


class TagBar(QWidget):
    def __init__(self):
        super(TagBar, self).__init__()
        self.tags = []
        self.main_layout = QHBoxLayout(self)
        self.main_layout.setAlignment(Qt.AlignJustify)
        self.main_layout.setContentsMargins(0,0,0,0)

        self.root_widget = QWidget()
        self.root_layout = QHBoxLayout()
        self.root_widget.setLayout(self.root_layout)
        self.main_layout.addWidget(self.root_widget)
        self.root_layout.setContentsMargins(0, 0, 0, 0)

        # TagsLine wdg
        self.tags_widget = QWidget()
        self.tags_layout = QHBoxLayout()
        self.tags_widget.setLayout(self.tags_layout)
        self.tags_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addWidget(self.tags_widget)

        # Plus Button
        plusButton = BaseTag(self, '')
        self.main_layout.addWidget(plusButton)

        self.setup_ui()
        self.refresh()
        self.show()

    def setup_ui(self):
        self.create_tags()
        # self.lineEdit_widget.returnPressed.connect(self.create_tags)

    def create_tags(self):
        new_tags = ['lucasm',
                    'jennyp']
        self.tags.extend(new_tags)
        self.tags = list(set(self.tags))
        self.tags.sort(key=lambda x: x.lower())
        self.refresh()

    def refresh(self):
        for i in reversed(range(self.tags_layout.count())):
            self.tags_layout.itemAt(i).widget().setParent(None)
        for tag in self.tags:
            plusButton = BaseTag(self, tag)
            self.tags_layout.addWidget(plusButton)

    def delete_tag(self, tag_name):
        self.tags.remove(tag_name)
        self.refresh()


if __name__ == '__main__':
    qt_app = QApplication(sys.argv)
    tag_bar = TagBar()
    qt_app.exec_()
