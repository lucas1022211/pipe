import qtawesome
from functools import cached_property
from typing import Optional, List, Any, Tuple, Union
from qtpy import QtCore, QtWidgets
from pathlib import Path


class TreeItem:
    """Tree node with tri-state checkbox support and path compression."""

    def __init__(self, path: Path, parent: Optional['TreeItem'] = None) -> None:
        self.path: Path = path
        self.parent_item = parent
        self.child_items: List[TreeItem] = []
        self.check_state = QtCore.Qt.CheckState.Unchecked
        self.compressed_path: List[str] = [path.name]
        self._populate_children()
        self._compress_single_child_paths()

    def _populate_children(self) -> None:
        """Populate children, excluding empty folders."""
        if not self.is_dir:
            return

        potential_children = []
        for child_path in sorted(self.path.iterdir()):
            child = TreeItem(child_path, self)
            if not child.is_dir or (child.is_dir and child.child_items) and not child.is_work:
                potential_children.append(child)

        self.child_items = potential_children

    def _compress_single_child_paths(self) -> None:
        """Compress paths with single directory children into a single item."""
        while len(self.child_items) == 1 and self.child_items[0].is_dir:
            child = self.child_items[0]
            self.compressed_path.extend(child.compressed_path)
            self.child_items = child.child_items
            for grandchild in self.child_items:
                grandchild.parent_item = self

    @cached_property
    def is_dir(self) -> bool:
        """Check if the item represents a directory."""
        return self.path.is_dir()

    @cached_property
    def is_work(self) -> bool:
        """Check if the item represents a directory."""
        return self.path.stem == "work"

    @cached_property
    def name(self) -> str:
        """Get the display name for the item, including file count for folders."""
        if self.is_dir:
            display_name = '/'.join(self.compressed_path)
            file_count = self.file_count
            files_text = "file" if file_count == 1 else "files"
            return f"{display_name} ({file_count} {files_text})"
        return self.path.name

    @cached_property
    def file_count(self) -> int:
        """Count files in the model hierarchy (not filesystem)."""
        if not self.is_dir:
            return 0
        return sum(1 if not child.is_dir else child.file_count for child in self.child_items)

    def child(self, row):
        return self.child_items[row]

    def childCount(self):
        return len(self.child_items)

    def parent(self):
        return self.parent_item

    def row(self):
        if self.parent_item:
            return self.parent_item.child_items.index(self)
        return 0


class TriStateTreeModel(QtCore.QAbstractItemModel):
    """Model for tri-state checkboxes in a tree structure with path compression."""
    stateChanged = QtCore.Signal(QtCore.QModelIndex, QtCore.Qt.CheckState)

    def __init__(self, root_path: Path) -> None:
        super().__init__()
        self.root_item = TreeItem(root_path)

    def columnCount(self, parent=None):
        return 1

    def rowCount(self, parent=None):
        return self.getItem(parent).childCount()

    def data(self, index: Union[QtCore.QModelIndex, QtCore.QPersistentModelIndex],
             role: QtCore.Qt.ItemDataRole = QtCore.Qt.ItemDataRole.EditRole) -> Any:
        if not index.isValid():
            return None

        item = self.getItem(index)
        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            return item.name
        elif role == QtCore.Qt.ItemDataRole.DecorationRole:
            return qtawesome.icon("fa.folder") if item.is_dir else qtawesome.icon("fa.file-o")
        if role == QtCore.Qt.ItemDataRole.CheckStateRole:
            return item.check_state
        return None

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        if not index.isValid():
            return QtCore.Qt.ItemFlag.NoItemFlags
        return (QtCore.Qt.ItemFlag.ItemIsEnabled | QtCore.Qt.ItemFlag.ItemIsSelectable |
                QtCore.Qt.ItemFlag.ItemIsUserCheckable)

    def getItem(self, index: QtCore.QModelIndex) -> TreeItem:
        if index and index.isValid():
            item = index.internalPointer()
            if item:
                return item
        return self.root_item

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = QtCore.QModelIndex()) -> QtCore.QModelIndex:
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        parent_item = self.getItem(parent)
        child_item = parent_item.child(row)
        if child_item:
            return self.createIndex(row, column, child_item)
        return QtCore.QModelIndex()

    def parent(self, index: QtCore.QModelIndex) -> QtCore.QModelIndex:
        if not index.isValid():
            return QtCore.QModelIndex()

        parent_item = self.getItem(index).parent()
        if parent_item == self.root_item:
            return QtCore.QModelIndex()

        return self.createIndex(parent_item.row(), 0, parent_item)

    def setData(self, index: Union[QtCore.QModelIndex, QtCore.QPersistentModelIndex], value: Any,
                role: QtCore.Qt.ItemDataRole = QtCore.Qt.ItemDataRole.EditRole) -> bool:
        if not index.isValid() or role != QtCore.Qt.ItemDataRole.CheckStateRole:
            return False

        try:
            item = self.getItem(index)
            state = QtCore.Qt.CheckState(value)
            if item.check_state != state:
                item.check_state = state
                self._propagateStateChange(index, state)
                return True
        except (ValueError, TypeError):
            pass
        return False

    def _propagateStateChange(self, index: QtCore.QModelIndex, state: QtCore.Qt.CheckState) -> None:
        self.stateChanged.emit(index, state)
        self._updateChildrenState(index, state)
        self._updateParentState(index)

    def _updateChildrenState(self, parent_index: QtCore.QModelIndex, state: QtCore.Qt.CheckState) -> None:
        item = self.getItem(parent_index)
        item.check_state = state
        for row in range(item.childCount()):
            child_index = self.index(row, 0, parent_index)
            child_item = self.getItem(child_index)
            if child_item.check_state != state:
                child_item.check_state = state
                self.dataChanged.emit(child_index, child_index, [QtCore.Qt.ItemDataRole.CheckStateRole])
                self._updateChildrenState(child_index, state)

    def _updateParentState(self, index: QtCore.QModelIndex) -> None:
        parent_index = self.parent(index)
        if not parent_index.isValid():
            return

        parent_item = self.getItem(parent_index)
        child_states = []

        for row in range(parent_item.childCount()):
            child_index = self.index(row, 0, parent_index)
            child_item = self.getItem(child_index)
            child_states.append(child_item.check_state)

        if all(state == QtCore.Qt.CheckState.Checked for state in child_states):
            new_state = QtCore.Qt.CheckState.Checked
        elif all(state == QtCore.Qt.CheckState.Unchecked for state in child_states):
            new_state = QtCore.Qt.CheckState.Unchecked
        else:
            new_state = QtCore.Qt.CheckState.PartiallyChecked

        if parent_item.check_state != new_state:
            parent_item.check_state = new_state
            self.dataChanged.emit(parent_index, parent_index, [QtCore.Qt.ItemDataRole.CheckStateRole])
            self._updateParentState(parent_index)


class TriStateListView(QtWidgets.QWidget):
    """Widget for hierarchical data selection using tri-state checkboxes."""
    stateChanged = QtCore.Signal(str, QtCore.Qt.CheckState)

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None) -> None:
        super().__init__(parent)
        test_path = Path(r"G:\Shared drives\BM_Lucas\__PRIMARY_ROOT__\MansourSara")
        self.tree_view = QtWidgets.QTreeView()
        self.tree_view.setSelectionMode(QtWidgets.QTreeView.ExtendedSelection)
        self.model = TriStateTreeModel(root_path=test_path)
        self.tree_view.setModel(self.model)
        self.tree_view.expandAll()
        self.tree_view.setHeaderHidden(True)
        self.setLayout(QtWidgets.QVBoxLayout(self))
        self.layout().addWidget(self.tree_view)
        self.model.stateChanged.connect(self.update_selected_items_state)

    def get_checked_items(self) -> List[str]:
        """Get all fully checked items."""
        return self._get_checked_items_recursive(self.model.root_item)

    def _get_checked_items_recursive(self, item: TreeItem) -> List[str]:
        result = []
        if item.check_state == QtCore.Qt.CheckState.Checked:
            result.append(item.name)
        for child in item.child_items:
            result.extend(self._get_checked_items_recursive(child))
        return result

    def update_selected_items_state(self, index: QtCore.QModelIndex, state: QtCore.Qt.CheckState) -> None:
        """Update the check state of all selected items."""
        selection_model = self.tree_view.selectionModel()
        selected_indexes = selection_model.selectedIndexes()
        for index in selected_indexes:
            if index.isValid():
                item = self.model.getItem(index)
                item.check_state = state
                self.model.dataChanged.emit(index, index, [QtCore.Qt.ItemDataRole.CheckStateRole])


if __name__ == "__main__":
    from teide_qt import utils

    app = QtWidgets.QApplication([])
    utils.set_theme(app)
    view = TriStateListView()

    view.show()
    app.exec()
