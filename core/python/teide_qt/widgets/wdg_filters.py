import qtawesome
from qtpy import QtWidgets, QtCore, QtGui


class DemoFilterWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        # Set up the layout
        self.layout = QtWidgets.QVBoxLayout(self)

        self.layout.addWidget(QtWidgets.QLabel("Filters:"))
        self.layout.addWidget(QtWidgets.QCheckBox("Option 1"))
        self.layout.addWidget(QtWidgets.QCheckBox("Option 2"))
        self.layout.addWidget(QtWidgets.QCheckBox("Option 3"))

        # Add a close button
        close_button = QtWidgets.QPushButton("Close")
        close_button.clicked.connect(self.close)
        self.layout.addWidget(close_button)


class FilterWindow(QtWidgets.QWidget):
    filters_updated = QtCore.Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    def init_ui(self): pass
    def populate_ui(self): pass
    def connect_signals(self): pass

    def hideEvent(self, event):
        self.filters_updated.emit()
        event.accept()


class FilterWidget(QtWidgets.QPushButton):
    filter_window_class = FilterWindow

    def __init__(self, parent=None):
        super().__init__(parent)

        # Set up the button
        filters_icon = qtawesome.icon("fa.sliders")
        self.setIcon(filters_icon)
        self.setToolTip("Open Filter Options")
        self.clicked.connect(self.show_filter_window)

        # Create the floating filter window if it hasn't been created yet
        self.filter_wdg = self.filter_window_class(self)
        self.filter_wdg.setWindowFlags(QtCore.Qt.Popup | QtCore.Qt.FramelessWindowHint)

    def show_filter_window(self):
        # Show the floating filter window
        if self.filter_wdg.isHidden():
            self.filter_wdg.move(self.mapToGlobal(QtCore.QPoint(self.width() - self.filter_wdg.width(), self.height())))
            self.filter_wdg.show()

    def toggle_filter_window(self):
        """Toggle the visibility of the filter window."""
        if self.filter_wdg.isVisible():
            self.filter_wdg.hide()
        else:
            self.show_filter_window()

    def update_icon(self, icon_name):
        """Update the icon of the filter button."""
        new_icon = qtawesome.icon(icon_name)
        self.setIcon(new_icon)


class DemoApp(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        # Set up the main layout
        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.setContentsMargins(10, 10, 10, 10)

        # Row 1: Search bar and filter widget
        row_layout = QtWidgets.QHBoxLayout()
        self.search_bar = QtWidgets.QLineEdit()
        self.search_bar.setPlaceholderText("Search...")
        self.search_bar.setToolTip("Type to search items")
        row_layout.addWidget(self.search_bar)

        self.filter_widget = FilterWidget()
        row_layout.addWidget(self.filter_widget)

        self.reload_widget = QtWidgets.QPushButton(icon=qtawesome.icon('fa.refresh'))
        row_layout.addWidget(self.reload_widget)
        main_layout.addLayout(row_layout)

        # Row 2: List widget
        self.list_widget = QtWidgets.QListWidget()
        main_layout.addWidget(self.list_widget)

        # Populate the list widget with demo items
        self.populate_list()

    def populate_list(self):
        for i in range(1, 21):
            self.list_widget.addItem(f"Item {i}")


if __name__ == "__main__":
    from teide_qt import utils
    import sys

    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    demo = DemoApp()
    demo.resize(400, 300)
    demo.setWindowTitle("Demo App with Search and Filters")
    demo.show()
    sys.exit(app.exec_())
