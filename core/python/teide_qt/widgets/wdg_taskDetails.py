import datetime
import enum
import sys
from typing import TYPE_CHECKING, Dict, List, Any, Optional

from qtpy import QtCore, QtGui, QtWidgets

import cmd
import teide_core.helpers
from teide_core import tasks
from teide_core import settings
from teide_qt.widgets.wdg_tagBar import TagBar
from teide_qt.widgets.wdg_tagBarTest import TagEditor


if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class TaskDetailsWidget(QtWidgets.QWidget):
    # todo: make better widget for List attributes such as Tags, Assignees, Reviewers, Notes?
    #  // Lets try to have a widget that has a hyper-link to the webpage of the sg entity for:
    #       notes, linked_entity, assignees, assignees, step, project (In order of relevance/usefulness)
    properties = [
            {'attr_name': 'prName',         'display_name': "Project",      'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'step',           'display_name': "Step",         'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'linked_entity',  'display_name': "Entity",       'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'status',         'display_name': "Status",       'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'priority',       'display_name': "Priority",     'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'assignees',      'display_name': "Assignees",    'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'reviewers',      'display_name': "Reviewers",    'wdg_class': QtWidgets.QLineEdit},
            {'attr_name': 'start_date',     'display_name': "Start Date",   'wdg_class': QtWidgets.QDateEdit},
            {'attr_name': 'due_date',       'display_name': "Due Date",     'wdg_class': QtWidgets.QDateEdit},
            {'attr_name': 'time_bid',       'display_name': "Bid Time",     'wdg_class': QtWidgets.QSpinBox},
            {'attr_name': 'time_logged',    'display_name': "Used Time",    'wdg_class': QtWidgets.QSpinBox},
            {'attr_name': 'notes',          'display_name': "Notes",        'wdg_class': QtWidgets.QLineEdit},
        ]

    def __init__(self):
        super(TaskDetailsWidget, self).__init__()

        self.init_ui()

    def init_ui(self):
        self.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding))

        # Main Layout
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.setContentsMargins(0, 0, 0, 0)

        # Tabs
        self.details_tabs_widget = QtWidgets.QTabWidget()
        self.main_layout.addWidget(self.details_tabs_widget)

        # Properties - Tab1
        self.properties_widget = QtWidgets.QWidget(self)
        self.details_tabs_widget.addTab(self.properties_widget, 'Task Details')
        self.property_layout = QtWidgets.QFormLayout()
        self.property_layout.setLabelAlignment(QtCore.Qt.AlignmentFlag.AlignRight)
        self.properties_widget.setLayout(self.property_layout)

        for ui_property in self.properties:
            task_widget = ui_property['wdg_class']()
            ui_property['wdg_instance'] = task_widget
            task_widget.setEnabled(False)

            if isinstance(task_widget, QtWidgets.QDateEdit):
                task_widget.setCalendarPopup(True)

            self.property_layout.addRow(ui_property['display_name'], task_widget)

        # Dependencies Graph - Tab2
        self.dependenciesGraph_widget = QtWidgets.QWidget()
        self.dependenciesGraph_layout = QtWidgets.QVBoxLayout(self.dependenciesGraph_widget)
        self.details_tabs_widget.addTab(self.dependenciesGraph_widget, 'Dependencies')
    
    def populate(self, task):

        def format_value(value):
            if isinstance(value, list):
                return ', '.join([format_value(v) for v in value])
            elif isinstance(value, dict):
                return value.get('name', None)
            elif isinstance(value, QtCore.QDate):
                print(">>type QDate found!")
                return datetime.date(value.year(), value.month(), value.day())
            elif issubclass(type(value), enum.Enum):
                return value.name.capitalize()
            return value

        for ui_property in self.properties:
            attr_name = ui_property['attr_name']
            task_widget = ui_property['wdg_instance']

            value = next((v for k, v in task.get_attributes().items() if k == attr_name), None)
            if not value:
                continue

            # Use the formatter to adjust the value
            formatted_value = format_value(value)

            value_func = None
            if isinstance(task_widget, QtWidgets.QDateEdit):
                value_func = task_widget.setDate
            elif isinstance(task_widget, QtWidgets.QLineEdit):
                value_func = task_widget.setText
            elif isinstance(task_widget, QtWidgets.QSpinBox):
                value_func = task_widget.setValue

            # Set value
            if value_func:
                value_func(formatted_value)
                if isinstance(formatted_value, str) and ', ' in formatted_value:
                    tooltip_msg = formatted_value.replace(', ', '\n')
                    task_widget.setToolTip(tooltip_msg)


if __name__ == "__main__":
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)

    demo_task = tasks.Task(
        id=6108,
        prName='MansourSara',
        name='turnaround',
        step='mod',
        linked_entity={'type': "Asset", 'name': "MyAsset"},
        assignees=[{'type': "HumanUser", 'name': "Lucas"}, {'type': "HumanUser", 'name': "Jenny"}],
        status=tasks.Status.READY_TO_PUBLISH,
        start_date=datetime.date.today(),
        due_date=datetime.datetime.now() + datetime.timedelta(days=14)
    )

    dialog = TaskDetailsWidget()
    dialog.show()
    dialog.populate(demo_task)

    sys.exit(app.exec_())
