import sys
import datetime
from typing import TYPE_CHECKING, Optional, List, Tuple

from qtpy import QtCore, QtGui, QtWidgets

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class TimeRangeFilterWidget(QtWidgets.QWidget):
    """
    A widget for filtering data based on a selected time range.

    This widget provides a user-friendly interface for selecting predefined time ranges,
    such as "7 days", "1 month", etc. It emits a QtCore.Signal with the number of days when
    a new range is selected.

    Attributes
    ----------
    filterChanged : QtCore.Signal
        QtCore.Signal emitted when the selected time range changes. Emits the number of days.
    time_ranges : List[Tuple[str, int]]
        List of tuples containing the display text and corresponding number of days
        for each time range option.

    Methods
    -------
    get_filter_days() -> int
        Get the number of days for the currently selected time range.
    set_filter_days(days: int) -> None
        Set the current selection based on the given number of days.
    get_date_range() -> Tuple[QtCore.QDate, QtCore.QDate]
        Get the start and end dates for the currently selected time range.
    set_time_ranges(new_ranges: List[Tuple[str, int]]) -> None
        Update the available time range options.
    """

    filterChanged = QtCore.Signal(int)
    descriptionText = "Modified in the last"

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None) -> None:
        """
        Initialize the EnhancedLastXDaysFilterWidget.

        Parameters
        ----------
        parent : Optional[QtWidgets.QWidget]
            The parent widget, by default None
        """
        super().__init__(parent)
        self.time_ranges: List[Tuple[str, int]] = [
            ("1 days", 1),
            ("7 days", 7),
            ("14 days", 14),
            ("1 month", 30),
            ("3 months", 90),
            ("6 months", 180),
            ("1 year", 365),
            ("All Time", -1),
        ]
        self._init_ui()

    def _init_ui(self) -> None:
        """Initialize the user interface."""
        layout = QtWidgets.QHBoxLayout(self)
        layout.setAlignment(QtCore.Qt.AlignLeft)
        layout.setContentsMargins(0, 0, 0, 0)

        description_label = QtWidgets.QLabel(f"{self.descriptionText}:")
        layout.addWidget(description_label)

        self.range_combo = QtWidgets.QComboBox()
        for label, _ in self.time_ranges:
            self.range_combo.addItem(label)
        self.range_combo.currentIndexChanged.connect(self._on_range_changed)
        layout.addWidget(self.range_combo)

        self.range_combo.setCurrentIndex(1)  # Default to 14 days

    def _on_range_changed(self, index: int) -> None:
        """
        Handle the change of selected time range.

        Parameters
        ----------
        index : int
            The index of the newly selected item in the combo box.
        """
        _, days = self.time_ranges[index]
        self.filterChanged.emit(days)

    @property
    def is_off(self):
        return self.time_ranges[self.range_combo.currentIndex()][1] == -1

    def get_filter_days(self) -> int:
        """
        Get the number of days for the currently selected time range.

        Returns
        -------
        int
            The number of days for the current selection.
        """
        index = self.range_combo.currentIndex()
        return self.time_ranges[index][1]

    def set_filter_days(self, days: int) -> None:
        """
        Set the current selection based on the given number of days.

        Parameters
        ----------
        days : int
            The number of days to set the filter to.
        """
        for index, (_, range_days) in enumerate(self.time_ranges):
            if days == range_days:
                self.range_combo.setCurrentIndex(index)
                return
        _logger.warning(f"{days} days is not a predefined option")

    def get_date_range(self) -> Tuple[datetime.datetime, datetime.datetime]:
        """
        Get the start and end dates for the currently selected time range.

        Returns
        -------
        Tuple[datetime.datetime, datetime.datetime]
            A tuple containing the start date and end date.
        """
        days = self.get_filter_days()
        end_date = datetime.datetime.now()
        start_date = end_date.replace(hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=days - 1)
        return start_date, end_date

    def set_time_ranges(self, new_ranges: List[Tuple[str, int]]) -> None:
        """
        Update the available time range options.

        Parameters
        ----------
        new_ranges : List[Tuple[str, int]]
            A list of tuples, each containing a display label and the corresponding
            number of days.
        """
        self.time_ranges = new_ranges
        self.range_combo.clear()
        for label, _ in self.time_ranges:
            self.range_combo.addItem(label)
        self.range_combo.setCurrentIndex(0)


if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    widget = TimeRangeFilterWidget()
    widget.show()

    sys.exit(app.exec())
