from typing import Optional, List
from qtpy import QtWidgets, QtCore, QtGui
import qtawesome
from teide_qt.widgets import lyt_flowLayout


class TagSelectorWidget(QtWidgets.QPushButton):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        self.setCheckable(True)
        self.setFlat(True)
        self.setFixedHeight(30)
        main_layout = QtWidgets.QHBoxLayout()
        main_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(main_layout)
        self.flow_layout = lyt_flowLayout.FlowLayout()
        self.tag_container = QtWidgets.QWidget()
        self.tag_container.setLayout(self.flow_layout)
        main_layout.addWidget(self.tag_container, stretch=1)

        clear_icon = qtawesome.icon("fa.remove")
        self.clear_button = QtWidgets.QPushButton(icon=clear_icon)
        self.clear_button.setFlat(True)
        self.clear_button.clicked.connect(self.clear_tags)
        main_layout.addWidget(self.clear_button)

        self.tags = []
        self.floating_window = FloatingWindow(self)

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.toggle()
            self.show_filter_window()
        super().mousePressEvent(event)

    def add_items(self, items):
        self.floating_window.add_items(items)
        self.update_tags()

    def update_tags(self):
        for tag in self.tags:
            tag.deleteLater()
        self.tags.clear()

        for item in self.floating_window.get_checked_items():
            tag = TagButton(self, item)
            self.flow_layout.addWidget(tag)
            self.tags.append(tag)

        self.update_clear_button()

    def clear_tags(self):
        self.floating_window.uncheck_all_items()
        self.update_tags()

    def update_clear_button(self):
        self.clear_button.setVisible(any(self.floating_window.get_checked_items()))

    def show_filter_window(self):
        # Show the floating filter window
        if self.floating_window.isHidden():
            self.floating_window.move(self.mapToGlobal(QtCore.QPoint(self.width() - self.floating_window.width(), self.height())))
            self.floating_window.show()


class FloatingWindow(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.Popup)
        self.init_ui()
        self.installEventFilter(self)

    def init_ui(self):
        self.setLayout(QtWidgets.QVBoxLayout())

        self.search_bar = QtWidgets.QLineEdit()
        self.search_bar.setPlaceholderText("Search...")
        self.search_bar.textChanged.connect(self.filter_items)
        self.layout().addWidget(self.search_bar)

        self.item_list = QtWidgets.QListWidget()
        self.item_list.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.item_list.itemChanged.connect(self.notify_parent_update)  # Notify parent on item state change
        self.layout().addWidget(self.item_list)

    def add_items(self, items: List[str]):
        for item in items:
            q_item = QtWidgets.QListWidgetItem(item)
            q_item.setFlags(q_item.flags() | QtCore.Qt.ItemIsUserCheckable)
            q_item.setCheckState(QtCore.Qt.Unchecked)
            self.item_list.addItem(q_item)

    def filter_items(self, text):
        for i in range(self.item_list.count()):
            item = self.item_list.item(i)
            item.setHidden(text.lower() not in item.text().lower())

    def get_checked_items(self):
        return [self.item_list.item(i).text() for i in range(self.item_list.count()) if
                self.item_list.item(i).checkState() == QtCore.Qt.Checked]

    def uncheck_all_items(self):
        for i in range(self.item_list.count()):
            self.item_list.item(i).setCheckState(QtCore.Qt.Unchecked)

    def uncheck_item(self, item_name):
        for i in range(self.item_list.count()):
            if self.item_list.item(i).text() == item_name:
                self.item_list.item(i).setCheckState(QtCore.Qt.Unchecked)

    def notify_parent_update(self):
        parent = self.parent()
        if isinstance(parent, TagSelectorWidget):
            parent.update_tags()

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            if not self.rect().contains(self.mapFromGlobal(QtGui.QCursor.pos())):
                self.close()
        return super().eventFilter(source, event)


class TagButton(QtWidgets.QPushButton):
    def __init__(self, parent, text):
        super().__init__(parent)
        self.parent = parent
        self.init_ui(text)
        self.clicked.connect(self.remove_tag)

    def init_ui(self, text):

        # Set object name for stylesheet targeting
        self.setObjectName("TagButton")
        self.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.setText(text)

        # Set default icon
        self.default_icon = qtawesome.icon("mdi6.tag")
        self.hover_icon = qtawesome.icon("fa.remove")
        self.setIcon(self.default_icon)

        # Use palette for default and hover background colors
        self.default_color = self.palette().color(QtGui.QPalette.Window).name()
        self.hover_color = self.palette().color(QtGui.QPalette.AlternateBase).name()

        # Adjust icon placement and style dynamically
        self.setStyleSheet(f"""
            QPushButton#TagButton {{
                border: 2px solid {self.hover_color};
                border-radius: 10px;
                padding: 2px 4px;
                background-color: {self.default_color};
            }}
            QPushButton#TagButton:hover {{
                background-color: {self.hover_color};
            }}
        """)

        # Enable hover events
        self.setMouseTracking(True)

    def enterEvent(self, event):
        """Change to hover icon on mouse enter."""
        self.setIcon(self.hover_icon)
        super().enterEvent(event)

    def leaveEvent(self, event):
        """Revert to default icon on mouse leave."""
        self.setIcon(self.default_icon)
        super().leaveEvent(event)

    def remove_tag(self):
        if not isinstance(self.parent, TagSelectorWidget):
            return
        self.parent.floating_window.uncheck_item(self.text())
        self.parent.update_tags()


if __name__ == "__main__":
    from teide_qt import utils
    import sys

    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    main_window = QtWidgets.QMainWindow()
    selector_widget = TagSelectorWidget()
    main_window.setCentralWidget(selector_widget)
    main_window.resize(600, 300)
    main_window.show()

    app.exec()
