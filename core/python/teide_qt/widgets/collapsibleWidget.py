import sys
from pathlib import Path

import qtawesome
from teide_core import publish
from qtpy import QtCore, QtWidgets, QtGui

# done: make use of nice qtpy imports.
# done: Lets make a custom list view that takes this kind of collapsable widgets and for each of them
#  we have a remove icon [X]. Needs scroll. and maybe a [+] icon too?
#  All this should be configurable with class variables. ex: INCLUDE_REMOVE,  INCLUDE_ADD


class CollapsibleWidget(QtWidgets.QFrame):
    def __init__(self, file: Path, content: QtWidgets.QWidget, parent=None):
        super().__init__(parent)
        self.file = file
        self.content_widget = content

        self.init_ui()
        self.connect_signals()

    def init_ui(self):
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setObjectName("collapsibleWidget")
        self.setStyleSheet("""
            #collapsibleWidget {
                border: 2px solid #888;  /* Border color */
                border-radius: 4px;     /* Corner radius */
            }
        """)

        # Main layout for the entire collapsible widget
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.setContentsMargins(2, 2, 2, 2)

        # Create a frame for the header
        self.header_frame = QtWidgets.QWidget()
        self.header_layout = QtWidgets.QHBoxLayout(self.header_frame)
        self.header_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addWidget(self.header_frame)

        # Replace arrow button with simple icon label
        self.arrow_label = QtWidgets.QLabel()
        self.collapsed_icon = qtawesome.icon('fa5s.chevron-right')
        self.expanded_icon = qtawesome.icon('fa5s.chevron-down')
        self.arrow_label.setPixmap(self.collapsed_icon.pixmap(16, 16))
        self.header_layout.addWidget(self.arrow_label)

        # Label and combo box in the header
        self.label = QtWidgets.QLabel(self.file.name)
        self.header_layout.addWidget(self.label)

        self.header_layout.addStretch()

        # Content widget setup wrapped in a frame
        self.content_frame = QtWidgets.QWidget()
        self.content_frame.setVisible(False)
        self.content_layout = QtWidgets.QVBoxLayout(self.content_frame)
        self.content_layout.setContentsMargins(0, 0, 0, 0)
        self.content_layout.addWidget(self.content_widget)
        self.main_layout.addWidget(self.content_frame)

    def connect_signals(self):
        self.header_frame.mousePressEvent = self._header_clicked

    def _toggle_content(self):
        is_visible = not self.content_frame.isVisible()
        self.arrow_label.setPixmap(
            (self.expanded_icon if is_visible else self.collapsed_icon).pixmap(16, 16)
        )
        self.content_frame.setVisible(is_visible)

    def _header_clicked(self, event):
        self._toggle_content()


class OutputItemWidget(CollapsibleWidget):
    def __init__(self, file: Path, tokens, content: QtWidgets.QWidget, parent=None):
        self.tokens = tokens
        super().__init__(file, content, parent)

    def init_ui(self):
        super().init_ui()
        self.combo_box = QtWidgets.QComboBox()
        for label, output_type in self._get_output_types():
            self.combo_box.addItem(label, output_type)
        self.header_layout.addWidget(self.combo_box)

    @staticmethod
    def _get_output_types():
        output_types = publish.OutputType.get_all()
        return ((output_type.name.replace('_', ' ').title(), output_type) for output_type in output_types)


class CollapsibleListWidget(QtWidgets.QWidget):
    itemWidgetCls = CollapsibleWidget

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setAcceptDrops(True)
        self.init_ui()

    def init_ui(self):
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)

        # Scroll area for collapsible widgets
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        
        # Container widget for scroll area
        self.scroll_content = QtWidgets.QWidget()
        self.scroll_layout = QtWidgets.QVBoxLayout(self.scroll_content)
        self.scroll_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
        self.scroll_area.setWidget(self.scroll_content)
        
        self.main_layout.addWidget(self.scroll_area)
        
        # Create empty state label with object name
        self.empty_label = QtWidgets.QLabel("Drag and Drop Files Here")
        self.empty_label.setObjectName("dropZone")
        self.empty_label.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.empty_label.setAlignment(QtCore.Qt.AlignCenter)
        self.main_layout.addWidget(self.empty_label)
        
        # Set initial style
        self.set_drop_zone_style()
        
        # Initially hide the scroll area
        self.scroll_area.hide()

    def set_drop_zone_style(self, highlight=False):
        """
        Set the drop zone style with consistent styling
        Args:
            highlight (bool): Whether to highlight the drop zone
        """
        border = 2
        border_radius = 8
        padding = 40
        font_size = 14
        background_color = (0, 0, 0, 0.1)
        background_color_highlight = (52, 152, 219, 0.1)

        base_style = """
            QLabel#dropZone {
                border: 2px dashed #666;
                border-radius: 8px;
                padding: 40px;
                background: rgba(0, 0, 0, 0.1);
                font-size: 14px;
            }
        """
        
        highlight_style = """
            QLabel#dropZone {
                border: 2px dashed #3498db;
                border-radius: 8px;
                padding: 40px;
                background: rgba(52, 152, 219, 0.1);
                font-size: 14px;
            }
        """
        
        self.empty_label.setStyleSheet(highlight_style if highlight else base_style)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
            self.set_drop_zone_style(highlight=True)
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        self.set_drop_zone_style(highlight=False)

    def dropEvent(self, event):
        files = []
        for url in event.mimeData().urls():
            file_path = Path(url.toLocalFile())
            if file_path.is_file():
                files.append(file_path)

        if files:
            # Add each dropped file
            for file_path in files:
                self.add_collapsible(str(file_path))

            # Show scroll area and hide empty label
            self.toggle_drop_list_wdg()

        # Reset style
        self.set_drop_zone_style(highlight=False)
        event.accept()

    def toggle_drop_list_wdg(self):
        is_empty = self.scroll_layout.count() == 0
        self.empty_label.setVisible(is_empty)
        self.scroll_area.setVisible(not is_empty)

    def add_collapsible(self, file_path):
        # Extract file info
        path = Path(file_path)

        # Create content widget with file-specific information
        content = QtWidgets.QLabel('Demo Widget')

        # Create container for collapsible widget and remove button
        container = QtWidgets.QWidget()
        container_layout = QtWidgets.QHBoxLayout(container)
        container_layout.setContentsMargins(0, 0, 0, 0)

        # Create collapsible widget using filename
        collapsible = self.itemWidgetCls(file=path, content=content)
        container_layout.addWidget(collapsible, stretch=1)

        # Add remove button
        remove_button = QtWidgets.QPushButton()
        remove_button.setIcon(qtawesome.icon('fa5s.times'))
        remove_button.setFixedSize(24, 24)
        remove_button.clicked.connect(lambda: self.remove_collapsible(container))
        container_layout.addWidget(remove_button, alignment=QtCore.Qt.AlignmentFlag.AlignTop)

        self.scroll_layout.addWidget(container)

    def remove_collapsible(self, container):
        self.scroll_layout.removeWidget(container)
        container.deleteLater()

        # Show empty label with default style if no items remain
        if self.scroll_layout.count() == 0:
            self.scroll_area.hide()
            self.empty_label.show()
            self.set_drop_zone_style(highlight=False)

    def get_collapsible_widgets(self) -> list:
        """
        Returns a list of all collapsible widgets currently in the scroll layout.
        
        Returns:
            list: List of CollapsibleWidget instances
        """
        collapsible_widgets = []
        
        # Iterate through all containers in the scroll layout
        for i in range(self.scroll_layout.count()):
            container = self.scroll_layout.itemAt(i).widget()
            
            # Get the collapsible widget from the container's layout
            # (It's always the first widget, index 0)
            collapsible = container.layout().itemAt(0).widget()
            if isinstance(collapsible, self.itemWidgetCls):
                collapsible_widgets.append(collapsible)
        return collapsible_widgets


class OutputListWidget(CollapsibleListWidget):
    itemWidgetCls = OutputItemWidget

    def __init__(self, tokens, parent=None):
        self.tokens = tokens
        super().__init__(parent)

    def add_collapsible(self, file_path):
        # Extract file info
        path = Path(file_path)

        # Create content widget with file-specific information
        # fixme: make a custom widget to compute missing tokens and store methods to populate ui and get values.
        content = QtWidgets.QWidget()
        content_layout = QtWidgets.QVBoxLayout(content)
        param_layout = QtWidgets.QFormLayout()
        param_layout.setLabelAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

        # Create form fields
        variant_text = QtWidgets.QLineEdit()
        descriptor_text = QtWidgets.QLineEdit()
        param_layout.addRow('Variant', variant_text)
        param_layout.addRow('Descriptor', descriptor_text)

        # Show full file path
        preview_label = QtWidgets.QLabel(file_path)
        preview_label.setWordWrap(True)

        content_layout.addLayout(param_layout)
        content_layout.addWidget(preview_label)

        # Create container for collapsible widget and remove button
        container = QtWidgets.QWidget()
        container_layout = QtWidgets.QHBoxLayout(container)
        container_layout.setContentsMargins(0, 0, 0, 0)

        # Create collapsible widget using filename
        collapsible = self.itemWidgetCls(file=path, tokens=self.tokens, content=content)
        container_layout.addWidget(collapsible, stretch=1)

        # Add remove button
        remove_button = QtWidgets.QPushButton()
        remove_button.setIcon(qtawesome.icon('fa5s.times'))
        remove_button.setFixedSize(24, 24)
        remove_button.clicked.connect(lambda: self.remove_collapsible(container))
        container_layout.addWidget(remove_button, alignment=QtCore.Qt.AlignmentFlag.AlignTop)

        self.scroll_layout.addWidget(container)


if __name__ == "__main__":
    from teide_qt import utils
    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    # Create main window
    main_window = QtWidgets.QMainWindow()
    main_widget = CollapsibleListWidget()
    main_window.setCentralWidget(main_widget)
    main_window.setWindowTitle("Collapsible List Widget")
    main_window.show()

    sys.exit(app.exec())
