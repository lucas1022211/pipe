""" FileTableWidget
"""
import os
import sys
import enum
from typing import TYPE_CHECKING, Optional, Union, List, Tuple, Callable
from pathlib import Path

import attr
import qtawesome
from qtpy import QtCore, QtWidgets, QtGui

import files
import teide_core.helpers
from teide_core import filePath, publish

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


ITEM_DATA_ROLE = QtCore.Qt.UserRole + 1
ITEM_EXPANDED_ROLE = QtCore.Qt.UserRole + 2
DEFAULT_COLOR = QtGui.QPalette().color(QtGui.QPalette.Text)
PUBLISH_COLOR = QtGui.QColor('LightSkyBlue')
HIGHLIGHT_COLOR = QtGui.QColor('deepPink')
EDIT_ADD_COLOR = QtGui.QColor('forestgreen')
EDIT_REMOVE_COLOR = QtGui.QColor(255, 50, 50)
DATE_FORMAT = "yyyy/MM/dd hh:mm"


class ItemDataType(enum.Enum):
    """Valid Item Data Types"""

    FILE_SEQUENCE = 'file_sequence'
    ROOT = 'root'
    SCENE = 'scene'
    OUTPUT = 'output'


SKIP_TYPES = [ItemDataType.ROOT]


def size_formatter(size):
    # Define size units in bytes
    KB = 1024
    MB = KB * 1024
    GB = MB * 1024
    TB = GB * 1024
    PB = TB * 1024

    size_bytes = size
    if size_bytes < KB:
        return f"{size_bytes} B"
    elif size_bytes < MB:
        return f"{size_bytes / KB:.2f} KB"
    elif size_bytes < GB:
        return f"{size_bytes / MB:.2f} MB"
    elif size_bytes < TB:
        return f"{size_bytes / GB:.2f} GB"
    elif size_bytes < PB:
        return f"{size_bytes / TB:.2f} TB"
    else:
        return f"{size_bytes / PB:.2f} PB"


@attr.s
class BaseItemData(object):
    """Base Item object"""

    itemType = attr.ib(type=ItemDataType, validator=attr.validators.in_(list(ItemDataType)))
    filePath = attr.ib(type=Optional[Path], default=None)  # fixme: rename to path --collision with filePath.FilePath
    fileInfo = attr.ib(type=Optional[os.stat_result])
    tokens = attr.ib(type=dict, factory=dict)

    name = attr.ib(type=str)
    display_type = attr.ib(type=str)
    size = attr.ib(type=str)
    date = attr.ib(type=bool)

    # Pipe attributes (isPublish, task, version, variant) aka template fields
    isPublish = attr.ib(type=bool, default=False)  # todo: access from teide.filePath.isPublish or similar
    task = attr.ib(type=str, default='')
    variant = attr.ib(type=str, default='')
    version = attr.ib(type=str, default='')

    parent = attr.ib(type=Optional['BaseItemData'], default=None)
    treeChildren = attr.ib(type=List['BaseItemData'], factory=list)

    display_attributes = [
        {"name": "File", "attr": "name"},
        {"name": "Type", "attr": "display_type"},
        {"name": "Version", "attr": "version"},
        {"name": "Variant", "attr": "variant"},
        {"name": "Size", "attr": "size_formatted"},
        {"name": "Modified Date", "attr": "date"}
    ]

    def __str__(self) -> str:
        return f'{self.name}, {self.date}, {self.size}, {self.size_formatted}'

    @fileInfo.default
    def fileInfoDefault(self) -> Optional[os.stat_result]:
        """
        Get the filePath file info
        """
        if not self.filePath:
            return
        return os.stat(self.filePath)

    @name.default
    def nameDefault(self) -> str:
        """
        Generate the default name of the attribute
        """
        if not self.filePath:
            return 'unknown'
        return os.path.basename(self.filePath)

    @display_type.default
    def display_typeDefault(self) -> str:
        """
        Generate the default display_type of the attribute
        """
        if not self.itemType:
            return 'unknown'
        return self.itemType.value

    @size.default
    def sizeDefault(self) -> int:
        """
        Get the filePath size
        """
        if not self.filePath or not self.fileInfo:
            return 0
        return self.fileInfo.st_size

    @date.default
    def dateDefault(self) -> str:
        """
        Get the modification date of the filePath
        """
        if not self.fileInfo:
            return 'unknown'
        modified_timestamp = self.fileInfo.st_mtime
        milliseconds_since_epoch = int(modified_timestamp * 1000)
        return QtCore.QDateTime.fromMSecsSinceEpoch(milliseconds_since_epoch).toString(DATE_FORMAT)

    @property
    def size_formatted(self):
        """Convert the size in bytes to a nice string"""
        return size_formatter(self.size)

    @property
    def itemType_formatted(self):
        """Convert the itemType Enum to a nice string"""
        return self.itemType.value

    def addChild(self, child: 'BaseItemData') -> None:
        if child.name not in [tc.name for tc in self.treeChildren]:
            self.treeChildren.append(child)
        self.treeChildren = sorted(self.treeChildren, key=lambda x: x.name)

    def addChildren(self, children: Union['BaseItemData', List['BaseItemData']], replace: bool = False) -> None:
        if replace:
            self.treeChildren = []
        if not isinstance(children, list):
            self.addChild(children)
        else:
            for child in children:
                self.addChild(child)

    def sort(self, column, order):
        if self.treeChildren is not None:
            if column == len(self.display_attributes) - 2:  # Sort by size (column second to latest index)
                self.treeChildren.sort(key=lambda node: node.size, reverse=(order == QtCore.Qt.DescendingOrder))
            else:
                self.treeChildren.sort(key=lambda node: getattr(node, self.display_attributes[column]["attr"]),
                                       reverse=(order == QtCore.Qt.DescendingOrder))


class FileTreeModel(QtCore.QAbstractItemModel):
    itemDataCls = BaseItemData

    def __init__(self, data: Optional[BaseItemData] = None, parent=None):
        super(FileTreeModel, self).__init__(parent)
        if data:
            self._data = data
        else:
            self._data = self.itemDataCls(filePath=None, name='root',
                                          itemType=ItemDataType.ROOT)

    def index(self, row, column, parent):
        if not parent.isValid():
            parent_item = self._data.treeChildren[row]
            return self.createIndex(row, column, parent_item)
        else:
            parent_item = parent.internalPointer()
            child_item = parent_item.treeChildren[row]
            return self.createIndex(row, column, child_item)

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        child_item = index.internalPointer()
        parent_item = child_item.parent

        if not parent_item or parent_item == self._data:
            return QtCore.QModelIndex()

        row = parent_item.treeChildren.index(child_item)
        return self.createIndex(row, 0, parent_item)

    def rowCount(self, parent):
        if not self._data:
            return 0
        if not parent.isValid():
            return len(self._data.treeChildren)
        else:
            parent_item = parent.internalPointer()
            return len(parent_item.treeChildren)

    def columnCount(self, parent):
        return len(self.itemDataCls.display_attributes)  # Number of columns based on display attributes

    def data(self, index, role):
        if not index.isValid():
            return None
        if not self._data:
            return None
        itemData = index.internalPointer()

        if role == QtCore.Qt.DisplayRole:
            attr_name = self.itemDataCls.display_attributes[index.column()]["attr"]
            return getattr(itemData, attr_name)
        elif role == QtCore.Qt.TextColorRole and itemData.isPublish:
            return QtGui.QColor(PUBLISH_COLOR)
        if role is ITEM_DATA_ROLE:
            return itemData
        return None

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self.itemDataCls.display_attributes[section]["name"]
        return None

    def sort(self, column, order):
        self.layoutAboutToBeChanged.emit()
        self._data.sort(column, order)
        self.layoutChanged.emit()


class FileTreeView(QtWidgets.QTreeView):

    def __init__(self, parent):
        super().__init__(parent=parent)
        self.setSortingEnabled(True)
        self.setSelectionMode(QtWidgets.QTreeView.SingleSelection)
        self.setSelectionBehavior(QtWidgets.QTreeView.SelectRows)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def mousePressEvent(self, event):
        index = self.indexAt(event.pos())
        if not index.isValid():
            self.clearSelection()
            self.setCurrentIndex(QtCore.QModelIndex())
        super().mousePressEvent(event)


class FileTableWidget(QtWidgets.QWidget):
    """
    The entry point is just like defining any Qt widget and add it to a layout.
    Without needing to provide any arguments. We get an empty list. self._data=[]

    we will need a method to update the

    """
    treeModelCls = FileTreeModel
    treeViewCls = FileTreeView
    itemDataCls = BaseItemData
    DEFAULT_SEARCH_PATH = os.path.expanduser("~/Downloads")
    DISPLAY_OPEN_BUTTON = True
    DISPLAY_COPY_BUTTON = True
    DISPLAY_REFRESH_BUTTON = True
    refresh_sgn = QtCore.Signal()

    def __init__(self, demo_dir=False):
        super().__init__()
        self._demo_dir = demo_dir
        self.context_menu = QtWidgets.QMenu(self)

        self.init_ui()
        self.connect_signals()
        self.populate_ui()
        self.setup_actions()

    def init_ui(self):
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        # Header - File Path + buttons
        self.header_layout = QtWidgets.QHBoxLayout()

        self.filepath_wdg = QtWidgets.QLabel(self)
        self.header_layout.addWidget(self.filepath_wdg)

        # Copy Button
        copy_icon = qtawesome.icon('fa.paperclip')  # alternative icon 'fa.copy'
        self.copy_path_button = QtWidgets.QPushButton(icon=copy_icon)
        self.copy_path_button.setFlat(True)
        self.copy_path_button.setToolTip('Copy path to clipboard')
        self.header_layout.addWidget(self.copy_path_button) if self.DISPLAY_COPY_BUTTON else None

        # Open Button
        open_icon = qtawesome.icon('fa.folder-open-o')
        self.open_button = QtWidgets.QPushButton(icon=open_icon)
        self.open_button.setFlat(True)
        self.open_button.setToolTip('Open folder')
        self.header_layout.addWidget(self.open_button) if self.DISPLAY_OPEN_BUTTON else None

        # Refresh Button
        refresh_icon = qtawesome.icon('fa.refresh')
        self.refresh_button = QtWidgets.QPushButton(icon=refresh_icon)
        self.refresh_button.setFlat(True)
        self.refresh_button.setToolTip('Refresh files')
        self.header_layout.addWidget(self.refresh_button) if self.DISPLAY_REFRESH_BUTTON else None

        self.header_layout.setStretch(0, 1)
        self.header_layout.setStretch(1, 0)
        self.header_layout.setStretch(2, 0)

        layout.addLayout(self.header_layout)

        # Set up the model
        self.file_model = self.treeModelCls()

        # Set up the TreeView
        self.treeView = self.treeViewCls(self)
        self.treeView.setModel(self.file_model)
        self.treeView.header().setStretchLastSection(False)
        self.treeView.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.treeView.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.treeView.customContextMenuRequested.connect(self._show_context_menu)

        layout.addWidget(self.treeView)
        self.setLayout(layout)

    def populate_ui(self):
        if self._demo_dir:
            self.simple_populate_files(self.DEFAULT_SEARCH_PATH if self._demo_dir is True else self._demo_dir)

    def connect_signals(self):
        self.copy_path_button.clicked.connect(self._copy_to_clipboard)
        self.open_button.clicked.connect(self._open_folder)
        self.refresh_button.clicked.connect(lambda: self.refresh_sgn.emit())
        if self._demo_dir:
            self.refresh_sgn.connect(self._refresh_files)

    def setup_actions(self):
        self.global_actions: List[Tuple[str, Callable]] = [
            ('Copy Path', self._copy_path),
            ('Show in Folder', self._show_in_explorer),
        ]

        self.custom_actions: List[Tuple[str, Callable]] = []

        self.type_specific_actions = {
            ItemDataType.SCENE: [
                # ('Show outputs', self._open_in_sg)
                # ('Open in SG', self._open_in_sg)
            ]
        }

    def add_custom_action(self, label: str, function: callable):
        # fixme: add argument to specify List[ItemDataType] target. empty list 'apply to all'
        new_action = (label, function)
        if label not in [l for l, c in self.custom_actions]:
            self.custom_actions.append(new_action)

    def add_type_specific_action(self, label: str, function: callable, action_type:ItemDataType):
        # fixme: add argument to specify List[ItemDataType] target. empty list 'apply to all'
        new_action = (label, function)
        self.type_specific_actions[action_type].append(new_action)

    def add_action(self, label: str, function: callable):
        action = QtWidgets.QAction(label, self)
        action.triggered.connect(function)
        self.context_menu.addAction(action)

    def _build_context_menu(self, item):
        self.context_menu.clear()

        for label, function in self.global_actions:
            self.add_action(label, function)

        # Add type-specific actions
        if item.itemType in self.type_specific_actions:
            self.context_menu.addSeparator()
            for label, function in self.type_specific_actions[item.itemType]:
                self.add_action(label, function)

        # Add custom actions
        if len(self.custom_actions):
            self.context_menu.addSeparator()
            for label, function in self.custom_actions:
                self.add_action(label, function)

    def _show_context_menu(self, position):
        index = self.treeView.indexAt(position)
        if index.isValid():
            item = index.internalPointer()
            self._build_context_menu(item)
            self.context_menu.exec_(self.treeView.viewport().mapToGlobal(position))

    def _copy_path(self):
        index = self.treeView.currentIndex()
        if index.isValid():
            item = index.internalPointer()
            print(str(item.filePath))
            clipboard = QtWidgets.QApplication.clipboard()
            clipboard.setText(str(item.filePath))

    def _show_in_explorer(self):
        index = self.treeView.currentIndex()
        if index.isValid():
            item = index.internalPointer()
            print(item.filePath.parent)
            files.openFolder(str(item.filePath.parent))

    def _open_in_sg(self):
        # Get SG ID from item
        # Build page URL and open in browser
        # --check playlistdownloader._open_sg_page()
        raise NotImplementedError

    def _copy_to_clipboard(self):
        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.clear()
        clipboard.setText(self.filepath_wdg.text())

    def _open_folder(self):
        current_dir = self.filepath_wdg.text()
        files.openFolder(current_dir)

    def _refresh_files(self):
        current_dir = self.filepath_wdg.text()
        self.simple_populate_files(current_dir)

    def simple_populate_files(self, folder: Union[Path, str]):
        if not isinstance(folder, Path):
            folder = Path(folder)
        self.filepath_wdg.setText(folder.as_posix())
        if not folder.exists():
            self.file_model.beginResetModel()
            self.file_model._data = None
            self.file_model.endResetModel()
            return

        root_item = BaseItemData(filePath=folder, name='root',
                                 itemType=ItemDataType.ROOT)
        child_items = []
        for filename in os.listdir(root_item.filePath):
            c_path = Path(os.path.join(root_item.filePath, filename))
            p_item = root_item
            f_item = BaseItemData(filePath=c_path,
                                  itemType=ItemDataType.SCENE, parent=p_item)
            if f_item not in child_items:
                child_items.append(f_item)
        root_item.addChildren(child_items)
        # todo: move this to Model
        self.file_model.beginResetModel()
        self.file_model._data = root_item
        self.file_model.endResetModel()

    def populate_files(self, context: filePath.Context, ui_mode='work', show_outputs=False):
        # Wildcards to generate folder_dir
        dir_tokens = context.parameters.copy()
        dir_tokens['variant'] = '*'
        dir_tokens['version'] = '*'
        dir_tokens['descriptor'] = '*'
        dir_tokens['renderLayer'] = '*'
        dir_tokens['ext'] = '*'

        p_template = context.get_template(force_mode=publish.Mode.PUBLISH)
        search_templates = [p_template]

        if ui_mode == 'both':
            dir_tokens['mode'] = 'work'
            context.parameters['mode'] = 'work'
            w_template = context.get_template()
            search_templates = [p_template, w_template]
        elif ui_mode == 'work':
            w_template = context.get_template()
            search_templates = [w_template]

        teide = filePath.FilePath()
        s_filepath = teide.buildFilepath(template=search_templates[-1], tokenOverrides=dir_tokens, updateEnv=False,
                                         check_disk=False)
        folder = s_filepath.parent.as_posix()
        self.filepath_wdg.setText(folder)

        if not folder or not os.path.exists(str(folder)):
            self.file_model.beginResetModel()
            self.file_model._data = None
            self.file_model.endResetModel()
            return

        root_item = BaseItemData(filePath=Path(str(folder)), name='root',
                                 itemType=ItemDataType.ROOT)

        child_items = []
        for search_template in search_templates:
            w_versions = teide.getFilesFromTemplate(search_template, tokens=dir_tokens, extra_values={'ext': None})
            for filename in w_versions:
                c_path = Path(filename)
                p_item = root_item
                fields = search_template.parse(c_path.as_posix())
                scene_item = BaseItemData(filePath=c_path, isPublish='publish' in search_template.name,
                                          task=fields.get('task', ''), variant=fields.get('variant', ''),
                                          version=fields.get('version', ''),
                                          itemType=ItemDataType.SCENE, parent=p_item, tokens=fields)

                # get output items
                if show_outputs:
                    search_tokens = dir_tokens.copy()
                    search_tokens.update(fields)
                    area = publish.WorkArea.from_tokens(search_tokens)
                    mode = publish.Mode.PUBLISH if scene_item.isPublish else publish.Mode.WORK
                    output_items = area.get_output_items_by_type(mode=mode, existing_only=True,
                                                                 types=publish.OutputType.get_outputs())
                    for output_item in output_items:
                        o_filepath = output_item.dst if mode == publish.Mode.PUBLISH else output_item.src
                        if o_filepath == scene_item.filePath or not o_filepath.exists():
                            continue
                        output_item = BaseItemData(filePath=o_filepath, name=o_filepath.name, parent=scene_item,
                                                   isPublish=scene_item.isPublish, itemType=ItemDataType.OUTPUT,
                                                   display_type=output_item.output_type.value,
                                                   version=scene_item.version, variant=scene_item.variant)
                        scene_item.addChild(output_item)
                if scene_item not in child_items:
                    child_items.append(scene_item)
        root_item.addChildren(child_items)
        # todo: move this to Model
        self.file_model.beginResetModel()
        self.file_model._data = root_item
        self.file_model.endResetModel()


if __name__ == "__main__":
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)
    # qtawesome.dark(app)
    window = FileTableWidget(demo_dir=True)
    window.show()
    sys.exit(app.exec())
