""" Custom widget to show project tasks with filters and sorting.

A method can define what filters/sorting will be used by default, and what filters are displayed.

    _Tasks___________________________
    |        Filters # | Sort by  # |
    |_______________________________|
    |          [task.name]          |
    |          [task.name]          |
    |          [task.name]          |
    |          [task.name]          |
    |_______________________________|


## MVP
 - BaseItem class with task params/tokens
 - ListWidget showing BaseItems
 - Signal: change listItem -> populate Browser selectors
 - Default filters:
    - Assignee == current user
    - Task Status == [wtg, ip, fbg, rev, bypass]
    - DateRange == [today, today+2weeks]

# Next
 - Custom widgets to adjust filters and sorting
 - Add more filters and sorting options
 - make custom taskWdg instead of simple text item in listWidget.
    - show more details like: type, priority, status, step, name, dueDate ...
    - show bidTime vs loggedTime | dueDate vs currentDate
    - progress color coding: Green, Yellow, Orange, Red
 - Task time logging


## Procedure
 - get tasks from sg
 - initialize BaseItems
 -



"""
import datetime
from functools import cached_property
from typing import TYPE_CHECKING, Dict, List, Any, Optional

import qtawesome
from qtpy import QtWidgets, QtCore, QtGui

import teide_core.helpers
from teide_core.db import Connection
from teide_core import status
from teide_core import filePath
from teide_core import tasks
from teide_qt.widgets import wdg_dateRangeFilter
from teide_qt.widgets import wdg_filters, wdg_multiCombobox

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


DATE_FORMAT = "%Y-%m-%d"


# fixme: [NEXT] always ignore tasks.status = omt + Remove Omit status from the MultiCombobox
#  > This is needed because if user selects a task that is omitted, a error will be raised bc the task couldn't be
#  found in the selectors task combobox.
# done: bug with MultiCombobox, it shouldn't allow me to explore the combobox items while widget its disabled.
# done: disable dateRange filter by default
# todo: make the dateRange start on the monday of the current week instead of today.
#  ex: on friday I cant see these week's tasks
#  So essentially we are gonna show always this week's tasks + next week (+14 days)
#   Note for @Grisha:
#   We will need due_date dates to be used properly*. if due_date is Tuesday but its still getting feedback after
#   its due, a new due_date might be needed? --or will this destroy our data gathering (bid vs logged time)
#     properly*: In this case, properly means that an assigned task should always have a due_date (bid time too?)
# done: change task_label when task selection is changed.
# done: update tasks when filters popup is closed.
# done: add timer to make optimize search filter
# done: setup signal that gets emitted when a TaskItem is selected.
#  Has to carry the data that represents the Task. so we can populate the browser.selectors and the TaskDetailsWdg
#  The 'payload' of the signal should be a dictionary of tokens:
#   >> prName, entity_type, assettype, asset, episode, sequence, shot, step, task

# done: fuse ItemData() with tasks.BaseTask() keep the best of both --define in tasks.py
# done: add search functionality with QSortFilterProxyModel
# todo: Q: should the sorting be exposed? sort by due_date(default) | priority
# todo: make priority be used as a sort method instead of a filter (keep inside the config wdg)
# done: right click over list item to go see Sg page in browser
# done: make due_date_window adjust the due_date_wdg.end if necessary.


class CollapsibleGroupBox(QtWidgets.QGroupBox):
    def __init__(self, title: str, layout: QtWidgets.QLayout, parent: Optional[QtWidgets.QWidget] = None):
        super().__init__(title, parent)

        self.setObjectName("CollapsibleGroupBox")
        self.setCheckable(True)
        self.setChecked(False)

        self.toggled.connect(self.on_toggled)

        # Wrap the provided layout into a container widget
        self.content_widget = QtWidgets.QWidget(self)
        self.content_layout = layout
        self.content_layout.setContentsMargins(0, 0, 0, 0)
        self.content_widget.setLayout(self.content_layout)

        # Add the content widget to the main layout
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.addWidget(self.content_widget)
        self.setLayout(main_layout)

        self.setStyleSheet("""
            QGroupBox#CollapsibleGroupBox {
                border-top: 1px solid gray;
                margin-top: 0.5em;
                padding-top: 0.5em;
            }
            QGroupBox#CollapsibleGroupBox::title {
                subcontrol-origin: margin;
                left: 10px;
                padding: 0 3px 0 3px;
            }
        """)

        # Initialize the collapsed state
        self.on_toggled(False)

    def on_toggled(self, checked):
        """
        Show or hide the content widget based on the toggle state.
        """
        self.content_widget.setVisible(checked)
        self.updateGeometry()
        self.adjustSize()
        self.parent().updateGeometry()
        self.parent().adjustSize()


class TaskFilterWindow(QtWidgets.QWidget):
    filters_updated = QtCore.Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    def init_ui(self):
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
        self.setLayout(main_layout)

        self.filters_layout = QtWidgets.QFormLayout()
        self.filters_layout.setLabelAlignment(QtCore.Qt.AlignmentFlag.AlignRight)
        main_layout.addLayout(self.filters_layout)

        self.assignee_widget = wdg_multiCombobox.MultiSelectComboBox()
        self.filters_layout.addRow("Assigned to:", self.assignee_widget)

        self.due_date_wdg = wdg_dateRangeFilter.AdjustableDateRangeFilterWidget()
        self.due_date_wdg.enable_checkbox.setChecked(False)
        self.filters_layout.addRow("Due Date:", self.due_date_wdg)

        # Advanced section
        self.advanced_layout = QtWidgets.QFormLayout()
        self.advanced_layout.setLabelAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

        self.advanced_widget = CollapsibleGroupBox("Advanced", layout=self.advanced_layout, parent=self)
        main_layout.addWidget(self.advanced_widget)

        self.project_widget = QtWidgets.QComboBox(self)
        self.advanced_layout.addRow("Project:", self.project_widget)

        self.status_widget = wdg_multiCombobox.MultiSelectComboBox()
        self.advanced_layout.addRow("Status:", self.status_widget)

        self.tasks_widget = wdg_multiCombobox.MultiSelectComboBox()
        self.advanced_layout.addRow("Tasks:", self.tasks_widget)

        self.episodes_widget = wdg_multiCombobox.MultiSelectComboBox()
        self.advanced_layout.addRow("Episodes:", self.episodes_widget)

        self.reviewers_widget = wdg_multiCombobox.MultiSelectComboBox()
        self.advanced_layout.addRow("Reviewers:", self.reviewers_widget)

        self.priority_widget = wdg_multiCombobox.MultiSelectComboBox()
        self.advanced_layout.addRow("Priority:", self.priority_widget)

    def populate_ui(self):
        self.tasks_widget.setDisabled(True)
        self.reviewers_widget.setDisabled(True)
        self.priority_widget.setDisabled(True)

    def connect_signals(self): pass

    def hideEvent(self, event):
        # Emit the signal when the window is closed
        self.filters_updated.emit()
        event.accept()


class TaskFilterWidget(wdg_filters.FilterWidget):
    filter_window_class = TaskFilterWindow


class TasksModel(QtCore.QAbstractItemModel):
    def __init__(self, sg_tasks: List[Dict], parent=None):
        super().__init__(parent)
        self.items = [tasks.Task.from_db(task) for task in sg_tasks]

    def rowCount(self, parent: QtCore.QModelIndex = QtCore.QModelIndex()) -> int:
        return len(self.items)

    def columnCount(self, parent: QtCore.QModelIndex = QtCore.QModelIndex()) -> int:
        return 1

    def data(self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole) -> Any:
        if role == QtCore.Qt.ItemDataRole.DisplayRole and index.isValid():
            item = self.items[index.row()]
            return item.display_name
        return None

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = QtCore.QModelIndex()) -> QtCore.QModelIndex:
        if parent.isValid() or row < 0 or row >= self.rowCount():
            return QtCore.QModelIndex()
        return self.createIndex(row, column)

    def parent(self, index: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()  # No parent for top-level items

    def collect_tasks(self, connection: Connection,
                      f_project: Optional[int] = None,
                      f_status: List[str] = [],
                      f_assignee: List[int] = [],
                      f_episodes: List[int] = [],
                      f_date_range: Optional[wdg_dateRangeFilter.DateRange] = None):
        # This method should collect sg_tasks and create ItemData instances
        self.conn = connection

        filters = [
            ["project.Project.is_template", "is", False],
            ["project.Project.is_demo", "is", False],
            ['sg_status_list', 'is_not', 'omt'],
            ['entity.Asset.sg_status_list', 'is_not', 'omt'],
            ['entity.Episode.sg_status_list', 'is_not', 'omt'],
            ['entity.Sequence.sg_status_list', 'is_not', 'omt'],
            ['entity.Shot.sg_status_list', 'is_not', 'omt'],
        ]
        if f_project:
            filters += [['project', 'is', {'type': 'Project', 'id': f_project}]]
        if f_status:
            filters += [['sg_status_list', 'in', f_status]]
        if f_assignee:
            valid_users = [{'type': 'HumanUser', 'id': user_id} for user_id in f_assignee]
            filters += [['task_assignees', 'in', valid_users]]
        if f_date_range:
            formated_start = f_date_range.start.strftime(DATE_FORMAT)
            formated_end = f_date_range.end.strftime(DATE_FORMAT)
            filters += [['due_date', 'between', [formated_start, formated_end]]]

        episode_filters = []
        if f_episodes:
            valid_episodes = [{'type': 'Episode', 'id': ep_id} for ep_id in f_episodes]

            # Add the specific filters for Ep|Seq|Sh|Asset
            episode_filters.append(['entity.Asset.episodes', 'in', valid_episodes])
            episode_filters.append(['entity', 'in', valid_episodes])
            episode_filters.append(['entity.Sequence.episode', 'in', valid_episodes])
            episode_filters.append(['entity.Shot.sg_episode', 'in', valid_episodes])

        if episode_filters:
            filters.append({
                "filter_operator": "any",
                "filters": episode_filters
            })

        fields = ['content', 'project', 'entity', 'step', 'sg_status_list', 'due_date', 'task_assignees',
                  'entity.Asset.sg_asset_type', 'entity.Shot.sg_episode', 'entity.Shot.sg_sequence']
        order = [{'field_name': 'due_date', 'direction': 'asc'}]
        sg_tasks = self.conn.get_entities('Task', filters=filters, fields=fields, order=order)

        self.beginResetModel()
        self.items = [tasks.Task.from_db(task) for task in sg_tasks]
        self.endResetModel()


class TasksWidget(QtWidgets.QWidget):
    selection_changed_sgn = QtCore.Signal(filePath.Context)  # Update signal to carry Context

    def __init__(self, connection: Connection):
        super().__init__()
        self._conn = connection
        self.search_timer = QtCore.QTimer(self)
        self.search_timer.setSingleShot(True)
        self.search_timer.timeout.connect(self._apply_search_filter)

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    def init_ui(self):
        self.setFixedWidth(320)
        main_layout = QtWidgets.QVBoxLayout(self)

        # Filters
        row_layout = QtWidgets.QHBoxLayout()
        self.search_bar = QtWidgets.QLineEdit()
        # self.search_bar.setPlaceholderText("Search...")
        row_layout.addWidget(self.search_bar)

        self.filter_widget = TaskFilterWidget()
        row_layout.addWidget(self.filter_widget)

        self.reload_widget = QtWidgets.QPushButton(icon=qtawesome.icon('fa.refresh'))
        self.reload_widget.setToolTip("Reload tasks from SG.")
        row_layout.addWidget(self.reload_widget)
        main_layout.addLayout(row_layout)

        # List
        self.model = TasksModel([])
        self.proxy_model = QtCore.QSortFilterProxyModel(self)
        self.proxy_model.setSourceModel(self.model)
        self.proxy_model.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)

        self.list_view = QtWidgets.QListView(self)
        self.list_view.setModel(self.proxy_model)
        self.list_view.setContextMenuPolicy(QtCore.Qt.ContextMenuPolicy.CustomContextMenu)
        self.list_view.customContextMenuRequested.connect(self.show_context_menu)
        main_layout.addWidget(self.list_view)

    def populate_ui(self):
        # Date Range
        start_date = datetime.datetime.utcnow() + datetime.timedelta(hours=4)
        window_size = self.filter_widget.filter_wdg.due_date_wdg.range_adjuster.value()

        self.filter_widget.filter_wdg.due_date_wdg.set_date_range(start_date=start_date,
                                                                  end_date=start_date + datetime.timedelta(days=window_size))

        # Users
        sg_users = self._conn.get_users()
        connected_user = self._conn.get_connected_user()
        self.filter_widget.filter_wdg.assignee_widget.add_items(sg_users)
        if connected_user:
            self.filter_widget.filter_wdg.assignee_widget.check_item_by_name(connected_user['name'])
        self.filter_widget.filter_wdg.reviewers_widget.add_items(sg_users)

        # Projects
        sg_projects = self._conn.get_projects()
        [self.filter_widget.filter_wdg.project_widget.addItem(p['name'], p) for p in sg_projects]
        current_project = self._conn.current_project
        if current_project:
            index = self.filter_widget.filter_wdg.project_widget.findText(current_project['name'])
            if index:
                self.filter_widget.filter_wdg.project_widget.setCurrentIndex(index)
        self._update_episodes_from_project()

        # Status
        statuses = [{'name': s.name.capitalize(), 'data': s} for s in status.TaskStatuses]
        # fixme: make the MultiSelectComboBox() support given sort order, only sort by showing checked first.
        self.filter_widget.filter_wdg.status_widget.add_items(statuses)
        for default_status in status.OPEN_TASK_STATUSES:
            self.filter_widget.filter_wdg.status_widget.check_item_by_name(default_status.name.capitalize())

        self._filter_changed()

    def connect_signals(self):
        self.search_bar.textChanged.connect(self._search_filter)
        self.reload_widget.clicked.connect(self._filter_changed)

        # Filters
        self.filter_widget.filter_wdg.filters_updated.connect(self._filter_changed)

        # Selection changed
        self.list_view.selectionModel().selectionChanged.connect(self._emit_selection_changed_sgn)

        # Projects
        self.filter_widget.filter_wdg.project_widget.currentTextChanged.connect(self._update_episodes_from_project)

    @property
    def selected_index(self):
        selected_index = self.list_view.currentIndex()
        if not selected_index.isValid():
            return
        return self.proxy_model.mapToSource(selected_index)

    def _emit_selection_changed_sgn(self):
        item_data = self.model.items[self.selected_index.row()]
        context = filePath.Context.from_task(item_data)
        self.selection_changed_sgn.emit(context)

    def _search_filter(self, text):
        """Filter the items based on the search text."""
        self.search_timer.start(200)
        self.search_text = text
    
    def _apply_search_filter(self):
        self.list_view.clearSelection()
        self.proxy_model.setFilterWildcard(self.search_text)

    def _filter_changed(self):
        date_range = None
        if self.filter_widget.filter_wdg.due_date_wdg.checked:
            date_range = self.filter_widget.filter_wdg.due_date_wdg.get_date_range()

        checked_assignees = self.filter_widget.filter_wdg.assignee_widget.get_checked_items()
        if checked_assignees:
            checked_assignees = [i.data for i in checked_assignees]

        # Advanced
        checked_project = self.filter_widget.filter_wdg.project_widget.currentData().get('id', None) or None

        checked_episodes = self.filter_widget.filter_wdg.episodes_widget.get_checked_items()
        if checked_episodes:
            checked_episodes = [i.data for i in checked_episodes]

        # checked_reviewers = self.filter_widget.filter_wdg.reviewers_widget.get_checked_items()
        # if checked_reviewers:
        #     checked_reviewers = [i.data for i in checked_reviewers]

        checked_statuses = self.filter_widget.filter_wdg.status_widget.get_checked_items()
        if checked_statuses:
            checked_statuses = [i.data['data'] for i in checked_statuses]

        # query tasks
        self.model.collect_tasks(connection=self._conn,
                                 f_project=checked_project,
                                 f_assignee=[u['id'] for u in checked_assignees],
                                 f_episodes=[u['id'] for u in checked_episodes],
                                 f_status=[u.value for u in checked_statuses],
                                 f_date_range=date_range)
        self.update_search_placeholder()

    def _open_sg_page(self):
        item_data = self.model.items[self.selected_index.row()]
        sg_type = item_data._sg_data.get('type')
        sg_id = item_data.id
        if not sg_type or not sg_id:
            raise ValueError("Couldn't get inputs from selected index to open page in SG.")
        url = self._conn.get_sg_page_url(sg_type, sg_id)
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def _update_episodes_from_project(self):
        current_project = self.filter_widget.filter_wdg.project_widget.currentText()
        if not current_project:
            return None
        sg_episodes = self._conn.get_episodes_from_project(project=current_project)
        self.filter_widget.filter_wdg.episodes_widget.add_items(sg_episodes)

    def update_search_placeholder(self):
        """Update the search bar placeholder to show the total number of items."""
        total_items = self.model.rowCount()
        if not total_items:
            self.search_bar.setPlaceholderText("Search...")
            return
        self.search_bar.setPlaceholderText(f"Search...\t({total_items})")

    def show_context_menu(self, pos):
        """Show context menu on right-click."""
        context_menu = QtWidgets.QMenu(self)
        open_sg_action = context_menu.addAction("Open SG Page")
        action = context_menu.exec(self.list_view.mapToGlobal(pos))
        
        if action == open_sg_action:
            self._open_sg_page()


if __name__ == '__main__':
    from teide_qt import utils
    from teide_core import db

    app = QtWidgets.QApplication()
    utils.set_theme(app)

    conn = db.SGAuthentication.login()

    window = TasksWidget(conn)
    window.show()

    app.exec()
