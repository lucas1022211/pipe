import sys
from typing import TYPE_CHECKING

from qtpy import QtCore, QtGui, QtWidgets

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class TagEditor(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.lineEdit = QtWidgets.QLineEdit()
        self.lineEdit.returnPressed.connect(self.addTag)

        self.scrollArea = QtWidgets.QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)

        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSpacing(5)
        #self.gridLayout.setContentsMargins(5, 5, 5, 5)

        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setLayout(self.gridLayout)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.mainLayout = QtWidgets.QVBoxLayout()
        self.mainLayout.addWidget(self.lineEdit)
        self.mainLayout.addWidget(self.scrollArea)
        self.setLayout(self.mainLayout)

        self.row = 0
        self.col = 0

    def addTag(self):
        tagText = self.lineEdit.text().strip()
        if tagText:
            label = QtWidgets.QLabel(tagText)
            label.setStyleSheet("background-color: lightblue; padding: 5px;")
            label.setAlignment(QtCore.Qt.AlignCenter)
            label.setFixedWidth(label.sizeHint().width())
            self.gridLayout.addWidget(label)
            self.lineEdit.clear()

            self.col += 1
            if self.col == 4:
                self.row += 1
                self.col = 0


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    tagEditor = TagEditor()
    tagEditor.show()
    sys.exit(app.exec_())