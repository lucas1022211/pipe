import hashlib
import threading
import queue
import time
from pathlib import Path

from typing import Union, Optional, List

import cv2
import files
from qtpy import QtGui, QtCore, QtWidgets

QT_RESOURCES = files.get_parent_dir(__file__, level=2)

# fixme: replace cv2 library with ffmpeg and/or openimageio
# done: add a dict cache where the key is the Path.name and the value is the List[pixmap]
# todo: add a loading media animation while processing. (maybe have loading label like "loading... 2/4")
# todo: find ways to optimize as much as possible the pixmap generation, specially for video frame extraction
# note: seems like most of the speed issues were related to the networking speed of Triofox.
# todo: find way to queue media to be processed in the back while app launches as fast as possible.
#  queue the pixmaps just as the UI is being oppened, then I can not show the media until pixmaps are all generated.
#  first process the pixmap for the area.preview_media_item then the rest of the area.imageable_items. while the items
#  are being computed lets update a msg label with something like:   "loading... <loaded>/<total>"
# todo: also, while waiting for the first pixmap to be ready(area.preview_media_item), lets make sure the widget has the
#  right size using targetHeight & media_aspect_ratio. use defaults if needed. We could also have a empty image but
#  because of filesystem speed limitations, its gonna add some slowness. if we include a placeholder image in the
#  pipeline, it will be read locally.
# todo: Threading + Queue
#  - add_media(): adds media to queue, pixmaps get created and added to _cache_dict
#       // lets use a Path.hash for the _cache_dict keys instead of Path.name
#  - set_active_media():


def generate_path_hash(file_path):
    path = Path(file_path) if not isinstance(file_path, Path) else file_path
    path_string = path.as_posix().encode('utf-8')
    hash_obj = hashlib.sha256(path_string)
    return hash_obj.hexdigest()


class PixmapWorker(QtCore.QThread):
    pixmap_ready = QtCore.Signal(str, list)  # Signal to notify when pixmaps are ready
    all_jobs_done = QtCore.Signal()

    def __init__(self, pixmap_queue, parent=None):
        super().__init__(parent)
        self.pixmap_queue = pixmap_queue
        self.running = True

    def run(self):
        while self.running:
            try:
                media_file = self.pixmap_queue.get(timeout=1)  # Wait for a job
                if media_file:
                    media_file_hash = generate_path_hash(media_file)
                    pixmaps = self.process_media_item(media_file)
                    self.pixmap_ready.emit(media_file_hash, pixmaps)  # Emit signal

                # Check if the queue is now empty
                if self.pixmap_queue.empty():
                    self.all_jobs_done.emit()
            except queue.Empty:
                continue  # Keep looping if the queue is empty

    def stop(self):
        self.running = False

    def process_media_item(self, media_file):
        """Method with instruction on how to compute a job in the queue, stores the result in a dictionary"""
        media_file = Path(media_file)

        pixmaps = []
        if media_file.suffix.lower() in [".jpg", ".jpeg", ".png", ".bmp", ".gif"]:
            pixmaps = [QtGui.QPixmap(str(media_file))]
        elif media_file.suffix.lower() in [".mp4", ".mov", ".avi", ".mkv"]:
            cap = cv2.VideoCapture(str(media_file))
            if cap.isOpened():
                pixmaps = self._extract_frames(cap, 10)  # Example: Extract 10 frames
                cap.release()
        return pixmaps

    def _extract_frames(self, cap, num_frames=10):
        """Extract evenly spaced frames from the video."""
        frame_list = []
        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        step = max(total_frames // num_frames, 1)
        for i in range(0, total_frames, step):
            cap.set(cv2.CAP_PROP_POS_FRAMES, i)
            ret, frame = cap.read()
            if ret:
                frame_pixmap = self._convert_frame_to_pixmap(frame)
                aspect_ratio = frame_pixmap.width()/frame_pixmap.height()
                widget_size = [int(180 * aspect_ratio), 180]
                resized_pixmap = frame_pixmap.scaled(widget_size[0], widget_size[1], QtCore.Qt.AspectRatioMode.KeepAspectRatio, QtCore.Qt.TransformationMode.FastTransformation)
                frame_list.append(resized_pixmap)
        return frame_list

    def _convert_frame_to_pixmap(self, frame):
        """Convert an OpenCV frame to a QPixmap."""
        rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_frame.shape
        bytes_per_line = ch * w
        q_image = QtGui.QImage(rgb_frame.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        return QtGui.QPixmap.fromImage(q_image)


class MediaPreviewWidget(QtWidgets.QWidget):
    def __init__(self, height: Optional[int] = None, parent=None):
        super().__init__(parent)
        self.pixmap_queue = queue.Queue()
        self.pixmap_worker = PixmapWorker(self.pixmap_queue)
        self._recent_items_cache = {}  # {filename.hash: List[Pixmap]}
        self.scrubbing = False
        self.targetHeight = height or 120
        self.frame_list = []  # current List[pixmaps] to render in widget
        self.current_frame_index = 0
        self.media_aspect_ratio = 16 / 9
        self._total_media = None

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    def init_ui(self):
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)

        self.thumbnail_label = QtWidgets.QLabel(self)
        self.thumbnail_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.thumbnail_label.setScaledContents(True)
        self.layout().addWidget(self.thumbnail_label)

        # Hover logic
        self._hover_timer = QtCore.QTimer(self)
        self._hover_timer.setSingleShot(True)
        self.thumbnail_label.setMouseTracking(True)
        self.thumbnail_label.installEventFilter(self)

        self.loading_label = QtWidgets.QLabel(self)
        self.loading_label.setHidden(True)
        self.loading_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft | QtCore.Qt.AlignmentFlag.AlignBottom)
        self.loading_label.setFixedHeight(30)  # Set a smaller height for the loading label
        self.loading_label.setStyleSheet("color: white; background-color: rgba(0, 0, 0, 0.7); padding: 5px;")  # Adjusted background and padding
        self.layout().addWidget(self.loading_label)

    def populate_ui(self):
        self.thumbnail_label.setPixmap(self.placeholder_pixmap)

        self.pixmap_worker.pixmap_ready.connect(self.handle_pixmap_ready)
        self.pixmap_worker.start()

    def connect_signals(self):
        self._hover_timer.timeout.connect(self._start_scrubbing)

    @property
    def mid_frame_pixmap(self):
        return self.frame_list[int(len(self.frame_list)/2)] if self.frame_list else None

    @property
    def placeholder_pixmap(self):
        return self.resize_pixmap(Path(QT_RESOURCES) / "resources/emptyImage.png")

    def add_media(self, media_files: List[Union[Path, str]]):
        self._total_media = len(media_files)
        self.update_loading_label(len(self._recent_items_cache), self._total_media)
        for media_file in media_files:
            media_file = Path(media_file) if not isinstance(media_file, Path) else media_file
            # Check if in cache; add to queue if not queued already.
            media_file_hash = generate_path_hash(media_file)
            if media_file_hash not in self._recent_items_cache.keys():
                self.pixmap_queue.put(media_file)

    def handle_pixmap_ready(self, media_file_hash: str, pixmaps: list):
        self._recent_items_cache[media_file_hash] = pixmaps
        if len(self._recent_items_cache.keys()) == 1:
            self.frame_list = pixmaps
            self.set_active_item(self.mid_frame_pixmap)
        self.update_loading_label(len(self._recent_items_cache), self._total_media)
        QtWidgets.QApplication.processEvents()

    def set_active_media(self, media_file: Union[Path, str]):
        """given a media file, check if it exists, then get its pixmaps from the dictionary and populate the UI"""
        media_file = Path(media_file) if not isinstance(media_file, Path) else media_file

        # Check if in cache; add to queue if not queued already.
        media_file_hash = generate_path_hash(media_file)
        if media_file_hash in self._recent_items_cache.keys():
            self.frame_list = self._recent_items_cache[media_file_hash]
            self.set_active_item(self.mid_frame_pixmap)

    def set_active_item(self, pixmap):
        scaled_pixmap = self.resize_pixmap(pixmap)
        self.setFixedSize(scaled_pixmap.size())
        self.thumbnail_label.setFixedSize(scaled_pixmap.size())
        self.thumbnail_label.setPixmap(scaled_pixmap)

    def resize_pixmap(self, pixmap: Union[QtGui.QPixmap, Path]):
        pixmap = QtGui.QPixmap(str(pixmap)) if not isinstance(pixmap, QtGui.QPixmap) else pixmap
        self.media_aspect_ratio = (pixmap.width() / pixmap.height()) if pixmap.height() != 0 else 1.77
        self._adjust_widget_size()
        resized_pixmap = pixmap.scaled(self.width(), self.height(), QtCore.Qt.AspectRatioMode.KeepAspectRatio, QtCore.Qt.TransformationMode.FastTransformation)
        return resized_pixmap

    def _adjust_widget_size(self):
        """Adjust the widget size to match the media's aspect ratio based on target height."""
        parent_size = self.parent().size() if self.parent() else QtCore.QSize(320, 180)
        parent_width, parent_height = parent_size.width(), parent_size.height()

        new_width = int(self.targetHeight * self.media_aspect_ratio)
        # Ensure the new size does not exceed the parent dimensions
        if new_width > parent_width:
            new_width = parent_width
            self.targetHeight = int(new_width / self.media_aspect_ratio)

        new_size = QtCore.QSize(new_width, self.targetHeight)
        self.setFixedSize(new_size)
        self.thumbnail_label.setFixedSize(new_size)

    def eventFilter(self, source, event):
        if source == self.thumbnail_label:
            if event.type() == QtCore.QEvent.Enter:
                self._hover_timer.start(750)
            elif event.type() == QtCore.QEvent.Leave:
                self._hover_timer.stop()
                self.scrubbing = False
                self.set_active_item(self.mid_frame_pixmap or self.placeholder_pixmap)
            elif event.type() == QtCore.QEvent.MouseMove and self.scrubbing:
                if self.frame_list:
                    x = event.position().x()
                    frame_index = int(x / self.thumbnail_label.width() * len(self.frame_list))
                    frame_index = min(max(0, frame_index), len(self.frame_list) - 1)
                    if frame_index != self.current_frame_index:
                        self.current_frame_index = frame_index
                        self.thumbnail_label.setPixmap(self.frame_list[self.current_frame_index])
        return super().eventFilter(source, event)

    def _start_scrubbing(self):
        self.scrubbing = True

    def update_loading_label(self, loaded: int, total: int):
        """Update the loading label with the current progress."""
        self.loading_label.setText(f"loading... {loaded+1}/{total}")
        self.loading_label.setVisible(loaded < total)

    def closeEvent(self, event):
        self.pixmap_worker.stop()
        self.pixmap_worker.wait()
        super().closeEvent(event)


if __name__ == "__main__":
    app = QtWidgets.QApplication()

    preview_widget = MediaPreviewWidget(800)
    # preview_widget.add_media(r"X:\BM_Drive\projects\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.mp4")
    # preview_widget.add_media(r"X:\BM_Drive\projects\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.jpg")
    # preview_widget.add_media(r"X:\BM_Drive\projects\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.exr")
    files = [
        r"X:\BM_Drive\projects\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.png",
        r"X:\BM_Drive\projects\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.jpg",

    ]
    preview_widget.add_media(files)
    preview_widget.show()

    app.exec()
