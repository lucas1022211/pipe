import sys
from typing import TYPE_CHECKING, Optional, Tuple, Union
from datetime import datetime

from qtpy import QtCore, QtGui, QtWidgets

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class DateRange:
    def __init__(self, start: datetime, end: datetime):
        if start > end:
            raise ValueError("Start date must be before end date.")
        self.start = start
        self.end = end

    def contains(self, date: datetime) -> bool:
        """Check if a date is within the range."""
        return self.start <= date <= self.end

    def __str__(self):
        return f"From {self.start} to {self.end}"


class CustomDateEdit(QtWidgets.QDateEdit):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setDate(QtCore.QDate.currentDate())

    def wheelEvent(self, event):
        # Set the current section to the last one (day) before handling the wheel event
        self.setCurrentSection(QtWidgets.QDateEdit.Section.DaySection)
        super().wheelEvent(event)


class DateRangeFilterWidget(QtWidgets.QWidget):
    """
    A widget for selecting and filtering by a date range.

    This widget provides two date edit fields for selecting a start and end date.
    It emits a QtCore.Signal with the selected date range whenever either date is changed.

    Attributes
    ----------
    dateRangeChanged : QtCore.Signal
        Signal emitted when the date range is changed.
        Emits two QtCore.QDate objects representing the start and end dates.
    """

    dateRangeChanged = QtCore.Signal(QtCore.QDate, QtCore.QDate)

    DATE_FORMAT = "yyyy/MM/dd"

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None) -> None:
        """
        Initialize the DateRangeFilterWidget.

        Parameters
        ----------
        parent : Optional[QtWidgets.QWidget], optional
            The parent widget, by default None
        """
        super().__init__(parent)
        self.init_ui()
        self.connect_signals()
        self.populate_ui()

    def init_ui(self) -> None:
        """Initialize the user interface."""
        layout = QtWidgets.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        from_label = QtWidgets.QLabel(text="From:")
        self.start_date = CustomDateEdit(parent=self)
        self.start_date.setDisplayFormat(self.DATE_FORMAT)
        self.start_date.setCalendarPopup(True)
        self.start_date.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        layout.addWidget(self.start_date)

        to_label = QtWidgets.QLabel(text="To:")
        self.end_date = CustomDateEdit(parent=self)
        self.end_date.setDisplayFormat(self.DATE_FORMAT)
        self.end_date.setCalendarPopup(True)
        self.end_date.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        layout.addWidget(self.end_date)

    def populate_ui(self):
        """Populate the UI with default values."""
        end_date = QtCore.QDate.currentDate()
        start_date = end_date.addDays(-14)
        self.set_date_range(start_date, end_date)

    def connect_signals(self):
        # Validate value
        self.start_date.dateChanged.connect(self.validate_start_date)
        self.end_date.dateChanged.connect(self.validate_end_date)

        # Signal for debugging final values
        self.start_date.dateChanged.connect(lambda: self.dateRangeChanged.emit(self.start_date.date(), self.end_date.date()))
        self.end_date.dateChanged.connect(lambda: self.dateRangeChanged.emit(self.start_date.date(), self.end_date.date()))

    def validate_start_date(self):
        """Validate the start date and adjust the end date if necessary."""
        start_date = self.start_date.date()
        end_date = self.end_date.date()

        if start_date >= end_date:
            _logger.debug('Adjusting end date to be range days after start date.')
            new_end_date = start_date.addDays(1)
            self.set_date_range(start_date, new_end_date)

    def validate_end_date(self):
        """Validate the end date and adjust the start date if necessary."""
        start_date = self.start_date.date()
        end_date = self.end_date.date()

        if end_date <= start_date:
            _logger.debug('Adjusting start date to be range days before end date.')
            new_start_date = end_date.addDays(-1)  # Push start back by range value
            self.set_date_range(new_start_date, end_date)

    def set_date_range(self,
                       start_date: Union[QtCore.QDate, datetime],
                       end_date: Union[QtCore.QDate, datetime]) -> None:
        """Set the date range for the widget."""
        if isinstance(start_date, datetime) and isinstance(end_date, datetime):
            start_date = QtCore.QDate(start_date.year, start_date.month, start_date.day)
            end_date = QtCore.QDate(end_date.year, end_date.month, end_date.day)

        self.start_date.setDate(start_date)
        self.end_date.setDate(end_date)

    def get_date_range(self) -> DateRange:
        """Get the currently selected date range as a DateRange instance."""
        q_start_date = self.start_date.date()
        q_end_date = self.end_date.date()

        start_date = datetime(q_start_date.year(), q_start_date.month(), q_start_date.day())
        end_date = datetime(q_end_date.year(), q_end_date.month(), q_end_date.day())
        return DateRange(start_date, end_date)

    def set_range_days(self, days: int) -> None:
        """Set the date range to the last N days."""
        end_date = QtCore.QDate.currentDate()
        start_date = end_date.addDays(-days + 1)
        self.set_date_range(start_date, end_date)

    def set_range_this_week(self) -> None:
        """Set the date range to the current week."""
        today = QtCore.QDate.currentDate()
        start_of_week = today.addDays(-today.dayOfWeek() + 1)
        self.set_date_range(start_of_week, today)

    def set_range_this_month(self) -> None:
        """Set the date range to the current month."""
        today = QtCore.QDate.currentDate()
        start_of_month = QtCore.QDate(today.year(), today.month(), 1)
        self.set_date_range(start_of_month, today)

    def set_range_this_year(self) -> None:
        """Set the date range to the current year."""
        today = QtCore.QDate.currentDate()
        start_of_year = QtCore.QDate(today.year(), 1, 1)
        self.set_date_range(start_of_year, today)


class AdjustableDateRangeFilterWidget(DateRangeFilterWidget):
    """
    A widget for selecting and filtering by a date range with adjustable range and enable/disable functionality.
    """

    def init_ui(self) -> None:
        """Initialize the adjustable user interface."""
        super().init_ui()
        layout = self.layout()
        layout.setDirection(QtWidgets.QBoxLayout.TopToBottom)
        layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignHorizontal_Mask.AlignLeft)
        main_layout = QtWidgets.QHBoxLayout()

        # Add checkbox to enable/disable the filter
        self.enable_checkbox = QtWidgets.QCheckBox(self)
        main_layout.addWidget(self.enable_checkbox)

        # Add the date pickers to new hLayout
        main_layout.addWidget(self.start_date)
        main_layout.addWidget(self.end_date)
        layout.addLayout(main_layout)

        # Create a horizontal layout for the range label and adjuster
        range_layout = QtWidgets.QHBoxLayout()
        range_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
        self.range_label = QtWidgets.QLabel(self, text="Range:")
        self.range_adjuster = QtWidgets.QSpinBox(self)
        self.range_adjuster.setMinimum(1)
        self.range_adjuster.setToolTip("Number of days between Start and End date.")
        self.range_label.setToolTip(self.range_adjuster.toolTip())

        range_layout.addWidget(self.range_label)
        range_layout.addWidget(self.range_adjuster)
        layout.addLayout(range_layout)

    def populate_ui(self):
        super().populate_ui()
        self.enable_checkbox.setChecked(True)
        self.range_adjuster.setValue(14)

    def connect_signals(self):
        super().connect_signals()
        self.enable_checkbox.stateChanged.connect(self.toggle_filter)
        self.range_adjuster.valueChanged.connect(self.update_end_date)

        # Connect the dateChanged signal to the appropriate validation methods
        self.start_date.dateChanged.connect(self.validate_start_date)
        self.end_date.dateChanged.connect(self.validate_end_date)

    def toggle_filter(self, state: int) -> None:
        """Enable or disable the date range filter based on checkbox state."""
        self.start_date.setEnabled(self.checked)
        self.end_date.setEnabled(self.checked)
        self.range_adjuster.setEnabled(self.checked)
        self.range_label.setEnabled(self.checked)
        self.range_adjuster.setVisible(self.checked)
        self.range_label.setVisible(self.checked)

        if not self.checked:
            self.start_date.clear()
            self.end_date.clear()

        self.start_date.setDisplayFormat(self.DATE_FORMAT)
        self.end_date.setDisplayFormat(self.DATE_FORMAT)

    def update_end_date(self) -> None:
        """Update the end date based on the start date and the adjusted range."""
        if self.checked:
            start_date = self.start_date.date()
            range_days = self.range_adjuster.value()
            new_end_date = start_date.addDays(range_days - 1)
            self.set_date_range(start_date, new_end_date)

    def validate_start_date(self):
        """Validate the start date and adjust the end date if necessary."""
        start_date = self.start_date.date()
        end_date = self.end_date.date()
        range_days = self.range_adjuster.value()

        if start_date >= end_date:
            _logger.debug('Adjusting end date to be range days after start date.')
            new_end_date = start_date.addDays(range_days - 1)
            self.set_date_range(start_date, new_end_date)

    def validate_end_date(self):
        """Validate the end date and adjust the start date if necessary."""
        start_date = self.start_date.date()
        end_date = self.end_date.date()
        range_days = self.range_adjuster.value()

        if end_date <= start_date:
            _logger.debug('Adjusting start date to be range days before end date.')
            new_start_date = end_date.addDays(-range_days + 1)  # Push start back by range value
            self.set_date_range(new_start_date, end_date)

    @property
    def checked(self) -> bool:
        """Return whether the date range filter is enabled."""
        return self.enable_checkbox.isChecked()


if __name__ == '__main__':

    def on_date_range_changed(start_date: QtCore.QDate, end_date: QtCore.QDate) -> None:
        """
        Handle changes in the selected date range.

        Parameters
        ----------
        start_date : QtCore.QDate
            The start date of the selected range.
        end_date : QtCore.QDate
            The end date of the selected range.
        """
        date_format = "yyyy/MM/dd"
        print(f"Date range changed: {start_date.toString(date_format)} to {end_date.toString(date_format)}")

    import sys
    from teide_qt import utils

    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)
    
    widget = DateRangeFilterWidget()
    widgetB = AdjustableDateRangeFilterWidget()
    widget.show()
    widgetB.show()

    widget.dateRangeChanged.connect(on_date_range_changed)

    sys.exit(app.exec())
