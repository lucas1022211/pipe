"""
This widget could interact with a Context object that is stored locally.
If you close an app we store the current context of the app in its local dir so that when you open again, the context
will be the last you had.

## Selectors behaviour
- When closing the UI, we store the selections into a tmp file in ~/.teide/
- When Opening the UI we try to recover that and open where it was
- If nothing is found, we start a fresh procedure.
    - gather list of projects
    - select first project (or any project based on any rule)
    - gather project data (might be slow to gather all data, for performance we might consider different strategies)
        - A: gather either assets/shots mode, build data_tree for all entities
        - B: gather both asset_types/Episode entities. gather-as-you-go approach. --faster startup, slower use.
            --might be valuable if we reuse saved previous state.
    - When a field is updated the next field is populated/selected ond so on...
        (populated/selected depending on the approach)

"""
import time
import sys
import platform
from typing import TYPE_CHECKING, Optional, Dict, Any

import qtawesome
from qtpy import QtCore, QtGui, QtWidgets

import teide_core.helpers
from teide_core import filePath
from teide_core import package
from teide_core.db import Connection
from teide_core.project import SgProjectDataFactory, ProjectData

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)
IGNORE_COLOR_UI = False


# todo: add a method to compute the mode derived from the active child_wdg of modes_wdg.

def text_to_color(text, hue_min=0, hue_max=1, saturation=0.3, lightness=0.5, hue_shift=42):
    import colorsys
    import hashlib
    # Create a hash object
    hash_object = hashlib.md5(text.encode())
    hash_digest = hash_object.hexdigest()
    hash_int = int(hash_digest[:6], 16)

    # Normalize hash to 0-1 times hue shift
    hue = (hash_int % 1000) / 1000 * hue_shift

    # Scale hue to be within the hue_min and hue_max after applying the shift
    hue = hue_min + (hue_max - hue_min) * hue

    # Convert HSL to RGB
    r, g, b = colorsys.hls_to_rgb(hue, lightness, saturation)

    # Convert RGB from 0-1 to 0-255
    r, g, b = int(r * 255), int(g * 255), int(b * 255)

    # Format as a hex color
    return f'#{r:02x}{g:02x}{b:02x}'


class RefreshButton(QtWidgets.QPushButton):
    """
    Button with right-click actions to configure the refresh target.

    Refresh:  Current Project | Current mode | Current Entity

    Refresh current project:
        reloads the full project from SG similar to reopening the app and selecting the project again.
    Refresh current mode:
        gets the current mode(asset, ep, seq, sh) and reloads all data from SG of those entities (and their children)
    Refresh current entity:
        gets the current entity(field before Step) and reloads the full entity such as steps, tasks

    note: for now lets just leave it as refresh current Project. let's do some optimizations and re-evaluate if needed.
    """
    refresh_all_sgn = QtCore.Signal()
    refresh_current_sgn = QtCore.Signal()

    def __init__(self, parent=None):
        super().__init__("", parent)
        # self.setFixedSize(50, 50)  # Optional: Set a fixed size for the button if needed
        self.refresh_all_icon = qtawesome.icon('fa.refresh')
        self.refresh_current_icon = qtawesome.icon('fa.refresh', color='orange')

        self.setIcon(self.refresh_all_icon)
        self.setToolTip('Reload project data.')

    # def mousePressEvent(self, event: QtGui.QMouseEvent):
    #     if event.button() == QtCore.Qt.RightButton:
    #         self.showContextMenu(event.pos())
    #     else:
    #         super().mousePressEvent(event)

    def showContextMenu(self, position):
        # Create the context menu
        context_menu = QtWidgets.QMenu(self)

        # Create actions
        refresh_all_action = QtWidgets.QAction("Refresh Project", self)
        refresh_current_action = QtWidgets.QAction("Refresh Current Page", self)

        # Connect the actions to appropriate functions
        refresh_all_action.triggered.connect(lambda: self.refresh_all_icon.emit())
        refresh_all_action.triggered.connect(self._refresh_all_icon)
        refresh_current_action.triggered.connect(lambda: self.refresh_current_sgn.emit())
        refresh_current_action.triggered.connect(self._refresh_current_icon)

        # Add actions to the context menu
        context_menu.addAction(refresh_all_action)
        context_menu.addAction(refresh_current_action)

        # Show the context menu at the position of the right-click
        context_menu.exec(self.mapToGlobal(position))

    def _refresh_all_icon(self):
        self.setIcon(self.refresh_all_icon)

    def _refresh_current_icon(self):
        self.setIcon(self.refresh_current_icon)


class HorizontalScrollArea(QtWidgets.QScrollArea):
    def __init__(self):
        super().__init__()
        self.setContentsMargins(0, 0, 0, 0)
        self.setFrameStyle(QtWidgets.QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)

    def wheelEvent(self, event):
        # Invert horizontal scrolling
        delta = event.angleDelta().y()
        scroll_bar = self.horizontalScrollBar()
        scroll_bar.setValue(scroll_bar.value() - delta)


class EntitySelectors(QtWidgets.QWidget):
    field_updated_sgn = QtCore.Signal()
    initial_params_sgn = QtCore.Signal()

    DEFAULT_PROJECT = 'Select Project'
    ALL_ITEMS = 'All'

    project_tag = 'prName'
    entity_type_tag = 'entity_type'
    assettype_tag = 'assettype'
    asset_tag = 'asset'
    episode_tag = 'episode'
    sequence_tag = 'sequence'
    shot_tag = 'shot'
    step_tag = 'step'
    task_tag = 'task'

    ep_fields = [episode_tag]
    seq_fields = [episode_tag, sequence_tag]
    sh_fields = [episode_tag, sequence_tag, shot_tag]
    asset_fields = [assettype_tag, asset_tag]

    base_fields = [project_tag, entity_type_tag, step_tag, task_tag]
    display_fields = base_fields
    entity_types = ['Asset', 'Episode', 'Sequence', 'Shot']

    default_project = 'Select Project'
    default_entity_type = asset_tag

    fields = {
        project_tag: ('Project', QtWidgets.QComboBox, []),
        entity_type_tag: ('Mode', QtWidgets.QComboBox, entity_types),
        assettype_tag: ('Asset Type', QtWidgets.QComboBox, []),
        asset_tag: ('Asset', QtWidgets.QComboBox, []),
        episode_tag: ('Episode', QtWidgets.QComboBox, []),
        sequence_tag: ('Sequence', QtWidgets.QComboBox, []),
        shot_tag: ('Shot', QtWidgets.QComboBox, []),
        step_tag: ('Step', QtWidgets.QComboBox, []),
        task_tag: ('Task', QtWidgets.QComboBox, []),
    }

    def __init__(self, connection: Optional[Connection] = None, context: Optional[filePath.Context] = None,
                 parent=None):
        super().__init__(parent=parent)
        self._conn = connection
        self._context = context
        self._data = None
        
        self.init_ui()
        self.connect_signals()
        self.populate_ui()

    def init_ui(self):
        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setAlignment(QtCore.Qt.AlignLeft)

        for field, (label_text, widget_class, items) in self.fields.items():
            field_layout = QtWidgets.QHBoxLayout()
            field_layout.setAlignment(QtCore.Qt.AlignTop)

            label = QtWidgets.QLabel(label_text)
            widget = widget_class()
            if isinstance(widget, QtWidgets.QComboBox):
                widget.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)

            # Add projects
            if field == self.project_tag:
                projects = [self.default_project]
                projects += self.getProjects()
                widget.addItems(projects)

            # Add entity-types
            if field == self.entity_type_tag:
                widget.addItems(self.entity_types)
                widget.setEnabled(False)

            if field not in self.base_fields:
                label.setVisible(False)
                widget.setVisible(False)

            field_layout.addWidget(label)
            field_layout.addWidget(widget)
            layout.addLayout(field_layout)

            setattr(self, f'label_{field}', label)
            setattr(self, f'widget_{field}', widget)

        spacer = QtWidgets.QSpacerItem(0, 0, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        layout.addItem(spacer)

        self.refresh_button = RefreshButton()
        self.refresh_button.setEnabled(False)
        layout.addWidget(self.refresh_button)

    def populate_ui(self):
        pass

    def connect_signals(self):
        for field in self.fields:
            widget = self.get_widget(field)
            if field == self.project_tag:
                widget.currentTextChanged.connect(self.refreshProject)
                widget.currentTextChanged.connect(self._project_changed)
            elif field == self.entity_type_tag:
                widget.currentTextChanged.connect(self._entity_type_changed)
            elif field == self.assettype_tag:
                widget.currentTextChanged.connect(self._asset_type_changed)
            elif field == self.asset_tag:
                widget.currentTextChanged.connect(self._asset_changed)
            elif field == self.episode_tag:
                widget.currentTextChanged.connect(self._episode_changed)
            elif field == self.sequence_tag:
                widget.currentTextChanged.connect(self._sequence_changed)
            elif field == self.shot_tag:
                widget.currentTextChanged.connect(self._shot_changed)
            elif field == self.step_tag:
                widget.currentTextChanged.connect(self._step_changed)
            elif field == self.task_tag:
                widget.currentTextChanged.connect(self._task_changed)
            # widget.currentTextChanged.connect(lambda: self.field_updated_sgn.emit())

        self.field_updated_sgn.connect(self._context_changed)
        self.initial_params_sgn.connect(self._enable_secondary_params)
        self.refresh_button.clicked.connect(self._reload_current_project)

    @property
    def entity_type(self) -> str:
        entity_type_wdg = self.get_widget(field=self.entity_type_tag)
        return entity_type_wdg.currentText().lower()

    @property
    def context(self):
        tokens = {}
        active_values = self.get_current_values()
        for field, value in active_values.items():
            tokens[field] = value or None

        self._context = filePath.Context(tokens)
        return self._context

    def getProjects(self):
        """ Populates the empty projects to the stored self._data. Then retuns a list of sorted project names"""
        self._data = self.gatherData(projName=None, traverseProj=False)
        if not self._data:
            return []
        return sorted(list(self._data.keys()), key=lambda s: s.lower())

    def gatherData(self, data=None, projName=None, traverseProj=False) -> Dict[str, ProjectData]:
        if not data:
            data = {}

        if not projName:
            active_projects = self._conn.get_projects()
            foundProjects = [p['name'] for p in active_projects]
        else:
            foundProjects = [projName]

        for iterProj in foundProjects:
            if not traverseProj:
                if iterProj not in data.keys():
                    data[iterProj] = []
                continue
            data[iterProj] = SgProjectDataFactory.from_sg(self._conn, iterProj)
        return data

    def _reload_current_project(self):
        self.getProjectData(refresh=True, force=True)
        self._project_changed()

    def _enable_secondary_params(self):
        entity_type_wdg = self.get_widget(field=self.entity_type_tag)
        entity_type_wdg.setEnabled(True)
        self.refresh_button.setEnabled(True)

    def refreshProject(self):
        self.getProjectData(refresh=True)
        self.initial_params_sgn.emit()
        project_wdg = self.get_widget(field=self.project_tag)
        project_wdg.removeItem(project_wdg.findText(self.default_project))
        self._entity_type_changed()

    def getProjectData(self, refresh=False, force=False):
        if not self._data:
            return []

        current_project = self.get_value(self.project_tag)
        if refresh:
            if (current_project in self._data.keys() and not self._data[current_project]) or force:
                start_time = time.time()
                self._data = self.gatherData(data=self._data,
                                             projName=current_project,
                                             traverseProj=True)
                elapsed_time = time.time() - start_time
                _logger.debug('Project %s updated in %s seconds.' % (current_project, elapsed_time))
        if not self._data:
            return []
        pData = self._data[current_project]
        return pData

    def _project_changed(self):
        # Populate asset_types and episodes
        pData = self.getProjectData()
        if not pData:
            self.field_updated_sgn.emit()
            return
        self._entity_type_changed()

    def _entity_type_changed(self):
        # Has to manage QtWdg visibility
        # Has to trigger update on next display_field
        self.display_fields = self.base_fields

        if self.entity_type == self.episode_tag:
            self.display_fields = self.base_fields + self.ep_fields
        elif self.entity_type == self.sequence_tag:
            self.display_fields = self.base_fields + self.seq_fields
        elif self.entity_type == self.shot_tag:
            self.display_fields = self.base_fields + self.sh_fields
        elif self.entity_type == self.asset_tag:
            self.display_fields = self.base_fields + self.asset_fields
        else:
            raise ValueError(f'No display fields found for {self.entity_type}')

        for field in self.fields:
            visible = field in self.display_fields
            self.get_widget(field, name='label').setVisible(visible)
            self.get_widget(field).setVisible(visible)

        # Trigger next field
        asset_types_wdg = self.get_widget(self.assettype_tag)
        episodes_wdg = self.get_widget(self.episode_tag)

        asset_types_wdg.clear()
        episodes_wdg.clear()

        # Populate asset_types and episodes
        pData = self.getProjectData()
        if not pData:
            self.field_updated_sgn.emit()
            return
        asset_types = [k for k in pData.get_asset_types().keys()]
        episodes_names = [k for k in pData.get_episodes().keys()]

        if self.entity_type == self.asset_tag:
            asset_types_wdg.addItems(sorted(asset_types, key=lambda s: s.lower()))
        else:
            episodes_wdg.addItems(sorted(episodes_names, key=lambda s: s.lower()))

    def _asset_type_changed(self):
        current_project = self.get_value(self.project_tag)
        current_asset_type = self.get_value(self.assettype_tag)
        assets_wdg = self.get_widget(self.asset_tag)

        assets_wdg.clear()
        if not current_asset_type:
            return
        assets = self._data[current_project].get_assets(current_asset_type).keys()
        assets_wdg.addItems(sorted(assets, key=lambda s: s.lower()))

    def _asset_changed(self):
        current_project = self.get_value(self.project_tag)
        current_asset_type = self.get_value(self.assettype_tag)
        current_asset = self.get_value(self.asset_tag)
        steps_wdg = self.get_widget(self.step_tag)

        steps_wdg.clear()
        if not current_asset_type or not current_asset:
            return
        asset_entity = self.get_entity(entity_type=self.entity_type, entity_name=current_asset)
        if not asset_entity:
            return
        steps = self._data[current_project].get_entity_steps(asset_entity).keys()
        if not steps:
            sg_tasks = self._conn.get_tasks(current_project, asset_entity[self._data[current_project].tag_sg])
            steps = self._data[current_project].fetch_steps_tasks(asset_entity, sg_tasks)
        steps_wdg.addItems(steps)

    def _episode_changed(self):
        current_project = self.get_value(self.project_tag)
        current_episode = self.get_value(self.episode_tag)
        sequence_wdg = self.get_widget(self.sequence_tag)

        if self.entity_type == self.episode_tag:
            steps_wdg = self.get_widget(self.step_tag)
            ep_entity = self.get_entity(entity_type=self.entity_type, entity_name=current_episode)
            if not ep_entity:
                return
            steps_wdg.clear()
            steps = self._data[current_project].get_entity_steps(ep_entity).keys()
            if not steps:
                sg_tasks = self._conn.get_tasks(current_project, ep_entity[self._data[current_project].tag_sg])
                steps = self._data[current_project].fetch_steps_tasks(ep_entity, sg_tasks)
            steps_wdg.addItems(steps)
            return

        sequence_wdg.clear()
        if not current_episode:
            return
        sequences = self._data[current_project].get_sequences(current_episode).keys()
        sequence_wdg.addItems(sorted(sequences, key=lambda s: s.lower()))

    def _sequence_changed(self):
        current_project = self.get_value(self.project_tag)
        current_episode = self.get_value(self.episode_tag)
        current_sequence = self.get_value(self.sequence_tag)
        shot_wdg = self.get_widget(self.shot_tag)

        if self.entity_type == self.sequence_tag:
            steps_wdg = self.get_widget(self.step_tag)
            seq_entity = self.get_entity(entity_type=self.entity_type, entity_name=current_sequence)
            if not seq_entity:
                return
            steps_wdg.clear()
            steps = self._data[current_project].get_entity_steps(seq_entity).keys()
            if not steps:
                sg_tasks = self._conn.get_tasks(current_project, seq_entity[self._data[current_project].tag_sg])
                steps = self._data[current_project].fetch_steps_tasks(seq_entity, sg_tasks)
            steps_wdg.addItems(steps)
            return

        shot_wdg.clear()
        if not current_episode or not current_sequence:
            return
        shots = self._data[current_project].get_shots(current_episode, current_sequence).keys()
        shot_wdg.addItems(sorted(shots, key=lambda s: s.lower()))

    def _shot_changed(self):
        current_project = self.get_value(self.project_tag)
        current_episode = self.get_value(self.episode_tag)
        current_sequence = self.get_value(self.sequence_tag)
        current_shot = self.get_value(self.shot_tag)
        steps_wdg = self.get_widget(self.step_tag)

        steps_wdg.clear()
        if not current_episode or not current_sequence or not current_shot:
            return
        shot_entity = self.get_entity(entity_type=self.entity_type, entity_name=current_shot)
        if not shot_entity:
            return
        steps = self._data[current_project].get_entity_steps(shot_entity).keys()
        if not steps:
            sg_tasks = self._conn.get_tasks(current_project, shot_entity[self._data[current_project].tag_sg])
            steps = self._data[current_project].fetch_steps_tasks(shot_entity, sg_tasks)
        steps_wdg.addItems(steps)

    def _step_changed(self):
        # must populate step tasks
        current_project = self.get_value(self.project_tag)
        current_asset_type = self.get_value(self.assettype_tag)
        current_asset = self.get_value(self.asset_tag)
        current_episode = self.get_value(self.episode_tag)
        current_sequence = self.get_value(self.sequence_tag)
        current_shot = self.get_value(self.shot_tag)
        current_step = self.get_value(self.step_tag)
        tasks_wdg = self.get_widget(self.task_tag)

        tasks_wdg.clear()
        if self.entity_type == self.asset_tag:
            if not current_asset_type or not current_asset:
                return
            current_entity = current_asset
        elif self.entity_type == self.episode_tag:
            if not current_episode:
                return
            current_entity = current_episode
        elif self.entity_type == self.sequence_tag:
            if not current_episode or not current_sequence:
                return
            current_entity = current_sequence
        elif self.entity_type == self.shot_tag:
            if not current_episode or not current_sequence or not current_shot:
                return
            current_entity = current_shot
        else:
            return
        entity = self.get_entity(entity_type=self.entity_type, entity_name=current_entity)
        if not entity:
            return
        tasks = self._data[current_project].get_entity_tasks(entity, current_step).keys()
        tasks_wdg.addItems(tasks)

    def _task_changed(self):
        # must trigger file collection/UI refresh
        self.field_updated_sgn.emit()
        pass

    def _context_changed(self):
        # print(f'Context changed... {self.context}')
        return self.context

    def get_entity(self, entity_type: str, entity_name: str) -> Optional[Dict[str, Any]]:
        current_project = self.get_value(self.project_tag)
        current_asset_type = self.get_value(self.assettype_tag)
        current_episode = self.get_value(self.episode_tag)
        current_sequence = self.get_value(self.sequence_tag)

        project_data = self.getProjectData()

        if entity_type == self.asset_tag:
            return project_data.tree_asset_types[current_asset_type][entity_name]

        elif entity_type == self.episode_tag:
            if entity_name not in project_data.tree_episodes.keys():
                return
            return project_data.tree_episodes[entity_name]

        elif entity_type == self.sequence_tag:
            if current_episode not in project_data.tree_episodes.keys():
                return
            elif entity_name not in project_data.tree_episodes[current_episode][project_data.tag_seq].keys():
                return
            return project_data.tree_episodes[current_episode][project_data.tag_seq][entity_name]

        elif entity_type == self.shot_tag:
            if current_episode not in project_data.tree_episodes.keys():
                return
            elif current_sequence not in project_data.tree_episodes[current_episode][project_data.tag_seq].keys():
                return
            elif entity_name not in project_data.tree_episodes[current_episode][project_data.tag_seq][current_sequence][project_data.tag_sh].keys():
                return
            return project_data.tree_episodes[current_episode][project_data.tag_seq][current_sequence][project_data.tag_sh][entity_name]
        else:
            raise AttributeError(f'Entity not found with entity-type:{entity_type} and entity-name:{entity_name}')

    def get_widget(self, field, name: str = 'widget') -> QtWidgets.QWidget:
        return getattr(self, f'{name}_{field}')

    def get_value(self, field):
        widget = self.get_widget(field)
        if isinstance(widget, QtWidgets.QComboBox):
            return widget.currentText()

    def set_value(self, field, value):
        if field not in self.fields:
            return
        widget = self.get_widget(field)
        if not isinstance(widget, QtWidgets.QComboBox):
            return
        index = widget.findText(value)
        if index >= 0:
            widget.setCurrentIndex(index)

    def get_current_values(self):
        display_field_wdgs = {k: v for k, v in self.fields.items() if k in self.display_fields}

        active_values = {}
        for field, (_, widget_class, _) in display_field_wdgs.items():
            if widget_class == QtWidgets.QComboBox:
                widget = self.get_widget(field)
                if field == self.entity_type_tag:
                    active_values[field] = widget.currentText().lower()
                else:
                    active_values[field] = widget.currentText()
        return active_values

    def make_selections(self, context: filePath.Context):
        """
        Given a dict where the key is the field and the value is the text to pick.
        It will try to set the values in the comboboxes widgets.
        """
        for field, value in context.parameters.items():
            self.set_value(field, value)

    def set_combobox_items(self, field, items):
        """
        Method to update the contents of a field's combobox.
        It's recommended to wrap this method with blockSignals()
        """
        if field in self.fields and isinstance(self.get_widget(field), QtWidgets.QComboBox):
            widget = self.get_widget(field)
            widget.clear()
            widget.addItems(items)


class ContextSelectors(QtWidgets.QWidget):
    context_updated_sgn = QtCore.Signal()

    def __init__(self, connection: Optional[Connection] = None, context: Optional[filePath.Context] = None, parent=None):
        super().__init__(parent=parent)
        self._conn = connection
        self._context = context
        self._data = None

        self.show_outputs = False

        self.init_ui()
        self.connect_signals()
        self.populate_ui()

    def init_ui(self):
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Maximum)
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.setAlignment(QtCore.Qt.AlignTop)
        self.setLayout(self.main_layout)

        # Entity Selectors
        self.entity_stack_wdg = EntitySelectors(self._conn)
        self.main_layout.addWidget(self.entity_stack_wdg)

        # FILTERS
        self.filters_wdg = QtWidgets.QWidget(self)
        self.filters_layout = QtWidgets.QHBoxLayout(self.filters_wdg)
        self.filters_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addWidget(self.filters_wdg)

        # DCC
        pipe_packages = self._pipeline_packages()

        # fixme: turn this duplicated code into a teide_qt.utils function.
        self.dccs_scroll_area = HorizontalScrollArea()
        self.dccs_wdg = QtWidgets.QWidget()
        self.dccs_layout = QtWidgets.QHBoxLayout()
        self.dccs_layout.setContentsMargins(0, 0, 0, 0)
        self.dccs_layout.setAlignment(QtCore.Qt.AlignLeft)
        self.dccs_wdg.setLayout(self.dccs_layout)
        dccs_button_group = QtWidgets.QButtonGroup(self)
        for pipe_package in sorted(pipe_packages, key=lambda x: x.nice_name):
            pkg = pipe_package()
            pkg_wdg = QtWidgets.QPushButton(pkg.nice_name)
            pkg_wdg.toggled.connect(self._group_button_changed)
            dccs_button_group.addButton(pkg_wdg)
            self.dccs_layout.addWidget(pkg_wdg)

            if not pkg.is_valid:
                # pkg_wdg.setCheckable(False)
                # pkg_wdg.setDisabled(True)
                install_dir = pkg.get_default_version().install_dir
                tooltip_msg = f"Program not found.\n{install_dir}" if install_dir else \
                    f"Program not supported in {platform.system().lower()}."
                pkg_wdg.setToolTip(tooltip_msg)
                # continue

            pkg_wdg.setCheckable(True)
            if not IGNORE_COLOR_UI:
                color = text_to_color(pkg.nice_name, hue_shift=42)
                pkg_wdg.setStyleSheet(f"""
                    QPushButton {{
                        background-color: {color}; 
                        color: black;
                    }}
                    QPushButton:checked {{
                        color: lightgray;
                    }}
                """)

        self.dccs_scroll_area.setWidget(self.dccs_wdg)
        self.dccs_scroll_area.setFixedHeight(self.dccs_wdg.sizeHint().height())
        self.filters_layout.addWidget(self.dccs_scroll_area)

        # Toggle Outputs visibility
        self.show_icon = qtawesome.icon('ei.eye-open')
        self.hide_icon = qtawesome.icon('ei.eye-close')

        self.show_outputs_button = QtWidgets.QPushButton(icon=self.show_icon if self.show_outputs else self.hide_icon)
        self.show_outputs_button.setCheckable(True)
        self.show_outputs_button.setFlat(True)
        self.show_outputs_button.setToolTip("Show/Hide output items")
        self.filters_layout.addWidget(self.show_outputs_button)

        # Mode - ['Work', 'Publish', 'Both']
        self.modes_wdg = QtWidgets.QWidget(self)
        self.modes_layout = QtWidgets.QHBoxLayout(self.modes_wdg)
        self.modes_layout.setContentsMargins(0, 0, 0, 0)
        self.modes_layout.setSpacing(0)
        self.modes_layout.setAlignment(QtCore.Qt.AlignRight)
        mode_button_group = QtWidgets.QButtonGroup(self)
        self.filters_layout.addWidget(self.modes_wdg)

        # Work
        work_icon = qtawesome.icon('fa5.edit')
        self.work_button = QtWidgets.QPushButton(icon=work_icon)
        self.work_button.setObjectName('Work')
        self.work_button.setCheckable(True)
        self.work_button.setFlat(True)
        self.work_button.setToolTip('Work files')
        self.work_button.toggled.connect(self._group_button_changed)
        mode_button_group.addButton(self.work_button)
        self.modes_layout.addWidget(self.work_button)

        # Publish
        publish_icon = qtawesome.icon('fa.paper-plane-o')
        self.publish_button = QtWidgets.QPushButton(icon=publish_icon)
        self.publish_button.setObjectName('Publish')
        self.publish_button.setCheckable(True)
        self.publish_button.setFlat(True)
        self.publish_button.setToolTip('Published files')
        self.publish_button.toggled.connect(self._group_button_changed)
        mode_button_group.addButton(self.publish_button)
        self.modes_layout.addWidget(self.publish_button)

        # Both
        both_icon = QtGui.QIcon()
        both_icon = qtawesome.icon('mdi.expand-all')
        self.both_button = QtWidgets.QPushButton(icon=both_icon)
        self.both_button.setObjectName('Both')
        self.both_button.setCheckable(True)
        self.both_button.setFlat(True)
        self.both_button.setToolTip('All files')
        self.both_button.toggled.connect(self._group_button_changed)
        mode_button_group.addButton(self.both_button)
        self.modes_layout.addWidget(self.both_button)

    def populate_ui(self):
        self.filters_wdg.setEnabled(False)

        # Set default dcc
        default_wdgs = self.get_valid_dcc_wdgs()
        if default_wdgs:
            for default_wdg in default_wdgs:
                # fixme: use pkg name instead of index
                if default_wdg != default_wdgs[2]:
                    continue
                default_wdg.blockSignals(True)
                default_wdg.setChecked(True)
                default_wdg.blockSignals(False)

        # Set default mode Work | Publish | Both
        default_mode = self.both_button
        default_mode.blockSignals(True)
        default_mode.setChecked(True)
        default_mode.blockSignals(False)

    def connect_signals(self):
        self.context_updated_sgn.connect(self._context_changed)
        self.entity_stack_wdg.field_updated_sgn.connect(lambda: self.context_updated_sgn.emit())
        self.entity_stack_wdg.initial_params_sgn.connect(self._enable_secondary_params)
        self.show_outputs_button.clicked.connect(self._toggle_show_outputs)

    @property
    def context(self):
        return self._context

    @property
    def ui_mode(self) -> str:
        mode = [widget.objectName() for widget in self.modes_wdg.findChildren(QtWidgets.QPushButton) if widget.isChecked()][0]
        return mode.lower()

    @property
    def ui_show_outputs(self) -> bool:
        return self.show_outputs_button.isChecked()

    def _pipeline_packages(self):
        from teide_core import package
        found_ddcs = [p for p in package.get_packages(valid_only=False) if p.type == package.PackageType.DCC]
        return found_ddcs

    def _toggle_show_outputs(self):
        checked = self.show_outputs_button.isChecked()
        self.show_outputs_button.setIcon(self.show_icon if checked else self.hide_icon)
        self.context_updated_sgn.emit()

    def _enable_secondary_params(self):
        self.filters_wdg.setEnabled(True)

    def get_valid_dcc_wdgs(self):
        # fixme: rename to get_ui_dcc_wdgs() > We get the populated dccs, not necessarily have to be valid dccs.
        return [widget for widget in self.dccs_wdg.findChildren(QtWidgets.QPushButton)]

    def get_selected_dcc_wdg(self):
        return next((w for w in self.get_valid_dcc_wdgs() if w.isChecked()), None)

    def get_selected_package(self):
        widget = self.get_selected_dcc_wdg()
        if not widget:
            return None
        return package.get_package(nice_name=widget.text())

    def _group_button_changed(self):
        sender = self.sender()
        if sender.isChecked():
            self.context_updated_sgn.emit()

    def _context_changed(self):
        # Get Templates Work & Publish
        dcc = self.get_selected_package()
        parameters = self.entity_stack_wdg.context.parameters
        parameters['mode'] = self.ui_mode
        parameters['element_type'] = 'scene'
        parameters['entity_type'] = self.entity_stack_wdg.entity_type
        parameters['dcc'] = dcc.name if dcc else None
        self._context = filePath.Context(parameters)


if __name__ == "__main__":
    from teide_core import db
    from teide_qt import utils
    conn = db.SGAuthentication.login()

    app = QtWidgets.QApplication(sys.argv)
    utils.set_theme(app)

    dialog = ContextSelectors(connection=conn)
    dialog.show()

    sys.exit(app.exec())
