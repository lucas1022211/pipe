""" Comment
"""
from typing import TYPE_CHECKING

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


def main():
    pass


if __name__ == '__main__':
    main()
