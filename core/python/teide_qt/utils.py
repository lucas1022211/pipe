from typing import TYPE_CHECKING, Optional

import qtawesome
from qtpy import QtCore, QtGui, QtWidgets, QtSvg

from teide_core import settings
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

PYTHON_VERSION = 3


def create_colored_icon(svg_path, color):
    renderer = QtSvg.QSvgRenderer(svg_path)
    size = renderer.defaultSize()

    pixmap = QtGui.QPixmap(size)
    pixmap.fill(QtCore.Qt.GlobalColor.transparent)

    painter = QtGui.QPainter(pixmap)
    renderer.render(painter)
    painter.setCompositionMode(QtGui.QPainter.CompositionMode.CompositionMode_SourceIn)
    painter.fillRect(pixmap.rect(), color)
    painter.end()

    icon = QtGui.QIcon()
    icon.addPixmap(pixmap, QtGui.QIcon.Mode.Normal, QtGui.QIcon.State.On)
    return icon


class TeideStyle(QtWidgets.QProxyStyle):
    def __init__(self, name: str = 'Fusion', font_size: int = 10, mode: str = 'dark'):
        super().__init__(QtWidgets.QStyleFactory.create(name))
        self.mode = mode
        self._font_size = font_size
        self._default_font_size = 10
        self._default_icon_size = 16
        self._multiplier = self._font_size / self._default_font_size

    def styleHint(self, hint, option=None, widget=None, returnData=None):
        # Override to mimic Windows OS behavior for QComboBox dropdown
        if hint == QtWidgets.QStyle.StyleHint.SH_ComboBox_Popup:
            # Force the use of the Windows-style popup for QComboBox
            return False
        return super().styleHint(hint, option, widget, returnData)

    def subControlRect(self, control, option, subControl, widget):
        # Customize the appearance or dimensions of specific controls
        if (control == QtWidgets.QStyle.ComplexControl.CC_ComboBox and
                subControl == QtWidgets.QStyle.SubControl.SC_ComboBoxListBoxPopup):
            # Modify how the popup list box is displayed (like in Windows)
            return super().subControlRect(control, option, subControl, widget)
        return super().subControlRect(control, option, subControl, widget)

    def drawComplexControl(self, control, option, painter, widget):
        # Draw the QComboBox using the default Windows style for the popup
        if control == QtWidgets.QStyle.ComplexControl.CC_ComboBox:
            return super().drawComplexControl(control, option, painter, widget)
        return super().drawComplexControl(control, option, painter, widget)

    def polish(self, widget):
        if isinstance(widget, QtWidgets.QWidget):
            # Scale Font and Icon size based on font_size argument
            font = widget.font()
            font.setPointSize(int(self._default_font_size * self._multiplier))
            widget.setFont(font)
            if hasattr(widget, 'setIconSize'):
                icon_size = QtCore.QSize(self._default_icon_size, self._default_icon_size)
                widget.setIconSize(icon_size * self._multiplier)
            widget.setBackgroundRole(QtGui.QPalette.ColorRole.Window)
        super().polish(widget)

    def drawPrimitive(self, element, option, painter, widget=None):
        if element == QtWidgets.QStyle.PrimitiveElement.PE_IndicatorCheckBox:
            # Handle the checkbox indicator drawing
            rect = option.rect
            state = option.state

            palette = widget.palette() if widget else QtWidgets.QApplication.palette()
            base_color = palette.color(QtGui.QPalette.ColorRole.Base)
            light_color = palette.color(QtGui.QPalette.ColorRole.Light)
            highlighted_text_color = palette.color(QtGui.QPalette.ColorRole.HighlightedText)

            # Use a custom icon for the partially checked state
            if state & QtWidgets.QStyle.StateFlag.State_NoChange:
                if self.mode == 'dark':
                    painter.setBrush(base_color)
                    painter.fillRect(rect, base_color)
                else:
                    painter.setPen(light_color)
                    painter.drawRect(rect)

                icon = create_colored_icon(r"../resources/dash.svg", highlighted_text_color)
                icon.paint(painter, rect)
                return
        super().drawPrimitive(element, option, painter, widget)


def set_theme(app: QtWidgets.QApplication, style: str = "Fusion", mode: str = 'dark'):
    """Set Teide Style to any QApplication. Should work multiplatform."""
    if mode == 'dark':
        qtawesome.dark(app)
    else:
        qtawesome.light(app)

    font_size = settings.get_attribute('font_size')
    app.setStyle(TeideStyle(style, font_size=font_size, mode=mode))


def clear_layout(layout):
    while layout.count() > 0:
        item = layout.takeAt(0)
        widget = item.widget()
        if widget:
            widget.deleteLater()


def populate_combobox(items=None, widget=None, addAny=False):
    try:
        if items and widget:
            for item in items:
                if item not in [widget.itemText(i) for i in range(widget.count())]:
                    widget.addItem(item)
    except Exception as e:
        print(e)
    if addAny:
        widget.insertItem(0, 'Any')
    widget.setCurrentIndex(0)


def add_combobox_items(combobox, items):
    if not items:
        return
    unicode_list = list()
    for item in items:
        if PYTHON_VERSION != 3:
            unicode_list.append(item)
        else:
            if isinstance(item, str):
                unicode_list.append(item)
            else:
                unicode_list.append(str(item, 'utf-8'))

    combobox.addItems(unicode_list)


def get_selected_radio(widget):
    for widget in widget.findChildren(QtWidgets.QRadioButton):
        if widget.isChecked():
            return widget.text()
    return None


def open_filebrowser(valid_extensions, parent=None):
    all_files_filter = f"All files ({' '.join(['*.' + ext for ext in valid_extensions])})"
    valid_filters = ';;'.join([f"{ext.upper()} files (*.{ext})" for ext in valid_extensions])
    valid_filters += f";;{all_files_filter}"

    file_path, _ = QtWidgets.QFileDialog.getOpenFileName(parent, 'Select File', QtCore.QDir.homePath(), valid_filters)
    return file_path


def open_folderbrowser(parent=None):
    folder_path = QtWidgets.QFileDialog.getExistingDirectory(parent, 'Select Folder', QtCore.QDir.homePath())
    return folder_path
