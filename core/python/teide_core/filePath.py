"""
FileSystem API

Handles the parsing an construction of paths that comply with the pipeline and project rules.

FilePath class will generate and store paths based on the context that it has.
(pd: similar to Goblin or TeideCore)

there is a data dict with a tokens key. each token by default is None.

Environment tokens:
user, project, mode, episode, sequence, shot, asset_type, asset, step, task, variant
File path tokens:
volume, year, version, output, frame, ext, dcc, renderLayer, renderPass

Example of properties like:
- TeideCore.token.get()  // find a way to get the keys, maybe setup a getter?

Use example of methods like:
- TeideCore.printStringEnv()
- TeideCore.parseFilePathWithTemplate(scene_path, t_work)
- TeideCore.parseFilePath(scene_path)
- TeideCore.buildFilepath(template, tokenOverrides=tokens, nextVersion=True)
- TeideCore.buildFilepath(template, tokenOverrides=tokens, latestVersion=True)
- TeideCore.getLatestVersion()
- TeideCore.getNextVersion()
- TeideCore.getFilesFromTemplate(caches_template, env=None, strictDir=True, basenameMatch=True, extra_values=extra_values)
- TeideCore.getProjectSection('browser')

"""
import enum
import os
import re
import attr
import glob
import importlib.util
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Union, List, Dict, Any

import teide_core.helpers
import teide_lucidity
from teide_core import package
from teide_core import settings
from teide_core import tasks

if TYPE_CHECKING:
    from typing import *
    from teide_core import publish

_logger = teide_core.helpers.get_teide_logger(__name__)


TOKENS_TYPE = Dict[str, Optional[str]]
CONTEXT_PARAMETERS = ['prName', 'assettype', 'asset', 'episode', 'sequence', 'shot', 'step', 'task',
                      'dcc', 'entity_type', 'element_type', 'mode']
# todo: make CONTEXT_PARAMETERS contain all template_keys from the selected project --blocked by project-settings-conf


@attr.s
class Context:
    # todo: modify Context to hold a more attributes. like paramters and env
    #  where paramters are UI settings (not in templates) and env are the tokens used in the templates -max key-templates
    """
    Stores the parameters that represent a context. An example is the ContextSelectors() QtWidget.

    entity_type = shot | asset
    mode = work | publish
    element_type = scene | output  # might be missing granularity for textures, caches, renders ...

    Use cases:
        Context()
        Context(parameters)  # where parameters is a dict with or without keys

    """
    parameters = attr.ib(type=TOKENS_TYPE, factory=lambda: {key: None for key in CONTEXT_PARAMETERS},
                         converter=lambda value: {key: value.get(key, None) for key in CONTEXT_PARAMETERS})

    def __str__(self) -> str:
        return f'Context({self.parameters}) - {len(self.parameters)}'

    @classmethod
    def from_task(cls, task: tasks.Task):
        parameters = task.get_attributes()

        linked_entity_type = task.linked_entity['type']
        linked_entity_name = task.linked_entity['name']

        parameters['entity_type'] = linked_entity_type

        if linked_entity_type.lower() == 'asset':
            parameters['asset'] = linked_entity_name
        elif linked_entity_type.lower() == 'shot':
            parameters['shot'] = linked_entity_name

        parameters["task"] = task.name
        parameters["assettype"] = task._sg_data.get('entity.Asset.sg_asset_type')

        ep = task._sg_data.get('entity.Shot.sg_episode')
        if isinstance(ep, dict):
            ep = ep.get('name')
        parameters["episode"] = ep

        seq = task._sg_data.get('entity.Shot.sg_sequence')
        if isinstance(seq, dict):
            seq = seq.get('name')
        parameters["sequence"] = seq

        return cls(parameters)

    def get_template(self, force_mode: Optional["publish.Mode"] = None):
        """ Derives a template from the Context parameters. """
        template = FilePath().getElementTemplate(entity_type=self.parameters['entity_type'],
                                                 dcc='dcc' if True else self.parameters['dcc'],
                                                 mode=force_mode.value if force_mode else self.parameters['mode'],
                                                 element_type=self.parameters['element_type'])
        return template


class FilePath:
    _RESOLVER = dict()  # fixme-mypy add typing
    _TEMPLATES = dict()  # fixme-mypy add typing
    _KEY_TEMPLATES = dict()  # fixme-mypy add typing

    def __init__(self, path: Union[Path, str, None] = None):
        """
        TeideCore class initialization
        """
        self._template: Optional[teide_lucidity.Template] = None
        self._tokens: Optional[TOKENS_TYPE] = None
        self._path: Optional[Path] = Path(path) if path else None
        self.init_templates()

    def __repr__(self):
        '''Return unambiguous representation of template.'''
        # return '{0}(path={1!r}, templates={2!r})'.format(
        #     self.__class__.__name__, self._path.as_posix() if self._path else None,
        #     [key for key in self._TEMPLATES.keys()]
        # )
        return f'TeideObject()'

    @classmethod
    def from_filepath(cls, filepath: str, template=None):
        teide = cls()
        teide._path = Path(filepath)
        teide._tokens = teide.parseFilepath(filepath, template)
        return teide

    @classmethod
    def from_tokens(cls, tokens: TOKENS_TYPE, template: Optional[Union[teide_lucidity.Template, str]] = None):
        teide = cls()
        teide._tokens = tokens
        if template:
            teide._template = template
            # teide._path = teide.buildFilepath(template, updateEnv=False)
        return teide

    @property
    def context(self) -> Context:
        if not self._tokens:
            return Context()
        return Context(self._tokens)

    @property
    def tokens(self) -> Optional[TOKENS_TYPE]:
        for k, v in self.context.parameters.items():
            if k not in self._tokens:
                self._tokens[k] = v
        return self._tokens

    def validate_tokens(self, template, tokens):
        # check if all template keys are provided and not None
        # fixme: improve error handling
        missing_keys = []
        missing_keysTemplate = []
        invalid_values = []
        for key in template.keys():
            if key not in tokens:
                missing_keys.append(key)
                continue
            keyTemplate = self.getKeyTemplate(key)
            if not keyTemplate:
                missing_keysTemplate.append(key)
                continue
            if tokens[key] is None or not re.match(keyTemplate['pattern'], tokens[key]):
                msg = f'{key} = {tokens[key]}'
                invalid_values.append(key)
                continue
        msg = ''
        if missing_keys:
            # token key needed for template was not included in provided tokens -- Show in UI to user fillout?
            msg += f"\nMissing tokens: {missing_keys}"
        if missing_keysTemplate:
            # token key from template not found in templateKeys dictionary -- Critical
            msg += f"\nMissing templateKeys: {missing_keysTemplate}"
        if invalid_values:
            # token key needed for template has invalid value -- Show in UI to user fillout
            msg += f"\nInvalid values: {invalid_values}"

        return missing_keys, missing_keysTemplate, invalid_values

    def init_templates(self, project_data: dict = None):
        """
        Run over project data and initialize the necessary path templates.

        :param project_data:
        """
        if not project_data:
            project_data = settings.get_project_data('defaultProject')
        for template_name in project_data['templates'].keys():
            t_name = template_name[2:] if template_name.startswith('t_') else template_name
            template_pattern = project_data['templates'][template_name]['pattern']

            template = teide_lucidity.Template(t_name, template_pattern,
                                               duplicate_placeholder_mode=teide_lucidity.Template.STRICT,
                                               anchor=teide_lucidity.Template.ANCHOR_END)

            # Add to template collection
            self._TEMPLATES[template_name] = template
            self._RESOLVER[t_name] = template
        for template_name in self._TEMPLATES.keys():
            self._TEMPLATES[template_name].template_resolver = self._RESOLVER

    def getKeyTemplate(self, key: Optional[str] = None) -> Optional[dict]:
        project_data = settings.get_project_data('defaultProject')
        if not key:
            return project_data['templateKeys']
        return project_data['templateKeys'].get(key, None)

    def getElementTemplate(self, entity_type: Optional[str] = None,
                           dcc: Optional[str] = None,
                           mode: Optional[str] = None,
                           element_type: Optional[str] = None) -> Optional[teide_lucidity.Template]:
        """
        Get template given some parameters.
            t_{entity_type}_{element_type}
            t_{entity_type}_{mode}_{element_type}
            t_{entity_type}_{mode}_{dcc}_{element_type}
        :return:
        template
        """
        # entity_type = entity_type if entity_type else 'entity'
        dcc = dcc or 'dcc'

        template_name = f't_{entity_type}_{dcc}_{mode}_{element_type}'
        template = self.getTemplateByName(template_name)
        return template

    def getTemplateByName(self, template_name: str) -> Optional[teide_lucidity.Template]:
        if not template_name.startswith('t_'):
            template_name = 't_' + template_name
        template = self._TEMPLATES.get(template_name, None)
        if not template:
            raise ValueError(f"{template_name} not found.")
        return template

    def get_default_scene(self, dcc: str,
                           entity: Optional[str] = None,
                           step: Optional[str] = None,
                           filetype: Optional[str] = None) -> Optional[Path]:
        """
        Find the default scene matching the given fields. If no exact match is found, progressively swap placeholders.

        This method searches for a default scene file by constructing a pattern based on provided variables.
        It first attempts an exact match using all values. If no match is found, the method progressively replaces
        each variable with a wildcard one by one checking for a match at each step.
        The search continues until a file is found or all possibilities are exhausted.
        The function returns the file path if a match is found, otherwise, it returns None.

        Parameters
        ----------
        search_path
            Directory to search in.
        dcc
            The software (e.g., maya, blender).
        entity
            The entity (e.g., asset, shot).
        step
            The step (e.g., lgt, ass).
        filetype
            The file type (e.g., scene, workspace).

        Returns
        -------
        Optional[Path]
            The path to the found scene, or None if no match is found.
        """
        # fixme: bug if provided all args but the default_scene has more than one wildcard token
        #  ex: all args defined; only one default scene with all tokens in brackets; should return that scene
        #  but returns none because it doesnt generate enough permutations to check all possibilities.
        pkg = package.get_package(name=dcc)
        valid_extensions = pkg.extensions
        valid_extensions_pattern = "|".join(valid_extensions)

        # Initial tokens mapping, including placeholders
        tokens = {
            'entity': (entity, r'\[entity\]'),
            'step': (step, r'\[step\]'),
            'dcc': (dcc, r'\[dcc\]'),
            'filetype': (filetype, r'\[filetype\]')
        }

        def generate_pattern(search_tokens: Dict[str, str]):
            """Helper function to generate pattern based on current token values"""
            return "default_{entity}_{step}_{dcc}_{filetype}.({ext})".format(
                entity=search_tokens['entity'],
                step=search_tokens['step'],
                dcc=search_tokens['dcc'],
                filetype=search_tokens['filetype'],
                ext=valid_extensions_pattern
            )

        # get resources template
        resources_template = self.getTemplateByName('t_entity_resources')
        search_path = self.buildFilepath(resources_template, self._tokens) / "default_scenes"

        all_files = list(search_path.iterdir())

        # Create initial token values (start with all given values)
        search_tokens = {
            'entity': tokens['entity'][0] if entity else tokens['entity'][1],
            'step': tokens['step'][0] if step else tokens['step'][1],
            'dcc': tokens['dcc'][0] if dcc else tokens['dcc'][1],
            'filetype': tokens['filetype'][0] if filetype else tokens['filetype'][1]
        }

        # Try the exact match first
        wildcard_pattern = generate_pattern(search_tokens)
        matching_files = [f for f in all_files if re.match(wildcard_pattern, f.name)]
        if matching_files:
            return matching_files[0]

        # If no exact match, progressively relax the constraints
        for key in tokens.keys():
            original_value = search_tokens[key]
            search_tokens[key] = tokens[key][1]
            wildcard_pattern = generate_pattern(search_tokens)

            matching_files = [f for f in all_files if re.match(wildcard_pattern, f.name)]
            if matching_files:
                return matching_files[0]
            search_tokens[key] = original_value

        # PATCH: upper loop has to evaluate all permutations, for now we check a permutation with more wildcards so
        # we can get a default_[entity]_[step]_dcc_scene.ext (see bracket fields)
        if not matching_files:
            search = {
                'entity': tokens['entity'][1],
                'step': tokens['step'][1],
                'dcc': tokens['dcc'][0] if dcc else tokens['dcc'][1],
                'filetype': tokens['filetype'][0] if filetype else tokens['filetype'][1]
            }
            pattern = generate_pattern(search)
            matching_files = [f for f in all_files if re.match(pattern, f.name)]
            if matching_files:
                return matching_files[0]
        raise AttributeError(f'Default scene not found for entity: {entity} step: {step} dcc: {dcc} filetype: {filetype}')

    def getFilesFromTemplate(self, template, tokens, match_basename=True, extra_values: Optional[TOKENS_TYPE] = None):
        """making a big regex that has to match to files found."""
        tokens = tokens.copy()

        # Add element_type token from template
        element_type = template.name.split('_', 3)[-1]
        tokens.update({'element_type': element_type})

        # Apply overrides values
        if extra_values:
            tokens.update(extra_values)

        if match_basename:
            search_tokens = tokens.copy()
            missing_keys = template.missing_keys(search_tokens, scope=teide_lucidity.Template.BASENAME)
            for missing_key in missing_keys:
                search_tokens[missing_key] = '*'
            basename = template.format(search_tokens, scope=teide_lucidity.Template.BASENAME)

        # fixme: make this more elegant. Also, we should only have wildcards for non-context tokens. ex: renderLayer
        search_dir_tokens = tokens.copy()
        missing_dir_keys = template.missing_keys(search_dir_tokens, scope=teide_lucidity.Template.DIRNAME)
        for missing_dir_key in missing_dir_keys:
            if missing_dir_key in CONTEXT_PARAMETERS:
                continue
            search_dir_tokens[missing_dir_key] = '*'
        search_dir = Path(template.format(search_dir_tokens, scope=teide_lucidity.Template.DIRNAME))

        # Use wildcard for missing keys (key's regex pattern instead of *)
        missing_keys = template.missing_keys(tokens, scope=teide_lucidity.Template.DIRNAME)
        empty_keys = [key for key in template.keys() if key not in tokens or not tokens[key] or tokens[key] == '*']
        key_templates = self.getKeyTemplate()
        for missing_key in set(missing_keys + empty_keys):
            if missing_key == 'ext':
                if tokens['element_type'] == 'scene':
                    pkg = package.get_package(name=tokens['dcc'])
                    extensions = pkg.extensions
                    if extensions:
                        tokens[missing_key] = f"(%s|)$" % '|'.join(extensions)
                        continue
                else:
                    template_extensions = template.getKeyCustomOptions(key='ext')
                    if template_extensions:
                        tokens[missing_key] = f"(%s|)$" % '|'.join(template_extensions)
                        continue
            if missing_key in key_templates:
                tokens[missing_key] = key_templates[missing_key]['pattern']

        formatted_template = template.format(tokens)

        items = [Path(f) for f in glob.iglob((search_dir / basename).as_posix(), recursive=True)]
        filter_items = [item for item in items if re.fullmatch(formatted_template, item.as_posix())]

        matching_files = []
        for item in filter_items:
            try:
                template.parse((search_dir / item).as_posix())
            except Exception as e:
                continue
            matching_files.append(item)
        return matching_files

    def getLatestVersion(self, template: Union[teide_lucidity.Template, str, None] = None, tokens=None,
                         path: Union[Path, str, None] = None, check_disk=True):
        """
        Get the latest version of a given path.

        """
        # get Template by name
        if not template:
            template = self._template
        elif isinstance(template, str):
            template = self.getTemplateByName(template)

        if not tokens:
            if path:
                tokens = self.parseFilepath(path=path, template=template)
            elif self._tokens:
                tokens = self._tokens
            else:
                raise AttributeError('Provide path or tokens')

        current_version = int(tokens.get('version', 0))
        files_list = self.getFilesFromTemplate(template=template, tokens=tokens, extra_values={'version': None})
        if not files_list and check_disk:
            return 0
        latest_version = max((int(template.parse(f.as_posix())['version']) for f in files_list), default=current_version)
        return latest_version

    def getNextVersion(self, template=None, tokens=None, check_disk=False):
        """
        Get the latest version of a given path.

        """
        latest = self.getLatestVersion(template=template, tokens=tokens, check_disk=check_disk)
        return latest + 1

    def parseFilepath(self, path: Union[Path, str], template: Union[teide_lucidity.Template, str, None] = None) -> Optional[dict]:
        # try to find template
        # TODO use extra flags to refine search templates
        # fixme bugged tokens if template is not provided.

        # get Template by name
        if isinstance(template, str):
            template = self.getTemplateByName(template)

        # get Template by name
        if isinstance(path, Path):
            path = path.as_posix()

        if not template:
            # fixme: if we have enough tokens we can use self.getElementTemplate(entity_type, dcc, mode, element_type)
            attempts = {}
            for name, t in self._TEMPLATES.items():
                try:
                    tokens = t.parse(str(path))
                    attempts['name'] = {'tokens': len(tokens), 'template': t}
                except Exception as e:
                    continue
            if not attempts:
                return None
            template = max(attempts.items(), key=lambda x: x[1]['tokens'])[1]['template']
        self._template = template
        return template.parse(str(path))

    def buildFilepath(self, template: Union[teide_lucidity.Template, str],
                      tokenOverrides=None,
                      latestVersion=False,
                      nextVersion=False,
                      updateEnv=True,
                      check_disk=False) -> Path:
        """
        Construct a path with the 'self.env' data provided to match the template.
        :param template:
        :param tokenOverrides:
        :param latestVersion:
        :param nextVersion:
        :param updateEnv:
        """

        # get Template by name
        if isinstance(template, str):
            template = self.getTemplateByName(template)

        tokens = {}
        origTokens = self._tokens if self._tokens else {}
        for key in template.keys():
            if key in origTokens:
                tokens[key] = origTokens[key]

        # fallback to default values if missing key
        # for missing_key in template.missing_keys(tokens):
        #     default_value = self.getKeyTemplate(missing_key).get('default')
        #     if not default_value:
        #         continue
        #     tokens[missing_key] = default_value

        # Apply overrides
        if tokenOverrides:
            for key in tokenOverrides.keys():
                tokens[key] = tokenOverrides[key]

        # Getting version.
        if latestVersion:
            tokens['version'] = self.getLatestVersion(template, tokens, check_disk=check_disk)
        if nextVersion:
            tokens['version'] = self.getNextVersion(template, tokens, check_disk=check_disk)

        # format keyValues:
        for key in tokens.keys():
            if not isinstance(tokens[key], str):
                if key == 'version':
                    tokens[key] = str(tokens[key]).zfill(3)
                    continue
                tokens[key] = str(tokens[key])

        if updateEnv:
            self._tokens = tokens
        return Path(template.format(data=tokens))

    # def is_filesequence(self) -> bool:
    #     sequence_identifiers = ['frame', 'udim']
    #     return any(identifier in self._tokens for identifier in sequence_identifiers)


# Example usage:
# if __name__ == "__main__":
