"""
Entities Module

Describes a BaseEntity and sets a boilerplate to generate it from the DB.
The needed entities will subclass from BaseEntity.
"""
from typing import TYPE_CHECKING

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class BaseEntity:
    def __init__(self):
        pass


class SGBaseEntity(BaseEntity):
    def __init__(self):
        super().__init__()
        self._id = None
        self.name = None
        self.created_by = None
        self.creation_date = None
        self.modification_date = None

    @classmethod
    def from_sg(cls):
        return cls()
