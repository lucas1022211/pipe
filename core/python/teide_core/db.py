"""
DB API

Handles the connection with the pipeline's DB (MySQL | Shotgrig | Ftrack)

Documentation(SG):

# Simple usecase to use local authentication
conn = Authentication.login()
sg = conn.sg
asset = sg.find_one("Task", [], ['code', 'id'])

# Lower-level way to create a connection using SG credentials
conn = Connection(email, password)
sg = conn.sg
asset = sg.find_one("Task", [], ['code', 'id'])

data = {"project": {'assets': [Asset],
                    'shots': [shot]
        }
    }

where Asset = {'code': '', 'type': '', 'tasks': []}
      Shot  = {'code': '', 'episode': '', 'sequence': [], 'tasks': []}

Then from that i can construct a tree dict that i can use for the wdg_selectors()

"""
import os
from functools import cached_property
from typing import TYPE_CHECKING, Optional, Union, List, Any

from cryptography.fernet import Fernet
import cryptography
import shotgun_api3
from tank.authentication import shotgun_authenticator

import teide_core.helpers
from teide_core import settings
from teide_core import filePath

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


# todo: store more queries in self._data so that its not needed to call SG again. add flag to force the call.
#  ex: get_connected_user(self, force=False)
# todo: generate key in a way that its the same for 6 months or so.
#  lets try to add a global variable rule (int) that sets how long the key will stay static.
#  When this key changes, every user's cached credentials will not be valid any more. And the login window will pop up.
#    Q: Should we also have a method to have a permanent key?
# todo: add a few helper methods to make SG calls? similar to liju's
# todo: make a Dummy connection for testing purposes

SG_URL_SERVER = settings.get_attribute('shotgun_url')
key = b'CwzW1P6arZO4TGvBhnQS8fxCyKGCeAa7jVHsAFBj7dg='  # Fernet.generate_key()


class Connection:
    _SG_URL = SG_URL_SERVER

    def __init__(self, sg=None, email=None, password=None, url=None, project=None):
        self.sg = sg
        self.teide = filePath.FilePath()
        self._project = project
        self._email = email
        self._data = {}

        if self.sg is None and (email is not None and password is not None):
            self.make_connection(email, password, url=url)
            # self.test_connection()

    @classmethod
    def from_authkey(cls, auth):
        try:
            Authentication.cipher_suite.decrypt(auth)
        except Exception as e:
            if e == cryptography.fernet.InvalidToken:
                return None
            _logger.exception(e, exc_info=True)
            return None
        url, email, password = Authentication.cipher_suite.decrypt(auth).decode().split(';')
        return cls(email, password, url)

    @classmethod
    def from_tank(cls, sg, login):
        return cls(sg=sg, email=login)

    @cached_property
    def current_project(self):
        return next((proj for proj in self.get_projects() if proj.get('name') == self._project), None)

    def make_connection(self, email, password, url=None):
        if url is None:
            url = self._SG_URL
        self.sg = shotgun_api3.Shotgun(url, login=email, password=password)
        self._email = email

    def test_connection(self):
        try:
            self.sg.get_session_token()
            _logger.info('Valid Authentication key found.')
            return True
        except Exception as e:
            if e == shotgun_api3.shotgun.AuthenticationFault:
                return False
            _logger.exception(e)

    def get_connected_user(self, force=False):
        if not self._data.get('connected_user', None) or force:
            self._data['connected_user'] = self.sg.find_one("HumanUser", [["login", "is", self._email]], ["id", "name"])
        return self._data['connected_user']

    def get_projects(self, fix_spaces=True):
        if not self._data.get('projects', None):
            self._data['projects'] = self.sg.find("Project", [["is_template", "is", False], ["archived", "is", False]], ["name", "id", "code"])
            if fix_spaces:
                for proj in self._data['projects']:
                    proj['name'] = proj['name'].replace(' ', '-')
        return self._data['projects']

    def get_project_id(self, project_name: Optional[str] = None, project_code: Optional[str] = None):
        if not self._data.get('projects', None):
            self.get_projects()
        return [proj for proj in self._data['projects']
                if proj['name'] == project_name or proj['code'] == project_code][0].get('id')

    def get_assets(self, project: Union[dict, str] = None, asset_type: str = None, order: Optional[List[Any]] = None,
                   limit: int = 0, extra_fields: Optional[List[str]] = None):
        filters = [['sg_status_list', 'is_not', 'omt']]
        if project:
            if isinstance(project, dict):
                project = project.get('id')
            elif isinstance(project, str):
                project = self.get_project_id(project)
            filters.append(['project', 'is', {'type': 'Project', 'id': project}])

        if asset_type:
            filters.append(['sg_asset_type', 'is', asset_type])

        fields = ["code", "id", "sg_asset_type"]
        if extra_fields:
            [fields.append(extra_field) for extra_field in extra_fields if extra_field not in fields]

        assets = self.get_entities('Asset', filters, fields, order=order, limit=limit)
        return assets

    def get_asset_types(self):
        res = self.sg.schema_field_read('Asset', field_name="sg_asset_type")
        return res['sg_asset_type'].get('properties').get('valid_values').get('value')

    def get_shots(self, project: Union[str, dict, int], sequence=False, order: Optional[List[Any]] = None,
                  limit: int = 0, extra_fields: Optional[List[str]] = None):
        filters = []
        # filters += ['sg_status_list', 'is_not', 'omt']
        if isinstance(project, dict):
            project = project.get('id')
        elif isinstance(project, str):
            project = self.get_project_id(project)
        filters.append(['project', 'is', {'type': 'Project', 'id': project}])

        fields = ['code', 'sg_episode', 'sg_sequence']
        if extra_fields:
            [fields.append(extra_field) for extra_field in extra_fields if extra_field not in fields]

        return self.get_entities('Shot', filters, fields, order=order, limit=limit)

    def get_tasks(self, project: Union[dict, str], sg_entity: Optional[dict] = None):
        filters = [['sg_status_list', 'is_not', 'omt']]
        if sg_entity:
            filters += [['entity', 'is', sg_entity]]
        if isinstance(project, dict):
            project = project.get('id')
        elif isinstance(project, str):
            project = self.get_project_id(project)
        filters.append(['project', 'is', {'type': 'Project', 'id': project}])
        fields = ['content', 'step', 'sg_sort_order']
        order = [{'field_name': 'sg_sort_order', 'direction': 'asc'}]
        return self.get_entities(entity='Task', filters=filters, fields=fields, order=order)

    def get_entities(self, entity, filters, fields, order=None, limit=0, latest=False):
        if latest is True:
            additional_filter_presets = [
                {
                    "preset_name": "LATEST",
                    "latest_by": "ENTITIES_CREATED_AT"
                }
            ]
            result = self.sg.find(entity, filters, fields, order=order, limit=limit, additional_filter_presets = additional_filter_presets)
        else:
            result = self.sg.find(entity, filters, fields, order=order, limit=limit)
        return result

    def update_entity(self, entity_type, entity_id, data):
        return self.sg.update(entity_type, entity_id, data)

    def create_entity(self, entity_type, data, fields):
        return self.sg.create(entity_type, data, fields)

    def get_sg_page_url(self, entity: str, entity_id: int):
        return f"{self._SG_URL}/detail/{entity}/{str(entity_id)}"

    def get_playlist_date_from_utc(self, prefix):
        import datetime
        from teide_core.utils import make_dailies_playlist_name
        now_uae = datetime.datetime.utcnow() + datetime.timedelta(hours=4)
        playlist_name = make_dailies_playlist_name(date=now_uae, name_prefix=prefix)

        # Validate playlist doesn't exist or if it exists is pending review, else return next day playlist.
        found_playlist = self.sg.find_one("Playlist", [['code', 'is', playlist_name], ['locked', 'is', True]], ['id', 'code', 'locked'])
        if found_playlist:
            next_day = now_uae + datetime.timedelta(days=1)
            playlist_name = make_dailies_playlist_name(date=next_day, name_prefix=prefix)
        return playlist_name

    def get_users(self, active_only=True, **kwargs):
        filters = [['name', 'not_in', ['Template User', 'Shotgrid Support']]]
        if active_only:
            filters.append(['sg_status_list', 'is', 'act'])

        fields = ['name', 'id', 'projects', 'email']
        if 'fields' in kwargs:
            fields.extend(kwargs['fields'])

        order = [{'field_name': 'name', 'direction': 'asc'}]
        users = self.get_entities('HumanUser', filters, fields, order, **kwargs)
        return users

    def get_episodes_from_project(self, project, **kwargs):
        filters = [['sg_status_list', 'is_not', 'omt']]
        if isinstance(project, dict):
            project = project.get('id')
        elif isinstance(project, str):
            project = self.get_project_id(project)
        filters.append(['project', 'is', {'type': 'Project', 'id': project}])

        fields = ['code', 'id', 'sg_status_list']
        if 'fields' in kwargs:
            fields.extend(kwargs['fields'])

        order = [{'field_name': 'name', 'direction': 'asc'}]
        return self.get_entities('Episode', filters, fields, order, **kwargs)

    def get_sequences_from_project(self, project, **kwargs):
        filters = [['sg_status_list', 'is_not', 'omt']]
        if isinstance(project, dict):
            project = project.get('id')
        elif isinstance(project, str):
            project = self.get_project_id(project)
        filters.append(['project', 'is', {'type': 'Project', 'id': project}])

        fields = ['code', 'id', 'sg_status_list', 'episode']
        if 'fields' in kwargs:
            fields.extend(kwargs['fields'])

        order = [{'field_name': 'name', 'direction': 'asc'}]
        return self.get_entities('Sequence', filters, fields, order, **kwargs)


class Authentication:
    cipher_suite = Fernet(key)
    authentication_file = os.path.join(settings.TEIDE_LOCAL_DIR, 'authentication.key') if settings.TEIDE_LOCAL_DIR else None

    @classmethod
    def save_authentication(cls, url, email, password):
        encrypted_authentication = cls.cipher_suite.encrypt(f'{url};{email};{password}'.encode())
        with open(cls.authentication_file, 'wb') as file:
            file.write(encrypted_authentication)
        return encrypted_authentication

    @classmethod
    def get_authentication(cls) -> Optional[bytes]:
        if os.path.exists(cls.authentication_file):
            with open(cls.authentication_file, 'rb') as file:
                return file.read()
        else:
            return None

    @staticmethod
    def validate_authentication(auth) -> bool:
        conn = Connection.from_authkey(auth)
        return conn.test_connection()

    @classmethod
    def login(cls) -> Optional[Connection]:
        from teide_qt.widgets import login_dialog

        auth = cls.get_authentication()
        if auth:
            conn = Connection.from_authkey(auth)
            if not conn:
                return login_dialog.main()
            if conn.test_connection():
                return conn
        return login_dialog.main()


class SGAuthentication:
    _SG_URL = SG_URL_SERVER

    # This connection will automatically monitor itself and in the
    # case the user credentials (session token) that the user object
    # encapsulates expires or become invalid, the shotgun connection
    # instance will automatically pop up a UI, asking the user
    # to type in their password. This typically happens after
    # 24 hours of inactivity.
    @classmethod
    def validate_authentication(cls, sg):
        try:
            sg.find('HumanUser', [], [])
            return True
        except Exception as e:
            print(e)
            return False

    @classmethod
    def login(cls):
        from qtpy import QtWidgets
        app_instance = QtWidgets.QApplication.instance()
        if not app_instance:
            raise ValueError('Missing QtInstance')

        sa = shotgun_authenticator.ShotgunAuthenticator()
        user = sa.get_default_user()
        if not user:
            user = sa.get_user_from_prompt(host=cls._SG_URL, is_host_fixed=True)
            sa._defaults_manager.set_host(user.host)
            sa._defaults_manager.set_login(user.login)

        sg = user.create_sg_connection()
        conn = Connection.from_tank(sg=sg, login=user.login)
        if conn.test_connection():
            _logger.info("Successfully logged in!")
        return conn

    @staticmethod
    def logout():
        # Clear the saved user.
        sa = shotgun_authenticator.ShotgunAuthenticator()
        user = sa.get_user()
        result = sa.clear_default_user()
        if not result:
            _logger.info("Successfully logged out from %s." % user.host)
        else:
            _logger.warning("Not logged in.")


if __name__ == "__main__":
    print('Testing Authentication...')
    connection = SGAuthentication.login()
    sg = connection.sg
    asset = sg.find_one("Task", [], ['code', 'id'])
    print('Result: %s' % asset)
