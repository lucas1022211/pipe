""" PublishAPI to handle OutputItems.

WorkArea:
    - tokens: dict
    - scene_template: Template
    - teide: FilePath
    - scene_path: str
    - description: Optional[str]
    - add_to_dailies: bool
    - OutputItems: List[OutputItem]
    - build_jobs() -> List[OutputItem]

OutputItemTypes (enum)
    - <TYPE-NAME>

OutputItem
    - type: OutputItemTypes
    - w_template: Template
    - p_template: Template
    - name: str
    - publish_jobs: List[OutputItem]

OutputItem
    - output_type
    - src
    - dst

PublishAPI
    - from_area()
    - local_publish(jobs)
    - sg_publish(jobs)
    - do_publish()  # publish a Version and its OutputItem as Publish

"""
import enum
import shutil
import copy

import attr
import mimetypes
from pathlib import Path
from typing import TYPE_CHECKING, List, Union, Optional

from teide_lucidity import Template
from teide_core import settings
from teide_core import filePath
from teide_core import db
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class Mode(enum.Enum):
    WORK: str = 'work'
    PUBLISH: str = 'publish'
    BOTH: str = 'both'
    ANY: str = 'any'


class OutputType(enum.Enum):
    """
    Types of outputs generated form a work file. Each corresponds to a template to drive naming conventions.

    SG mapping (SG Published File Type::OutputType)
        Scene: SCENE
        Texture: TEXTURE
        Thumbnail: THUMBNAIL
        Playblast: PLAYBLAST
        Cache: OUTPUT that are not previewMedia (non-img/video)
        Render: RENDER_LAYER + RENDER_STILL + Any previewMedia other than THUMBNAIL, TEXTURE or PLAYBLAST.

    """
    RENDER_LAYER = "render_layer"
    RENDER_STILL = "render_still"
    OUTPUT = "output"
    PLAYBLAST = "playblast"
    THUMBNAIL = "thumbnail"
    TEXTURE = "texture"
    SCENE = "scene"

    @classmethod
    def get_all(cls):
        return [item for item in cls]

    @classmethod
    def get_outputs(cls):
        black_list = [cls.SCENE]
        return [item for item in cls if item not in black_list]

    @classmethod
    def get_imageable(cls):
        return [cls.RENDER_LAYER, cls.RENDER_STILL, cls.OUTPUT, cls.PLAYBLAST, cls.THUMBNAIL, cls.TEXTURE]

    @classmethod
    def match_type(cls, string):
        for output_type in cls:
            if output_type.value != string:
                continue
            return output_type


def ensure_template(value: Union[Template, str]) -> Template:
    """Convert a string to Template, or return if already a Template."""
    return filePath.FilePath().getTemplateByName(value) if not isinstance(value, Template) else value


@attr.s
class OutputItem:
    output_type = attr.ib(type=OutputType)
    name = attr.ib(type=str)
    src = attr.ib(type=Path)
    dst = attr.ib(type=Path)


class WorkArea:
    """
    A workarea has a teide instance with the tokens from the given workscene + the context

    The possible outputs are also stored here as templates+tokens and we can gather the ones that actually exist with another
    method that checks if the files exist. and uses wildcards to find multiple, variants, descriptors, renderLayers etc

    mediaPreviw is a @property list of imageable output_items (think of images and videos based on extension & order by precedence)

    """
    # todo: add a validate method to check the types of the output_item. (ingest)
    #   required:  scene == 1
    #              previewMedia > 1;
    #   optional: all other types;
    teide = filePath.FilePath()
    scene_path: Optional[Path] = None

    def __init__(self, tokens: filePath.TOKENS_TYPE, template: Union[Template, str]):
        # validate tokens
        self.validate_ui_tokens(tokens)

        if not isinstance(template, Template):
            template = self.teide.getTemplateByName(template)

        self.tokens = tokens.copy()
        self.scene_template = template
        self.description: Optional[str] = None
        self.add_to_dailies: bool = False
        self.output_items: List[OutputItem] = list()
        self.imageable_items: List[OutputItem] = list()
        self.preview_media_item: Optional[OutputItem] = None

        # build scene path & store
        if not self.scene_template.missing_keys(self.tokens):
            self.scene_path = self.teide.buildFilepath(self.scene_template, self.tokens)

        self.initialize_output_items()
        self.initialize_imageable_items()

    @classmethod
    def from_tokens(cls, tokens: filePath.TOKENS_TYPE):
        cls.validate_ui_tokens(tokens)

        # get scene template
        scene_template = cls.teide.getElementTemplate(entity_type=tokens['entity_type'],
                                                      dcc='dcc',
                                                      mode=tokens['mode'],
                                                      element_type=tokens['element_type'])
        return cls(tokens, scene_template)

    @staticmethod
    def validate_ui_tokens(tokens):
        ui_tokens = ['dcc', 'entity_type', 'element_type', 'mode']
        missing_ui_tokens = []
        for ui_token in ui_tokens:
            if not tokens.get(ui_token, None):
                missing_ui_tokens.append(ui_token)
        if missing_ui_tokens:
            raise AttributeError(f"Missing tokens: {missing_ui_tokens}")
        return missing_ui_tokens

    @staticmethod
    def get_scene_outputs(dcc, entity, step):
        """"From ProjectSettings get the outputs of a given scene type"""
        project_data = settings.get_project_data('defaultProject')

        if dcc not in project_data['publishes']:
            dcc = 'dcc'
        if entity not in project_data['publishes'][dcc]:
            entity = 'entity'
        if step not in project_data['publishes'][dcc]:
            step = 'step'

        return project_data['publishes'][dcc][entity][step]

    def initialize_output_items(self):
        """Returns the output_items that can be generated from this workArea. OutputItem store instructions."""
        outputs = self.get_scene_outputs(dcc=self.tokens['dcc'],
                                         entity=self.tokens['entity_type'],
                                         step=self.tokens['step'])

        # generate templates and other instructions
        self.output_items.clear()
        for output_item in outputs.get('output_items', []):
            output_type = OutputType.match_type(output_item)

            w_template = self.teide.getElementTemplate(entity_type=self.tokens['entity_type'],
                                                       dcc=None,
                                                       mode=Mode.WORK.value,
                                                       element_type=output_type.value)

            p_template = self.teide.getElementTemplate(entity_type=self.tokens['entity_type'],
                                                       dcc=None,
                                                       mode=Mode.PUBLISH.value,
                                                       element_type=output_type.value)
            w_found_files = self.teide.getFilesFromTemplate(w_template, self.tokens, extra_values={'ext': None})
            for w_found_file in w_found_files:
                src_path = w_found_file
                src_tokens = self.teide.parseFilepath(src_path, w_template)
                dest_path = self.teide.buildFilepath(p_template, src_tokens, updateEnv=False)
                job = OutputItem(output_type=output_type, name=src_path.name, src=src_path, dst=dest_path)
                self.output_items.append(job)

    def initialize_imageable_items(self):
        """Return a list of imageable_items based on extension. order by descending preference"""
        imageable_output_items = self.get_output_items(existing_only=True, imageable_only=True,
                                                       types=OutputType.get_imageable())

        # add OpenExr type to mimetypes. probably should be moved to a utils module.
        mimetypes.add_type('image/x-exr', '.exr', strict=True)

        self.imageable_items = []
        for job in copy.deepcopy(imageable_output_items):
            media_type = mimetypes.guess_type(job.src.as_posix())
            if not media_type[0]:
                continue
            if job not in self.imageable_items:
                self.imageable_items.append(job)
        if self.imageable_items:
            self.preview_media_item = self.imageable_items[0]

    def get_output_items_by_type(self, types: Optional[List[OutputType]] = None, existing_only=False, 
                                 mode: Optional[Mode] = None):
        if not types:
            types = OutputType.get_outputs()
        selected_output_items = []
        for output_item in self.output_items:
            if output_item.output_type not in types:
                continue
            selected_output_items.append(output_item)
        return self._filter_existing_output_items(output_items=selected_output_items, mode=mode) if existing_only \
            else selected_output_items

    def get_output_items(self, existing_only=False, types: Optional[List[OutputType]] = None, imageable_only=False, mode: Optional[Mode] = None):
        if not types:
            types = OutputType.get_outputs()

        if imageable_only:
            types = [output_type for output_type in types if output_type in OutputType.get_imageable()]

        output_items = self.get_output_items_by_type(types=types, existing_only=existing_only, mode=mode)
        output_items.sort(key=lambda item: list(OutputType).index(item.output_type))
        return output_items

    def _filter_existing_output_items(self, output_items: List[OutputItem], mode: Optional[Mode] = Mode.WORK) -> List[OutputItem]:
        """Given a list of output items, return the ones that exist and match the templates. 
        Uses Wildcards to return user-defined tokens"""
        if not mode:
            mode = Mode(self.tokens['mode'])

        existing_output_items = []
        for output_item in output_items:
            eval_file = output_item.dst if mode == Mode.PUBLISH else output_item.src 
            if not eval_file.exists():
                continue
            if output_item not in existing_output_items:
                existing_output_items.append(output_item)
        return existing_output_items


class PublishAPI:
    """
    1. From a given scene (template + scene fullpath or full-tokens) get the Project.output_items
    Lets provide a fullpath scene and hardcode the w_template with dcc_work_scene* get entity_type(asset/ep/seq/sh) from tokens
    That will prepare the minimum stuff needed of template + fullpath --> tokens (missing some other context tokens such as mode, dcc, )
    2. From output_items generate w_template and p_template

    """

    def __init__(self, conn: db.Connection, area: WorkArea):
        self.teide = filePath.FilePath()
        self.conn = conn
        self.area = area

    @classmethod
    def from_area(cls, conn: db.Connection, area: WorkArea):
        return cls(conn, area)

    @staticmethod
    def _local_publish(output_items: List[OutputItem]):
        for output_item in output_items:
            if not output_item.src.exists():
                raise FileNotFoundError(f"File not found: {output_item.src.as_posix()}")
            if output_item.dst.exists():
                _logger.info(f"Skipping existing file {str(output_item.dst)}")
                continue
            _logger.info(f"Copy to publish location {str(output_item.src)} --> {str(output_item.dst)}")
            output_item.dst.parent.mkdir(exist_ok=True, parents=True)
            shutil.copy2(src=output_item.src, dst=output_item.dst)

    def _sg_publish(self, output_items: List[OutputItem], description: Optional[str] = None, add_to_dailies: bool = False):
        """Create or update SG Publishes and Version entities for the given output_items."""
        project_name = self.area.tokens['prName']
        project_id = self.conn.get_project_id(project_name)

        # Get entity details
        entity_type = self.area.tokens['entity_type'].capitalize()
        entity_name = self._get_entity_name()

        # Find entity in Shotgun
        entity_filters = [
            ['project', 'is', {'type': 'Project', 'id': project_id}],
            ['code', 'is', entity_name]
        ]
        sg_entity = self.conn.sg.find_one(entity_type, entity_filters)
        if not sg_entity:
            raise ValueError(f"Could not find {entity_type} with name {entity_name}")

        # Find or create task
        task_name = self.area.tokens.get('task')
        if task_name:
            sg_task = self._find_or_create_task(project_id, sg_entity, task_name)
        else:
            sg_task = None

        # Create Publishes for each output_item
        sg_publishes = []
        for output_item in output_items:
            dest_path = output_item.dst
            _logger.info(f"Creating SG Publish for {dest_path.as_posix()}")

            # Check if publish already exists
            publish_filters = [
                ['project', 'is', {'type': 'Project', 'id': project_id}],
                ['code', 'is', dest_path.name],
                ['entity', 'is', {'type': entity_type, 'id': sg_entity['id']}]
            ]
            existing_publish = self.conn.sg.find_one('PublishedFile', publish_filters)

            if existing_publish:
                sg_publishes.append(existing_publish)
                continue

            # Create new publish
            publish_data = {
                'project': {'type': 'Project', 'id': project_id},
                'code': dest_path.name,
                'name': dest_path.name,
                'published_file_type': self._get_publish_type(output_item.output_type),
                'path': {'local_path': str(dest_path), 'local_storage': {'type': 'LocalStorage', 'id': 3}},
                'path_cache': self._generate_path_cache(dest_path),
                'version_number': int(self.area.tokens['version']),
                'entity': {'type': entity_type, 'id': sg_entity['id']},
            }

            if sg_task:
                publish_data['task'] = {'type': 'Task', 'id': sg_task['id']}
            sg_pub = self.conn.create_entity('PublishedFile', data=publish_data, fields=['id', 'code'])
            sg_publishes.append(sg_pub)

        # Create or update version
        version_name = self.area.scene_template.format(self.area.tokens, scope=3).rsplit('.', 1)[0]
        preview_media = self.area.preview_media_item.dst if self.area.preview_media_item else None

        version_data = {
            'project': {'type': 'Project', 'id': project_id},
            'code': version_name,
            'entity': {'type': entity_type, 'id': sg_entity['id']},
            'sg_status_list': 'rev',
            'published_files': [{'type': 'PublishedFile', 'id': p['id']} for p in sg_publishes]
        }

        if sg_task:
            version_data['sg_task'] = {'type': 'Task', 'id': sg_task['id']}

        if preview_media:
            version_data['sg_path_to_movie'] = preview_media.as_posix()

        if description:
            version_data['description'] = description

        if add_to_dailies:
            step_name = self.area.tokens.get('step')
            prefix = "Assets" if entity_type.lower() == 'asset' else step_name.capitalize()
            playlist_name = self.conn.get_playlist_date_from_utc(prefix=prefix)
            found_playlist = self.conn.sg.find_one("Playlist", [['code', 'is', playlist_name]], ['id', 'code'])
            if not found_playlist:
                _logger.info(f'Playlist not found in SG, creating {playlist_name}...')
                data = {
                    'code': playlist_name,
                    'description': "Automatic playlist for dailies reviews.",
                    'project': version_data['project']
                }
                found_playlist = self.conn.sg.create("Playlist", data, ['id', 'code'])
            version_data["playlists"] = [found_playlist]

        # Find existing version or create new
        version_filters = [
            ['project', 'is', {'type': 'Project', 'id': project_id}],
            ['code', 'is', version_name],
            ['entity', 'is', {'type': entity_type, 'id': sg_entity['id']}]
        ]
        existing_version = self.conn.sg.find_one('Version', version_filters, fields=['id', 'code', 'published_files', 'playlists'])
        # fixme: raise error msg if publishing a version made by other user.
        #  get the existing_version's creator and if different than current_user and current_user is Artist; raise
        #   <current_user> doesn't have permissions to modify version made by another user.
        #   tip: version up and publish again. or ask wither the original Author or a Manager/Admin user.
        #   >> cancel publish procedure.
        _logger.info(f"Create/Update SG Version {version_name}")
        if not existing_version:
            sg_version = self.conn.create_entity('Version', version_data, fields=[])
        else:
            # merge data
            if 'published_files' in version_data.keys():
                new_pub_ids = {n['id'] for n in version_data['published_files']}
                version_data['published_files'].extend(p for p in existing_version['published_files'] if p['id'] not in new_pub_ids)
            if 'playlists' in version_data.keys():
                new_playlist_ids = {n['id'] for n in version_data['playlists']}
                version_data['playlists'].extend(p for p in existing_version['playlists'] if p['id'] not in new_playlist_ids)
            self.conn.sg.update('Version', existing_version['id'], data=version_data)
            sg_version = existing_version

        # Upload preview if available
        if preview_media and preview_media.exists():
            self.conn.sg.upload('Version', sg_version['id'], preview_media.as_posix(), 'sg_uploaded_movie')

        return sg_version

    def _get_entity_name(self) -> str:
        """Extract entity name from tokens based on entity type."""
        entity_type = self.area.tokens['entity_type'].lower()
        value = self.area.tokens.get(entity_type)
        if not value:
            raise ValueError(f"Unsupported entity type: {entity_type}")
        return value

    def _find_or_create_task(self, project_id: int, entity: dict, task_name: str) -> dict:
        """Find or create a task for the given entity."""
        filters = [
            ['project', 'is', {'type': 'Project', 'id': project_id}],
            ['entity', 'is', entity],
            ['content', 'is', task_name]
        ]

        task = self.conn.sg.find_one('Task', filters)
        if not task:
            task_data = {
                'project': {'type': 'Project', 'id': project_id},
                'entity': entity,
                'content': task_name,
                'sg_status_list': 'wtg'
            }
            # task = self.conn.sg.create('Task', task_data)
        return task

    def _generate_path_cache(self, full_path: Path) -> str:
        """Generate the path_cache value for a given path."""
        project_root = Path(self.area.tokens['volume'])

        # Remove root path and ensure proper format
        path_cache = full_path.relative_to(project_root).as_posix()
        return path_cache

    @staticmethod
    def _get_publish_type(output_type: OutputType) -> dict:
        """Map OutputType to SG PublishedFileType."""
        type_mapping = {
            OutputType.RENDER_LAYER: {'type': 'PublishedFileType', 'id': 1},
            OutputType.RENDER_STILL: {'type': 'PublishedFileType', 'id': 1},
            OutputType.OUTPUT: {'type': 'PublishedFileType', 'id': 6},
            OutputType.PLAYBLAST: {'type': 'PublishedFileType', 'id': 4},
            OutputType.THUMBNAIL: {'type': 'PublishedFileType', 'id': 5},
            OutputType.TEXTURE: {'type': 'PublishedFileType', 'id': 2},
            OutputType.SCENE: {'type': 'PublishedFileType', 'id': 3}
            # ... add other mappings
        }
        publish_type = type_mapping.get(output_type, None)
        if not publish_type:
            raise AttributeError(f"Couldn't find valid PublishedFileType for {output_type}")
        return publish_type

    def do_publish(self, output_items: Optional[OutputItem] = None, description: Optional[str] = None,
                   add_to_dailies: bool = False):
        if output_items is None:
            output_items = self.area.get_output_items(existing_only=True, types=OutputType.get_all())

        self._local_publish(output_items)
        self._sg_publish(output_items, description, add_to_dailies)


def demo():
    root = "P:/pipe_projects"  # Home-dev
    root = "X:/BM_Drive/projects"  # Bidaya-Triofox
    input_scene = Path(root + r"\MansourSara\06_Shots\episodes\000\sequences\010\shots\0010\anim\blender\work\mns_000_010_0010_anim_animation_main_001.blend")
    # input_scene = Path(root + r"\MansourSara\05_Assets\chr\devChar\art\blender\work\mns_chr_devChar_art_design_main_002.blend")

    # Define Tokens and Template
    teide = filePath.FilePath()
    w_template = teide.getTemplateByName('t_shot_dcc_work_scene')
    # w_template = teide.getTemplateByName('t_asset_dcc_work_scene')
    tokens = teide.parseFilepath(input_scene, w_template)
    _logger.info(tokens)

    full_tokens = tokens.copy()
    full_tokens['entity_type'] = 'shot'
    # full_tokens['entity_type'] = 'asset'
    full_tokens['dcc'] = 'blender'
    full_tokens['mode'] = 'work'
    full_tokens['element_type'] = 'scene'

    # WORK AREA
    area = WorkArea.from_tokens(tokens=full_tokens)

    # Exercise: Show compatible preview media
    # products = area.previewMedia
    # _logger.info('-------------------------------')
    # for prod in products:
    #     _logger.info(prod.name)
    #     for job in prod.publish_jobs:
    #         _logger.info(job)

    # Exercise: Lets create a w_path to store an output (ex: anim cache)
    # for product in area.get_output_items_by_type(types=[OutputType.OUTPUT]):
    #     export_anm_cam_tokens = area.tokens.copy()
    #     export_anm_cam_tokens['descriptor'] = "renderCam"
    #     export_anm_cam_tokens['ext'] = "abc"
    #
    #     w_out_path = area.teide.buildFilepath(template=product.w_template, tokenOverrides=export_anm_cam_tokens)
    #     _logger.info(w_out_path)
    #
    #     p_out_path = area.teide.buildFilepath(template=product.p_template, tokenOverrides=export_anm_cam_tokens)
    #     _logger.info(p_out_path)

    # Exercise: Lets list all existing_products of multiple working areas

    # from teide_core import db
    # conn = db.SGAuthentication.login()
    # publisher = PublishAPI(conn=conn, area=area)

    # Exercise: Lets do a local publish
    # publisher.do_publish()


if __name__ == '__main__':
    demo()
