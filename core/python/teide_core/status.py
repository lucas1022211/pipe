""" Comment
"""
import enum
from typing import TYPE_CHECKING

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class BaseStatuses(enum.Enum):
    @classmethod
    def get_order(cls, status):
        # Get the order based on the enum member's position
        for index, member in enumerate(cls):
            if member.value == status:
                return index
        # Return a large number if the status is not found, to push it to the end
        return float('inf')


class VersionStatuses(BaseStatuses):
    FEEDBACK_GIVEN = "fbg"
    PENDING_REVIEW = "rev"
    BYPASS = "bypass"
    ART_DIR_APP = "ardiap"
    DIR_APP = "dirapp"
    EXEC_APP = "eap"
    READY_FOR_DELIVERY = "rddlv"
    DELIVERED = "dlvr"


class TaskStatuses(BaseStatuses):
    OMIT = "omt"
    WAITING_TO_START = "wtg"
    IN_PROGRESS = "ip"
    FEEDBACK_GIVEN = "fbg"
    PENDING_REVIEW = "rev"
    BYPASS = "bypass"
    ART_DIR_APP = "ardiap"
    DIR_APP = "dirapp"
    EXEC_APP = "eap"
    READY_FOR_DELIVERY = "rddlv"
    DELIVERED = "dlvr"


class ShotOrAssetStatuses(BaseStatuses):
    OMIT = "omt"
    WAITING_TO_START = "wtg"
    IN_PROGRESS = "ip"
    FINAL = "fin"


OPEN_TASK_STATUSES = [TaskStatuses.WAITING_TO_START, TaskStatuses.IN_PROGRESS, TaskStatuses.FEEDBACK_GIVEN,
                      TaskStatuses.PENDING_REVIEW]


# fixme: right now we dont have a status api per se, just the enum classes.
#  We need to define a StatusGraph() for Versions, Tasks, ShotOrAssets
#  Define Global Status and Actions enum classes, then pass only the needed ones to each StatusGraph()
#  RND: What Actions are needed and how are they used in the pipeline/workflow.
#  RND: What statuses are needed. Extract from database (SG) maybe can start as a manual copy, but eventually should
#  be driven by either the config settings or pulled from the DB(SG).
#  // Production managers might need to rename/adjust project statuses, actions and graph.


# # --All available Status and Actions
# # Statuses: Not Started | Wip | In Review | Ready to Publish | Publish | Locked
# # Actions: Start | Submit for Review | Publish | Feedback | Approve | Rollback | Lock | Unlock
#
# # --Actions by status
# # Not Started: Start
# # Wip: Submit for Review
# # In Review: Feedback, Approve
# # Ready to Publish: Publish
# # Publish: Feedback, Rollback, Lock
# # Locked: Unlock
#
# # --Status set by Action
# # [start] -> Wip
# # [Submit for Review] -> In Review
# # [Feedback] -> Wip
# # [Rollback] -> Publish
# # [Approve] -> Ready to Publish
# # [Publish] -> Publish
# # [Lock] -> Locked
# # [Unlock] -> Publish
#
#
# @enum.unique
# class Status(enum.Enum):
#     NOT_STARTED = "Not Started"
#     WIP = "Wip"
#     IN_REVIEW = "In Review"
#     READY_TO_PUBLISH = "Ready to Publish"
#     PUBLISH = "Publish"
#     LOCKED = "Locked"
#
#
# @enum.unique
# class Action(enum.Enum):
#     START = "Start"
#     SUBMIT_FOR_REVIEW = "Submit for Review"
#     PUBLISH = "Publish"
#     FEEDBACK = "Feedback"
#     APPROVE = "Approve"
#     ROLLBACK = "Rollback"
#     LOCK = "Lock"
#     UNLOCK = "Unlock"
#
#
# class StatusGraph(object):
#     def __init__(self):
#         self.status_actions = {}
#         self.action_statuses = {}
#
#     def set_status_actions(self, status_actions):
#         self.status_actions = status_actions
#
#     def set_action_statuses(self, action_statuses):
#         self.action_statuses = action_statuses
#
#     def get_next_status(self, current_status, action):
#         if action in self.get_statusActions(current_status):
#             return self.action_statuses[action]
#         else:
#             raise ValueError(f"Invalid action '{action}' for status '{current_status}'")
#
#     def get_statusActions(self, current_status):
#         return self.status_actions.get(current_status, [])
#
#     def validate_action_for_status(self, current_status, action):
#         return action in self.status_actions.get(current_status, [])
#
#
# status_actions = {
#     Status.NOT_STARTED: [Action.START],
#     Status.WIP: [Action.SUBMIT_FOR_REVIEW],
#     Status.IN_REVIEW: [Action.FEEDBACK, Action.APPROVE],
#     Status.READY_TO_PUBLISH: [Action.PUBLISH],
#     Status.PUBLISH: [Action.FEEDBACK, Action.ROLLBACK, Action.LOCK],
#     Status.LOCKED: [Action.UNLOCK]
# }
#
# action_statuses = {
#     Action.START: Status.WIP,
#     Action.SUBMIT_FOR_REVIEW: Status.IN_REVIEW,
#     Action.FEEDBACK: Status.WIP,
#     Action.ROLLBACK: Status.PUBLISH,
#     Action.APPROVE: Status.READY_TO_PUBLISH,
#     Action.PUBLISH: Status.PUBLISH,
#     Action.LOCK: Status.LOCKED,
#     Action.UNLOCK: Status.PUBLISH
# }
# statusAPI = StatusGraph()
# statusAPI.set_status_actions(status_actions)
# statusAPI.set_action_statuses(action_statuses)
#
#
# def iterStatus(status):
#     if status == Status.LOCKED:
#         exit()
#     options = statusAPI.get_statusActions(status)
#     for option in reversed(options):
#         nextStatus = statusAPI.get_next_status(status, option)
#         print('{2}\n\t{0} -> {1}\n{3}'.format(options, option, status, nextStatus))
#         iterStatus(nextStatus)

