import os
import pathlib
import platform
from typing import TYPE_CHECKING, Optional, Any, Union

import attr
import files

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

# todo: move to a new constants.py module
#   use case: constants.TEIDE_FRAMEWORK_PATH  (in the backend it will try to use the envVar; else Default value)
_DEFAULT_TEIDE_FRAMEWORK_PATH = files.get_parent_dir(__file__, level=4)
_DEFAULT_TEIDE_CONFIG_DIR = os.path.join(_DEFAULT_TEIDE_FRAMEWORK_PATH, 'configuration')
_DEFAULT_TEIDE_LOCAL_DIR = pathlib.Path('~/.teide').expanduser().as_posix()

MEMORY_LAYER = '_memoryLayer'
GLOBAL_LAYER_NAME = 'global'
USER_LAYER_NAME = 'user'
TEIDE_FRAMEWORK_PATH = os.environ.get('TEIDE_FRAMEWORK_PATH', _DEFAULT_TEIDE_FRAMEWORK_PATH)
TEIDE_CONFIG_DIR = os.environ.get('TEIDE_CONFIGURATION', _DEFAULT_TEIDE_CONFIG_DIR)
TEIDE_LOCAL_DIR = os.environ.get('TEIDE_LOCAL', _DEFAULT_TEIDE_LOCAL_DIR)


# todo: Validate TEIDE_CONFIG_DIR is not none.

""" 
- Parameter types: bool, str, float, int, color (use this to generate formUI)
    - plain text > str > textfield
    - True/False > bool > checkbox
    - decimal numbers > float
    - absolute numbers > int
    - color picker > color > color selector
    - pick option > int > combobox
    - add tags > list[str] >tags
    - select a filepath > str > filepath
"""


def update_nested_dict(source, edit_to_make):
    """
    Recursively updates the values in a nested dictionary.

    Parameters:
        source (dict): The original dictionary to be updated.
        edit_to_make (dict): The dictionary containing the updates.

    Returns:
        None
    """
    # todo: Rename method
    for key, val in edit_to_make.items():
        if isinstance(val, dict) and key in source and isinstance(source[key], dict):
            update_nested_dict(source[key], val)
        else:
            source[key] = val


@attr.s
class ConfigParam(object):
    """Base Parameter class."""
    # todo: add isValidPath property
    # todo: add parameter namingConvention validator

    parameter = attr.ib(type=str)
    path = attr.ib(type=dict)
    value = attr.ib(type=Optional[Any])

    @value.default
    def defaultValue(self):
        return self.getValue()

    def getValue(self, data=None):
        if data is None:
            data = self.path
        if isinstance(data, dict):
            for key, val in data.items():
                if key == self.parameter:
                    return val
                return self.getValue(val)
        return None

    def setValue(self, new_value, data=None):
        if data is None:
            data = self.path
        if isinstance(data, dict):
            for key, val in data.items():
                if key == self.parameter:
                    data[key] = new_value
                return self.setValue(new_value, val)
        self.getValue()
        return None


class ConfigLayer(object):
    """
    Base Layer class.

    """
    # todo: add validators
    #   -naming conventions
    #   -required path if name is not MEMORY_LAYER

    def __init__(self, name: str, path: Optional[str] = None):
        self.name = name
        self.path = path
        self.data: dict = dict()

    def __repr__(self):
        return f'ConfigLayer(\'{self.name}\', {self.path})'

    @property
    def isMemoryLayer(self) -> bool:
        return bool(self.name == MEMORY_LAYER)

    def read(self) -> dict:
        if self.isMemoryLayer:
            raise Exception('Memory layers cant be read. %s' % self)
        self.data = files.readJSONFile(path=self.path)
        return self.data

    def save(self):
        if self.isMemoryLayer:
            raise Exception('Memory layers cant be saved. %s' % self)
        files.writeToJSONFile(path=self.path,
                              data=self.data)

    def findOrCreate(self) -> dict:
        if self.isMemoryLayer:
            raise Exception('Memory layers cant be opened. %s' % self)
        if not self.path:
            raise AttributeError('self.path attribute is not defined.')
        if os.path.isfile(self.path):
            return self.read()
        else:
            self.save()

    def flattenParameter(self, parameter: str, layerData: Optional[dict] = None) -> dict:
        # todo: make this method return a ConfigParam object.
        #  -Merge this method with getParam()?

        if not layerData:
            layerData = self.data
        result_json = {}
        for key, value in layerData.items():
            if value and isinstance(value, dict):
                sub_json = self.flattenParameter(parameter, value)
                if sub_json:
                    result_json[key] = sub_json
        if parameter in layerData:
            result_json = {parameter: layerData[parameter]}
        return result_json

    def getParam(self, parameter: str) -> ConfigParam:
        """Given a parameterName get a ConfigParam object."""
        flatParam = self.flattenParameter(parameter=parameter)
        return ConfigParam(parameter=parameter, path=flatParam)

    def setParam(self, param: Union[str, ConfigParam], value=None):
        """Given a ConfigParam set a ConfigParam object."""
        if isinstance(param, str):
            param = self.getParam(parameter=param)

        # Set the value to the ConfigParam
        if value is not None:
            param.setValue(value)

        return update_nested_dict(self.data, param.path)


class ConfigAPI(object):
    """
    parameter can be a str as a user input but should be converted to a ConfigParam().
    There will be a check to validate the parameter flattening,
    if parameter is unique there shouldn't be any problems.
    At first, we will do all operations over the composed result, in other words using the
    ConfigAPI instead of asking an ConfigLayer object.
    """
    # todo: add error handling, fix exceptions etc...
    composedLayer: ConfigLayer = ConfigLayer(name=MEMORY_LAYER)

    def __init__(self):
        self.layerstack: List[ConfigLayer] = list()
        self.setup_baseLayer()
        self.setup_userLayer()

    def __repr__(self):
        layerStackNames = [lay.name for lay in self.layerstack]
        return f'ConfigAPI({layerStackNames})'

    def setup_baseLayer(self):
        config_path = f'{TEIDE_CONFIG_DIR}/{GLOBAL_LAYER_NAME}_config.json'
        layer = ConfigLayer(GLOBAL_LAYER_NAME, config_path)
        layer.findOrCreate()
        self.layerstack.append(layer)

    def setup_userLayer(self):
        config_path = f'{TEIDE_LOCAL_DIR}/{USER_LAYER_NAME}_config.json'
        layer = ConfigLayer(USER_LAYER_NAME, config_path)
        layer.findOrCreate()
        self.layerstack.append(layer)

    def setup_projectLayers(self):
        import glob
        pattern = f'{TEIDE_CONFIG_DIR}/projects/*_config.json'
        config_paths = glob.glob(pattern)
        for config_path in config_paths:
            name = os.path.basename(config_path).rsplit('_', 1)[0]
            layer = ConfigLayer(name, config_path)
            layer.read()
            self.layerstack.insert(1, layer)

    def find_projectConfig(self, projectName):
        """
        Given a project name, it will try to find the project config in the
        TEIDE_CONFIG_DIR. And then add it to the layer stack.
        
        Parameters
        ----------
        projectName : str
            Name of the project to find.

        Returns
        -------
        ConfigLayer
        """

        config_path = f'{TEIDE_CONFIG_DIR}/projects/{projectName}_config.json'
        if os.path.isfile(config_path):
            layer = ConfigLayer(projectName, config_path)
            layer.read()
            self.layerstack.insert(1, layer)
            return layer

        # No matching layer found, raise an error
        raise Exception(f"'{projectName}' config file not found in path.")

    def overrideParameter(self, layer: Union[int, ConfigLayer],
                          parameter: Union[str, ConfigParam],
                          value: Any) -> ConfigLayer:
        # fixme: use api.getLayerFromStack() and add str support
        opLayer = None
        if isinstance(layer, ConfigLayer):
            opLayer = layer
        elif isinstance(layer, int):
            opLayer = self.getLayerFromStack(index=layer)
        elif isinstance(layer, str):
            opLayer = self.getLayerFromStack(name=layer)
        if not opLayer:
            raise Exception('No valid layer provided.')
        if isinstance(parameter, str):
            parameter = self.layerstack[0].getParam(parameter=parameter)
        opLayer.setParam(param=parameter, value=value)
        opLayer.save()
        return opLayer

    def resolve(self, parameter: Union[str, ConfigParam]) -> Optional[Any]:
        """
        Given a parameter return the highest opinion value.

        Parameters
        ----------
        parameter : Union[str, ConfigParam]
            Name of the attribute we want to access.

        Returns
        -------
        Optional[Any]

        """
        # todo: add return typing like Union[bool, str, float, int, dict]
        #  or something like that, avoid Any.
        #  At this point this will not be a static typed output...?

        # todo: add debug argument, will show what layer was resolved.
        #  Or maybe a new method to see what layer has the highest opinion
        #  on a specific attribute.
        if isinstance(parameter, ConfigParam):
            parameter = parameter.parameter
        # Getting the values and composing the result.
        self.composedLayer.data = dict()
        for layer in self.layerstack:
            layer_param = layer.getParam(parameter).path
            update_nested_dict(self.composedLayer.data, layer_param)

        # Getting composed value
        composed_value = self.composedLayer.getParam(parameter).value
        if composed_value:
            return composed_value
        return None

    def getLayerFromStack(self, name=None, index=None):
        """
        Returns a layer from the layerstack if any matches the arguments.

        Parameters
        ----------
        name : str
            Name of the layer or name of the project.
        index : int
            layerStack index

        Returns
        -------
        ConfigLayer
        """
        if isinstance(index, int):
            # Check if the index is a valid value within the bounds of the layerstack
            if -1 <= index < len(self.layerstack):
                return self.layerstack[index]
            else:
                raise Exception(
                    'Index out of bounds [{1} to {2}]. Got {0}'.format(index, -1,
                                                                       len(self.layerstack) - 1))
        if isinstance(name, str):
            # Check if any layer's name matches the provided name
            for layer in self.layerstack:
                if layer.name == name:
                    return layer

        # If no layer matches by name, try finding the layer in the configs dir
        return self.find_projectConfig(name)


def get_attribute(attributeName, layer: Optional[Union[str, int]] = None) -> Optional[Any]:
    """Returns the resolved value of the specified attribute.
    If a layer is specified, it will get the value in that layer."""
    # todo: should we error if a layer doesnt have the attribute? or compose the layerstack
    #  up to that layer? For now im inclined to get an error.
    #  -There could be another method to check if a layer has an opinion on an attribute.
    api = ConfigAPI()
    if not layer:
        return api.resolve(attributeName)
    else:
        opLayer = None
        if isinstance(layer, int):
            opLayer = api.getLayerFromStack(index=layer)
        elif isinstance(layer, str):
            opLayer = api.getLayerFromStack(name=layer)
        if not opLayer:
            raise Exception('No valid layer provided.')
        return api.resolve(opLayer.getParam(attributeName))


def set_attribute(attributeName: str, layer, value):
    """Sets the attribute value in the specified layer.
    Layers can be specified as int, string"""
    api = ConfigAPI()
    if not layer:
        raise Exception('Not provided layer')
    else:
        opLayer = None
        if isinstance(layer, int):
            opLayer = api.getLayerFromStack(index=layer)
        elif isinstance(layer, str):
            opLayer = api.getLayerFromStack(name=layer)
        if not opLayer:
            raise Exception('No valid layer provided.')
        api.overrideParameter(opLayer, attributeName, value)


def get_volume():
    roots_attribute = get_attribute('volumes_roots', None)
    if not roots_attribute:
        return None
    return roots_attribute.get(platform.system(), None)


def get_project_data(project_name: str):
    import importlib.util
    default_project_path = os.path.join(TEIDE_CONFIG_DIR, 'projects', f'{project_name}.py')
    spec = importlib.util.spec_from_file_location(project_name, default_project_path)
    project = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(project)

    return project.DATA


# ------------------------- Testing Demo -----------------------------------

if __name__ == "__main__":
    project_data = get_project_data('defaultProject')
    print(project_data['templateKeys'])
    # set_attribute(attributeName='tokens', layer=GLOBAL_LAYER_NAME, value=DATA['templateKeys'])

    pass
    # api = ConfigAPI()
    # api.find_projectConfig('defaultProject')
    # api.overrideParameter(layer=1, parameter='fps', value=30)

    # # layerStudio = api.layers[0]
    # # layerProject = api.layers[1]
    # # layerUser = api.layers[2]
    #
    # targetA = 'resolution'
    # targetB = 'project_name'
    # targetC = 'project_shortName'
    # targetD = 'OCIO_path'
    # targetE = 'task_status'
    # targetF = 'testNest'
    # target = targetE
    #
    # test_targets = [targetA, targetB, targetC, targetD, targetE, targetF]
    # # test_targets = [targetE]  # , targetB, targetC, targetD, targetE, targetF]
    #
    # for test_target in test_targets:
    #     # Get Resolved
    #     test_value = api.resolve(test_target)
    #     print(test_target, test_value)
    #     continue
    #
    #     # Get Param
    #     test_param = layerStudio.getParam(parameter=test_target)
    #     if test_param and isinstance(test_param, ConfigParam):
    #         report = f'@{test_param.parameter} = {test_param.value}\n\t\t\t{test_param.getValue()}'
    #         print(report)
    #
    #     # Set Param
    #     test_param.setValue(new_value='1234')
    #     layerUser.setParam(test_param, value='4567')
    #     layerUser.save()

    # ------------------------- List Leaf Keys -----------------------------------
    # import collections.abc

    # def isDict(d):
    #     return isinstance(d, collections.abc.Mapping)

    # def isAtomOrFlat(d):
    #     return not isDict(d) or not any(isDict(v) for v in d.values())

    # def leafPaths(nestedDicts, noDeeper=isAtomOrFlat):
    #     """
    #         For each leaf in NESTEDDICTS, this yields a
    #         dictionary consisting of only the entries between the root
    #         and the leaf.
    #     """
    #     for key, value in nestedDicts.items():
    #         if noDeeper(value):
    #             yield {key: value}
    #         else:
    #             for subpath in leafPaths(value):
    #                 yield {key: subpath}
