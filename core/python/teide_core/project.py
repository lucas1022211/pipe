""" ProjectDataTree
{"entity-types": {
        "asset-types": {
            "<asset-type>": {
                "<asset-name>": {
                    "steps": {
                        "<step-name>": {
                            "task-name": {}
                        }
                    }
                }
            }
        },
        "episodes": {
            "<ep_name>": {
                "sequences": {
                    "<seq_name>": {
                        "shots": {
                            "<sh_name>": {
                                "steps": {
                                    "<step-name>": {
                                        "<task_name>": {}
                                    }
                                }
                            }
                        },
                        "steps": {
                            "<step-name>": {
                                "<task_name>": {}
                            }
                        }
                    }
                },
                "steps": {
                    "<step-name>": {
                        "<task_name>": {}
                    }
                }
            }
        }
    }
}

"""
from typing import TYPE_CHECKING, Any, Dict

import teide_core.helpers
from teide_core.db import Connection

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class ProjectData:
    tag_project_name = 'name'
    tag_entity_types = 'entity-types'
    tag_at = 'asset-types'
    tag_ep = 'episodes'
    tag_seq = 'sequences'
    tag_sh = 'shots'
    tag_sg = 'sg_data'
    tag_steps = 'steps'
    tag_tasks = 'tasks'

    def __init__(self, project_name: str):
        self.project_name = project_name
        self.data: Dict[str, Any] = {
            self.tag_project_name: project_name,
            self.tag_entity_types: {
                self.tag_at: {},
                self.tag_ep: {}
            }
        }
        self._init_tree_templates()
    
    def _init_tree_templates(self):
        self.tree_entity_types = self.data[self.tag_entity_types]
        self.tree_asset_types = self.tree_entity_types[self.tag_at]
        self.tree_episodes = self.tree_entity_types[self.tag_ep]

    def add_asset_type(self, asset_type: str) -> Dict[str, Any]:
        if asset_type not in self.tree_asset_types:
            self.tree_asset_types[asset_type] = {}
        return self.tree_asset_types[asset_type]

    def add_asset(self, asset_type: str, asset_name: str, sg_asset: dict) -> Dict[str, Any]:
        if asset_name not in self.tree_asset_types[asset_type]:
            self.tree_asset_types[asset_type][asset_name] = {self.tag_steps: {}, self.tag_sg: sg_asset}
        return self.tree_asset_types[asset_type][asset_name]

    def add_episode(self, episode_name: str, sg_episode: dict) -> Dict[str, Any]:
        if episode_name not in self.tree_episodes:
            self.tree_episodes[episode_name] = {self.tag_seq: {}, self.tag_steps: {}, self.tag_sg: sg_episode}
        return self.tree_episodes[episode_name]

    def add_sequence(self, episode_name: str, sequence_name: str, sg_sequence: dict) -> Dict[str, Any]:
        if sequence_name not in self.tree_episodes[episode_name][self.tag_seq]:
            self.tree_episodes[episode_name][self.tag_seq][sequence_name] = {
                self.tag_sh: {}, self.tag_steps: {}, self.tag_sg: sg_sequence}
        return self.tree_episodes[episode_name][self.tag_seq][sequence_name]

    def add_shot(self, episode_name: str, sequence_name: str, shot_name: str, sg_shot: dict) -> Dict[str, Any]:
        if shot_name not in self.tree_episodes[episode_name][self.tag_seq][sequence_name][self.tag_sh]:
            self.tree_episodes[episode_name][self.tag_seq][sequence_name][self.tag_sh][shot_name] = {
                self.tag_steps: {}, self.tag_sg: sg_shot}
        return self.tree_episodes[episode_name][self.tag_seq][sequence_name][self.tag_sh][shot_name]

    def add_step(self, entity: dict, step_name: str) -> Dict[str, Any]:
        if step_name not in entity[self.tag_steps].keys():
            entity[self.tag_steps][step_name] = {}
        return entity[self.tag_steps][step_name]

    @staticmethod
    def add_task(entity: dict, task_name: str) -> Dict[str, Any]:
        if task_name not in entity.keys():
            entity[task_name] = None
        return entity[task_name]

    def get_asset_types(self) -> Dict[str, Any]:
        """Retrieve all asset types."""
        return self.tree_asset_types

    def get_assets(self, asset_type: str) -> Dict[str, Any]:
        """Retrieve all asset types."""
        return self.tree_asset_types[asset_type]

    def get_episodes(self) -> Dict[str, Any]:
        """Retrieve all episodes."""
        return self.tree_episodes

    def get_sequences(self, episode_name: str) -> Dict[str, Any]:
        """Retrieve all sequences for a specific episode."""
        return self.tree_episodes[episode_name][self.tag_seq]

    def get_shots(self, episode_name: str, sequence_name: str) -> Dict[str, Any]:
        """Retrieve all shots for a specific episode and sequence."""
        return self.tree_episodes[episode_name][self.tag_seq][sequence_name][self.tag_sh]

    def get_entity_steps(self, entity: Dict[str, Any]) -> Dict[str, Any]:
        return entity[self.tag_steps]

    def get_entity_tasks(self, entity: Dict[str, Any], step_name: str) -> Dict[str, Any]:
        if step_name not in entity[self.tag_steps]:
            return {}
        return entity[self.tag_steps][step_name]

    def fetch_steps_tasks(self, entity, tasks):
        steps_and_tasks = {}
        for task in tasks:
            if not task or not task.get('step'):
                continue
            step = task['step']['name']
            task_name = task['content']

            if step not in steps_and_tasks:
                steps_and_tasks[step] = {self.tag_tasks: {}}
            steps_and_tasks[step][self.tag_tasks][task_name] = {}

        for step, tasks in steps_and_tasks.items():
            i_step = self.add_step(entity, step)
            for task_name in tasks[self.tag_tasks]:
                self.add_task(i_step, task_name)
        return steps_and_tasks


class SgProjectDataFactory:
    """Gathers data from external DB system and populates the BaseProject.data with the right structure."""
    # todo: merge with db.conn
    # todo: handle SG calls parallelization here.

    def __init__(self, connection: Connection, project_name: str):
        self.conn = connection
        self.project_name = project_name
        self.project_data = ProjectData(project_name)

    @classmethod
    def from_sg(cls, connection: Connection, project_name: str) -> ProjectData:
        obj = cls(connection, project_name)
        obj.fetch_and_populate()
        return obj.project_data

    def fetch_and_populate(self):
        """Fetch data from an external source and populate ProjectData."""
        # Assets
        sg_assets = self.conn.get_assets(self.project_name, extra_fields=['tasks'])

        for sg_asset in sg_assets:
            asset_type = sg_asset.get('sg_asset_type')
            asset_name = sg_asset['code']

            # Populate ProjectData
            self.project_data.add_asset_type(asset_type)
            self.project_data.add_asset(asset_type, asset_name, sg_asset)

        # Episodes
        sg_episodes = self.conn.get_episodes_from_project(self.project_data.project_name)
        for sg_episode in sg_episodes:
            episode_name = sg_episode['code']
            # Populate ProjectData
            self.project_data.add_episode(episode_name, sg_episode)

        # Sequences
        sg_sequences = self.conn.get_sequences_from_project(self.project_data.project_name)
        for sg_sequence in sg_sequences:
            episode_name = sg_sequence['episode']['name']
            sequence_name = sg_sequence['code']
            # Populate ProjectData
            self.project_data.add_sequence(episode_name, sequence_name, sg_sequence)

        # Shots
        sg_shots = self.conn.get_shots(self.project_data.project_name, sequence=True)
        for sg_shot in sg_shots:
            sg_episode = sg_shot.get('sg_episode')
            sg_sequence = sg_shot.get('sg_sequence')

            if not sg_episode or not sg_sequence:
                continue

            episode_name = sg_episode['name']
            sequence_name = sg_sequence['name']
            shot_name = sg_shot['code']

            # Populate ProjectData
            self.project_data.add_shot(episode_name, sequence_name, shot_name, sg_shot)


def main():
    import pprint
    from teide_core.db import SGAuthentication

    conn = SGAuthentication.login()

    # Gather ProjectData from SG
    project_name = 'defaultProject'
    project_dtree = SgProjectDataFactory.from_sg(conn, project_name)

    # Query full data tree
    pp = pprint.PrettyPrinter(sort_dicts=False, compact=True)
    pp.pprint(project_dtree.data)


if __name__ == '__main__':
    main()
