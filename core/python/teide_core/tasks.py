"""
Tasks Module

Describes a Task Entity and sets a boilerplate to generate it from the DB.
The needed entities will subclass from BaseEntity.
"""
import os
import datetime
from functools import cached_property
import enum
from pathlib import Path
from attrs import define, field, asdict
from typing import TYPE_CHECKING, Optional, Type, TypeVar, Dict, List, Any
from datetime import date

import attr

import teide_core.helpers
from teide_core.status import TaskStatuses, OPEN_TASK_STATUSES
from teide_core import settings

_logger = teide_core.helpers.get_teide_logger(__name__)


T = TypeVar("T", bound="BaseClass")

DATE_FORMAT = "%Y-%m-%d"
DEFAULT_FIELD_MAPPING = {}


def date_converter(date_str: str) -> Optional[datetime.date]:
    if isinstance(date_str, str):
        return datetime.datetime.strptime(date_str, DATE_FORMAT).date()
    return None


def status_converter(status_str: str) -> Optional[TaskStatuses]:
    if isinstance(status_str, str):
        return TaskStatuses(status_str)
    return None


@define
class BaseClass:
    id: int = field()

    @classmethod
    def from_db(cls, sg_data: Dict[str, Any], field_mapping: Dict[str, str] = DEFAULT_FIELD_MAPPING) -> 'BaseClass':
        """
        Create an instance of the class from a DB response.
        Field mapping allows for different DB column names and class attribute names.
        """
        mapped_data = {}

        for db_field, class_attr in field_mapping.items():
            if '.' in db_field:
                n_entity, n_field = db_field.split('.', 1)
                entity = sg_data.get(n_entity)
                if entity:
                    mapped_data[class_attr] = entity[n_field]
            elif db_field in sg_data:
                mapped_data[class_attr] = sg_data[db_field]

        return cls(**mapped_data)

    def get_attributes(self) -> Dict[str, str]:
        """
        Get the attributes as a dictionary.

        Returns
        -------
        Dict[str, str]
            Dictionary containing attribute names and values.
        """
        return asdict(self)


@define
class Task(BaseClass):
    name: str = field()
    prName: str = field()
    assignees: List['User'] = field(factory=list)
    reviewers: List['User'] = field(factory=list)
    status: Optional[TaskStatuses] = field(default=None, converter=status_converter)
    step: Optional['Step'] = field(default=None)
    linked_entity: Optional['Entity'] = field(default=None)
    start_date: Optional[datetime.date] = field(default=None, converter=date_converter)
    due_date: Optional[datetime.date] = field(default=None, converter=date_converter)
    priority: Optional['Priority'] = field(default=None)
    notes: List['Note'] = field(factory=list)
    time_bid: float = field(default=0)
    time_logged: float = field(default=0)
    _sg_data: dict = field(factory=dict)

    @classmethod
    def from_db(cls, sg_data: Dict[str, Any], **kwargs) -> "Task":
        # fixme: all required attributes have to be defined in the mapping even if they map to the same name.
        #   Lets make it so we only use the field mapping for the situations where an override is needed. ex: id
        #   A way of blacklisting attributes might be needed so they are not considered for the missing keys checks.
        field_mapping = {
            "id": "id",
            "content": "name",
            "project.name": "prName",
            "task_assignees": "assignees",
            "task_reviewers": "reviewers",
            "sg_status_list": "status",
            "step.name": "step",
            "entity": "linked_entity",
            "start_date": "start_date",
            "due_date": "due_date",
            "sg_priority_1": "priority",
            "open_notes": "notes",
            "est_in_mins": "time_bid",
            "time_logs_sum": "time_logged",
        }
        task_obj = super().from_db(sg_data, field_mapping)
        task_obj._sg_data = sg_data
        return task_obj

    @cached_property
    def display_name(self):
        """display_name == link.step.task"""
        # todo: count days from today to due_date; that is what we will show, days remaining.
        # now = datetime.datetime.utcnow() + datetime.timedelta(hours=4)
        prefix = ""
        asset_type = self._sg_data.get('entity.Asset.sg_asset_type')
        episode = self._sg_data.get('entity.Shot.sg_episode')
        sequence = self._sg_data.get('entity.Shot.sg_sequence')
        if asset_type:
            prefix = f"{asset_type}_"
        else:
            if episode and sequence:
                prefix = f"{episode.get('name')}_{sequence.get('name')}_"
        return f"{prefix}{self.linked_entity.get('name')}_{self.step}_{self.name}"

    def is_shot_or_asset(self) -> bool:
        """
        Check if the linked entity is an Asset or any of Episode, Seq, Shot.
        """
        # fixme: refactor method to be return True|False if isAsset()
        # todo: implement check of linked_entity type and return bool
        pass

    def is_open(self) -> bool:
        """
        Check if the task status is in the list of open statuses.
        """
        return self.status in OPEN_TASK_STATUSES
