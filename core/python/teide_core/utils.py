import os
import datetime
import calendar
from typing import TYPE_CHECKING, Optional

import teide_core.helpers
from teide_core import db

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


def defaultTeide() -> db.Connection:
    """
    Crate Teide object with default tokens.

    :return:
    """
    teide = db.Authentication.login()
    return teide


# def initializeTeide(silent=True) -> teide_core.filePath.TeideCore:
#     """
#     Crate Teide object with local tokens.
#
#     :return:
#     """
#     from files import readJSONFile
#     path = os.environ['TEIDE_LOCAL'].replace("\\", "/")
#     teide_tokens = readJSONFile(path, fileName='tmpEnvTokens')
#     teide = filePath.TeideCore(tokens=teide_tokens)
#     if not silent:
#         print("===================================================")
#         print("Loading TEIDE FRAMEWORK environment ...")
#         print("===================================================")
#         print("")
#         print(getContextData(teide))
#         print("")
#         print("===================================================")
#     return teide


# def getContextData(teide: teide_core.filePath.TeideCore):
#     if not teide and not teide.getEnv().keys():
#         raise 'Not valid environment. - Try again in a pipeline context.'
#     teide.printStringEnv()


def get_next_weekday(date: datetime.date, week_start: int):
    """Return the next date for the given weekday (0 = Monday, 6 = Sunday)"""
    days_ahead = week_start - date.weekday()
    if days_ahead <= 0:
        days_ahead += 7
    return date + datetime.timedelta(days=days_ahead)


def get_playlist_date(date):
    if calendar.MONDAY <= date.weekday() <= calendar.FRIDAY:
        playlist_date = date
    else:
        playlist_date = get_next_weekday(date, calendar.MONDAY)
    return playlist_date


def make_dailies_playlist_name(date: datetime.datetime, name_prefix: Optional[str] = None) -> str:
    playlist_name = f"Dailies {date.day}-{date.month}-{date.year}"
    if name_prefix:
        playlist_name = ' '.join([name_prefix, playlist_name])
    return playlist_name
