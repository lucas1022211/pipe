import os
import enum
import glob
import importlib
import platform
from pathlib import Path
from typing import TYPE_CHECKING, List, Dict, Optional, Type

import attr

import cmd as command_line
import teide_core.helpers
from teide_core import settings

_logger = teide_core.helpers.get_teide_logger(__name__)

TEIDE_FRAMEWORK = settings.TEIDE_FRAMEWORK_PATH

IMAGE_EXTENSIONS = ['exr', 'tiff', 'png', 'tga', 'jpg', 'tx']
VIDEO_EXTENSIONS = ['mov', 'mp4']
GEO_EXTENSIONS = ['obj', 'fbx', 'usd', 'usda', 'usdc', 'usdz', 'bgeo', 'vdb', 'abc']


class PackageType(enum.Enum):
    """Valid Package Types"""

    DCC = 'dcc'
    CLI = 'cli'
    DEV = 'dev'
    VIEWER = 'viewer'


@attr.s
class BaseExecutable:
    name: str = attr.ib()
    nice_name: str = attr.ib(factory=str)
    args: List[str] = attr.ib(factory=list)

    def add_args(self, new_args: List[str]) -> None:
        """Add additional arguments to the executable."""
        self.args.extend(new_args)

    def get_args_string(self) -> str:
        """Return the command line arguments as a single string."""
        return ' '.join(self.args)

    def get_args_list(self) -> List[str]:
        """Return the command line arguments as a list for use with subprocess."""
        return self.args


@attr.s
class BaseVersion:
    name: str = attr.ib()
    windows_dir: Optional[str] = attr.ib(default=None)
    macos_dir: Optional[str] = attr.ib(default=None)
    linux_dir: Optional[str] = attr.ib(default=None)

    @property
    def install_dir(self) -> str:
        """Return the installation directory based on the current platform."""
        current_platform = platform.system().lower()
        if current_platform == 'windows':
            return self.windows_dir
        elif current_platform == 'darwin':
            return self.macos_dir
        elif current_platform == 'linux':
            return self.linux_dir


class BasePackage:
    """Base Package object to define common package functionality."""
    name: str = None
    nice_name: str = None
    type: PackageType = None
    versions: List[BaseVersion] = []
    extensions: List[str] = []
    default_extension: Optional[str] = None
    steps: List[str] = []
    pythonbin: str = 'python'
    executables: Dict[str, List[BaseExecutable]] = {}

    def __init__(self):
        pass

    def __repr__(self):
        return f'BasePackage(name={self.name!r})'

    @property
    def is_valid(self) -> bool:
        """Check if the package is valid (i.e., if any version's install_dir exists)."""
        return any(self._is_version_valid(version) for version in self.versions)

    def _is_version_valid(self, version: BaseVersion) -> bool:
        """Check if a specific version is valid for the current platform."""
        try:
            return os.path.exists(version.install_dir) if version.install_dir else False
        except ValueError:
            return False

    def pprint(self) -> None:
        """Print details about the package."""
        print(f'> Name: {self.name}')
        print(f'- Nice Name: {self.nice_name}')
        print(f'- Extensions: {self.extensions}')
        print(f'- Is Valid: {self.is_valid}')
        for version in self.versions:
            print(f'- Version: {version.name}')
            try:
                print(f'  - Install Directory: {version.install_dir}')
            except ValueError as e:
                print(f'  - Install Directory: {str(e)}')
        for category, execs in self.executables.items():
            for executable in execs:
                print(f'- Executable [{category}]: {executable}')

    def get_default_version(self, latest: bool = True) -> BaseVersion:
        """Return the default version, either the latest or the oldest."""
        if latest:
            for version in sorted(self.versions, key=lambda v: v.name, reverse=True):
                if self._is_version_valid(version):
                    return version
        return self.versions[-1] if self.versions else None

    def get_default_executable(self) -> Optional[BaseExecutable]:
        """Return the default executable for the package."""
        return self.executables.get('session', [None])[0]

    def build_cmd(self, version: Optional[BaseVersion] = None, executable: Optional[BaseExecutable] = None) -> List[str]:
        """Build and return the command line string to run the package."""
        version = version or self.get_default_version()
        executable = executable or self.get_default_executable()

        if version and executable:
            try:
                executable_path = str(Path(version.install_dir) / executable.name)
                return [executable_path] + executable.get_args_list()
            except ValueError as e:
                raise ValueError(f"Cannot build command: {str(e)}")
        raise ValueError('Version or executable not found.')

    def run_package(self, version: Optional[BaseVersion] = None,
                    executable: Optional[BaseExecutable] = None) -> None:
        """Run the package using the specified version and executable, with optional extra arguments."""
        try:
            cmd = self.build_cmd(version=version, executable=executable)
            self.set_env_vars()
            command_line.runCommandLine(cmd)
        except ValueError as e:
            _logger.error(f"Failed to run package: {str(e)}")
        except Exception as e:
            _logger.exception(e, exc_info=True)

    def set_env_vars(self):
        """Set the necessary environment variables for the package."""
        DCC_ROOT = os.path.join(TEIDE_FRAMEWORK, 'packages', self.name)
        DCC_BIN = os.path.join(DCC_ROOT, 'bin')
        DCC_PYTHON = os.path.join(DCC_ROOT, 'python')

        os.environ["PATH"] = os.pathsep.join((os.getenv("PATH", ""), DCC_BIN, DCC_ROOT))
        os.environ["PYTHONPATH"] = os.pathsep.join((os.getenv("PYTHONPATH", ""), DCC_PYTHON))


def get_packages(valid_only: bool = True) -> List[Type[BasePackage]]:
    """Recursively search for packages in the specified root folder."""
    packages = []
    pkg_files = glob.glob(os.path.join(TEIDE_FRAMEWORK, '**', 'pkg.py'), recursive=True)

    def load_class_from_file(class_name, file_path):
        """Load the module from the file path"""
        spec = importlib.util.spec_from_file_location("module.name", file_path)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        return getattr(module, class_name)

    for file_path in pkg_files:
        package_path = os.path.dirname(os.path.normpath(file_path))
        package_name = os.path.basename(package_path)
        module_name = f"{package_name}.pkg"

        try:
            package_class = load_class_from_file(package_name.capitalize(), os.path.normpath(file_path))

            # Check if the package class is a valid subclass
            if package_class and issubclass(package_class, BasePackage):
                if valid_only:
                    if package_class.is_valid:
                        packages.append(package_class)
                else:
                    packages.append(package_class)
            else:
                _logger.warning(
                    f"Class '{package_name.capitalize()}' in '{module_name}' is not a valid BasePackage subclass.")

        except ImportError as e:
            _logger.error(f"Failed to import '{module_name}': {e}")
        except AttributeError:
            _logger.error(f"'{package_name.capitalize()}' class not found in '{module_name}'.")

    return packages


def list_packages(expand: bool = False) -> None:
    """List all available packages, optionally expanding to show details."""
    for package in get_packages(valid_only=False):
        pkg = package()
        if expand:
            pkg.pprint()


def get_package(name: Optional[str] = None, nice_name: Optional[str] = None, valid_only: bool = True) -> BasePackage:
    """Retrieve a package by name or nice name."""
    for package in get_packages(valid_only=valid_only):
        pkg_instance = package()
        if (name and name == pkg_instance.name) or (nice_name and nice_name == pkg_instance.nice_name):
            return pkg_instance
    raise ValueError(
        f'Package not found: {name or nice_name}. Available packages: {[p.nice_name for p in get_packages()]}')
