import os
import sys
import logging
import traceback
from typing import TYPE_CHECKING, Optional

from qtpy import QtCore

if TYPE_CHECKING:
    from typing import *

TEIDE_LOGGER_NAME = 'TEIDE'
TEIDE_LOGGER_LEVEL = logging.INFO


class GlobalSignal(QtCore.QObject):
    updateConsoleWidget = QtCore.Signal(str)


TEIDE_LOGGER_SIGNAL = GlobalSignal()

teide_formatter = logging.Formatter(
    '[%(name)s] [%(asctime)s] %(levelname)-8s %(message)s',
    datefmt='%H:%M:%S'
)


class QTextEditHandler(logging.Handler):
    def __init__(self, stream=None):
        super().__init__()
        self.global_signal = TEIDE_LOGGER_SIGNAL

    def emit(self, record):
        msg = self.format(record)
        color_code = self.get_color_code(record.levelno)
        if record.exc_info:
            self.emit_with_traceback(record)
            msg = msg.split(r"Traceback")[0]
        if record.levelno == logging.CRITICAL:
            formatted_msg = f'<span style="color: {color_code}; background-color: white;">{msg}</span>'
        else:
            formatted_msg = f'<span style="color: {color_code};">{msg}</span>'
        print(f'{formatted_msg}<br>')
        self.global_signal.updateConsoleWidget.emit(formatted_msg)

    def emit_with_traceback(self, record):
        tb = ''.join(traceback.format_exception(record.exc_info[0], record.exc_info[1], record.exc_info[2]))
        color_code = self.get_color_code(logging.ERROR)
        formatted_tb = f'<pre style="color: {color_code};">{tb}</pre>'
        print(f'{formatted_tb}<br>')
        self.global_signal.updateConsoleWidget.emit(formatted_tb)

    def get_color_code(self, level: int):
        log_level_color = {
            logging.DEBUG: '#007bff',  # cyan
            logging.WARNING: '#ffc107',  # Yellow
            logging.ERROR: '#dc3545',  # red
            logging.CRITICAL: '#dc3545',  # red
        }
        return log_level_color.get(level, '')


def get_teide_logger(name: str = TEIDE_LOGGER_NAME, level: int = TEIDE_LOGGER_LEVEL,
                     log_file: Optional[str] = None) -> logging.Logger:
    """Return a logger configured for the Teide project."""
    _logger = logging.getLogger(TEIDE_LOGGER_NAME)
    _logger.setLevel(level)

    # Clean handlers
    for handler in _logger.handlers:
        _logger.removeHandler(handler)

    if os.environ.get('QT_HANDLER'):
        qt_handler = QTextEditHandler()
        _logger.addHandler(qt_handler)
    else:
        stdout_handler = logging.StreamHandler(stream=sys.stdout)
        _logger.addHandler(stdout_handler)

    for handler in _logger.handlers:
        handler.setFormatter(teide_formatter)

    return _logger


def log_uncaught_exceptions(ex_cls, ex, tb):
    """Logs uncaught exceptions to the logger."""
    _logger = get_teide_logger()
    msg = f"{ex_cls.__name__}: {str(ex)}"
    _logger.error(msg, exc_info=(ex_cls, ex, tb))


# Redirect uncaught exceptions to the logger
sys.excepthook = log_uncaught_exceptions
