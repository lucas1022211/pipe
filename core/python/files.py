#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is intended to contain all functions that work with files
like find a specific file, get the number of versions of a task in the server
and functions like that
"""
import os
import json
import subprocess
import platform


def normpath(path: str) -> str:
    return os.path.normpath(path)


def make_dir(path: str, open=False) -> str:
    """ Create a path or the dirname of a file. """
    # fix path to use / bars.
    path = normpath(path)

    # Check if directory exists
    dir_path = os.path.dirname(path)
    if not os.path.exists(dir_path):
        # Create if not
        os.makedirs(dir_path)

    # Open directory
    if open:
        openFolder(dir_path)
    return dir_path


def openFolder(dir_path):
    """ Opens in explorer the specified path. """
    # Check if is file
    if os.path.isfile(dir_path):
        # Get directory
        dir_path = os.path.dirname(dir_path)

    if not os.path.exists(dir_path):
        os.makedirs(dir_path, exist_ok=True)

    # Open directory with system file explorer
    psystem = platform.system()
    try:
        if psystem == 'Windows':
            dir_path = dir_path.replace("/", "\\")
            subprocess.call('explorer.exe "%s"' % dir_path)
        elif psystem == 'Darwin':
            subprocess.call(['open', '-R', dir_path])
        elif psystem == 'Linux':
            subprocess.Popen(['xdg-open', dir_path])
        else:
            subprocess.call(['open', dir_path])
    except Exception as e:
        raise OSError(f"{e}. Failed to open file in Explorer: {dir_path}")


def get_systemUser() -> str:
    """
    Get the username of the active user.
        Only tested in windows.
    """
    return os.environ.get("USERNAME")


def get_parent_dir(path: str, level: int = 1):
    """
    Returns the parent dir if level = 1; its grandparent if 2 and so on.
    """
    upper_path = path
    for _ in range(level):
        upper_path = os.path.dirname(upper_path)
    return upper_path.replace(os.sep, '/')


def writeTextToFile(path, text):
    make_dir(path)
    with open(path, 'w') as file:
        file.write(text)
        return path


def writeToJSONFile(path, data, fileName=None):
    make_dir(path)
    if fileName:
        path = path + '/' + fileName + '.json'
    with open(path, 'w') as fp:
        json.dump(data, fp, indent=4, sort_keys=True)
        return path


def readJSONFile(path, fileName=None):
    if fileName:
        path = path + '/' + fileName + '.json'
    try:
        with open(path) as json_file:
            json_data = json.load(json_file)
        return json_data
    except json.JSONDecodeError as e:
        raise ValueError(f"Invalid JSON: {e}")

