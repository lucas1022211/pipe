import sys
import subprocess
import tempfile
import platform

import teide_core.helpers

_logger = teide_core.helpers.get_teide_logger(__name__)


def runCommandLine(cmd, env=None):
    _logger.debug(msg=f"Executing command: {cmd}")
    subprocess.Popen(cmd, env=env)


def cmd_exec(cmd, show_shell=False):
    log = ''
    if show_shell:
        with tempfile.TemporaryFile() as f:
            proc = subprocess.Popen(cmd, shell=not show_shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            for c in iter(lambda: proc.stdout.read(1), b''):  # replace '' with b'' for Python 3
                sys.stdout.write(c)
                log += c
                f.write(c)

        retval = proc.wait()
        f.close()
    else:
        proc = subprocess.Popen(cmd, shell=not show_shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in proc.stdout.readlines():
            log += line
        retval = proc.wait()
        print(log)

    return retval, log


def copy_to_clipboard(text):
    psystem = platform.system()
    if psystem == 'Windows':
        command = 'clip'
        subprocess.run(command, universal_newlines=True, input=text, check=True)
    elif psystem == 'Darwin':
        command = 'pbcopy'
        subprocess.run(command, universal_newlines=True, input=text, check=True)
    else:
        command = 'xclip -selection clipboard'
        subprocess.run(command, shell=True, universal_newlines=True, input=text, check=True)


def openFolder(dir_path):
    """
    Opens in is explorer the specified path
    """
    psystem = platform.system()
    if psystem == 'Windows':
        dir_path = dir_path.replace("/", "\\")
        subprocess.call('explorer.exe "%s"' % dir_path)
    else:
        subprocess.call(['open', dir_path])
