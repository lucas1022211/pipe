import os
from typing import TYPE_CHECKING

import teide_core.helpers
import teide_core.db as db
from teide_core import settings

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


# TODO: implement sql DB api for BaseUser.
#  - User-level methods:
#       - setActive(bool)
#       - setAttribute(attribute, value)
#               -How to have read only attributes?
#       - getAttribute(attribute)
#       - delete()
#  - DB-level methods:
#       - addUser(BaseUser)
#       - getUsers() -> List[User]
#       - getUser(active | username | ID | WS | timezone | roles ) -> BaseUser


# class User:
#     def __init__(self, fullName: str, name: str, email: str, osUsername: str, initials: str, timezone: str, roles: List[str]):
#         self.fullName = fullName
#         self.name = name
#         self.email = email
#         self.osUsername = osUsername
#         self.initials = initials
#         self.timezone = timezone
#         self.roles = roles
#
#     def __repr__(self):
#         return f"User(fullName={self.fullName}, name={self.name}, email={self.email}, osUsername={self.osUsername}, initials={self.initials}, timezone={self.timezone}, roles={self.roles})"

# class UserDatabase:
#     def __init__(self, database: str):
#         self.connection = sqlite3.connect(database)
#         self.cursor = self.connection.cursor()
#
#     def create_table(self):
#         self.cursor.execute("""
#             CREATE TABLE IF NOT EXISTS users (
#                 id INTEGER PRIMARY KEY AUTOINCREMENT,
#                 fullName TEXT,
#                 name TEXT,
#                 email TEXT,
#                 osUsername TEXT,
#                 initials TEXT,
#                 timezone TEXT
#             )
#         """)
#         self.cursor.execute("""
#             CREATE TABLE IF NOT EXISTS roles (
#                 id INTEGER PRIMARY KEY AUTOINCREMENT,
#                 name TEXT
#             )
#         """)
#         self.cursor.execute("""
#             CREATE TABLE IF NOT EXISTS user_roles (
#                 user_id INTEGER,
#                 role_id INTEGER,
#                 FOREIGN KEY (user_id) REFERENCES users (id),
#                 FOREIGN KEY (role_id) REFERENCES roles (id)
#             )
#         """)
#         self.connection.commit()
#
#     def _get_or_create_role_id(self, role: str):
#         self.cursor.execute("""
#             SELECT id
#             FROM roles
#             WHERE name = ?
#         """, (role,))
#         role_id = self.cursor.fetchone()
#
#         if role_id is None:
#             self.cursor.execute("""
#                 INSERT INTO roles (name)
#                 VALUES (?)
#             """, (role,))
#             role_id = self.cursor.lastrowid
#
#         return role_id
#
#     def add_user(self, user: User):
#         self.cursor.execute("""
#             INSERT INTO users (fullName, name, email, osUsername, initials, timezone)
#             VALUES (?, ?, ?, ?, ?, ?)
#         """, (user.fullName, user.name, user.email, user.osUsername, user.initials, user.timezone))
#         user_id = self.cursor.lastrowid
#
#         try:
#             for role in user.roles:
#                 role_id = self._get_or_create_role_id(role)
#                 self.cursor.execute("""
#                     INSERT INTO user_roles (user_id, role_id)
#                     VALUES (?, ?)
#                 """, (user_id, role_id))
#         except:
#             pass
#         self.connection.commit()
#
#     def modify_user(self, user: User):
#         self.cursor.execute("""
#             UPDATE users
#             SET fullName = ?, name = ?, email = ?, osUsername = ?, initials = ?, timezone = ?
#             WHERE id = ?
#         """, (user.fullName, user.name, user.email, user.osUsername, user.initials, user.timezone, user.id))
#
#         self.cursor.execute("""
#             DELETE FROM user_roles
#             WHERE user_id = ?
#         """, (user.id,))
#
#         for role in user.roles:
#             role_id = self._get_or_create_role_id(role)
#             self.cursor.execute("""
#                 INSERT INTO user_roles (user_id, role_id)
#                 VALUES (?, ?)
#             """, (user.id, role_id))
#
#         self.connection.commit()
#
#     def remove_user(self, user: User):
#         self.cursor.execute("""
#             DELETE FROM users
#             WHERE id = ?
#         """, (user.id,))
#         self.connection.commit()
#
#     def get_all_users(self):
#         self.cursor.execute("""
#             SELECT users.id, fullName, users.name, email, osUsername, initials, timezone, roles.name
#             FROM users
#             JOIN user_roles ON users.id = user_roles.user_id
#             JOIN roles ON user_roles.role_id = roles.id
#         """)
#         rows = self.cursor.fetchall()
#
#         users = {}
#         for row in rows:
#             user_id, fullName, name, email, osUsername, initials, timezone, role = row
#             if user_id not in users:
#                 users[user_id] = User(fullName, name, email, osUsername, initials, timezone, [])
#             users[user_id].roles.append(role)
#
#         return list(users.values())
#
#     def close(self):
#         self.connection.close()

# def add_user(name,
#             fullName,
#             osUsername,
#             initials,
#             roles
#             ):
#     userData = {}
#
#     # Check name is valid. only lowerCap letters, no spaces.
#     if name.isValid():
#         userData['name'] = name
#
#     # Check fullName is valid. only letters and spaces. Support for non ascii characters.
#     if fullName.isValid():
#         userData['fullName'] = fullName
#
#     # Check osUsername is valid. only letters. Typical OS username.
#     if osUsername.isValid():
#         userData['osUsername'] = osUsername
#
#     # Check initials is valid. Generated form fullName.
#     if initials.isValid():
#         userData['initials'] = initials
#
#     # Check roles is valid. only valid roles from global settings.
#     if roles.isValid():
#         userData['roles'] = roles
#
#     # Add id. Generated automatically
#     userData['id'] = len(USERS)
#
#     USERS.append(userData)


def get_allProjects():
    """
    get projects in search path.

    :return: (list) list of string name
    """
    import re
    # get projects from search path.
    search_path = settings.get_attribute('search_project_dir')
    year_pattern = r'^(\d{4})$'
    pr_pattern = r'^(^[a-z][a-zA-Z]+)$'  # anyText starting in lowercase
    config_file = 'specs.py'

    def match(elements, pattern):
        for element in elements:
            if re.match(pattern, element):
                yield element

    projects = []
    for year in match(os.listdir(search_path), year_pattern):
        for project in match(os.listdir(f'{search_path}/{year}'), pr_pattern):
            if os.path.isfile(f'{search_path}/{year}/{project}/{config_file}'):
                projects.append(project)
    return projects

