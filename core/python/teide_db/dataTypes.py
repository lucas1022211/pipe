import enum
import random
import re
import json
import datetime
from typing import TYPE_CHECKING, List, Dict, Optional, Tuple, Any

import attr

import teide_core.helpers
import teide_core.settings as settings
# import teide_core.package as package
# import teide_core.tasks as tasks
# import teide_core.db as db

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


# -----------------DATA TYPES---------------------------
# USER = BaseUser
#   ROLE = BaseRole
# PACKAGE = package.BasePackage
# SHOT = {}
# ASSET = {}
# PROJECT = BaseProject
# TASK = tasks.Task
# STATUS = BaseStatus
#   ACTIONS
# --------------------------------------------------------

def mappingName(name):
    alphanumeric_string = re.sub(r'\W|_', '', name)
    db_name = f'id_{alphanumeric_string}'.lower()
    return str(db_name)


TYPE_MAPPING = {
    str: 'VARCHAR(255)',
    int: 'INT',
    bool: 'BOOLEAN',
    datetime.datetime: 'DATETIME',
    # datetime.timezone: 'DATE',
    float: 'FLOAT'
}


def sqlFormatter(attribute):
    """
    The idea is to return a MySQL valid type given an attribute from a BaseDataType subclass.

    """
    # Get attr name
    attr_name = attribute.name
    # Get attr type
    attr_type = attribute.type
    # Get attr value
    attr_value = None
    # Get value type
    attr_value_type = type(attr_value)

    # Branch out by name. Some attributes might need an specific treatment so
    # we can filter them out by name.

    # Branch out by type. Different types need to be encoded differently.
    # The strategy will be to refer to a mapping where we have some relationships to
    # determine what to do with each

    sqlType = type
    return sqlType


class RoleTypes(enum.Enum):
    ARTIST = 'Artist'
    REVIEWER = 'Reviewer'
    MANAGER = 'Manager'
    CLIENT = 'Client'
    DEV = 'Dev'


# Get a list of available roles
available_roles = [role.name for role in RoleTypes]
# print('Available roles: {0}\n'.format(", ".join(available_roles)))


@attr.s
class BaseDataType(object):
    # has to be a class that can be instantiated from the api, passing each argument or
    # from an undetermined connection with the DB. If it gets initialized by the API,
    # it has to have a way to be added to the DB
    # todo: find a way to have the DB id as an attr. Cant be unsync!
    # done: rename as BaseDataType() - we will have at least 3 levels:
    #   - BaseDataType()
    #   - BaseEntity(BaseDataType)
    #   - User(BaseEntity) / Task / Project / Asset / Shot (where each entity has a Table)

    def get_attributes(self):
        """Returns class attributes in a friendly way:
            (name->attrib_name; value->attrib_value)"""
        attributes = []
        for a in attr.asdict(self):
            attribute = (a, getattr(self, a))
            attributes.append(attribute)
        return attributes

    # def getKeys_dict(self):
    #     return attr.asdict(self)
    #
    # def getKeys_tuple(self):
    #     return attr.astuple(self)

    # @classmethod
    # def serializedAttrs(cls):
    #     """Gets all attributes form the class and converts them into valid data
    #     for the sql table."""
    #     data = []
    #     for attribute in attr.fields(cls):
    #         cls_attr_name = attribute.name
    #         cls_attr_type = attribute.type
    #         map_name = mappingName(cls_attr_name)
    #         map_type = TYPE_MAPPING.get(cls_attr_type, 'VARCHAR(80)')
    #
    #         # Exceptions
    #         if cls_attr_name == 'id':
    #             map_name = 'INT AUTO_INCREMENT PRIMARY KEY'
    #             continue
    #         if attribute.alias == 'unique':
    #             map_type += ' UNIQUE'
    #
    #         row = {'name': cls_attr_name, 'type': cls_attr_type,
    #                'db_name': map_name, 'db_type': map_type}
    #         data.append(row)
    #     return data

    @classmethod
    def from_row(cls, row):
        """
        Gets the DB row data and reconstructs it into class attributes.
        The round tripping should be lossless. We will use some formatters for that.
        """
        raise NotImplementedError

    def to_row(self) -> Tuple[List[str], List[str]]:
        """
        Prepares the class attributes to DB valid data returned as two lists:
            - columns: List[str], values: List[str]
        """
        raise NotImplementedError

    # @classmethod
    # def getKeys(cls):
    #     return attr.fields_dict(cls)
    #     return attr.fields(cls)
    #
    # def encodeAttrs(self):
    #     for k, v in self.getKeys().items():
    #         # print(k, v, type(v))
    #         # print(k, v['name'], v['type'], v['alias'], v['default'])
    #         # print(k, v.name, v.type, v.alias, v.default)
    #         value = None
    #         typeFormatter = v.type
    #         result = {'field': mappingName(v.name), 'value': value, 'type': typeFormatter}
    #         print(result)

    # @classmethod
    # def serialize(cls, inst=None, field=None, value=None):
    #     """
    #                         ENCODE <-> DECODE STRATEGY
    #     Should convert the class attributes to valida data for the DB table.
    #     This means that we need to run over each attribute, and get the value type.
    #     For different types we might have different rules to convert.
    #
    #     Output expected for the DB:
    #     We need to provide the column(field) <> value pairs. The value has to be one of
    #     the accepted SQL types that we map with TYPE_MAPPING.
    #     And we get the column(field) name from the attribute name using mappingName().
    #
    #     TYPE_MAPPING and mappingName() should be unified for encode-decode steps.
    #     we will call them formatters(for types) and mapping(for attr names)
    #
    #     Parameters
    #     ----------
    #     inst :
    #     field :
    #     value :
    #
    #     Returns
    #     -------
    #
    #     """
    #     if isinstance(value, list):
    #         return [cls.serialize(inst, field, val) for val in value]
    #     if isinstance(value, RoleTypes):
    #         return value.value
    #     if isinstance(value, datetime.timezone):
    #         delta = value.utcoffset(datetime.datetime.now()).total_seconds()
    #         return [str(delta), str(value)]
    #     # if isinstance(value, datetime.datetime):
    #     #     return value.isoformat()
    #     return value
    #
    # @property
    # def serializedAttrs(self):
    #     """Encode for DB"""
    #     user_document = attr.asdict(self, value_serializer=self.serialize)
    #     return user_document
    #
    # @classmethod
    # def encode(cls):
    #     """Gets all attributes form the class and converts them into valid data
    #     for the sql table."""
    #     data = []
    #     for attribute in attr.fields(cls):
    #         cls_attr_name = attribute.name
    #         cls_attr_type = attribute.type
    #         map_name = mappingName(cls_attr_name)
    #         map_type = TYPE_MAPPING.get(cls_attr_type, 'VARCHAR(80)')
    #
    #         # Exceptions
    #         if cls_attr_name == 'id':
    #             map_name = 'INT AUTO_INCREMENT PRIMARY KEY'
    #             continue
    #
    #         row = {'name': cls_attr_name, 'type': cls_attr_type,
    #                'db_name': map_name, 'db_type': map_type}
    #         print(row)
    #         data.append(row)
    #     return data
    #
    # @property
    # def deserializedAttrs(self):
    #     """Decode for obj"""
    #     user_document = attr.asdict(self, value_serializer=self.serialize)
    #     return user_document


@attr.s
class User(BaseDataType):
    """
    Base User Class. Its object is represented in the DB.

    -id:
        Unique ID number of the user. Will be used for anonymity.
        Can't be changed afterwards.
    -fullName: str
        Main input, from where we create a suggestion/default for all other required fields.
        It is expected to be a real world name and can have any letters.
        Can't be changed after wards.
    -username: str
        This is your OS username, has to be lower-case letters.
        Can't be changed after wards.
    -email: str
        This is generated from the username@DOMAIN.
        Can't be changed after wards.
    -initials: str
        Main letters of full name, it has to be 3 capital letters. Used in filenames, UI, statistics.
        Can't be changed after wards.
    -name: str
        This is the main displayed name in the UI. It should be a reasonable length. name+Lastname is the default.
        Can be changed at anytime by User.
    -job_title: str
        Description of the user's skills. Displayed in some UIs.
        Can be changed at anytime by User.
    -start_time: datetime
        Specifies the users incorporation date. Could be used to general HR purposes.
        Can be changed at anytime by Managers.
    -timezone:
        Specifies the user local timezone. Could be used to coordinate sync or general UI purpose.
        Can be changed at anytime by Managers and User.
    -roles: List[RoleTypes]
        Specifies the level of clarence in the studio. Will be used for permissions and workflow automations.
        Can be changed at anytime by Managers.
    -ws: Optional[int]
        Designated workstation. More than one user can have the same WS, but its not common.
        Can be changed at anytime by Managers.
    -active: bool
        Specifies if the user is active.
        Can be changed at anytime by Managers.
    """

    fullName = attr.ib(type=str)
    name = attr.ib(type=str)

    timezone = attr.ib(type=Optional[datetime.timezone])
    start_date = attr.ib(type=datetime.datetime)
    ws = attr.ib(type=int)

    username = attr.ib(type=str)
    email = attr.ib(type=str)
    initials = attr.ib(type=str)
    active = attr.ib(type=bool, default=True)
    job_title = attr.ib(type=str, default=str())
    roles = attr.ib(type=List[RoleTypes], default=list())

    @fullName.validator
    def validate_fullName(self, attr, value):
        if not isinstance(value, str):
            return False
        if len(value) == 0:
            return False
        return True

    @username.default
    def usernameDefault(self):
        if ' ' in self.fullName:
            fields = self.fullName.split(' ')
        else:
            return self.fullName.lower()
        username = fields[0]

        for i in range(1, len(fields)):
            if len(fields[i]) <= 3 and len(fields) >= 3:
                username += fields[i][0]+fields[i+1][0]
                return username.lower()
            else:
                username += fields[i][0]
                return username.lower()

    @initials.default
    def initialsDefault(self):
        words = self.fullName.split()
        initials = words[0][0]

        if len(words) == 1:
            initials += words[0][1:3]
            return initials.upper()
        elif len(words) == 2:
            if len(words[1]) < 2:
                initials += words[0][1]+words[1][0]
                return initials.upper()
            initials += words[1][:2]
            return initials.upper()
        else:
            for i in range(1, len(words)):
                if i+1 < len(words) and len(words[i]) <= 3 and len(words) >= 3:
                    if len(words[i+1]) <= 3:
                        initials += words[i+1][0]
                        return initials.upper()
                    initials += words[i][0]+words[i+1][0]
                    return initials.upper()
                else:
                    initials += words[i][0]
            return initials.upper()

    @initials.validator
    def validate_initials(self, attr, value):
        if not len(value) == 3:
            raise ValueError("Invalid number of characters.")

    @email.default
    def emailDefault(self):
        studio_name = settings.get_attribute('studio_name')
        studio_domain = f'{studio_name}.com'.lower()
        email = '{0}@{1}'.format(self.username, studio_domain)
        return email

    @email.validator
    def validate_email(self, attr, value):
        if not re.match(r"^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$", value):
            raise ValueError("Invalid email address: %s" % value)

    @name.default
    def nameDefault(self):
        if ' ' in self.fullName:
            fields = self.fullName.split(' ')
        else:
            return self.fullName

        name = fields[0].capitalize()

        for i in range(1, len(fields)):
            if len(fields[i]) <= 3 and len(fields) >= 3:
                name += fields[i].capitalize()+fields[i+1].capitalize()
                return name
            else:
                name += fields[i].capitalize()
                return name

    @roles.validator
    def validate_roles(self, attr, value):
        for role in value:
            if not isinstance(role, RoleTypes):
                raise ValueError(f"Invalid role: {role}")

    @timezone.default
    def timezoneDefault(self):
        NOW = datetime.datetime.now()
        return datetime.timezone(offset=NOW.astimezone().tzinfo.utcoffset(NOW),
                                 name=str(NOW.astimezone().tzinfo))

    @start_date.default
    def start_dateDefault(self):
        return datetime.datetime.now(datetime.timezone.utc)

    @ws.default
    def wsDefault(self):
        # todo: [BLOCKED] get the next available or choose from available.
        ws = random.randint(10, 99)
        return ws

    @property
    def id(self) -> int:
        import teide_core.db as db
        cmd = f"""SELECT id FROM {db.UsersTable.table_name} WHERE username='{self.username}' LIMIT 1"""
        return db.simpleQuery(cmd)[0]['id']

    @property
    def timezoneInfo(self):
        info = {'name': str(self.timezone),
                'utcoffset': self.timezone.utcoffset(datetime.datetime.now()).total_seconds()}
        return info

    @classmethod
    def from_row(cls, row):
        # Roles
        roles_json = row.get('roles')
        roles = [RoleTypes(role) for role in json.loads(roles_json)] if roles_json else None
        # Timezone
        deserialized_timezone = None
        raw_timezone = row.get('timezone', None)
        if '@' in raw_timezone:
            delta, value = raw_timezone.split('@')
            deserialized_timezone = datetime.timezone(offset=datetime.timedelta(seconds=float(delta)),
                                                      name=value)
        return cls(
            username=row['username'],
            fullName=row['fullName'],
            name=row['name'],
            initials=row['initials'],
            email=row['email'],
            start_date=row['start_date'],
            timezone=deserialized_timezone,
            ws=row['ws'],
            active=row['active'],
            job_title=row['job_title'] if 'job_title' in row else None,
            roles=roles
        )

    def to_row(self):
        columns = []
        values = []
        for attribute, value in self.get_attributes():
            columns.append(attribute)
            if attribute == 'roles':
                value = json.dumps([role.value for role in value])  # Convert roles to a JSON string
            elif attribute == 'timezone' and isinstance(value, datetime.timezone):
                delta = value.utcoffset(datetime.datetime.now()).total_seconds()
                value = f'{str(delta)}@{str(value)}'

            values.append(value)

        return columns, values


used_ids = set()


@attr.s  # fixme: DEPRECATED - use BaseDataType()
class BaseDataType_DEPRECATED(object):
    # fixme: use as reference/reuse serializers.

    @property
    def id(self):
        return

    @classmethod
    def serialize(cls, inst=None, field=None, value=None):
        if isinstance(value, list):
            return [cls.serialize(inst, field, val) for val in value]
        if isinstance(value, RoleTypes):
            return value.value
        if isinstance(value, datetime.timezone):
            delta = value.utcoffset(datetime.datetime.now()).total_seconds()
            return [str(delta), str(value)]
        # if isinstance(value, datetime.datetime):
        #     return value.isoformat()
        return value

    # @property
    # def serializedAttrs(self) -> dict[str, Any]:
    #     """Gets all attributes form the class and converts them into valid data
    #     for the sql table."""
    #     # return attr.asdict(self, value_serializer=self.serialize)

    @classmethod
    def serializedAttrs(cls):
        """Gets all attributes form the class and converts them into valid data
        for the sql table."""
        data = []
        for attribute in attr.fields(cls):
            cls_attr_name = attribute.name
            cls_attr_type = attribute.type
            map_name = mappingName(cls_attr_name)
            map_type = TYPE_MAPPING.get(cls_attr_type, 'VARCHAR(80)')

            # Exceptions
            if cls_attr_name == 'id':
                map_name = 'INT AUTO_INCREMENT PRIMARY KEY'
                continue
            if attribute.alias == 'unique':
                map_type += ' UNIQUE'

            row = {'name': cls_attr_name, 'type': cls_attr_type,
                   'db_name': map_name, 'db_type': map_type}
            data.append(row)
        return data


@attr.s  # fixme: DEPRECATED - use User()
class BaseUser_DEPRECATED(BaseDataType):
    """
    Min Initialization attributes:
    -fullName: Main input, from where we create a suggestion/default for all other required fields.
               It is expected to be a real world name and can have any letters.
               Can't be changed after wards.
    -initials: Main letters of full name, it has to be 3 capital letters. It can be used in filenames, displayed in
               user interfaces, statistics. Can't be changed after wards.
    -username: This is your OS username, has to be lower-case letters. Can't be changed after wards.
    -email: This is generated from the username@DOMAIN. Can't be changed after wards.
    -id: Unique ID number of the user. Will be used for anonymity. Can't be changed afterwards.
    -name: This is the main displayed name in the UI. It should be a reasonable length. name+Lastname is the default.
           Can be changed at anytime by User.
    -job_title: Description of the user's skills. Displayed in some UIs. Can be changed at anytime by User.
    -timezone: Specifies the user local timezone. Could be used to coordinate sync and general operations.
               Can be changed at anytime by Managers and User.
    -roles: Specifies the level of clarence in the studio. Will be used for permissions and workflow automations.
               Can be changed at anytime by Managers.
    -ws: Designated Work Station. More than one user can have the same WS, but its not common.
         Can be changed at anytime by Managers.
    -active: Specifies if the user is active. Can be changed at anytime by Managers.
    """
    # todo: some attributes might need to be locked to be read only and cant be defined by arguments at initialization.?
    # Make defaults be a try except, if fails; we need user input.
    # username validator, make unique. first lucasm if overlap try lmorante if overlap manual input.
    # add validators to check unique id, username, initials

    fullName = attr.ib(type=str)
    username = attr.ib(type=str)
    initials = attr.ib(type=str)
    email = attr.ib(type=str)
    name = attr.ib(type=str)
    active = attr.ib(type=bool, default=True)
    # roles = attr.ib(type=Optional[List[RoleTypes]], default=None)
    job_title = attr.ib(type=Optional[str], default=None)
    # timezone = attr.ib(type=Optional[datetime.timezone])
    start_date = attr.ib(type=Optional[datetime.datetime])
    ws = attr.ib(type=int)
    id = attr.ib(type=int)

    @fullName.validator
    def validate_fullName(self, attr, value):
        if not isinstance(value, str):
            return False
        if len(value) == 0:
            return False
        return True

    @username.default
    def usernameDefault(self):
        if ' ' in self.fullName:
            fields = self.fullName.split(' ')
        else:
            return self.fullName.lower()
        username = fields[0]

        for i in range(1, len(fields)):
            if len(fields[i]) <= 3 and len(fields) >= 3:
                username += fields[i][0]+fields[i+1][0]
                return username.lower()
            else:
                username += fields[i][0]
                return username.lower()

    @initials.default
    def initialsDefault(self):
        words = self.fullName.split()
        initials = words[0][0]

        if len(words) == 1:
            initials += words[0][1:3]
            return initials.upper()
        elif len(words) == 2:
            if len(words[1]) < 2:
                initials += words[0][1]+words[1][0]
                return initials.upper()
            initials += words[1][:2]
            return initials.upper()
        else:
            for i in range(1, len(words)):
                if i+1 < len(words) and len(words[i]) <= 3 and len(words) >= 3:
                    if len(words[i+1]) <= 3:
                        initials += words[i+1][0]
                        return initials.upper()
                    initials += words[i][0]+words[i+1][0]
                    return initials.upper()
                else:
                    initials += words[i][0]
            return initials.upper()

    @initials.validator
    def validate_initials(self, attr, value):
        if not len(value) == 3:
            raise ValueError("Invalid number of characters.")

    @email.default
    def emailDefault(self):
        studio_name = settings.get_attribute('studio_name')
        studio_domain = f'{studio_name}.com'.lower()
        email = '{0}@{1}'.format(self.username, studio_domain)
        return email

    @email.validator
    def validate_email(self, attr, value):
        if not re.match(r"^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$", value):
            raise ValueError("Invalid email address: %s" % value)

    @name.default
    def nameDefault(self):
        if ' ' in self.fullName:
            fields = self.fullName.split(' ')
        else:
            return self.fullName

        name = fields[0].capitalize()

        for i in range(1, len(fields)):
            if len(fields[i]) <= 3 and len(fields) >= 3:
                name += fields[i].capitalize()+fields[i+1].capitalize()
                return name
            else:
                name += fields[i].capitalize()
                return name

    # @roles.validator
    # def validate_roles(self, attr, value):
    #     for role in value:
    #         if not isinstance(role, RoleTypes):
    #             raise ValueError(f"Invalid role: {role}")

    # @timezone.default
    # def timezoneDefault(self):
    #     NOW = datetime.datetime.now()
    #     return datetime.timezone(offset=NOW.astimezone().tzinfo.utcoffset(NOW),
    #                              name=str(NOW.astimezone().tzinfo))

    @start_date.default
    def start_dateDefault(self):
        return datetime.datetime.now(datetime.timezone.utc)

    @ws.default
    def wsDefault(self):
        # todo: [BLOCKED] get the next available or choose from available.
        ws = random.randint(10, 99)
        return ws

    @id.default
    def idDefault(self):
        # todo: [BLOCKED] make a unique id per user
        while True:
            user_id = random.randint(10000000, 99999999)  # Generate 8-digit random number
            if user_id not in used_ids:
                used_ids.add(user_id)
                return user_id

    @property
    def timezoneInfo(self):
        info = {'name': str(self.timezone),
                'utcoffset': self.timezone.utcoffset(datetime.datetime.now()).total_seconds()}
        return info

    @classmethod
    def get_columns(cls):
        columns = {}
        for attribute in attr.fields(cls):
            column_name = attribute.name
            if column_name == 'id':
                columns[column_name] = 'INT AUTO_INCREMENT PRIMARY KEY'
                continue
            column_type = TYPE_MAPPING.get(attribute.type, 'VARCHAR(255)')
            columns[column_name] = column_type
        return columns


# --All available Status and Actions
# Statuses: Not Started | Wip | In Review | Ready to Publish | Publish | Locked
# Actions: Start | Submit for Review | Publish | Feedback | Approve | Rollback | Lock | Unlock

# --Actions by status
# Not Started: Start
# Wip: Submit for Review
# In Review: Feedback, Approve
# Ready to Publish: Publish
# Publish: Feedback, Rollback, Lock
# Locked: Unlock

# --Status set by Action
# [start] -> Wip
# [Submit for Review] -> In Review
# [Feedback] -> Wip
# [Rollback] -> Publish
# [Approve] -> Ready to Publish
# [Publish] -> Publish
# [Lock] -> Locked
# [Unlock] -> Publish


@enum.unique
class Status(enum.Enum):
    NOT_STARTED = "Not Started"
    WIP = "Wip"
    IN_REVIEW = "In Review"
    READY_TO_PUBLISH = "Ready to Publish"
    PUBLISH = "Publish"
    LOCKED = "Locked"


@enum.unique
class Action(enum.Enum):
    START = "Start"
    SUBMIT_FOR_REVIEW = "Submit for Review"
    PUBLISH = "Publish"
    FEEDBACK = "Feedback"
    APPROVE = "Approve"
    ROLLBACK = "Rollback"
    LOCK = "Lock"
    UNLOCK = "Unlock"


class StatusGraph(object):
    def __init__(self):
        self.status_actions = {}
        self.action_statuses = {}

    def set_status_actions(self, status_actions):
        self.status_actions = status_actions

    def set_action_statuses(self, action_statuses):
        self.action_statuses = action_statuses

    def get_next_status(self, current_status, action):
        if action in self.get_statusActions(current_status):
            return self.action_statuses[action]
        else:
            raise ValueError(f"Invalid action '{action}' for status '{current_status}'")

    def get_statusActions(self, current_status):
        return self.status_actions.get(current_status, [])

    def validate_action_for_status(self, current_status, action):
        return action in self.status_actions.get(current_status, [])


status_actions = {
    Status.NOT_STARTED: [Action.START],
    Status.WIP: [Action.SUBMIT_FOR_REVIEW],
    Status.IN_REVIEW: [Action.FEEDBACK, Action.APPROVE],
    Status.READY_TO_PUBLISH: [Action.PUBLISH],
    Status.PUBLISH: [Action.FEEDBACK, Action.ROLLBACK, Action.LOCK],
    Status.LOCKED: [Action.UNLOCK]
}

action_statuses = {
    Action.START: Status.WIP,
    Action.SUBMIT_FOR_REVIEW: Status.IN_REVIEW,
    Action.FEEDBACK: Status.WIP,
    Action.ROLLBACK: Status.PUBLISH,
    Action.APPROVE: Status.READY_TO_PUBLISH,
    Action.PUBLISH: Status.PUBLISH,
    Action.LOCK: Status.LOCKED,
    Action.UNLOCK: Status.PUBLISH
}
statusAPI = StatusGraph()
statusAPI.set_status_actions(status_actions)
statusAPI.set_action_statuses(action_statuses)


def iterStatus(status):
    if status == Status.LOCKED:
        exit()
    options = statusAPI.get_statusActions(status)
    for option in reversed(options):
        nextStatus = statusAPI.get_next_status(status, option)
        print('{2}\n\t{0} -> {1}\n{3}'.format(options, option, status, nextStatus))
        iterStatus(nextStatus)


# iterStatus(status)
# Starting from status "IN_REVIEW", we list the available options [],
# status = Status.READY_TO_PUBLISH
# available_actions = statusAPI.get_statusActions(status)
# selected_action = available_actions[-1]
# nextStatus = statusAPI.get_next_status(status, selected_action)
# print("From the status: ", status.name)
# print("We have this options: ", [action.name for action in available_actions], f"We select {selected_action.name}.")
# print("This is the new status: ", nextStatus.name)


@attr.s
class BaseProject(object):
    # todo: add validators
    # Parameters
    name = attr.ib(type=str)
    shortName = attr.ib(type=str)
    description = attr.ib(type=str)
    fps = attr.ib(type=float)
    resolution = attr.ib(type=tuple)
    thumbnail_size = attr.ib(type=tuple)
    playblast_size = attr.ib(type=tuple)

    # Color management
    color_config = attr.ib(type=str)
    # Statuses (Status, statusTree)
    # Tasks (tasks, taskTree)
    # Outputs
    # Tokens (token - pattern)
    # Templates (template - pattern)


class Camera(object):
    focal_length = None
    focus_distance = None
    sensor_size = None
    plates = {'bg': None,
              'fg': None,
              'ref': None}


class Manifest(object):
    assets = None
    start_frame = None
    end_frame = None
    cut_order = None
    camera: Optional[Camera] = None


class BaseShot(object):
    # rename to BaseEntity?
    def __init__(self, project, shot, episode=None, sequence=None):
        self._project = project
        self._episode = str(episode)
        self._sequence = str(sequence)
        self._shot = str(shot)
        self._name = None

        # shot attributes
        self._cutOrder = None
        self._start_frame = None
        self._end_frame = None
        self.manifest: Manifest = None

        super(BaseShot, self).__init__(project, shot)

    def __str__(self):
        return self.name

    def __repr__(self):
        return "%s('%s', '%s')" % (self.__class__.__name__, self.project, self.name)

    def __reduce__(self):
        return self.__class__, (self.project.name, self.name)

    @property
    def project(self):
        """

        :return: BaseProject
        """
        return self._project

    @property
    def episode(self):
        """

        :return: str
        """
        return self._episode

    @property
    def sequence(self):
        """

        :return: str
        """
        return self._sequence

    @property
    def shot(self):
        """

        :return: str
        """
        return self._shot

    @property
    def name(self):
        """

        :return: str
        """
        fields = []
        if self._episode:
            fields.append(self._episode)
        if self._sequence:
            fields.append(self._sequence)
        fields.append(self._shot)
        self._name = '_'.join(fields)
        return self._name



# USER = BaseUser
# PACKAGE = package.BasePackage
# SHOT = {}
# ASSET = {}
# PROJECT = BaseProject
# TASK = tasks.Task
# STATUS = BaseStatus
