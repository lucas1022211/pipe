"""
Studio level

Default project settings and templates.

Concept reference:

#   GLOBAL TEMPLATES
root = 'c:/'
project_year = '2022'
project_name = 'exampleProject'
project_root = 'C:/Users/LucasMorante/Desktop/_Repos/usdResources/exampleProject'

#   ASSET TEMPLATES
asset_name = 'teapot'
asset_category = 'Model'  # [Model, Material, HDRI]
asset_type = 'prp'  # [prp, chr, cam]
ext = 'usd'
step = 'mod'  # [mod, shd, lgt]
t_asset_root = f'{project_root}/05_Assets/{asset_category}/{asset_name}'
t_model = f'{t_asset_root}/{asset_type}{asset_name}.{ext}'
t_payload_layer = f'{t_asset_root}/{asset_type}{asset_name}_payload.{ext}'
t_step_layer = f'{t_asset_root}/{asset_type}{asset_name}_{step}.{ext}'

#   SHOT TEMPLATES
sequence_name = 'seq010'
shot_name = 'shot0010'
step = 'anm'  # [lay, anm, lgt]
t_sequence_root = f'{project_root}/06_Shots/{sequence_name}'
t_shot_root = f'{project_root}/06_Shots/{sequence_name}/{shot_name}'

t_sequence = f'{t_sequence_root}/{sequence_name}.{ext}'
t_sequence_step = f'{t_sequence_root}/{step}/{sequence_name}_{step}.{ext}'

t_shot = f'{t_shot_root}/{sequence_name}_{shot_name}.{ext}'
t_shot_step = f'{t_shot_root}/{step}/{sequence_name}_{shot_name}_{step}.{ext}'

"""
ANY_TEXT = r'[a-zA-Z0-9\-]+'
NUMERIC = r'\d+'
PATH = r'[\w\-\:/]+'

DATA = {
    "project": {
        "project_name": "defaultProject",
        "project_shortName": "TST",
        "description": "Default project.",
        "fps": 24,
        "resolution": [1920, 1080],
        "year": "2024",
    },
    "templates": {
        # ENTITIES
        # ==========
        't_entity_project':     {'pattern': '{volume:%s}/{prName}' % PATH, },
        't_entity_deliveries':   {'pattern': '{t_entity_project}/01_Deliveries', },
        't_entity_preproduction': {'pattern': '{t_entity_project}/02_Preproduction', },
        't_entity_resources':   {'pattern': '{t_entity_project}/03_Resources', },
        't_entity_asset':       {'pattern': '{t_entity_project}/05_Assets/{assettype}/{asset}', },
        't_entity_episode':     {'pattern': '{t_entity_project}/06_Shots/episodes/{episode}', },
        't_entity_sequence':    {'pattern': '{t_entity_project}/06_Shots/episodes/{episode}/sequences/{sequence}', },
        't_entity_shot':        {'pattern': '{t_entity_project}/06_Shots/episodes/{episode}/sequences/{sequence}/shots/{shot}', },

        # TASKS
        # ==========
        't_task_asset':     {'pattern': '{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}', },
        't_task_sequence':  {'pattern': '{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}', },
        't_task_shot':      {'pattern': '{prShortName}_{episode}_{shot}_{step}_{task}_{variant}', },

        # COMMON
        # ==========
        't_asset_render':   {'pattern': '{t_entity_project}/09_Render/assets/{assettype}/{asset}'},
        't_asset_outputs':   {'pattern': '{t_entity_project}/08_Outputs/assets/{assettype}/{asset}'},
        't_episode_render': {'pattern': '{t_entity_project}/09_Render/episodes/{episode}'},
        't_episode_outputs': {'pattern': '{t_entity_project}/08_Outputs/episodes/{episode}'},
        't_sequence_render': {'pattern': '{t_entity_project}/09_Render/episodes/{episode}/sequences/{sequence}'},
        't_sequence_outputs': {'pattern': '{t_entity_project}/08_Outputs/episodes/{episode}/sequences/{sequence}'},
        't_shot_render': {'pattern': '{t_entity_project}/09_Render/episodes/{episode}/sequences/{sequence}/shots/{shot}'},
        't_shot_outputs': {'pattern': '{t_entity_project}/08_Outputs/episodes/{episode}/sequences/{sequence}/shots/{shot}'},

        # ASSETS
        # ==========
        # DCC
        't_asset_dcc_work_scene':       {'pattern': '{t_entity_asset}/{step}/{dcc}/work/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'Asset work scene'},
        't_asset_dcc_publish_scene':    {'pattern': '{t_entity_asset}/{step}/{dcc}/publish/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'Asset publish scene'},
        't_asset_dcc_work_thumbnail':       {'pattern': '{t_entity_asset}/{step}/{dcc}/work/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'Asset work thumbnail'},
        't_asset_dcc_publish_thumbnail':    {'pattern': '{t_entity_asset}/{step}/{dcc}/publish/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'Asset publish thumbnail'},
        't_asset_dcc_work_playblast':      {'pattern': '{t_entity_asset}/{step}/{dcc}/work/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}.{ext:(mov|mp4)}', 'description': 'Asset work playblast'},
        't_asset_dcc_publish_playblast':   {'pattern': '{t_entity_asset}/{step}/{dcc}/publish/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}.{ext:(mov|mp4)}', 'description': 'Asset publish playblast'},
        't_asset_dcc_work_output':       {'pattern': '{t_asset_outputs}/{step}/{dcc}/work/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'Asset work cache'},
        't_asset_dcc_publish_output':    {'pattern': '{t_asset_outputs}/{step}/{dcc}/publish/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'Asset publish cache'},
        't_asset_dcc_work_render_still':    {'pattern': '{t_asset_render}/{step}/{dcc}/work/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}_{frame}.{ext:(exr|jpg|tga|png|tif)}','description': 'Asset work still render'},
        't_asset_dcc_publish_render_still': {'pattern': '{t_asset_render}/{step}/{dcc}/publish/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}_{frame}.{ext:(exr|jpg|tga|png|tif)}','description': 'Asset publish still render'},
        't_asset_dcc_work_render_layer':     {'pattern': '{t_asset_render}/{step}/{dcc}/work/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'shot work render'},
        't_asset_dcc_publish_render_layer':  {'pattern': '{t_asset_render}/{step}/{dcc}/publish/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{assettype}_{asset}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'shot publish render'},

        # EPISODES
        # ==========
        # DCC
        't_episode_dcc_work_scene':             {'pattern': '{t_entity_episode}/{step}/{dcc}/work/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'episode work scene'},
        't_episode_dcc_publish_scene':          {'pattern': '{t_entity_episode}/{step}/{dcc}/publish/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'episode publish scene'},
        't_episode_dcc_work_thumbnail':         {'pattern': '{t_entity_episode}/{step}/{dcc}/work/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'episode work thumbnail'},
        't_episode_dcc_publish_thumbnail':      {'pattern': '{t_entity_episode}/{step}/{dcc}/publish/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'episode publish thumbnail'},
        't_episode_dcc_work_playblast':         {'pattern': '{t_entity_episode}/{step}/{dcc}/work/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'episode work playblast'},
        't_episode_dcc_publish_playblast':      {'pattern': '{t_entity_episode}/{step}/{dcc}/publish/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'episode publish playblast'},
        't_episode_dcc_work_output':            {'pattern': '{t_episode_outputs}/{step}/{dcc}/work/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'episode work output'},
        't_episode_dcc_publish_output':         {'pattern': '{t_episode_outputs}/{step}/{dcc}/publish/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'episode publish output'},
        't_episode_dcc_work_render_layer':      {'pattern': '{t_episode_render}/{step}/{dcc}/work/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'episode work render_layer'},
        't_episode_dcc_publish_render_layer':   {'pattern': '{t_episode_render}/{step}/{dcc}/publish/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}/renderLayer/{prShortName}_{episode}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'episode publish render_layer'},


        # SEQUENCES
        # ==========
        # DCC
        't_sequence_dcc_work_scene':            {'pattern': '{t_entity_sequence}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'Sequence work scene'},
        't_sequence_dcc_publish_scene':         {'pattern': '{t_entity_sequence}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'Sequence publish scene'},
        't_sequence_dcc_work_thumbnail':        {'pattern': '{t_entity_sequence}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'Sequence work thumbnail'},
        't_sequence_dcc_publish_thumbnail':     {'pattern': '{t_entity_sequence}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'Sequence publish thumbnail'},
        't_sequence_dcc_work_playblast':        {'pattern': '{t_entity_sequence}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}.{ext:(mov|mp4)}', 'description': 'Sequence work playblast'},
        't_sequence_dcc_publish_playblast':     {'pattern': '{t_entity_sequence}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}.{ext:(mov|mp4)}', 'description': 'Sequence publish playblast'},
        't_sequence_dcc_work_output':           {'pattern': '{t_sequence_outputs}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'sequence work output'},
        't_sequence_dcc_publish_output':        {'pattern': '{t_sequence_outputs}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'sequence publish output'},
        't_sequence_dcc_work_render_layer':     {'pattern': '{t_sequence_render}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'Sequence work render_layer'},
        't_sequence_dcc_publish_render_layer':  {'pattern': '{t_sequence_render}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{episode}_{sequence}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'Sequence publish render_layer'},


        # SHOTS
        # ==========
        # DCC
        't_shot_dcc_work_scene':            {'pattern': '{t_entity_shot}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'shot work scene'},
        't_shot_dcc_publish_scene':         {'pattern': '{t_entity_shot}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}.{ext}', 'description': 'shot publish scene'},
        't_shot_dcc_work_thumbnail':        {'pattern': '{t_entity_shot}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'shot work thumbnail'},
        't_shot_dcc_publish_thumbnail':     {'pattern': '{t_entity_shot}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}.{ext:(jpg|png)}', 'description': 'shot publish thumbnail'},
        't_shot_dcc_work_playblast':        {'pattern': '{t_entity_shot}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}.{ext:(mov|mp4)}', 'description': 'shot work playblast'},
        't_shot_dcc_publish_playblast':     {'pattern': '{t_entity_shot}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}.{ext:(mov|mp4)}', 'description': 'shot publish playblast'},
        't_shot_dcc_work_output':           {'pattern': '{t_shot_outputs}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'shot work output'},
        't_shot_dcc_publish_output':        {'pattern': '{t_shot_outputs}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}_{descriptor}.{ext}', 'description': 'shot publish output'},
        't_shot_dcc_work_render_layer':     {'pattern': '{t_shot_render}/{step}/{dcc}/work/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'shot work render'},
        't_shot_dcc_publish_render_layer':  {'pattern': '{t_shot_render}/{step}/{dcc}/publish/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}/{renderLayer}/{prShortName}_{episode}_{sequence}_{shot}_{step}_{task}_{variant}_{version:\d+}_{renderLayer}_{frame}.{ext:(exr|jpg|tga|png|tif)}', 'description': 'shot publish render'},
    },
    "templateKeys": {
        'project_path':      {'type': str, 'pattern': PATH},
        'render_path':       {'type': str, 'pattern': PATH},
        'cache_path':        {'type': str, 'pattern': PATH},
        'volume':            {'type': str, 'pattern': PATH},
        'year':              {'type': str, 'pattern': r'\d{4}'},
        'dcc':               {'type': str, 'pattern': ANY_TEXT},
        'project':           {'type': str, 'pattern': ANY_TEXT},
        'prName':            {'type': str, 'pattern': ANY_TEXT},
        'prShortName':       {'type': str, 'pattern': ANY_TEXT},
        'assettype':         {'type': str, 'pattern': ANY_TEXT},
        'asset':             {'type': str, 'pattern': ANY_TEXT},
        'episode':           {'type': str, 'pattern': ANY_TEXT, 'tip': 'Write 3 digits'},
        'sequence':          {'type': str, 'pattern': ANY_TEXT, 'tip': 'Write 3 digits'},
        'shot':              {'type': str, 'pattern': ANY_TEXT, 'tip': 'Write 4 digits'},
        'step':              {'type': str, 'pattern': ANY_TEXT},
        'task':              {'type': str, 'pattern': ANY_TEXT},
        'variant':           {'type': str, 'pattern': ANY_TEXT, 'default': 'main'},
        'lod':               {'type': str, 'pattern': ANY_TEXT},
        'version':           {'type': int, 'pattern': NUMERIC, 'format_spec': '{:03d}', 'tip': 'Write a number', 'padding': 3},
        'descriptor':        {'type': str, 'pattern': ANY_TEXT, 'default': 'main'},
        'ext':               {'type': str, 'pattern': r'[a-zA-Z]+'},
        'renderLayer':       {'type': str, 'pattern': ANY_TEXT, 'default': 'masterLayer'},
        'renderPass':        {'type': str, 'pattern': ANY_TEXT, 'default': 'beauty'},
        'frame':             {'type': int, 'pattern': r'\d{4}', 'default': 1001, 'format_spec': '{:04d}', 'padding': 4},
        'udim':              {'type': int, 'pattern': r'\d{4}', 'default': 1001, 'tip': 'Follow UDIM rules', 'padding': 4},
    },
    "publishes": {
        'dcc': {
            'entity': {
                'step': {
                    "output_items": ["scene", "thumbnail", "playblast", "output", "render_layer"],
                }
            },
            'asset': {
                'step': {
                    "output_items": ["scene", "thumbnail", "playblast", "output", "render_still", "render_layer"],
                }
            },
        }
    }
}

# templateKeys could be converted to a attr.s class in filepath.py
