# Pipe

## Freatures

- Software deployment tools. 
- My Tasks Dashboard
- Asset Management ( library of models, materials, textures ...)
- Review
- Production tracking ( asset and shot stats, data analytics ...)
- Farm submitters ( renders, caches ...)
- ... this is boring, lets code!


### Production tracking tool
- studio settings
- project settings
- asset / shot blueprints. Authoring
- Review

### Authoring tools
- ingest
- exporters
- publish tools
- submitters

## Done Tasks
- [core] configuration API
- [core] user authentication
- [core] logger

## TODO - WIP
- [core] DB API
- [core] FilePath API
- [core] entity from/to SG
- [fileSysten] lucidity templates with json/toml instead of py
- [browser] startup procedure
- [db] raise/pop error if can't establish connection. --subclass for network, credentials...