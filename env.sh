#!/bin/bash

# Define colors
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[38;5;153m"
RESET="\033[0m"

# Determine the directory where this script is located
DEFAULT_TEIDE_FRAMEWORK_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
: "${TEIDE_FRAMEWORK_PATH:=$DEFAULT_TEIDE_FRAMEWORK_PATH}"

DEFAULT_TEIDE_CONFIGURATION="$TEIDE_FRAMEWORK_PATH/configuration"
: "${TEIDE_CONFIGURATION:=$DEFAULT_TEIDE_CONFIGURATION}"


export PYTHONPATH="$TEIDE_FRAMEWORK_PATH/apps/python:$TEIDE_FRAMEWORK_PATH/core/python:$TEIDE_FRAMEWORK_PATH"
export PATH="$TEIDE_FRAMEWORK_PATH:$PATH"

export PYTHONUNBUFFERED=1

echo "${BLUE}---------------------------------------"
echo "  TEIDE Environment Loaded!"
echo "    Framework path:"
echo "    $TEIDE_FRAMEWORK_PATH"
echo "    Framework configuration:"
echo "    $TEIDE_CONFIGURATION"
echo "---------------------------------------${RESET}"

cd "${TEIDE_FRAMEWORK_PATH}" || exit
