#!/bin/bash

# Get the directory where the script is located
SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_PATH" || exit

# Create, activate, and update the virtual environment
python3 -m venv ./.venv_py39_mac_linux
source .venv_py39_mac_linux/bin/activate
python3 -m pip install --upgrade pip

# Install requirements
pip3 install -r requirements.txt
