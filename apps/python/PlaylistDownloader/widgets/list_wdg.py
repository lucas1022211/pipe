""" Comment
"""
import sys
from typing import TYPE_CHECKING, Optional, Dict, List, Set

from qtpy import QtCore, QtWidgets, QtGui

import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

ITEM_DATA_ROLE = QtCore.Qt.UserRole + 1


class BaseItemData:
    HEADER_COLUMNS = ['name']
    EXCLUDE_ATTRIBUTES = []

    def __init__(self, name):
        self.name = name

    def get_attributes(self) -> Dict[str, str]:
        """
        Get attributes of the Clip object.

        Returns
        -------
        Dict[str, str]
            Dictionary containing attribute names and values.
        """
        return {attr: getattr(self, attr) for attr in vars(self)
                if not callable(getattr(self, attr))
                and not attr.startswith("__")
                and attr not in self.EXCLUDE_ATTRIBUTES}


class BaseListModel(QtCore.QAbstractItemModel):
    ITEM_DATA_CLASS = BaseItemData

    def __init__(self, data, parent=None):
        super(BaseListModel, self).__init__(parent)
        self._data = data
        self._header_labels = self.ITEM_DATA_CLASS.HEADER_COLUMNS

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._data)

    def columnCount(self, parent=QtCore.QModelIndex()):
        if self.rowCount() > 0:
            clip_attributes = [attr for attr in self._data[0].get_attributes()]
            return len(clip_attributes)
        else:
            return 0

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if len(self._data):
            return self.createIndex(row, column)
        return QtCore.QModelIndex()

    def parent(self, index):
        return QtCore.QModelIndex()

    def data(self, index, role=QtCore.Qt.DisplayRole):
        itemData = self._data[index.row()]
        if role == QtCore.Qt.DisplayRole:
            clip_attributes = [attr for attr in itemData.get_attributes()]
            attribute_name = clip_attributes[index.column()]
            return getattr(itemData, attribute_name)
        elif role == ITEM_DATA_ROLE:
            return itemData
        return None

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self._header_labels[section]
        return None

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemNeverHasChildren

    def sort(self, column: int, order: QtCore.Qt.SortOrder = QtCore.Qt.AscendingOrder) -> None:
        self.layoutAboutToBeChanged.emit()
        self._data.sort(key=lambda x: getattr(x, list(x.get_attributes().keys())[column]))
        if order == QtCore.Qt.DescendingOrder:
            self._data.reverse()
        self.layoutChanged.emit()

    def update_data(self, new_data):
        """
        Update the underlying data of the model.

        Parameters
        ----------
        new_data: List[Clip]
            New data to replace the current data.

        Returns
        -------
        None
        """
        self.beginResetModel()
        self._data = new_data
        self.endResetModel()


class BaseListView(QtWidgets.QTreeView):
    def __init__(self, parent=None):
        super(BaseListView, self).__init__(parent)
        self.setSortingEnabled(True)
        self.setSelectionMode(QtWidgets.QTreeView.ExtendedSelection)
        self.setSelectionBehavior(QtWidgets.QTreeView.SelectRows)
        self.header().setStretchLastSection(False)

    def get_selected_items(self) -> Optional[Set[BaseItemData]]:
        selected_indexes = self.selectedIndexes()

        # Group indexes by row to avoid duplicates
        selected_items = set()
        selected_rows = {}
        for index in selected_indexes:
            if index.row() not in selected_rows:
                selected_rows[index.row()] = index
                itemData = self.model().data(index, ITEM_DATA_ROLE)
                if itemData not in selected_items:
                    selected_items.add(itemData)
        return selected_items


class BaseListWidget(QtWidgets.QWidget):
    TABLE_MODEL_CLASS = BaseListModel
    TABLE_VIEW_CLASS = BaseListView

    def __init__(self, parent=None):
        super(BaseListWidget, self).__init__(parent)

        # Create a list widget
        self.list_model = self.TABLE_MODEL_CLASS(data=[])
        self.list_view = self.TABLE_VIEW_CLASS()
        self.list_view.setModel(self.list_model)

        # Set up layout
        main_layout = QtWidgets.QVBoxLayout()
        main_layout.setAlignment(QtCore.Qt.AlignTop)
        main_layout.setContentsMargins(0, 0, 0, 0)
        main_layout.addWidget(self.list_view)
        self.setLayout(main_layout)

    def populate_list(self, data):
        """
        Populate the list with Clip objects.

        Parameters
        ----------
        data: List[Clip]
            List of Clip objects.

        Returns
        -------
        None
        """
        self.list_model.update_data(data)
        self.list_view.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    # qtawesome.dark(app)

    # from teide_core import db
    # conn = db.Authentication.login()

    widget = BaseListWidget()
    widget.show()
    demo_data = [BaseItemData(name="lucasm")]
    widget.populate_list(demo_data)

    sys.exit(app.exec())
