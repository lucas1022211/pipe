""" Comment
"""
import argparse
import os
import sys
import time
import datetime
import platform
import requests
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from typing import TYPE_CHECKING, Optional, Any, Dict, Set

import qtawesome
from qtpy import QtWidgets, QtCore, QtGui
from typing import List, Optional
from PySide6.QtWidgets import QApplication, QMainWindow, QPushButton, QVBoxLayout, QWidget, QProgressBar, QLabel, \
    QHBoxLayout
from PySide6.QtCore import Qt, QThread, Signal, Slot

from PlaylistDownloader.widgets import list_wdg

import files
import teide_core.helpers
from teide_qt import utils
from teide_qt.widgets import wdg_timeRangeFilter
from teide_core.db import Connection

if TYPE_CHECKING:
    from typing import *

if platform.system() == 'Windows':
    from ctypes import windll  # Only exists on Windows.
    app_id = u'teide.pipe.playlistDownloader.beta'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)

# done: find a way to cancel download. (wait to finish active item)
# done: add warning dialog if nothing selected when clicking Download
# done: gather selected playlists and trigger download.
# done: connect output folder browser button.
# done: if no playlists for project, empty list.
# done: add default output folder in ~/Downloads
# done: fix sorting. it should show first the most recent items.
# done: fix search filter to be compatible with sorting.
# done: add a filter widget to get this week/month playlists (make SG query faster, on by default)
# done: if description is None, False or '' shot empty field or ' '
# done: add refresh button (get proj data again from SG)
#  [next] connect button to refresh project data.
# done: add button to open output folder
# done: add checkbox to enable folder creation per playlist (default on)
# done: RnD how to handle many downloads.
#  - multithreading: make selection > download (using new thread for selection) --serial
#  - multithreading: make selection > download (using new thread for each playlist) --parallel
#  - progressbar? lock UI?
# done: adjust widget UI:
#   Projects: <select> | Modified in the last: <select> | Refresh
# done: limit description column width
# done: show number of versions contained in each playlist.
# done: add right click to show in SG
# todo: support search on all columns
# fixme: change phrasing on the timeRangeFilterWidget.
#  Modified in the last: (X days, months, years) --current
#  Modified: (Today, Last week, Last 14 days, Last month, Last n months, All time) --proposal
# fixme: refactor threading and progressbar widgets. move into teide_qt

_logger = teide_core.helpers.get_teide_logger(__name__)

DATE_FORMAT = "%Y/%m/%d %H:%M"


class Worker(QThread):
    progressChanged = Signal(int)
    taskCompleted = Signal()
    errorOccurred = Signal(str)

    def __init__(self, path: Path, versions):
        super().__init__()
        self.path = path
        self.versions = versions
        self._cancel = False

    def run(self):
        try:
            self.download_playlist(self.path, self.versions)
            if not self._cancel:
                self.taskCompleted.emit()
        except Exception as e:
            self.errorOccurred.emit(str(e))

    def cancel(self):
        self._cancel = True

    def download_version(self, url: str, path: Path) -> Optional[Path]:
        if self._cancel:
            return None
        if path.exists():
            _logger.warning(f"{path} already exists, skipping downloading")
            return None
        path.parent.mkdir(exist_ok=True, parents=True)
        try:
            with requests.get(url, stream=True) as r:
                r.raise_for_status()
                with open(path, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        f.write(chunk)
            return path
        except requests.RequestException as e:
            _logger.error(f'Failed to download file: {path.as_posix()} {str(e)}')
            return None

    def download_playlist(self, path: Path, versions: Optional[Dict] = None):
        total_versions = len(versions)
        for j, each_version in enumerate(versions):
            if self._cancel:
                break
            filename = each_version["code"]
            if not each_version["sg_uploaded_movie"]:
                _logger.error(f"Version doesn't have a sg_uploaded_movie. {filename}")
                continue
            content_type = each_version["sg_uploaded_movie"]["content_type"]
            url = each_version['sg_uploaded_movie']['url']
            ext = os.path.splitext(each_version['sg_uploaded_movie']['name'])[-1]

            if content_type is None:
                continue
            output_filepath = path / "{0}{1}".format(filename, ext).replace("\\", "/")
            self.download_version(url, output_filepath)
            self.progressChanged.emit(j + 1)


class ProgressWidget(QWidget):
    def __init__(self, task_name, path, versions):
        super().__init__()
        self.task_name = task_name
        self.total_items = len(versions)
        self.setup_ui()
        self.worker = Worker(path, versions)
        self.worker.progressChanged.connect(self.updateProgress)
        self.worker.taskCompleted.connect(self.onTaskFinished)
        self.worker.errorOccurred.connect(self.onError)

    def setup_ui(self):
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.task_name)

        h_layout = QHBoxLayout()
        h_layout.setContentsMargins(0, 0, 0, 0)
        self.progressBar = QProgressBar()
        h_layout.addWidget(self.progressBar)
        self.progressBar.setMaximum(self.total_items)
        cancel_icon = qtawesome.icon('fa.close')  # mdi.close-circle, mdi.close-circle color,color_active
        self.cancelButton = QPushButton(icon=cancel_icon)
        self.cancelButton.setToolTip("Cancel download.")
        h_layout.addWidget(self.cancelButton)
        self.cancelButton.clicked.connect(self.cancelTask)

        layout.addWidget(self.label)
        layout.addLayout(h_layout)
        self.setLayout(layout)

    def startTask(self):
        if not self.total_items:
            self.worker.errorOccurred.emit("Playlist doesn't have any items to process")
            return
        self.worker.start()

    @Slot(int)
    def updateProgress(self, completed_items):
        self.progressBar.setValue(completed_items)
        self.progressBar.setFormat(f"{completed_items} / {self.total_items}")

    @Slot()
    def onTaskFinished(self):
        self.change_progress_bar_color()
        self.label.setText(f"{self.task_name} - Completed")
        self.cancelButton.setEnabled(False)

    @Slot(str)
    def onError(self, error_message):
        self.change_progress_bar_color(color="red")
        self.label.setText(f"{self.task_name} - Error: {error_message}")
        self.cancelButton.setEnabled(False)

    def cancelTask(self):
        self.worker.cancel()
        self.change_progress_bar_color(color="red")
        self.label.setText(f"{self.task_name} - Cancelled")
        self.cancelButton.setEnabled(False)

    def change_progress_bar_color(self, color: str = "green"):
        self.progressBar.setStyleSheet("""
            QProgressBar {
                text-align: center;
            }
            QProgressBar::chunk {
                background-color: %s;
                border-radius: 3px;
            }
        """ % color)


class ProgressDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Progress")
        self.setWindowModality(Qt.ApplicationModal)
        self.setFixedWidth(600)
        self.setMinimumHeight(150)
        self.setMaximumHeight(800)
        self.setup_ui()

    def setup_ui(self):
        self.layout = QVBoxLayout()
        self.layout.setAlignment(QtCore.Qt.AlignTop)
        self.setLayout(self.layout)

        self.label = QLabel("Processing...")
        self.layout.addWidget(self.label)

        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_wdg = QtWidgets.QWidget()
        self.scroll_area_layout = QtWidgets.QVBoxLayout()
        self.scroll_area_layout.setAlignment(QtCore.Qt.AlignTop)
        self.scroll_wdg.setLayout(self.scroll_area_layout)
        self.scroll_area.setWidget(self.scroll_wdg)
        self.layout.addWidget(self.scroll_area)

        self.button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
        self.button_box.rejected.connect(self.cancel_all)
        self.button_box.accepted.connect(self.accept)
        self.layout.addWidget(self.button_box)

    def closeEvent(self, event):
        self.cancel_all()
        super().closeEvent(event)

    def update_overall_progress(self, completed, total):
        self.setWindowTitle(f"Progress - {int(completed / total * 100)}%")

    def start_all(self):
        for i in range(self.scroll_area_layout.count()):
            widget = self.scroll_area_layout.itemAt(i).widget()
            if isinstance(widget, ProgressWidget):
                widget.startTask()

    def cancel_all(self):
        for i in range(self.scroll_area_layout.count()):
            widget = self.scroll_area_layout.itemAt(i).widget()
            if isinstance(widget, ProgressWidget):
                widget.cancelTask()
        self.reject()


class ItemData(list_wdg.BaseItemData):
    HEADER_COLUMNS = ['Name', 'Description', 'Date Updated']
    EXCLUDE_ATTRIBUTES = ['sg_id', 'sg_data']

    def __init__(self, sg_id, name, description, date_updated, sg_data):
        super().__init__(name)
        self.sg_data: Dict = sg_data
        self.sg_id: int = sg_id
        self.name: str = name
        self.description: str = description
        self.date_updated: str = date_updated

    @classmethod
    def from_sg_playlist(cls, sg_playlist):
        desc = sg_playlist.get('description')
        return cls(
            sg_id=sg_playlist.get('id'),
            name=sg_playlist.get('code'),
            description=desc if desc else "None",
            date_updated=cls.date_formatter(sg_playlist.get('updated_at')),
            sg_data=sg_playlist
        )

    @staticmethod
    def date_formatter(datetime: datetime.datetime) -> str:
        return datetime.strftime(DATE_FORMAT)


class SearchProxyModel(QtCore.QSortFilterProxyModel):

    def __init__(self):
        super().__init__()
        self._filterString = ''

    def setFilterString(self, input_string: str) -> None:
        self._filterString = input_string
        self.invalidateFilter()

    def filterAcceptsRow(self, sourceRow: int, sourceParent: QtCore.QModelIndex) -> bool:
        if not self._filterString:
            return True
        sourceIndex = self.sourceModel().index(sourceRow, 0, sourceParent)
        itemData = self.sourceModel().data(sourceIndex, list_wdg.ITEM_DATA_ROLE)
        if self._filterString.lower() in itemData.name.lower():
            return True
        return False

    def data(self, index: QtCore.QModelIndex, role: QtCore.Qt.ItemDataRole = QtCore.Qt.DisplayRole) -> Any:
        return self.sourceModel().data(self.mapToSource(index), role)

    def sort(self, column: int, order: QtCore.Qt.SortOrder = QtCore.Qt.AscendingOrder) -> None:
        return self.sourceModel().sort(column, order)


class PlaylistListModel(list_wdg.BaseListModel):
    ITEM_DATA_CLASS = ItemData

    def data(self, index, role=QtCore.Qt.DisplayRole):
        itemData = self._data[index.row()]
        if role == QtCore.Qt.DisplayRole:
            clip_attributes = [attr for attr in itemData.get_attributes()]
            attribute_name = clip_attributes[index.column()]
            if attribute_name == 'name':
                number_versions = len(itemData.sg_data['versions'])
                return f"{getattr(itemData, attribute_name)} ({number_versions})"
            return getattr(itemData, attribute_name)
        elif role == list_wdg.ITEM_DATA_ROLE:
            return itemData
        return None


class PlaylistListWidget(list_wdg.BaseListWidget):
    TABLE_MODEL_CLASS = PlaylistListModel
    openPageActionClicked = Signal(set)

    def contextMenuEvent(self, event):
        context_menu = QtWidgets.QMenu(self)
        open_page_action = context_menu.addAction('Show SG page.')
        action = context_menu.exec(event.globalPos())
        if action == open_page_action:
            selected_items = self.list_view.get_selected_items()
            self.openPageActionClicked.emit(selected_items)


class PlaylistDownloader(QtWidgets.QMainWindow):
    window_name = 'Playlist Downloader'
    window_icon = None
    DEFAULT_PROJECT = 'Select Project'
    DEFAULT_OUTPUT_ROOT = Path('~/Downloads').expanduser()
    ALL_ITEMS = 'All'
    AUTO_SEARCH_TIMEOUT = 5

    def __init__(self, connection: Optional[Connection] = None):
        super().__init__()
        self._conn = connection
        self._data = None
        if not self._conn:
            sys.exit()
        self.tasks = []

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

        # Shortcuts
        QtWidgets.QShortcut(
            QtGui.QKeySequence("Ctrl+F"),
            self,
            self.playlist_lineEdit.setFocus,
        )

    def init_ui(self):
        self.setWindowTitle(self.window_name)
        self.setMinimumSize(QtCore.QSize(800, 600))
        default_icon = qtawesome.icon('fa5s.download')
        self.setWindowIcon(default_icon if not self.window_icon else QtGui.QIcon(self.window_icon))

        QtCore.QCoreApplication.setApplicationName(self.window_name)
        QtGui.QGuiApplication.setWindowIcon(default_icon if not self.window_icon else QtGui.QIcon(self.window_icon))

        # MAIN Widget
        self.main_wdg = QtWidgets.QWidget()
        self.setCentralWidget(self.main_wdg)
        # MAIN Layout
        self.layout = QtWidgets.QVBoxLayout(self.main_wdg)
        self.layout.setAlignment(QtCore.Qt.AlignTop)
        self.main_wdg.setLayout(self.layout)

        # Parameters

        self.project_label = QtWidgets.QLabel("Project:")
        self.project_cmbx = QtWidgets.QComboBox()
        refresh_icon = qtawesome.icon('fa.refresh')
        self.refresh_button = QtWidgets.QPushButton(icon=refresh_icon)
        self.refresh_button.setToolTip("Refresh Shotgrid data.")

        self.time_range_filter_wdg = wdg_timeRangeFilter.TimeRangeFilterWidget()

        self.playlist_label = QtWidgets.QLabel("Search:")
        self.playlist_lineEdit = QtWidgets.QLineEdit()
        self.playlist_lineEdit.setPlaceholderText('Search playlist')

        self.parmeters_layout = QtWidgets.QHBoxLayout()
        self.parmeters_layout.setAlignment(QtCore.Qt.AlignLeft)
        self.parmeters_layout.addWidget(self.project_label)
        self.parmeters_layout.addWidget(self.project_cmbx)
        self.parmeters_layout.addWidget(self.time_range_filter_wdg)
        self.parmeters_layout.addStretch()
        self.parmeters_layout.addWidget(self.refresh_button)
        self.layout.addLayout(self.parmeters_layout)

        self.layout.addWidget(self.playlist_lineEdit)

        # List Widget
        self.playlists_list = PlaylistListWidget()
        self.layout.addWidget(self.playlists_list)

        self.search_model = SearchProxyModel()
        self.search_model.setSourceModel(self.playlists_list.list_model)
        self.playlists_list.list_view.setModel(self.search_model)

        # Download
        download_layout = QtWidgets.QHBoxLayout()
        self.layout.addLayout(download_layout)

        self.download_label = QtWidgets.QLabel('Output folder:')
        download_layout.addWidget(self.download_label)

        self.download_line_edit = QtWidgets.QLineEdit(self.DEFAULT_OUTPUT_ROOT.as_posix())
        self.download_line_edit.setPlaceholderText('Select output folder')
        download_layout.addWidget(self.download_line_edit)

        # Open Button
        open_icon = qtawesome.icon('fa.folder-open-o')
        self.open_button = QtWidgets.QPushButton(icon=open_icon)
        self.open_button.setFlat(True)
        self.open_button.setToolTip('Open folder')
        download_layout.addWidget(self.open_button)

        self.browse_button = QtWidgets.QPushButton('Select Folder')
        self.browse_button.setToolTip('Change output folder')
        download_layout.addWidget(self.browse_button)

        self.subfolders_chbox = QtWidgets.QCheckBox('Folder Per Playlist')
        self.subfolders_chbox.setToolTip("Create a folder for each selected playlist.")
        self.subfolders_chbox.setChecked(True)
        self.layout.addWidget(self.subfolders_chbox)

        self.download_button = QtWidgets.QPushButton(text='Download')
        self.layout.addWidget(self.download_button)

    def populate_ui(self):
        # Projects
        sg_projects = [self.DEFAULT_PROJECT]
        sg_projects += self.getProjects()
        self.project_cmbx.addItems(sg_projects)

        # Filter
        self.time_range_filter_wdg.range_combo.setCurrentIndex(1)

    def connect_signals(self):
        # Project
        self.project_cmbx.currentTextChanged.connect(self._project_changed)
        self.refresh_button.clicked.connect(lambda x: self.getProjectData(refresh=True))
        self.refresh_button.clicked.connect(self._project_changed)

        # Open SG page
        self.playlists_list.openPageActionClicked.connect(self._open_sg_page)

        # Search
        self.playlist_lineEdit.textChanged.connect(self.search_model.setFilterString)
        self.open_button.clicked.connect(self._open_folder)

        # Output
        self.browse_button.clicked.connect(self._select_output)
        self.download_button.clicked.connect(self._trigger_download)

    def _open_sg_page(self, selected_items: Set[ItemData]):
        for selected_item in selected_items:
            url = self._conn.get_sg_page_url(selected_item.sg_data['type'], selected_item.sg_data['id'])
            QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

    def _select_output(self):
        folder = utils.open_folderbrowser(parent=self)
        if folder:
            self.download_line_edit.setText(folder)

    def _open_folder(self):
        current_dir = self.download_line_edit.text()
        files.openFolder(current_dir)

    def _trigger_download(self):
        selected_items = self.playlists_list.list_view.get_selected_items()
        if not selected_items:
            QtWidgets.QMessageBox.warning(self, "Warning", "Nothing is selected. Please select a Playlist.")
            return None

        dialog = ProgressDialog()
        base_output_dir = Path(self.download_line_edit.text())
        total_versions = sum(len(selected_item.sg_data['versions']) for selected_item in selected_items)
        completed_versions = 0

        def update_overall_progress():
            nonlocal completed_versions
            completed_versions += 1
            dialog.update_overall_progress(completed_versions, total_versions)

        with ThreadPoolExecutor(max_workers=16) as executor:
            for selected_item in selected_items:
                output_dir = base_output_dir / selected_item.name if self.subfolders_chbox.isChecked() else base_output_dir
                filters = [["playlists", "in", [selected_item.sg_data]],]
                fields = ["sg_uploaded_movie", "code"]
                versions = self._conn.get_entities("Version", filters, fields)
                progress_wdg = ProgressWidget(selected_item.name, output_dir, versions)
                dialog.scroll_area_layout.addWidget(progress_wdg)
                progress_wdg.worker.progressChanged.connect(update_overall_progress)
                executor.submit(progress_wdg)

            dialog.start_all()

        if dialog.exec() == QtWidgets.QDialog.Rejected:
            _logger.info("Download cancelled by user")

    def gatherData(self, data=None, projName=None, traverseProj=False):
        if not data:
            data = {}

        if not projName:
            active_projects = self._conn.get_projects(fix_spaces=False)
            foundProjects = [p['name'] for p in active_projects]
        else:
            foundProjects = [projName]

        for iterProj in foundProjects:
            if not traverseProj:
                if iterProj not in data.keys():
                    data[iterProj] = []
                continue
            filters = [
                ['project', 'name_is', iterProj],
            ]
            if not self.time_range_filter_wdg.is_off:
                filters += [['updated_at', 'greater_than', self.time_range_filter_wdg.get_date_range()[0]]]

            # fields = ['code', 'id', 'updated_at', 'description', 'sg_review_status', 'sg_delivery_status', 'versions']
            fields = ['code', 'id', 'description', 'updated_at', 'versions']
            order = [{'field_name': 'updated_at', 'direction': 'desc'}]
            found_playlists = self._conn.get_entities("Playlist", filters, fields, order, limit=0)
            data[iterProj] = [ItemData.from_sg_playlist(playlist) for playlist in found_playlists]
        return data

    def getProjects(self):
        """ Populates the empty projects to the stored self._data. Then retuns a list of sorted project names"""
        self._data = self.gatherData(projName=None, traverseProj=False)
        if not self._data:
            return []
        return sorted(list(self._data.keys()))

    def getProjectData(self, refresh=False):
        current_project = self.project_cmbx.currentText()
        if not self._data or not current_project or current_project not in self._data.keys():
            return []

        if not len(self._data[current_project]) or refresh:
            if current_project in self._data.keys():
                start_time = time.time()
                self._data = self.gatherData(data=self._data,
                                             projName=current_project,
                                             traverseProj=True)
                elapsed_time = time.time() - start_time
                _logger.debug('Project %s updated in %s seconds.' % (current_project, elapsed_time))
                print('Project %s updated in %s seconds.' % (current_project, elapsed_time))
        return self._data[current_project]

    def _project_changed(self):
        """ When a project changes, we get the project data and populate the listWidget"""
        # Cleanup default item
        self.project_cmbx.removeItem(self.project_cmbx.findText(self.DEFAULT_PROJECT))

        # Populate asset_types and episodes
        pData = self.getProjectData()
        if pData is None:
            return
        # Refresh listWidget items
        self.playlists_list.populate_list(pData)


def launch_desktop(conn: Connection, project: Optional[str] = None) -> None:
    """run application in desktop"""
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)

    if not conn:
        from teide_core import db
        conn = db.SGAuthentication.login()
    window = PlaylistDownloader(conn)
    window.show()

    window.project_cmbx.setCurrentText(project)

    sys.exit(app.exec())

