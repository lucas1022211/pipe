import argparse
from PlaylistDownloader import playlistdownloader

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("conn", nargs='?', help="Teide Connection Instance")
    parser.add_argument("project", nargs='?', help="Shotgrid Project Name")
    known_args, unknown_args = parser.parse_known_args()

    conn = known_args.conn
    if not conn:
        from teide_core import db
        conn = db.SGAuthentication.login()

    playlistdownloader.launch_desktop(conn, known_args.project)
