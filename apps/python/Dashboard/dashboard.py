""" MyDashboard widget
Shows the assigned tasks by default (can access other tasks)
there are filters to be able to look for different tasks, by default go to user's tasks.
From a selected task we get insight into all data related to that task.
References, Description, Work/Output files, Notes, Status Details ...

Overview of Apps, Tools and Panels

Editorial Publisher
Playlist Downloader
Project Manager (Currently split into individual apps)
    Asset Library
    Dashboard
        Tasks panel (Task browser and picker, default to MyAssignedTasks)
        References (Reference files for entity)
        Browser (Scenes, Outputs)
        Notes (Sg Notes for entity, filters by step, task, status)
        Task Details (Read-only section to see all SG fields used in pipe)
            Task Dependencies (Unknown)
    Browser
    Time Logging
    Delivery Tool
    Analytics
    Settings


"""
import sys
import platform
from typing import TYPE_CHECKING, Optional

import qtawesome
from qtpy import QtWidgets, QtCore, QtGui

import teide_core.helpers
from teide_core import filePath
from teide_core.db import Connection
from teide_qt.widgets import wdg_tasks
from Browser.browser import Browser

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

if platform.system() == 'Windows':
    from ctypes import windll  # Only exists on Windows.

    app_id = u'teide.pipe.manager.beta'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)


# todo: add resizeable groups for Tasks | Info | Details
#  also collapse widgets if small enough and reset on new task selected.
# todo: move Dependencies to info.tabs


class MyDashboard(QtWidgets.QWidget):
    SHOW_TASK_HEADER = False

    def __init__(self, connection: Optional[Connection] = None):
        super().__init__()
        self._conn = connection

        self.init_ui()
        self.connect_signals()
        self.populate_ui()

    def init_ui(self):

        # MAIN Layout
        self.layout = QtWidgets.QHBoxLayout()
        self.setLayout(self.layout)

        # Create a QSplitter to hold the resizable sections
        self.splitter = QtWidgets.QSplitter(QtCore.Qt.Orientation.Horizontal)
        self.layout.addWidget(self.splitter)

        # Tasks
        self.tasks_wdg = wdg_tasks.TasksWidget(self._conn)
        # Add tasks widget to the splitter
        self.splitter.addWidget(self.tasks_wdg)

        # Info
        self.info_wdg = QtWidgets.QWidget()
        self.info_layout = QtWidgets.QVBoxLayout(self.info_wdg)
        self.info_layout.setContentsMargins(0, 0, 0, 0)
        self.info_wdg.setLayout(self.info_layout)
        self.splitter.addWidget(self.info_wdg)

        font = QtGui.QFont("Helvetica", 12, QtGui.QFont.Weight.Bold)
        self.task_label = QtWidgets.QLabel("TASK_NAME_PLACEHOLDER")
        self.task_label.setFont(font)
        if self.SHOW_TASK_HEADER:
            self.info_layout.addWidget(self.task_label)

        self.task_summary = QtWidgets.QTextEdit()
        self.task_summary.setFixedHeight(60)
        if self.SHOW_TASK_HEADER:
            self.info_layout.addWidget(self.task_summary)

        # Browser
        self.browser_wdg = Browser(self._conn)
        self.browser_wdg.main_layout.setContentsMargins(0, 0, 0, 0)
        self.info_layout.addWidget(self.browser_wdg)

        # Details
        from teide_qt.widgets import wdg_taskDetails
        self.details_wdg = wdg_taskDetails.TaskDetailsWidget()
        # self.splitter.addWidget(self.details_wdg)

        self.splitter.setSizes([1, 1, 0])

    def populate_ui(self):
        # fixme: uncomment when selection_changed_sgn is setup. Decide what to do with the browser.selectors.
        # self.browser_wdg.context_selectors_wdg.entity_stack_wdg.setVisible(False)
        # self.task_label.setVisible(True)
        if self.tasks_wdg.list_view.model().rowCount() > 0:
            self.tasks_wdg.list_view.setCurrentIndex(self.tasks_wdg.list_view.model().index(0, 0))
        pass

    def connect_signals(self):
        # self.tasks_wdg.selection_changed_sgn.connect(self._task_selection_changed)
        self.tasks_wdg.selection_changed_sgn.connect(self._update_task_data)
        self.tasks_wdg.selection_changed_sgn.connect(
            self.browser_wdg.context_selectors_wdg.entity_stack_wdg.make_selections)

    def _task_selection_changed(self, context):
        item_name = self.tasks_wdg.list_view.model().data(self.tasks_wdg.selected_index, QtCore.Qt.ItemDataRole.DisplayRole)
        item_name = str(item_name).split(" ", 1)[0]
        self.task_label.setText(item_name.upper())

    def _update_task_data(self, context):
        item_data = self.tasks_wdg.list_view.model().sourceModel().items[self.tasks_wdg.selected_index.row()]
        self.details_wdg.populate(task=item_data)

        item_name = str(item_data.display_name).split(" ", 1)[0]
        self.task_label.setText(item_name.upper())


class Manager(QtWidgets.QMainWindow):
    WINDOW_NAME = 'Teide Manager'
    WINDOW_ICON = None

    def __init__(self, connection: Optional[Connection] = None):
        super().__init__()
        self._conn = connection

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

        self.resize(self.width()+800, 540)

    def init_ui(self):
        self.setUnifiedTitleAndToolBarOnMac(True)
        self.setWindowTitle(self.WINDOW_NAME)
        icon = qtawesome.icon('mdi.view-dashboard')
        self.setWindowIcon(icon if not self.WINDOW_ICON else QtGui.QIcon(self.WINDOW_ICON))

        # # MAIN Widget
        # self.main_wdg = QtWidgets.QWidget()
        # self.setCentralWidget(self.main_wdg)
        # # MAIN Layout
        # self.layout = QtWidgets.QVBoxLayout(self.main_wdg)
        # self.main_wdg.setLayout(self.layout)

        # Dashboard
        self.dashboard_wdg = MyDashboard(self._conn)
        self.setCentralWidget(self.dashboard_wdg)

        # self.layout.addWidget(self.dashboard_wdg)

    def populate_ui(self): pass

    def connect_signals(self): pass


def launch_desktop(conn: Connection, project: Optional[str] = None) -> None:
    """run application in desktop"""
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)

    if not conn:
        from teide_core import db
        conn = db.SGAuthentication.login()
        # Context setup
        conn._project = project
    window = Manager(conn)
    window.show()

    sys.exit(app.exec())

