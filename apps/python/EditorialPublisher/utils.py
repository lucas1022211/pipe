""" Comment
"""
import os
import re
import sys
import getpass
import datetime
import platform
from pathlib import Path
from typing import TYPE_CHECKING, Optional, Union

import files
import teide_core.helpers
from teide_core import settings
from teide_core.db import Connection
import opentimelineio as otio

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

VOLUME_ROOT = settings.get_volume()


class EditorialUpdater:

    PROJECT_NAMING_PATTERN = r"^([a-zA-Z]{3})_.+$"  # r"^[a-zA-Z]{3}_.*$"
    EPISODE_NAMING_PATTERN = r"(e\d{3})"
    SHOT_SEARCH_PATTERN = r'\Ds(\d{4})\D'
    SHOT_NAMING_PATTERN = r"e\d{3}_s\d{4}"
    CLIP_NAMING_PATTERN = r"^(?P<code>\w{3})_(?P<episode>e\d{3})_(?P<shot>s\d{4})_(?P<descriptor>.+)$"
    NEW_SHOT_TEMPLATE = "bm_3d_shot"
    PREFERRED_REPORT_DIR = os.path.join(VOLUME_ROOT, "BM_PIPELINE/tool_reports/EditorialPublisher")
    REPORT_EXTENSION = ".txt"

    CUT_INFO_FIELDS = ['sg_status_list', 'sg_cut_in', 'sg_cut_out', 'sg_cut_duration', 'sg_cut_order']

    def __init__(self, connection: Optional[Connection] = None,
                 project: Optional[dict] = None,
                 episode: Optional[str] = None,
                 file: Optional[Path] = None):
        self._conn = connection
        if not self._conn:
            sys.exit()

        self.file = file
        self.report_path = ''
        self.project_id = project.get('id')
        self.episode = episode

        self.sg_project = project
        self.sg_episode = None
        self.sg_template = None
        self.sg_shots = None

        self.skip_shots = []
        self.new_shots = []
        self.omit_shots = []
        self.modify_shots = []

    @classmethod
    def from_file(cls, conn: Connection, file: Union[Path, str]):
        if isinstance(file, str):
            file = Path(file)

        # Derive project_id from file & SG
        match = re.search(cls.PROJECT_NAMING_PATTERN, Path(file).stem)
        if not match:
            raise ValueError('Missing project short name in filename, '
                             'rename the file or provide the project_name argument.')
        project = conn.sg.find_one("Project",
                                   [["is_template", "is", False], ["archived", "is", False], ["code", "is", match.group(1)]],
                                   ["name", "code", "id"])
        if not project:
            raise ValueError(f'Project not found with code {match.group(1)}')

        # Derive episode from file naming convention.
        match = re.search(cls.EPISODE_NAMING_PATTERN, Path(file).stem)
        if not match:
            raise ValueError('Missing episode name in filename, rename the file or provide the episode argument.')
        episode = match.group(1)

        return cls(connection=conn, project=project, episode=episode, file=file)

    def validate_episode(self):
        if not self.sg_episode:
            filters = [['project', 'is', {'type': 'Project', 'id': self.project_id}]]

            sg_ep = self._conn.get_entities('Episode', filters + [['code', 'is', self.episode]], ['code'])
            if not sg_ep:
                raise ValueError(f'Episode {self.episode} does not exist in SG.')
            elif len(sg_ep) > 1:
                raise ValueError(f'More than one episode found.')
            self.sg_episode = sg_ep[0]
        return self.sg_episode

    def get_template(self, template_name: str = NEW_SHOT_TEMPLATE):
        if not self.sg_template:
            template = self._conn.get_entities('TaskTemplate', [['code', 'is', template_name]], ['code'])
            if not template:
                raise ValueError("Task Template {} doesnt exists ".format(template_name))
            self.sg_template = template[0]
        return self.sg_template

    def get_sg_shots(self):
        if not self.sg_shots:
            extra_fields = ['code', 'id'] + self.CUT_INFO_FIELDS
            self.sg_shots = self._conn.get_shots(self.project_id, extra_fields=extra_fields)
        return self.sg_shots

    def evaluate_timeline(self):
        """Main entry point to compare the timeline and SG, Then store the actions to take"""
        self.validate_episode()
        self.get_template()
        self.get_sg_shots()

        sg_shot_codes = [sg_shot['code'] for sg_shot in self.get_sg_shots() if 'code' in sg_shot.keys()]

        # Get edit shots
        used_shots = []
        timeline = otio.adapters.read_from_file(self.file)
        for track in timeline.video_tracks():
            for item in track:
                if not isinstance(item, otio.schema.Clip):
                    continue
                clip = item
                record_range = clip.transformed_time_range(clip.trimmed_range(), track)
                cut_in = record_range.start_time
                # cut_out = record_range.end_time_exclusive()  # 0-60, 60-106
                cut_out = record_range.end_time_inclusive()  # 0-59, 60-105
                shot_duration = clip.duration().value
                cut_order = cut_in
                match = re.search(self.CLIP_NAMING_PATTERN, clip.name)
                if match:
                    episode_code = match.group(2)
                    shot_code = match.group(3)
                else:
                    ep_fallback = re.search(self.EPISODE_NAMING_PATTERN, clip.name)
                    sh_fallback = re.search(self.SHOT_SEARCH_PATTERN, clip.name)
                    episode_code = ep_fallback.group(1)
                    shot_code = f"s{sh_fallback.group(1)}"
                if not episode_code or not shot_code or not cut_in or not cut_out or not cut_order or not shot_duration:
                    _logger.error(f"{clip.name} - {episode_code} - {shot_code} - {int(cut_in.value)} - {int(cut_out.value)} - {int(cut_order.value)} - {shot_duration}")
                    continue
                clip_name = f"{episode_code}{shot_code}"
                if episode_code != self.episode:
                    self.skip_shots.append({'sg_shot': {'code': clip_name}, 'data': {}})
                    continue
                match = re.match(self.SHOT_NAMING_PATTERN, clip_name)
                if not match:
                    raise ValueError('Invalid shot name. %s' % clip_name)

                if not clip_name in used_shots:
                    used_shots.append(clip_name)
                else:
                    raise ValueError('More than one clip for the same shot {0} [{1}-{2}] {3}'.format(clip_name, int(cut_in.value), int(cut_out.value), clip.media_reference))

                # Cut-info data to add to update/create in SG
                data = {
                    "sg_cut_in": int(cut_in.value),
                    "sg_cut_out": int(cut_out.value),
                    "sg_cut_duration": int(shot_duration),
                    "sg_cut_order": int(cut_order.value),
                    "sg_status_list": 'wtg'
                }
                # Check if clip exists in sg_shots:
                if clip_name in sg_shot_codes:
                    _shot = next(filter(lambda d: d.get('code') == clip_name, self.sg_shots), None)
                    modified_atts = []
                    for attribute in self.CUT_INFO_FIELDS:
                        old_value = _shot[attribute]
                        value = data.get(attribute)
                        if value != old_value:
                            modified_atts.append(attribute)
                        continue
                    if modified_atts:
                        self.modify_shots.append({'sg_shot': _shot, 'data': data})
                    else:
                        self.skip_shots.append({'sg_shot': _shot, 'data': data})
                else:
                    data['code'] = clip_name
                    data['task_template'] = self.sg_template
                    data['sg_episode'] = self.sg_episode
                    data['project'] = {'type': 'Project', 'id': self.project_id}
                    self.new_shots.append({'sg_shot': None, 'data': data})

        # Omit unused shots
        for sg_shot in self.sg_shots:
            if sg_shot['sg_status_list'] == 'omt':
                continue
            if sg_shot['code'] not in used_shots:
                data = {'sg_status_list': 'omt'}
                self.omit_shots.append({'sg_shot': sg_shot, 'data': data})

        # Sort lists
        self.omit_shots.sort(key=lambda x: x['sg_shot']['code'])
        self.new_shots.sort(key=lambda x: x['data']['code'])
        self.modify_shots.sort(key=lambda x: x['sg_shot']['code'])
        self.skip_shots.sort(key=lambda x: x['sg_shot']['code'])

    def generate_report(self):

        data = {}
        data['execution_date'] = datetime.datetime.now().strftime("%y-%m-%d %H:%M")
        data['input_file_path'] = self.file
        data['project_name'] = self.sg_project.get('name')
        data['episode_name'] = self.episode
        data['omitted_shots'] = self.omit_shots
        data['created_shots'] = self.new_shots
        data['modified_shots'] = self.modify_shots
        data['user_name'] = getpass.getuser()
        data['platform'] = platform.system()

        # Generate the Report msg
        summary = "Editorial Report\n"
        summary += "\nDate: %s" % data['execution_date']
        summary += "\nUser: %s" % data['user_name']
        summary += "\nPlatform: %s" % data['platform']
        summary += "\nSource Input: %s" % data['input_file_path']
        summary += "\nProject: %s" % data['project_name']
        summary += "\nEpisode: %s" % data['episode_name']

        omitted_shots = "\n\nOmit Shots: ({})".format(len(data['omitted_shots']))
        for shot in data['omitted_shots']:
            omitted_shots += '\n - {0}'.format(shot['sg_shot']['code'])

        new_shots = "\n\nCreate Shots: ({})".format(len(data['created_shots']))
        for shot in data['created_shots']:
            new_attrs = ''
            for attribute in self.CUT_INFO_FIELDS:
                value = shot['data'].get(attribute)
                new_attrs += '\n\t- @{0:<16}\t{1}'.format(attribute, value)
            new_shots += '\n - {0}{1}'.format(shot['data']['code'], new_attrs)

        modified_shots = "\n\nModify Shots: ({})".format(len(data['modified_shots']))
        for shot in data['modified_shots']:
            modified_attrs = ''
            for attribute in self.CUT_INFO_FIELDS:
                old_value = shot['sg_shot'][attribute]
                value = shot['data'].get(attribute)
                expanded = False
                if value == old_value:
                    continue
                modified_attrs += '\n\t- @{0:<16}\t{1} --> {2}'.format(attribute, old_value, value)
                if expanded:
                    modified_attrs += '\t(Expanded)'
            modified_shots += '\n - {0}{1}'.format(shot['sg_shot']['code'], modified_attrs)

        skipped_shots = "\n\nSkip Shots: ({})".format(len(self.skip_shots))
        for shot in self.skip_shots:
            skipped_shots += '\n - {0}'.format(shot['sg_shot']['code'])

        report = summary + omitted_shots + new_shots + modified_shots + skipped_shots

        # Write report to disk
        self.report_path = self.file.with_suffix(self.REPORT_EXTENSION)
        if os.path.exists(self.PREFERRED_REPORT_DIR):
            self.report_path = os.path.join(self.PREFERRED_REPORT_DIR, self.report_path.name)
        files.writeTextToFile(path=self.report_path, text=report)
        _logger.info(f"Saved Report to path: {self.report_path}")
        return report

    def do_sg_update(self, dry_run: bool = True):
        for shot in self.new_shots:
            if not dry_run:
                self._conn.create_entity('Shot', data=shot['data'], fields=["id", "code"])
            _logger.info(f"Creating shot: {shot['data']['code']}")
        for shot in self.omit_shots:
            if not dry_run:
                self._conn.update_entity('Shot', shot['sg_shot']['id'], shot['data'])
            _logger.info(f"Omitting shot: {shot['sg_shot']['code']}")
        for shot in self.modify_shots:
            if not dry_run:
                self._conn.update_entity('Shot', shot['sg_shot']['id'], shot['data'])
            _logger.info(f"Updating shot: {shot['sg_shot']['code']}")


if __name__ == '__main__':
    from teide_core import db
    conn = db.Authentication.login()

    TIMELINE_FILE = r"C:\Users\DELL\Desktop\test_files\e999\msr_e999_v003.aaf"

    updater = EditorialUpdater.from_file(conn, TIMELINE_FILE)
    updater.evaluate_timeline()
    report = updater.generate_report()
    # updater.do_sg_update()
    print(report)

