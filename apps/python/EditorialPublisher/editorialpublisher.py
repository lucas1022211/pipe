""" Application to update the cut info in SG given a editorial file (EDL, AAF, CSV, OTIO, XML)

Workflow:
#  1. Minimal Qt App launch
#  2. Provide Timeline file --> file browser
#  3. Select project (if could not be extracted from input)
#  4. Select episode (if could not be extracted from input)
#  5. Generate Deltas (Button to generate a preview report and show the user whats gonna happen.)
#  6. Run (button to confirm that we want to proceed)
#  7. Open final Report?

"""
import sys
import platform
import argparse
from typing import TYPE_CHECKING, Optional

import qtawesome
from qtpy import QtWidgets, QtCore, QtGui

from EditorialPublisher import utils

import teide_core.helpers
from teide_core.db import Connection

if TYPE_CHECKING:
    from typing import *

if platform.system() == 'Windows':
    from ctypes import windll  # Only exists on Windows.
    app_id = u'teide.pipe.editorialPublisher.beta'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)


_logger = teide_core.helpers.get_teide_logger(__name__)

# Required before release
# todo: Sort the Report.
# todo: Make the log_text dont wait for stuff to finish, might need threding
#  - We need a simpler, testing example of what its actually blocking the update.
# fixme: Make the log_text show more logs.
# todo: show timestamps for read_timeline, update_SG operations
# todo: add user to report
# todo: Better default location for the file browser
#  - Use timeline dir if timeline_file is not empty

# Next
# fixme: support freeze frame / extend shot -- more than one continuous clip with same name/media
#   take the chain's range for the shot range.
# todo: re-open support for non-aaf timelines (it works now)
#  - we need to raise an error if the timeline cant be read (bc the way they export only AAF for now
# todo: upload media to sg if there is none


class EditorialPublisher(QtWidgets.QWidget):
    WINDOW_NAME = 'Editorial Publisher'
    WINDOW_ICON = None
    VALID_EXTENSIONS = ['aaf']
    IDLE_LOG_MSG = 'Waiting for timeline file...'
    PROCESSING_LOG_MSG = 'Reading ...'

    def __init__(self, connection: Optional[Connection] = None):
        super().__init__()
        self._conn = connection
        self._updater = None
        if not self._conn:
            sys.exit()

        self.init_ui()
        self.connect_signals()

    def init_ui(self):
        # MAIN WINDOW
        self.setWindowTitle(self.WINDOW_NAME)
        icon = qtawesome.icon('fa5.share-square')
        self.setWindowIcon(icon if not self.WINDOW_ICON else QtGui.QIcon(self.WINDOW_ICON))

        # MAIN LAYOUT
        layout = QtWidgets.QVBoxLayout(self)

        # Timeline
        timeline_layout = QtWidgets.QHBoxLayout()
        layout.addLayout(timeline_layout)

        self.timeline_label = QtWidgets.QLabel('Timeline File:')
        timeline_layout.addWidget(self.timeline_label)

        self.timeline_line_edit = QtWidgets.QLineEdit()
        self.timeline_line_edit.setPlaceholderText('Select timeline file')
        self.timeline_line_edit.setToolTip('Timeline file naming convention: <project_code>_<episode>_*.<extension>')
        self.timeline_line_edit.setReadOnly(True)
        self.timeline_line_edit.setFocusPolicy(QtCore.Qt.NoFocus)
        timeline_layout.addWidget(self.timeline_line_edit)

        self.browse_button = QtWidgets.QPushButton('Browse')
        self.browse_button.setToolTip('Browse a the timeline file')
        timeline_layout.addWidget(self.browse_button)

        # Log
        self.log_text_edit = QtWidgets.QTextEdit()
        self.log_text_edit.setReadOnly(True)
        self.log_text_edit.setText(self.IDLE_LOG_MSG)
        layout.addWidget(self.log_text_edit)

        # Run
        self.run_button = QtWidgets.QPushButton('Update SG')
        self.run_button.setToolTip('Confirm and run the update in SG')
        self.run_button.setEnabled(False)
        layout.addWidget(self.run_button)

        # Open Report
        self.open_report_button = QtWidgets.QPushButton('Open Report')
        self.open_report_button.setEnabled(False)
        # layout.addWidget(self.open_report_button)

    def connect_signals(self):
        # Browser
        self.browse_button.clicked.connect(self.open_filebrowser)

        # Validate not empty fields
        self.timeline_line_edit.textChanged.connect(self._validate_inputs)
        self.timeline_line_edit.textChanged.connect(self._updated_file)

        # Run & open
        self.run_button.clicked.connect(self.run_process)
        self.open_report_button.clicked.connect(self.open_report)

    @property
    def file(self):
        return self.timeline_line_edit.text().strip()

    def open_filebrowser(self):
        all_files_filter = f"All files ({' '.join(['*.' + ext for ext in self.VALID_EXTENSIONS])})"
        valid_filters = ';;'.join([f"{ext.upper()} files (*.{ext})" for ext in self.VALID_EXTENSIONS])
        valid_filters += f";;{all_files_filter}"

        file_path, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Select File', QtCore.QDir.homePath(),
                                                             valid_filters)
        if file_path:
            self.timeline_line_edit.setText(file_path)

    def show_report(self):
        self._clear_log_text()
        try:
            report = self._updater.generate_report()
            self.log_text_edit.append(report)
        except Exception as e:
            _logger.exception(e, exc_info=True)
        self.log_text_edit.verticalScrollBar().setValue(0)

    def run_process(self):
        # fixme: make do_sg_update() yield the msg instead of print it so that we can pass it to the self.log_text
        self.log_text_edit.append('Running process...')
        self._updater.do_sg_update(dry_run=False)
        self.log_text_edit.append('Finish')

    def open_report(self):
        # self._updater.evaluate_timeline()
        # report = self._updater.generate_report()
        # todo: dump report into desired file relative to the timeline file.
        self.log_text_edit.append('Opening report...')

    def _clear_log_text(self):
        self.log_text_edit.clear()

    def _validate_inputs(self):
        line_edits = [self.timeline_line_edit]
        inputs_empty = any(not line_edit.text().strip() for line_edit in line_edits)

        self.run_button.setEnabled(not inputs_empty)
        self.open_report_button.setEnabled(not inputs_empty)

    def _updated_file(self):
        self.log_text_edit.setText(self.PROCESSING_LOG_MSG)
        try:
            self._updater = utils.EditorialUpdater.from_file(self._conn, self.file)
            self._updater.evaluate_timeline()
        except Exception as e:
            _logger.exception(e, exc_info=True)
        self.show_report()


def launch_desktop(conn: Connection) -> None:
    """run application in desktop"""
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)

    if not conn:
        from teide_core import db
        conn = db.SGAuthentication.login()
    window = EditorialPublisher(conn)
    window.show()

    sys.exit(app.exec())
