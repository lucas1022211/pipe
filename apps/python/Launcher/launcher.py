""" Comment
"""
import os
import sys
import platform
import qtawesome
from typing import TYPE_CHECKING, Optional

from qtpy import QtWidgets, QtCore, QtGui

import files
from Launcher.widgets import outputWindow, launcherWidget

import teide_core.helpers
from teide_core.db import SGAuthentication, Connection


if TYPE_CHECKING:
    from typing import *

if platform.system() == 'Windows':
    from ctypes import windll  # Only exists on Windows.
    app_id = u'teide.pipe.app.launcher'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)

_logger = teide_core.helpers.get_teide_logger(__name__)

# todo: rewrite to have a CLI api to be able to launch the apps. ex: render farm open app in environment.
#  Then be able to also access the launcher though the system tray.
#  > CLI
#  - launcher browser
#  - launcher <app_name> --project <project_name>
#  > UI
#  - LauncherSystemTray(QObject)
#       - tray_icon = QSystemTrayIcon()
#       - tray_menu = actions like "Quit", "Open Console"
#       - tray_window = LauncherWindow()
# done: split LauncherTray in smaller widgets like Console, LauncherWidget ...
# fixme: support more than one QProcess() to be running  -- introduce ExecutableRunnerThread(QThread)
# done: find a robust way to place the Launcher.
# todo: find a robust way to scale the Launcher.
# fixme: close LauncherWindow/systemtray is already exits before opening anotherone.

# todo: add a settings Action -- will open a UI to configure the some ENV paths.
#  # Maybe better to just show the current Loaded ENV: env paths, versions, Mounts
#  - TEIDE_FRAMEWORK | TEIDE_CONFIGURATION | TEIDE_LOCAL | Mounts


SCRIPT_DIR = files.get_parent_dir(__file__, level=1)


class LauncherSystemTray(QtCore.QObject):
    APP_NAME = "Launcher"
    TRAY_ICON = os.path.join(SCRIPT_DIR, 'resources/rocket_64.png')

    def __init__(self, connection: Optional[Connection] = None):
        super().__init__()
        self.app = QtWidgets.QApplication.instance()
        self.conn = connection
        self.global_signal = teide_core.helpers.TEIDE_LOGGER_SIGNAL
        self.tray_process = None

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    def init_ui(self):

        # SystemTray
        self.tray_icon = QtWidgets.QSystemTrayIcon(self)
        self.tray_icon.setIcon(QtGui.QIcon(self.TRAY_ICON))
        self.tray_icon.show()

        # Console
        self.console_window = outputWindow.Console()

        # Launcher Window
        self.launcher_window = launcherWidget.LauncherWindow(conn=self.conn)
        self.launcher_window.setWindowFlag(QtCore.Qt.WindowType.WindowStaysOnTopHint)
        self.launcher_window.setAttribute(QtCore.Qt.WidgetAttribute.WA_ShowWithoutActivating)

        QtCore.QCoreApplication.setApplicationName(self.APP_NAME)
        QtGui.QGuiApplication.setWindowIcon(QtGui.QIcon(self.TRAY_ICON))

        # Tray Context Menu
        if platform.system() == 'Windows':
            self.tray_icon.setContextMenu(self.launcher_window.context_menu)

    def populate_ui(self):
        self._update_launch_function()

    def connect_signals(self):
        self.global_signal.updateConsoleWidget.connect(self.console_window.receive_logger_signal)
        self.global_signal.updateConsoleWidget.emit('Logger Connection established!')
        self.launcher_window.action_open_console.triggered.connect(self._open_console)
        self.launcher_window.action_logout.triggered.connect(self._logout)
        self.launcher_window.action_quit.triggered.connect(self.app.quit)
        self.tray_icon.activated.connect(self._tray_icon_clicked)
        self.app.focusChanged.connect(self.hide_if_not_active)
        self.launcher_window.refresh_widget.clicked.connect(self._update_launch_function)

    def _update_launch_function(self):
        for app in self.launcher_window.found_apps:
            app.launch_function(self.launch_app, app=app)

    def launch_app(self, app):
        selected_project = self.launcher_window.projects_combobox.currentText()
        _logger.info('Opening App')
        self.tray_process = QtCore.QProcess(self)
        self.tray_process.readyReadStandardOutput.connect(self.handle_output)
        self.tray_process.readyReadStandardError.connect(self.handle_output)
        if str(app.app.executable).endswith('.py'):
            command = [str(app.app.executable)]
        else:
            command = ['-m', app.app.name, self.conn, selected_project]
        current_python = sys.executable
        process_env = QtCore.QProcessEnvironment.systemEnvironment()
        process_env.insert("QT_HANDLER", "True")
        self.tray_process.setProcessEnvironment(process_env)
        self.tray_process.start(current_python, command)

    def hide_if_not_active(self):
        if QtWidgets.QApplication.instance():
            active_window = QtWidgets.QApplication.instance().activeWindow()
            if active_window != self.launcher_window and active_window != self.tray_icon:
                self.launcher_window.hide()

    def _tray_icon_clicked(self, reason):
        if reason == QtWidgets.QSystemTrayIcon.Trigger:
            if self.launcher_window.isHidden():
                self.open_launcher()
            else:
                self.launcher_window.hide()
        elif reason == QtWidgets.QSystemTrayIcon.Context and platform.system() == "Windows":
            self.tray_icon.contextMenu().exec(QtGui.QCursor.pos())

    def on_tray_icon_activated(self):
        """Shows the tray menu at the cursor's position."""

        # Calculate taskbar position
        screen = QtGui.QGuiApplication.primaryScreen()
        screen_geometry = screen.geometry()
        available_geometry = screen.availableGeometry()
        tray_icon_geometry = self.tray_icon.geometry()

        # Calculate the center position of the tray icon
        tray_icon_center = tray_icon_geometry.center()

        menu_width = self.launcher_window.width()
        menu_height = self.launcher_window.height()

        margin = 20  # Margin between the taskbar and the system tray menu

        if available_geometry.y() > screen_geometry.y():
            # Taskbar is on the top
            pos = QtCore.QPoint(tray_icon_center.x() - menu_width / 2, tray_icon_geometry.bottom() + margin)
        elif available_geometry.x() > screen_geometry.x():
            # Taskbar is on the left
            pos = QtCore.QPoint(tray_icon_geometry.right() + margin, tray_icon_center.y() - menu_height / 2)
        elif available_geometry.height() < screen_geometry.height():
            # Taskbar is on the bottom
            pos = QtCore.QPoint(tray_icon_center.x() - menu_width / 2, tray_icon_geometry.top() - menu_height - margin)
        else:
            # Taskbar is on the right or default position
            pos = QtCore.QPoint(tray_icon_geometry.left() - menu_width - margin, tray_icon_center.y() - menu_height / 2)

        # Ensure the menu is completely visible
        if pos.x() < available_geometry.x():
            pos.setX(available_geometry.x())
        if pos.y() < available_geometry.y():
            pos.setY(available_geometry.y())
        if pos.x() + menu_width > available_geometry.right():
            pos.setX(available_geometry.right() - menu_width)
        if pos.y() + menu_height > available_geometry.bottom():
            pos.setY(available_geometry.bottom() - menu_height)

        geom = QtCore.QRect(pos.x(), pos.y(), self.launcher_window.width(), self.launcher_window.height())
        self.launcher_window.setGeometry(geom)

    def _open_console(self):
        if not self.console_window.isVisible():
            self.console_window.showNormal()
        self.console_window.activateWindow()

    def _logout(self):
        SGAuthentication.logout()
        self.app.quit()

    def open_launcher(self):
        if not self.launcher_window.isVisible():
            self.launcher_window.showNormal()
        self.launcher_window.activateWindow()
        self.on_tray_icon_activated()

    def handle_output(self):
        data = self.tray_process.readAll()
        output = bytes(data).decode('utf-8')
        self.console_window.text_edit.appendHtml(output)


def launch_desktop(conn: Connection) -> None:
    """run application in desktop"""
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)

    if not conn:
        from teide_core import db
        conn = db.SGAuthentication.login()
    LauncherSystemTray(conn)

    sys.exit(app.exec())
