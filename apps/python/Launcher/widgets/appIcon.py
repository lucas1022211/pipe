""" Qt Widget to represent an app to show in the launcher App
"""
import os
import sys
import subprocess
from pathlib import Path
from typing import TYPE_CHECKING, Optional

import attr
import qtawesome
from qtpy import QtWidgets, QtCore, QtGui

import teide_core.helpers
from teide_core import settings

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
IMAGE = os.path.join(os.path.dirname(SCRIPT_DIR), "resources/emptyImage.png")


@attr.s
class BaseAppData:
    executable = attr.ib(type=Path)
    name = attr.ib(type=str)
    nice_name = attr.ib(type=str)
    icon = attr.ib(type=Optional[QtGui.QIcon], default=None)

    @name.default
    def nameDefault(self):
        return self.executable.stem

    @nice_name.default
    def nice_nameDefault(self):
        return self.name.capitalize()


class AppIcon(QtWidgets.QPushButton):
    default_image = IMAGE
    minimum_size = 150
    icon_size = 24

    def __init__(self, app_data: BaseAppData):
        super().__init__()
        self.app = app_data

        self.init_ui()
        self.populate_ui()
        # self.connect_signals()

    def init_ui(self):
        self.setFlat(True)
        self.setText(self.app.name)
        default_icon = qtawesome.icon('fa.bullseye')
        self.setIcon(self.app.icon or default_icon)
        self.setStyleSheet("text-align: left;")

    def populate_ui(self):
        pass

    def connect_signals(self):
        self.clicked.connect(self._open_app)

    def _open_app(self):
        current_python = sys.executable
        subprocess.Popen([current_python, "-m", self.app.name])

    def launch_function(self, func, *args, **kwargs):
        self.clicked.connect(lambda: func(*args, **kwargs))


def get_teide_apps():
    black_list = ['Launcher']
    # todo: collect the teide Apps -- move to package.py

    # Collect all potential app paths
    base_dir = Path(f'{settings.TEIDE_FRAMEWORK_PATH}/apps/python')
    app_paths = [path for path in base_dir.iterdir() if path.is_dir()]
    valid_apps = [path for path in app_paths if path.name not in black_list]

    from Browser import browser
    from Dashboard import dashboard
    from EditorialPublisher import editorialpublisher
    from PlaylistDownloader import playlistdownloader

    icon_mapping = {
        'Browser': browser.Browser.window_icon,
        'Dashboard': dashboard.Manager.WINDOW_ICON,
        'EditorialPublisher': editorialpublisher.EditorialPublisher.WINDOW_ICON,
        'PlaylistDownloader': playlistdownloader.PlaylistDownloader.window_icon,
    }

    found_apps = []
    for path in valid_apps:
        app = BaseAppData(executable=path)
        app_icon = icon_mapping.get(app.name, None)
        if app_icon:
            print(app_icon)
            # app.icon = qtawesome.icon(app_icon) if isinstance(app_icon, str) else app_icon
        found_apps.append(app)
    return found_apps


def main():
    app = QtWidgets.QApplication(sys.argv)
    if sys.platform == 'darwin':
        app.setStyle(QtWidgets.QStyleFactory.create('macos'))
    qtawesome.dark(app)
    window = QtWidgets.QWidget()
    layout = QtWidgets.QVBoxLayout()
    layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
    window.setLayout(layout)
    for app_wdg in sorted(get_teide_apps(), key=lambda x: x.nice_name):
        widget = AppIcon(app_data=app_wdg)
        layout.addWidget(widget)
    window.show()
    app.exec()


if __name__ == '__main__':
    main()
