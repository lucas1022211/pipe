""" Launcher window. Contains widgets to change the pipeline version, logged user, and show the available apps.

## Note: All this comment is a brief description of the behaviour we might want. Not how its working now necessarily

Pipe: (Beta, RC, Release) Version: (latest, v.X)    User: <username>    Reload button
Apps: (Appicon) for app in found_apps()  Apps organized by category. Category filter.

Might need:
 User + Login/out section
 Settings: ?
 Current env: Show env vars, and env-tokens

Descriptions
Pipe: Version of the core to use.
    Release versions are locked stable versions.
    Beta and RC are live versions that access a git repo. (can be local or in a network folder)
    For testers (users that test beta and RC features)
    For Devs, they can have a repo in their workspace with any pipe branch and test locally.

BM specifics:
- we cant have shared network folder. We use cloud shared volumes that are not performant for git changes.
- for Release versions, we could have a .zip pkg in a shared volume, & code logic to make sure we have that in the local
pipe versions. (~/.teide/)
- for Dev (w/ live changes), we need to use git to pull from the repos.

## Install Teide:
- web download / cloud volume / wetransfer zipped pkg (python, git, 7z, binaries included)
- unzip into install dir:
    ~{USER}/Documents/teide/pkg-v.1
- install launcher & dependencies.
- execute launcher.
- read config
    - check mounting points
- validate credentials, prompt for login if necessary
- open launcher window

## Pipe: Release v.1    User: lucasm
Apps: Launcher(hidden)

// v.2


### Procedure

- run launcher.bat
    - activates venv
    - runs python launcher module
- in launcher module
    - read config
        -validate mounts
    - launch launcher.app

local:
- apps/config
- config

root:
- apps
- config
- core
- DCCs

"""
import os
import sys
import qtawesome
from typing import TYPE_CHECKING

from qtpy import QtWidgets, QtCore, QtGui

import files
from Launcher.widgets import appIcon

import teide_core.helpers
from teide_qt import utils
from teide_core.db import Connection

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


SCRIPT_DIR = files.get_parent_dir(__file__, level=2)


class LauncherWindow(QtWidgets.QMainWindow):
    TRAY_ICON = os.path.join(SCRIPT_DIR, 'resources/rocket_64.png')

    def __init__(self, conn: Connection, parent=None):
        super().__init__(parent=parent)
        self.conn = conn
        self._version = None
        self._pipeline = None
        self._pipeline_branch = None
        self._project = None
        self._apps = None
        self._dccs = None

        self.init_ui()
        self.populate_ui()
        self.connect_signals()
        self.adjustSize()

    def init_ui(self):
        # Set the window position to the lower right corner of the screen
        # screen_geometry = QtGui.QGuiApplication.primaryScreen().geometry()
        # size_y = screen_geometry.height()/4
        # size_x = size_y*1.7
        #
        # self.setFixedHeight(int(size_y))
        # self.setFixedWidth(int(size_x))

        icon = qtawesome.icon('mdi6.rocket-launch')
        self.setWindowIcon(icon if not self.TRAY_ICON else QtGui.QIcon(self.TRAY_ICON))
        self.setWindowFlags(QtCore.Qt.WindowType.Tool | QtCore.Qt.WindowType.CustomizeWindowHint)
        self.setAttribute(QtCore.Qt.WidgetAttribute.WA_TranslucentBackground, False)

        # Create central widget
        self.central_widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self.central_widget)

        # MAIN Layout
        self.layout = QtWidgets.QVBoxLayout(self.central_widget)
        self.layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
        self.layout.setContentsMargins(15, 15, 15, 15)
        self.central_widget.setLayout(self.layout)

        # App List
        self.app_head_layout = QtWidgets.QHBoxLayout()
        self.apps_label = QtWidgets.QLabel('Apps')
        self.app_head_layout.addWidget(self.apps_label)

        spacer = QtWidgets.QSpacerItem(40, 20,
                                       QtWidgets.QSizePolicy.Policy.Expanding, QtWidgets.QSizePolicy.Policy.Minimum)
        self.app_head_layout.addItem(spacer)

        # Projects
        active_projects = self.conn.get_projects()
        self.projects_combobox = QtWidgets.QComboBox(self)
        self.projects_combobox.addItems([p['name'] for p in active_projects])
        self.app_head_layout.addWidget(self.projects_combobox)
        self.layout.addLayout(self.app_head_layout)

        # Refresh
        self.refresh_widget = QtWidgets.QPushButton(icon=qtawesome.icon('fa.refresh'))
        self.refresh_widget.setToolTip("Reload apps and projects.")
        self.app_head_layout.addWidget(self.refresh_widget)

        self.separator = QtWidgets.QFrame()
        self.separator.setFrameShape(QtWidgets.QFrame.Shape.HLine)
        self.layout.addWidget(self.separator)

        self.found_apps = []
        self.list_wdg = QtWidgets.QWidget(self)
        self.list_layout = QtWidgets.QVBoxLayout()
        self.list_layout.setContentsMargins(0, 0, 0, 0)
        self.list_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)
        self.list_wdg.setLayout(self.list_layout)
        self.layout.addWidget(self.list_wdg)

        # Spacer
        spacer = QtWidgets.QSpacerItem(0, 40,
                                       QtWidgets.QSizePolicy.Policy.Expanding, QtWidgets.QSizePolicy.Policy.Minimum)
        self.layout.addItem(spacer)

        # Context Menu
        self.context_menu = QtWidgets.QMenu()
        self.action_open_console = QtWidgets.QAction('Open Console', self)
        self.action_logout = QtWidgets.QAction('Log Out', self)
        self.action_quit = QtWidgets.QAction('Quit', self)

        self.context_menu.addAction(self.action_open_console)
        self.context_menu.addAction(self.action_logout)
        self.context_menu.addAction(self.action_quit)

    def populate_ui(self):
        self._refresh_apps()

    def connect_signals(self):
        self.action_logout.triggered.connect(self._logout_sg)
        self.refresh_widget.clicked.connect(self._refresh_ui)

    def _collect_apps(self):
        self.found_apps = []
        for app_wdg in appIcon.get_teide_apps():
            app = appIcon.AppIcon(app_data=app_wdg)
            self.found_apps.append(app)

    def _refresh_apps(self):
        self._collect_apps()
        utils.clear_layout(self.list_layout)
        for app_wdg in sorted(self.found_apps, key=lambda x: x.app.nice_name):
            self.list_layout.addWidget(app_wdg)

    def _refresh_ui(self):
        self._refresh_apps()

    def contextMenuEvent(self, event):
        self.context_menu.exec(event.globalPos())

    def closeEvent(self, event):
        event.ignore()
        self.hide()

    def _logout_sg(self):
        from teide_core import db
        db.SGAuthentication.logout()
        self.action_quit.trigger()

    # def mousePressEvent(self, event):
    #     if event.button() == QtCore.Qt.LeftButton:
    #         self.drag_position = event.globalPos() - self.frameGeometry().topLeft()
    #         event.accept()
    #
    # def mouseMoveEvent(self, event):
    #     if event.buttons() == QtCore.Qt.LeftButton:
    #         self.move(event.globalPos() - self.drag_position)
    #         event.accept()


def main():
    from teide_core import db
    app = QtWidgets.QApplication(sys.argv)
    qtawesome.dark(app)

    conn = db.SGAuthentication.login()
    window = LauncherWindow(conn)
    window.show()
    app.exec()


if __name__ == '__main__':
    main()
