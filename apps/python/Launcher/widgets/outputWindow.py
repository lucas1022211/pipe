""" Output Console window. Shows the _logger output with the right formatting. logLevels, Color, Tracebacks and more """
import os
import sys
import qtawesome
from typing import TYPE_CHECKING

from qtpy import QtWidgets, QtCore, QtGui

import files
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


SCRIPT_DIR = files.get_parent_dir(__file__, level=2)


class Console(QtWidgets.QMainWindow):
    CUSTOM_FONT = os.path.join(SCRIPT_DIR, "resources/JetBrainsMono-Regular.ttf")
    WINDOW_ICON = os.path.join(SCRIPT_DIR, 'resources/rocket_64.png')

    def __init__(self):
        super().__init__()
        self.setWindowTitle('Console')
        self.resize(640, 380)
        icon = qtawesome.icon('mdi.console')
        self.setWindowIcon(icon if not self.WINDOW_ICON else QtGui.QIcon(self.WINDOW_ICON))

        # Text
        self.text_edit = QtWidgets.QPlainTextEdit(self)
        self.text_edit.setReadOnly(True)
        self.text_edit.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.setCentralWidget(self.text_edit)

        # Font
        if os.path.exists(self.CUSTOM_FONT):
            font_id = QtGui.QFontDatabase.addApplicationFont(self.CUSTOM_FONT)
            font_family = QtGui.QFontDatabase.applicationFontFamilies(font_id)[0]
            custom_font = QtGui.QFont(font_family, 10)
            self.text_edit.setFont(custom_font)

        # Context menu
        clear_action = QtWidgets.QAction("Clear", self)
        clear_action.triggered.connect(self.clear_text)
        self.text_edit.addAction(clear_action)

    def closeEvent(self, event):
        event.ignore()
        self.hide()

    def clear_text(self):
        self.text_edit.clear()

    def receive_logger_signal(self, msg):
        self.text_edit.appendHtml(msg)
        scroll_bar = self.text_edit.verticalScrollBar()
        scroll_bar.setValue(scroll_bar.minimum())


def main():
    app = QtWidgets.QApplication(sys.argv)
    qtawesome.dark(app)
    window = Console()
    window.show()
    app.exec()


if __name__ == '__main__':
    main()
