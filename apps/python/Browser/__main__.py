import argparse
from Browser import browser

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("conn", nargs='?', help="Teide Connection Instance")
    parser.add_argument("project", nargs='?', help="Shotgrid Project Name")
    known_args, unknown_args = parser.parse_known_args()

    conn = known_args.conn
    browser.launch_desktop(conn, known_args.project)
