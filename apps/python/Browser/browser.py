import os
import sys
import shutil
import platform
import webbrowser
from pathlib import Path
from typing import TYPE_CHECKING, Optional

import qtawesome
from qtpy import QtCore, QtGui, QtWidgets

import files
import teide_core.helpers
import teide_lucidity
from teide_qt.widgets import wdg_selectors
from teide_core import filePath
from teide_core import settings
from teide_core import package
from teide_core import publish
from teide_core.db import Connection
from teide_qt.widgets.fileTableWidget import FileTableWidget, BaseItemData, ItemDataType

if TYPE_CHECKING:
    from typing import *

if platform.system() == 'Windows':
    from ctypes import windll  # Only exists on Windows.
    app_id = u'teide.pipe.browser.beta'
    windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)


_logger = teide_core.helpers.get_teide_logger(__name__)

SCRIPT_DIR = files.get_parent_dir(__file__, level=1)
HELP_DOCS = 'https://www.bidayamedia.com/'


# BM-specific challenges  -- Gather-data, Parsing, templates...
# todo: make a function to gather the data to populate the UI.
# done: BM asset naming convention is incompatible bc <assetName> = prp_lucasAsset (_ conflicts with parser)
# fixme: [NEXT] episode and shot tokens are invalid bc the regex pattern is different (only numbers) so now we need to
#  start having access to the project-templates. There are a few blockers
#  - move away from .py prj config files and import DATA in favour of ConfigAPI.
#  - To use the ConfigAPI, we need to be able to set the projectLayer into the stack
#  - Also we need to handle the coding/decoding dictionary for json-python

# todo: [NEXT] try to get the Saved previous state of the App from ~/.teide
#  -we have to recover a the context and re-select the widgets to restore the same UI, if possible.
#  -if not, set as much as we can ex: prev project.
#  - When closing app, we should store the current state and save try to save to ~/.teide

# done: add support for "Both" mode. -- parity with Ref
#  - will need to handle "merging" the result items of Work and Publish templates and only keep/show the Publish as Blue
# done: add support in fileTableWdg to have child items (publish outputs) in the treeView.

# UI/UX elements
# done: connect action to "Copy Path" and "Explore" buttons. -- adding "Refresh" button too
# done: removed right-click actions to Open folder and Refresh in favour of Action Buttons next to path.
# done: changed buttons for icons - Copy | Explore | Refresh | Work | Publish | Both
# done: add delegate in fileTableWdg to have blue text for Publish items.
# done: rethink how to implement functionality to actions like delete, version up, open ...
#  We need to think of what the MVP and target behaviour.
#  - delete: Active if item selected; cant delete Publish
#  - new: Active if context is not Published
#  - version up --Bug with versioning different variants
#  - open: Active if item selected; cant open Publish -> version up popup confirmation
#  - publish: Active if item selected; and context is not Published, can publish a publish
#  >> MVP: has to be item based + ( warning popups | disable btm )
# done: in Selectors, make the labels be on top of the combobox wdg
# done: in Selectors, make the comboboxes scale properly to show the text.
# todo: add support for task=Any in context.
# todo: make buttons shrink in X axis as small as possible, looks that now it only shrinks so much
#       (all buttons) DCCs, mode, actions, copy, explore, version up, delete ...
# done: add refresh button in Context that triggers the signal (additional option to right click over the fileTableWdg)
#  - Or add it next to the Explore button in the fileTableWdg (I Rather have a refresh icon in Context upper right)
# todo: add column to fileTableWdg to represent sync status, Also to the BaseItem. (By default will be hidden like task)


def launch_wiki():
    webbrowser.open(HELP_DOCS)


class CreateNewVersionDialog(QtWidgets.QDialog):
    def __init__(self, tokens: filePath.TOKENS_TYPE, template: teide_lucidity.Template,
                 teide: Optional[filePath.FilePath] = None):
        super().__init__()
        self.tokens = tokens
        self.template = template
        self.teide = teide if teide else filePath.FilePath()

        self.path = None

        self.init_ui()
        self.populate_ui()
        self.connect_signals()

    @classmethod
    def from_tokens(cls, tokens):
        teide = filePath.FilePath()
        template = teide.getElementTemplate(entity_type=tokens['entity_type'],
                                            dcc='dcc',
                                            mode=tokens['mode'],
                                            element_type=tokens['element_type'])
        return cls(tokens, template)

    def init_ui(self):
        self.setWindowTitle("Create Version")
        self.setMinimumWidth(300)

        # Main Layout
        self.main_layout = QtWidgets.QVBoxLayout(self)

        # Labels
        self.version_label = QtWidgets.QLabel("New Version:")
        self.main_layout.addWidget(self.version_label)

        # Parameters
        form_layout = QtWidgets.QFormLayout(self)
        form_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.main_layout.addLayout(form_layout)

        # Create TOKEN Widgets
        self.extension_wdg = QtWidgets.QComboBox()
        form_layout.addRow("Extension", self.extension_wdg)

        self.variant_wdg = QtWidgets.QLineEdit()
        form_layout.addRow("Variant", self.variant_wdg)

        self.version_wdg = QtWidgets.QSpinBox()
        form_layout.addRow("Version", self.version_wdg)

        # Create Button
        self.create_button = QtWidgets.QPushButton("Create")
        self.main_layout.addWidget(self.create_button)

        self.main_layout.setStretch(0, 0)
        self.main_layout.setStretch(1, 1)
        self.main_layout.setStretch(2, 0)

    def populate_ui(self):
        # Extension
        pkg = package.get_package(name=self.tokens['dcc'])
        self.extension_wdg.addItems(pkg.extensions)
        self.extension_wdg.setCurrentText(pkg.default_extension)
        self.tokens['ext'] = pkg.default_extension

        # variant
        variant_suggestion = 'main'
        variant_token = 'variant'
        if variant_token in self.tokens.keys() and self.tokens[variant_token]:
            variant_suggestion = self.tokens[variant_token]
        self.variant_wdg.setText(variant_suggestion)
        self.tokens['variant'] = variant_suggestion

        # Version
        version_suggestion = self._suggest_version()
        self.version_wdg.setValue(int(version_suggestion))
        self.tokens['version'] = str(version_suggestion).zfill(3)

        # Label
        self._update_new_scene_label()

    def connect_signals(self):
        self.create_button.clicked.connect(self.create_version)

        self.extension_wdg.currentTextChanged.connect(self._extension_changed)
        self.extension_wdg.currentTextChanged.connect(self._update_new_scene_label)

        self.variant_wdg.textEdited.connect(self._variant_changed)
        self.variant_wdg.textEdited.connect(self._update_new_scene_label)

        self.version_wdg.valueChanged.connect(self._version_changed)
        self.version_wdg.valueChanged.connect(self._update_new_scene_label)

    def _suggest_version(self):
        return self.teide.getNextVersion(self.template, self.tokens, check_disk=True)

    def _extension_changed(self):
        ui_ext = self.extension_wdg.currentText()
        self.tokens['ext'] = ui_ext

    def _version_changed(self):
        ui_version = self.version_wdg.value()
        self.tokens['version'] = str(ui_version).zfill(3)

    def _variant_changed(self):
        ui_variant = self.variant_wdg.text()
        self.tokens['variant'] = ui_variant

        next_version = self._suggest_version()
        self.version_wdg.setValue(next_version)

    def _update_new_scene_label(self):
        create_scene = self.teide.buildFilepath(template=self.template, tokenOverrides=self.tokens)
        self.version_label.setText(str(create_scene.name))

    def create_version(self):
        create_scene = self.teide.buildFilepath(template=self.template, tokenOverrides=self.tokens)

        # Find default scene from element_template
        found_default_scene = self.teide.get_default_scene(entity=self.tokens['entity_type'],
                                                           step=self.tokens['step'],
                                                           dcc=self.tokens['dcc'],
                                                           filetype='scene')

        # Make new version by copying the default_scene into the new_scene's path.
        if create_scene and found_default_scene:
            create_scene.parent.mkdir(exist_ok=True, parents=True)
            shutil.copy2(src=str(found_default_scene), dst=str(create_scene))
        self.close()


class Browser(QtWidgets.QMainWindow):
    window_name = 'Browser'
    window_icon = None

    def __init__(self, connection: Optional[Connection] = None):
        QtWidgets.QMainWindow.__init__(self)
        self._conn = connection
        if not self._conn:
            sys.exit()

        self.init_ui()
        self.connect_signals()
        self.populate_ui()
        self.add_actions()

    def init_ui(self) -> None:
        # MAIN WINDOW
        self.setWindowTitle(self.window_name)
        icon = qtawesome.icon('mdi6.folder-search')
        self.setWindowIcon(icon if not self.window_icon else QtGui.QIcon(self.window_icon))
        self.resize(1100, 480)

        self.main_widget = QtWidgets.QWidget(parent=self)
        self.setCentralWidget(self.main_widget)

        self.main_layout = QtWidgets.QHBoxLayout()
        self.main_widget.setLayout(self.main_layout)

        # MAIN MENU
        # self.main_menu = QtWidgets.QMenuBar()
        # self.help_menu = self.main_menu.addMenu('&Help')
        # self.help_menu.addAction('See Documentation', launch_wiki)
        # self.layout().setMenuBar(self.main_menu)

        # FILES
        self.files_wdg = QtWidgets.QWidget(self)
        self.files_layout = QtWidgets.QVBoxLayout(self.files_wdg)
        self.files_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.addWidget(self.files_wdg)

        # PARAMETERS
        self.context_selectors_wdg = wdg_selectors.ContextSelectors(self._conn)
        self.files_layout.addWidget(self.context_selectors_wdg)

        # Table
        self.table_wdg = FileTableWidget()
        self.files_layout.addWidget(self.table_wdg)

        # ACTIONS
        self.actions_wdg = QtWidgets.QWidget(self)
        self.actions_layout = QtWidgets.QHBoxLayout(self.actions_wdg)
        self.actions_layout.setContentsMargins(0, 0, 0, 0)
        self.files_layout.addWidget(self.actions_wdg)
        # create new
        self.create_btn = QtWidgets.QPushButton('New')
        self.actions_layout.addWidget(self.create_btn)
        # delete
        self.delete_btn = QtWidgets.QPushButton('Delete')
        self.actions_layout.addWidget(self.delete_btn)
        # ingest
        self.ingest_btn = QtWidgets.QPushButton('Ingest')
        self.ingest_btn.setToolTip("Work in progress")
        self.ingest_btn.setDisabled(True)
        self.actions_layout.addWidget(self.ingest_btn, stretch=1)
        # version up
        self.versionup_btn = QtWidgets.QPushButton('Version Up and Open')
        self.actions_layout.addWidget(self.versionup_btn, stretch=1)
        # open
        self.open_btn = QtWidgets.QPushButton('Open')
        self.actions_layout.addWidget(self.open_btn, stretch=1)
        # publish
        self.publish_btn = QtWidgets.QPushButton('Publish')
        self.actions_layout.addWidget(self.publish_btn, stretch=1)

    def populate_ui(self):
        self.table_wdg.setEnabled(False)
        self.actions_wdg.setEnabled(False)

    def connect_signals(self):
        self.context_selectors_wdg.entity_stack_wdg.initial_params_sgn.connect(self._enable_secondary_params)
        self.context_selectors_wdg.context_updated_sgn.connect(self._refresh_context)
        self.delete_btn.clicked.connect(self.confirm_delete_item)
        self.create_btn.clicked.connect(self._create_new_version)
        self.ingest_btn.clicked.connect(self._ingest_version)
        self.versionup_btn.clicked.connect(self._version_up_open)
        self.open_btn.clicked.connect(self._open_version)
        self.publish_btn.clicked.connect(self._publish_version)
        self.table_wdg.refresh_sgn.connect(self._refresh_context)

    def add_actions(self):
        """Add menu actions to UI elements such as right-click show outputs"""
        self.table_wdg.add_type_specific_action("Show Outputs", self._show_outputs, ItemDataType.SCENE)
        self.table_wdg.add_type_specific_action("Show Renders", self._show_renders, ItemDataType.SCENE)

    def confirm_delete_item(self):
        selected_index = self.table_wdg.treeView.currentIndex()
        if not selected_index.isValid():
            QtWidgets.QMessageBox.warning(self, "Warning", "No item selected.")
            return
        itemData = selected_index.internalPointer()
        if itemData.isPublish:
            QtWidgets.QMessageBox.warning(self, "Warning", "Publish items can't be deleted.")
            return
        reply = QtWidgets.QMessageBox.question(
            self, 'Confirm Deletion',
            'Are you sure you want to permanently delete the selected item?',
            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No
        )

        if reply == QtWidgets.QMessageBox.Yes:
            self._delete_selected_versions(selected_index)

    def get_tokens_from_index(self, index: Optional[QtCore.QModelIndex] = None) -> Optional[filePath.TOKENS_TYPE]:
        """Returns all tokens from the given index. If no index given it will take the selected index."""
        if not index:
            index = self.table_wdg.treeView.currentIndex()
            if not index:
                QtWidgets.QMessageBox.warning(self, "Warning", "No item selected.")
                return
        tokens = self.context_selectors_wdg.context.parameters.copy()
        if index.isValid():
            itemData: BaseItemData = index.internalPointer()
            for k, v in itemData.tokens.items():
                if k not in tokens.keys() or tokens[k] is None:
                    tokens[k] = v
        return tokens

    def _enable_secondary_params(self):
        self.table_wdg.setEnabled(True)
        self.actions_wdg.setEnabled(True)

    def _refresh_context(self):
        """ Needs to refresh the context with the tokens from the UI widgets. """
        """Browser related behaviour --
        gets triggered when context_updated_sgn / field_updated_sgn emitted.
        We get the context_selectors_wdg's Context class that should have all the CONTEXT_TOKENS filled out.
        We derive the work and publish templates from the Context.
        we copy and expand the tokens dictionary to construct enough tokens to fill out the template_patterns
        With this we will get a teide obj with all the tokens and template info to be able to do the file search 
        operations and matching.
        
        with this teide obj, we can also pass it to the Actions functions like New Version, Version Up, Open       
        """

        context = self.context_selectors_wdg.context
        context.parameters['user'] = self._conn.get_connected_user().get('name')
        context.parameters['volume'] = settings.get_volume()
        context.parameters['year'] = settings.get_attribute('year')

        # Getting the project's shortName/code.  We can get it from the SG Entity of the project or, with the ConfigAPI
        # by accessing the project-config layer. {year} should work like that too
        # context.parameters['prShortName'] = settings.get_attribute('project_shortName', layer=None)  # get attr from project layer
        current_project = context.parameters.get('prName', None)
        if current_project != wdg_selectors.EntitySelectors.DEFAULT_PROJECT:
            match_sg_proj = [proj for proj in self._conn.get_projects() if proj.get('name') == current_project]
            if match_sg_proj:
                prjCode = match_sg_proj[0].get('code') if match_sg_proj else None
                context.parameters['prShortName'] = prjCode
        self.table_wdg.populate_files(context, ui_mode=self.context_selectors_wdg.ui_mode,
                                      show_outputs=self.context_selectors_wdg.ui_show_outputs)

        # Disable wdg if publish mode
        publish_inactive_wdg = [self.create_btn, self.delete_btn, self.versionup_btn, self.publish_btn]
        for wdg in publish_inactive_wdg:
            wdg.setDisabled(context.parameters['mode'] == 'publish')

    def _delete_selected_versions(self, index):
        itemData = index.internalPointer()
        if not itemData.filePath or not itemData.filePath.exists():
            return
        os.remove(itemData.filePath)
        self._refresh_context()

    def _create_new_version(self):
        tokens = self.get_tokens_from_index()
        dialog = CreateNewVersionDialog.from_tokens(tokens)
        dialog.create_button.clicked.connect(self._refresh_context)
        dialog.exec()

    def _ingest_version(self):
        from teide_qt.widgets import ingest_dialog
        tokens = self.get_tokens_from_index()
        dialog = ingest_dialog.IngestWindow.from_tokens(tokens)
        dialog.exec()

    def _version_up(self):
        """
        Version up the selected item to the latest version + 1
        make sure we keep all the same tokens but version.
        """
        selected_index = self.table_wdg.treeView.currentIndex()
        if not selected_index.isValid():
            return None
        itemData = selected_index.internalPointer()

        mode = publish.Mode.PUBLISH if itemData.isPublish else publish.Mode.WORK
        template = self.context_selectors_wdg.context.get_template(force_mode=mode)
        w_template = self.context_selectors_wdg.context.get_template(force_mode=publish.Mode.WORK)
        tokens = self._conn.teide.parseFilepath(path=itemData.filePath, template=template)
        latest_version = self._conn.teide.buildFilepath(template=w_template, tokenOverrides=tokens, latestVersion=True, check_disk=True)
        new_version = self._conn.teide.buildFilepath(template=w_template, tokenOverrides=tokens, nextVersion=True, check_disk=True)

        shutil.copy2(src=latest_version, dst=new_version)

        self._refresh_context()
        return new_version

    def _version_up_open(self):
        """
        Version up the selected item to the latest version + 1
        make sure we keep all the same tokens but version.
        """
        selected_index = self.table_wdg.treeView.currentIndex()
        if not selected_index.isValid():
            QtWidgets.QMessageBox.warning(self, "Warning", "No item selected.")
            return

        # Create a custom message box with a checkbox
        msg_box = QtWidgets.QMessageBox(self)
        msg_box.setWindowTitle("Version Up Confirmation")
        msg_box.setText("Are you sure you want to version up?\nThis will create a new file.")
        msg_box.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        # Add a checkbox to the message box
        open_new_file_checkbox = QtWidgets.QCheckBox("Open new file.")
        open_new_file_checkbox.setChecked(True)
        msg_box.setCheckBox(open_new_file_checkbox)

        # Show the message box and get the user's response
        reply = msg_box.exec_()

        if reply == QtWidgets.QMessageBox.Yes:
            new_version = self._version_up()
            if open_new_file_checkbox.isChecked():
                self.open_version(new_version)
            return new_version

    def _open_version(self):
        """
        Open selected version with default executor of the current dcc.
        If more than one selected, open last in list
        """
        selected_index = self.table_wdg.treeView.currentIndex()
        if not selected_index.isValid():
            QtWidgets.QMessageBox.warning(self, "Warning", "No item selected.")
            return
        itemData = selected_index.internalPointer()
        version_path = itemData.filePath
        if itemData.isPublish:
            reply = QtWidgets.QMessageBox.question(
                self, 'You cant open a published version.',
                'Do you want to version up and open?',
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No
            )
            if reply == QtWidgets.QMessageBox.No:
                return
            elif reply == QtWidgets.QMessageBox.Yes:
                version_path = self._version_up()

        self.open_version(version_path)

    def _publish_version(self):
        """
        From the selected version, get the publishes templates, and look for the published files.
        List all the found outputs and warn if missing required outputs.
        Then do the publish action. Should we create the next version? --better go with open warning popup.
        """
        # Get selected index if any (used for fill out UI)
        selected_index = self.table_wdg.treeView.currentIndex()
        if not selected_index.isValid():
            return
        itemData: BaseItemData = selected_index.internalPointer()
        if itemData.isPublish:
            QtWidgets.QMessageBox.warning(self, "Warning", "Cant publish an already published version.")
            return
        tokens = self.get_tokens_from_index(index=selected_index)

        from teide_qt.widgets import publish_dialog
        dialog = publish_dialog.Publisher.from_tokens(self._conn, tokens)
        dialog.exec()

        # Refresh UI
        self._refresh_context()

    def _show_outputs(self):
        selected_index = self.table_wdg.treeView.currentIndex()
        tokens = self.get_tokens_from_index(index=selected_index)

        itemData: BaseItemData = selected_index.internalPointer()
        mode = publish.Mode.PUBLISH if itemData.isPublish else publish.Mode.WORK
        tokens['mode'] = mode.value
        teide = filePath.FilePath()
        outputs_template = teide.getElementTemplate(entity_type=tokens['entity_type'],
                                                    dcc='dcc',
                                                    mode=tokens['mode'],
                                                    element_type='output')
        missing_tokens = outputs_template.missing_keys(tokens, scope=teide_lucidity.template.Template.DIRNAME)
        if missing_tokens:
            _logger.warning(f'Missing tokens for {outputs_template.name}: {missing_tokens}')
            return
        output_folder = outputs_template.format(tokens, scope=teide_lucidity.template.Template.DIRNAME)
        files.openFolder(str(output_folder))

    def _show_renders(self):
        selected_index = self.table_wdg.treeView.currentIndex()
        tokens = self.get_tokens_from_index(index=selected_index)

        itemData: BaseItemData = selected_index.internalPointer()
        mode = publish.Mode.PUBLISH if itemData.isPublish else publish.Mode.WORK
        tokens['mode'] = mode.value

        teide = filePath.FilePath()
        renders_template = teide.getElementTemplate(entity_type=tokens['entity_type'],
                                                    dcc='dcc',
                                                    mode=tokens['mode'],
                                                    element_type='render_layer')
        # fixme: move this fallback to filePath.py
        if 'renderLayer' not in tokens.keys():
            tokens['renderLayer'] = teide.getKeyTemplate('renderLayer').get('default')
        missing_tokens = renders_template.missing_keys(tokens, scope=teide_lucidity.template.Template.DIRNAME)
        if missing_tokens:
            _logger.warning(f'Missing tokens for {renders_template.name}: {missing_tokens}')
            return
        renders_folder = renders_template.format(tokens, scope=teide_lucidity.template.Template.DIRNAME)
        files.openFolder(str(renders_folder))

    def open_version(self, scene):
        tokens = self.get_tokens_from_index()
        files.writeToJSONFile(path=settings.TEIDE_LOCAL_DIR, fileName='tmpEnvTokens', data=tokens)

        pkg = package.get_package(name=tokens['dcc'])
        if not pkg.is_valid:
            QtWidgets.QMessageBox.critical(self, "Invalid Package",f"{pkg.nice_name} is not installed.")
            raise ValueError('Invalid pkg %s' % pkg.nice_name)
        executable = pkg.get_default_executable()
        executable.add_args(new_args=[str(scene)])
        pkg.run_package(executable=executable)


def launch_desktop(conn: Connection, project: Optional[str] = None) -> None:
    """run application in desktop"""
    from teide_qt import utils

    app = QtWidgets.QApplication()
    utils.set_theme(app)
    if not conn:
        from teide_core import db
        conn = db.SGAuthentication.login()
    dialog = Browser(conn)
    dialog.show()

    # todo: intercept with last-used context. // Help pre-selection for user.
    proj_context = filePath.Context(parameters={'prName': project})
    dialog.context_selectors_wdg.entity_stack_wdg.make_selections(proj_context)

    sys.exit(app.exec())
