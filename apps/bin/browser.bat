@ECHO OFF

call "%~dp0..\..\.venv_py39_windows\Scripts\activate" && (
    REM Setup pipe environment
    call "%~dp0..\..\env.bat"
    REM Launch App
    rem start /B pythonw -m browser
    python -m Browser
)
