#!/bin/bash

DEFAULT_FRAMEWORK_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DEFAULT_FRAMEWORK_PATH" || exit

cd "../../"
source .venv_py39_mac_linux/bin/activate
source env.sh

python3 -m Launcher > /dev/null 2>&1 & exit
