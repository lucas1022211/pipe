""" Comment
"""
from typing import TYPE_CHECKING

from teide_core.package import BasePackage, BaseVersion, BaseExecutable, PackageType, IMAGE_EXTENSIONS, VIDEO_EXTENSIONS
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class Afterfx(BasePackage):
    """After effects Package Class."""

    name = 'afterfx'
    nice_name = 'AfterFX'
    type = PackageType.DCC
    versions = [
        BaseVersion(name='2022',
                    windows_dir='C:/Program Files/Adobe/Adobe After Effects 2022/Support Files',
                    macos_dir='/Applications/Adobe After Effects 2022',
                    linux_dir=''),
        BaseVersion(name='2024',
                    windows_dir='C:/Program Files/Adobe/Adobe After Effects 2024/Support Files',
                    macos_dir='/Applications/Adobe After Effects 2024',
                    linux_dir=None),
    ]
    extensions = ['aep']
    default_extension = 'aep'
    executables = {
        'session': [
            BaseExecutable(name='AfterFX')
        ]
    }

    def set_env_vars(self):
        import os
        from teide_core import settings

        TEIDE_FRAMEWORK = settings.TEIDE_FRAMEWORK_PATH
        DCC = self.name
        DCC_ROOT = f'{TEIDE_FRAMEWORK}/packages/{DCC}'

        os.environ["PATH"] = ';'.join((os.getenv("PATH", ""), DCC_ROOT))


def main():
    pass


if __name__ == '__main__':
    main()
