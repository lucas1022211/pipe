""" Comment
"""
from typing import TYPE_CHECKING

from teide_core.package import BasePackage, BaseVersion, BaseExecutable, PackageType, IMAGE_EXTENSIONS, VIDEO_EXTENSIONS
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class Djv(BasePackage):
    """DJV Package Class."""

    name = 'djv'
    nice_name = 'DJV'
    type = PackageType.VIEWER
    versions = [
        BaseVersion(name='2.0.8',
                    windows_dir='C:/Program Files/DJV2/bin',
                    macos_dir=None,
                    linux_dir=None)
    ]

    extensions = IMAGE_EXTENSIONS + VIDEO_EXTENSIONS + ['otio', 'otioz']
    default_extension = 'exr'
    executables = {
        'session': [
            BaseExecutable(name='djv')
        ]
    }

    def set_env_vars(self):
        import os
        from teide_core import settings

        TEIDE_FRAMEWORK = settings.TEIDE_FRAMEWORK_PATH
        DCC = self.name
        DCC_ROOT = f'{TEIDE_FRAMEWORK}/packages/{DCC}'

        os.environ["PATH"] = ';'.join((os.getenv("PATH", ""), DCC_ROOT))


def main():
    pass


if __name__ == '__main__':
    main()
