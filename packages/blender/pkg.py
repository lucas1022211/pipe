""" Comment
"""
from typing import TYPE_CHECKING, Dict

from teide_core.package import BasePackage, BaseVersion, BaseExecutable, PackageType, TEIDE_FRAMEWORK
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class Blender(BasePackage):
    """Blender Package Class."""

    name = 'blender'
    nice_name = 'Blender'
    type = PackageType.DCC
    versions = [
        BaseVersion(name='4.1',
                    windows_dir='C:/Program Files/Blender Foundation/Blender 4.1',
                    macos_dir='/Applications/Blender.app',
                    linux_dir='/usr/share/blender'),
        BaseVersion(name='4.2',
                    windows_dir='C:/Program Files/Blender Foundation/Blender 4.2',
                    macos_dir='',
                    linux_dir=''),
    ]
    extensions = ['blend']
    default_extension = 'blend'
    executables = {
        'session': [
            BaseExecutable(name='blender-launcher')
        ]
    }

    def set_env_vars(self):
        import os
        from teide_core import settings

        TEIDE_FRAMEWORK = settings.TEIDE_FRAMEWORK_PATH
        DCC = self.name
        DCC_ROOT = f'{TEIDE_FRAMEWORK}/packages/{DCC}'

        os.environ["PATH"] = ';'.join((os.getenv("PATH", ""), DCC_ROOT))


def main():
    pass


if __name__ == '__main__':
    main()
