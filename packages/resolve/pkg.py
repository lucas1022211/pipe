""" Comment
"""
from typing import TYPE_CHECKING

from teide_core.package import BasePackage, BaseVersion, BaseExecutable, PackageType, IMAGE_EXTENSIONS, VIDEO_EXTENSIONS
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class Resolve(BasePackage):
    """Resolve Package Class."""

    name = 'resolve'
    nice_name = 'Resolve'
    type = PackageType.DCC
    versions = [
        BaseVersion(name='18.6',
                    windows_dir='C:/Program Files/Blackmagic Design/DaVinci Resolve',
                    macos_dir='/Applications/DaVinci Resolve',
                    linux_dir='/opt/resolve/')
    ]
    extensions = ['drp']
    default_extension = 'drp'
    executables = {
        'session': [
            BaseExecutable(name='Resolve')
        ]
    }

    def set_env_vars(self):
        import os
        from teide_core import settings

        TEIDE_FRAMEWORK = settings.TEIDE_FRAMEWORK_PATH
        DCC = self.name
        DCC_ROOT = f'{TEIDE_FRAMEWORK}/packages/{DCC}'

        os.environ["PATH"] = ';'.join((os.getenv("PATH", ""), DCC_ROOT))


def main():
    pass


if __name__ == '__main__':
    main()
