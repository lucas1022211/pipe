""" Comment
"""
from typing import TYPE_CHECKING

from teide_core.package import BasePackage, BaseVersion, BaseExecutable, PackageType, IMAGE_EXTENSIONS, VIDEO_EXTENSIONS
import teide_core.helpers

if TYPE_CHECKING:
    from typing import *

_logger = teide_core.helpers.get_teide_logger(__name__)


class Premierepro(BasePackage):
    """PremierePro Package Class."""

    name = 'premierepro'
    nice_name = 'PremierePro'
    type = PackageType.DCC
    versions = [
        BaseVersion(name='2022',
                    windows_dir='C:/Program Files/Adobe/Adobe Premiere Pro 2022',
                    macos_dir='/Applications/Adobe Premiere Pro 2022',
                    linux_dir=None),
        BaseVersion(name='2024',
                    windows_dir='C:/Program Files/Adobe/Adobe Premiere Pro 2024',
                    macos_dir='/Applications/Adobe Premiere Pro 2024',
                    linux_dir=None),
    ]
    extensions = ['prproj']
    default_extension = 'prproj'
    executables = {
        'session': [
            BaseExecutable(name='Adobe Premiere Pro')
        ]
    }

    def set_env_vars(self):
        import os
        from teide_core import settings

        TEIDE_FRAMEWORK = settings.TEIDE_FRAMEWORK_PATH
        DCC = self.name
        DCC_ROOT = f'{TEIDE_FRAMEWORK}/packages/{DCC}'

        os.environ["PATH"] = ';'.join((os.getenv("PATH", ""), DCC_ROOT))


def main():
    pass


if __name__ == '__main__':
    main()
