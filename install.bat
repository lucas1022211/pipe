@ECHO OFF

rem Create | Activate | Update the virtual environment
python -m venv .venv_py39_windows
call .venv_py39_windows\Scripts\activate
python -m pip install --upgrade pip

rem Install requirements
pip install -r requirements.txt
